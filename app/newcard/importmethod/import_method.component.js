"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var router_2 = require("nativescript-angular/router");
var ImportMethodComponent = /** @class */ (function () {
    function ImportMethodComponent(routerExtensions, activatedRoute) {
        this.routerExtensions = routerExtensions;
        this.activatedRoute = activatedRoute;
    }
    ImportMethodComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.activatedRoute.params.subscribe(function (params) {
            _this.type = params.type;
        });
    };
    ImportMethodComponent.prototype.navigate = function (method) {
        this.sub.unsubscribe();
        if (method === 'home') {
            this.routerExtensions.navigateByUrl("/home", { clearHistory: true });
        }
        else {
            this.routerExtensions.navigateByUrl("/newcard/" + this.type + "/" + method);
        }
    };
    ImportMethodComponent = __decorate([
        core_1.Component({
            selector: "SelectImportMethod",
            moduleId: module.id,
            template: "\n  <StackLayout class=\"page\" style=\"padding:10%;\">\n    <Label class=\"page-icon fa top topright closebtn\"\n          text=\"X\" (tap)=\"navigate('home')\"\n          horizontalAlignment=\"right\">\n    </Label>\n    <StackLayout class=\"section\">\n      <Label class=\"text\" text=\"IMPORT FROM LIBRARY\" (tap)=\"navigate('import')\"></Label>\n    </StackLayout>\n    <StackLayout class=\"section\">\n      <Label class=\"text\" text=\"SCAN Card\" (tap)=\"navigate('scan')\"></Label>\n    </StackLayout>\n    <StackLayout *ngIf=\"type === 'contact'\" class=\"section\">\n      <Label class=\"text\" text=\"SCAN QR Code\" (tap)=\"navigate('qr')\"></Label>\n    </StackLayout>\n  </StackLayout>\n  ",
            styleUrls: ["./import_method.common.css"]
        }),
        __metadata("design:paramtypes", [router_2.RouterExtensions,
            router_1.ActivatedRoute])
    ], ImportMethodComponent);
    return ImportMethodComponent;
}());
exports.ImportMethodComponent = ImportMethodComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW1wb3J0X21ldGhvZC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbXBvcnRfbWV0aG9kLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUNsRCwwQ0FBaUQ7QUFDakQsc0RBQStEO0FBMEIvRDtJQUtFLCtCQUNVLGdCQUFrQyxFQUNsQyxjQUE4QjtRQUQ5QixxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQ2xDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtJQUNwQyxDQUFDO0lBRUwsd0NBQVEsR0FBUjtRQUFBLGlCQUlDO1FBSEMsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsVUFBQyxNQUFNO1lBQ3JELEtBQUksQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQztRQUMxQixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFDRCx3Q0FBUSxHQUFSLFVBQVMsTUFBYztRQUNyQixJQUFJLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3ZCLElBQUksTUFBTSxLQUFLLE1BQU0sRUFBRTtZQUNyQixJQUFJLENBQUMsZ0JBQWdCLENBQUMsYUFBYSxDQUFDLE9BQU8sRUFBRSxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1NBQ3RFO2FBQU07WUFDTCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsYUFBYSxDQUFDLGNBQVksSUFBSSxDQUFDLElBQUksU0FBSSxNQUFRLENBQUMsQ0FBQztTQUN4RTtJQUVILENBQUM7SUF2QlUscUJBQXFCO1FBdkJqQyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLG9CQUFvQjtZQUM5QixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLGtzQkFnQlQ7WUFDRCxTQUFTLEVBQUUsQ0FBQyw0QkFBNEIsQ0FBQztTQUMxQyxDQUFDO3lDQVE0Qix5QkFBZ0I7WUFDbEIsdUJBQWM7T0FQN0IscUJBQXFCLENBd0JqQztJQUFELDRCQUFDO0NBQUEsQUF4QkQsSUF3QkM7QUF4Qlksc0RBQXFCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSBcInJ4anNcIjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBcIlNlbGVjdEltcG9ydE1ldGhvZFwiLFxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxuICB0ZW1wbGF0ZTogYFxuICA8U3RhY2tMYXlvdXQgY2xhc3M9XCJwYWdlXCIgc3R5bGU9XCJwYWRkaW5nOjEwJTtcIj5cbiAgICA8TGFiZWwgY2xhc3M9XCJwYWdlLWljb24gZmEgdG9wIHRvcHJpZ2h0IGNsb3NlYnRuXCJcbiAgICAgICAgICB0ZXh0PVwiWFwiICh0YXApPVwibmF2aWdhdGUoJ2hvbWUnKVwiXG4gICAgICAgICAgaG9yaXpvbnRhbEFsaWdubWVudD1cInJpZ2h0XCI+XG4gICAgPC9MYWJlbD5cbiAgICA8U3RhY2tMYXlvdXQgY2xhc3M9XCJzZWN0aW9uXCI+XG4gICAgICA8TGFiZWwgY2xhc3M9XCJ0ZXh0XCIgdGV4dD1cIklNUE9SVCBGUk9NIExJQlJBUllcIiAodGFwKT1cIm5hdmlnYXRlKCdpbXBvcnQnKVwiPjwvTGFiZWw+XG4gICAgPC9TdGFja0xheW91dD5cbiAgICA8U3RhY2tMYXlvdXQgY2xhc3M9XCJzZWN0aW9uXCI+XG4gICAgICA8TGFiZWwgY2xhc3M9XCJ0ZXh0XCIgdGV4dD1cIlNDQU4gQ2FyZFwiICh0YXApPVwibmF2aWdhdGUoJ3NjYW4nKVwiPjwvTGFiZWw+XG4gICAgPC9TdGFja0xheW91dD5cbiAgICA8U3RhY2tMYXlvdXQgKm5nSWY9XCJ0eXBlID09PSAnY29udGFjdCdcIiBjbGFzcz1cInNlY3Rpb25cIj5cbiAgICAgIDxMYWJlbCBjbGFzcz1cInRleHRcIiB0ZXh0PVwiU0NBTiBRUiBDb2RlXCIgKHRhcCk9XCJuYXZpZ2F0ZSgncXInKVwiPjwvTGFiZWw+XG4gICAgPC9TdGFja0xheW91dD5cbiAgPC9TdGFja0xheW91dD5cbiAgYCxcbiAgc3R5bGVVcmxzOiBbXCIuL2ltcG9ydF9tZXRob2QuY29tbW9uLmNzc1wiXVxufSlcblxuZXhwb3J0IGNsYXNzIEltcG9ydE1ldGhvZENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdHtcblxuICBwcml2YXRlIHR5cGU6IHN0cmluZztcbiAgcHJpdmF0ZSBzdWI6IFN1YnNjcmlwdGlvbjtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnMsXG4gICAgcHJpdmF0ZSBhY3RpdmF0ZWRSb3V0ZTogQWN0aXZhdGVkUm91dGVcbiAgKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICB0aGlzLnN1YiA9IHRoaXMuYWN0aXZhdGVkUm91dGUucGFyYW1zLnN1YnNjcmliZSgocGFyYW1zKSA9PiB7XG4gICAgICB0aGlzLnR5cGUgPSBwYXJhbXMudHlwZTtcbiAgICB9KTtcbiAgfVxuICBuYXZpZ2F0ZShtZXRob2Q6IHN0cmluZyk6IHZvaWQge1xuICAgIHRoaXMuc3ViLnVuc3Vic2NyaWJlKCk7XG4gICAgaWYgKG1ldGhvZCA9PT0gJ2hvbWUnKSB7XG4gICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGVCeVVybChgL2hvbWVgLCB7IGNsZWFySGlzdG9yeTogdHJ1ZSB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlQnlVcmwoYC9uZXdjYXJkLyR7dGhpcy50eXBlfS8ke21ldGhvZH1gKTtcbiAgICB9XG4gICAgXG4gIH1cbn1cbiJdfQ==