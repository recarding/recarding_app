import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";
import { Subscription } from "rxjs";

@Component({
  selector: "SelectImportMethod",
  moduleId: module.id,
  template: `
  <StackLayout class="page" style="padding:10%;">
    <Label class="page-icon fa top topright closebtn"
          text="X" (tap)="navigate('home')"
          horizontalAlignment="right">
    </Label>
    <StackLayout class="section">
      <Label class="text" text="IMPORT FROM LIBRARY" (tap)="navigate('import')"></Label>
    </StackLayout>
    <StackLayout class="section">
      <Label class="text" text="SCAN Card" (tap)="navigate('scan')"></Label>
    </StackLayout>
    <StackLayout *ngIf="type === 'contact'" class="section">
      <Label class="text" text="SCAN QR Code" (tap)="navigate('qr')"></Label>
    </StackLayout>
  </StackLayout>
  `,
  styleUrls: ["./import_method.common.css"]
})

export class ImportMethodComponent implements OnInit{

  private type: string;
  private sub: Subscription;

  constructor(
    private routerExtensions: RouterExtensions,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.sub = this.activatedRoute.params.subscribe((params) => {
      this.type = params.type;
    });
  }
  navigate(method: string): void {
    this.sub.unsubscribe();
    if (method === 'home') {
      this.routerExtensions.navigateByUrl(`/home`, { clearHistory: true });
    } else {
      this.routerExtensions.navigateByUrl(`/newcard/${this.type}/${method}`);
    }
    
  }
}
