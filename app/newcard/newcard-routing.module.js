"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var import_method_component_1 = require("./importmethod/import_method.component");
var newcard_component_1 = require("./newcard.component");
var routes = [
    { path: ":type/:importMethod", component: newcard_component_1.NewCardComponent },
    { path: ":type",
        component: import_method_component_1.ImportMethodComponent,
        data: {
            shouldDetach: true,
            title: null
        }
    }
];
var NewCardRoutingModule = /** @class */ (function () {
    function NewCardRoutingModule() {
    }
    NewCardRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.NativeScriptRouterModule.forChild(routes)],
            exports: [router_1.NativeScriptRouterModule]
        })
    ], NewCardRoutingModule);
    return NewCardRoutingModule;
}());
exports.NewCardRoutingModule = NewCardRoutingModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmV3Y2FyZC1yb3V0aW5nLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm5ld2NhcmQtcm91dGluZy5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBeUM7QUFFekMsc0RBQXVFO0FBRXZFLGtGQUErRTtBQUMvRSx5REFBdUQ7QUFFdkQsSUFBTSxNQUFNLEdBQVc7SUFDckIsRUFBRSxJQUFJLEVBQUUscUJBQXFCLEVBQUUsU0FBUyxFQUFFLG9DQUFnQixFQUFFO0lBQzVELEVBQUUsSUFBSSxFQUFFLE9BQU87UUFDYixTQUFTLEVBQUUsK0NBQXFCO1FBQ2hDLElBQUksRUFBRTtZQUNBLFlBQVksRUFBRSxJQUFJO1lBQ2xCLEtBQUssRUFBRSxJQUFJO1NBQ2Q7S0FDSDtDQUNILENBQUM7QUFNRjtJQUFBO0lBQW9DLENBQUM7SUFBeEIsb0JBQW9CO1FBSmhDLGVBQVEsQ0FBQztZQUNOLE9BQU8sRUFBRSxDQUFDLGlDQUF3QixDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNwRCxPQUFPLEVBQUUsQ0FBQyxpQ0FBd0IsQ0FBQztTQUN0QyxDQUFDO09BQ1csb0JBQW9CLENBQUk7SUFBRCwyQkFBQztDQUFBLEFBQXJDLElBQXFDO0FBQXhCLG9EQUFvQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IFJvdXRlcyB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdFJvdXRlck1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcblxuaW1wb3J0IHsgSW1wb3J0TWV0aG9kQ29tcG9uZW50IH0gZnJvbSBcIi4vaW1wb3J0bWV0aG9kL2ltcG9ydF9tZXRob2QuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBOZXdDYXJkQ29tcG9uZW50IH0gZnJvbSBcIi4vbmV3Y2FyZC5jb21wb25lbnRcIjtcblxuY29uc3Qgcm91dGVzOiBSb3V0ZXMgPSBbXG4gIHsgcGF0aDogXCI6dHlwZS86aW1wb3J0TWV0aG9kXCIsIGNvbXBvbmVudDogTmV3Q2FyZENvbXBvbmVudCB9LFxuICB7IHBhdGg6IFwiOnR5cGVcIixcbiAgICBjb21wb25lbnQ6IEltcG9ydE1ldGhvZENvbXBvbmVudCxcbiAgICBkYXRhOiB7XG4gICAgICAgICAgc2hvdWxkRGV0YWNoOiB0cnVlLFxuICAgICAgICAgIHRpdGxlOiBudWxsXG4gICAgICB9XG4gICB9XG5dO1xuXG5ATmdNb2R1bGUoe1xuICAgIGltcG9ydHM6IFtOYXRpdmVTY3JpcHRSb3V0ZXJNb2R1bGUuZm9yQ2hpbGQocm91dGVzKV0sXG4gICAgZXhwb3J0czogW05hdGl2ZVNjcmlwdFJvdXRlck1vZHVsZV1cbn0pXG5leHBvcnQgY2xhc3MgTmV3Q2FyZFJvdXRpbmdNb2R1bGUgeyB9XG4iXX0=