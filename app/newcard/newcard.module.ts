import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
// import { HttpModule } from "@angular/http";

import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";

import { SharedModule } from "../shared/shared.module";
import { NewCardRoutingModule } from "./newcard-routing.module";
import { NewCardComponent } from "./newcard.component";

import { StorageService } from "../shared/helper/app-settings/storage.service";
import { ImportCardComponent } from "./importCard/importcard.component";
import { ImportMethodComponent } from "./importmethod/import_method.component";
import { ScanCardComponent } from "./scanCard/scancard.component";
import { ScanQRComponent } from './scanQR/scanQR.component';

import {BarcodeScanner} from 'nativescript-barcodescanner';
import { registerElement } from "nativescript-angular/element-registry";
registerElement("BarcodeScanner", () => require("nativescript-barcodescanner").BarcodeScannerView);

@NgModule({
    imports: [
        NativeScriptCommonModule,
        NativeScriptHttpClientModule,
        NewCardRoutingModule,
        SharedModule
    ],
    declarations: [
        ScanQRComponent,
        NewCardComponent,
        ScanCardComponent,
        ImportCardComponent,
        ImportMethodComponent
    ],
    providers: [
      StorageService,
      BarcodeScanner
     ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class NewCardModule { }
