import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { ImportMethodComponent } from "./importmethod/import_method.component";
import { NewCardComponent } from "./newcard.component";

const routes: Routes = [
  { path: ":type/:importMethod", component: NewCardComponent },
  { path: ":type",
    component: ImportMethodComponent,
    data: {
          shouldDetach: true,
          title: null
      }
   }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class NewCardRoutingModule { }
