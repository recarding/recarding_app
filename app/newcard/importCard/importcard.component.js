"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ImagePicker = require("nativescript-imagepicker");
var ImportCardComponent = /** @class */ (function () {
    function ImportCardComponent() {
        this.imageSeleced = new core_1.EventEmitter();
        this.imagePath = new core_1.EventEmitter();
        this.uploadImage();
    }
    ImportCardComponent.prototype.uploadImage = function () {
        var _this = this;
        var imagePicker = ImagePicker.create({
            mode: "single"
        });
        imagePicker
            .authorize()
            .then(function () { return imagePicker.present(); })
            .then(function (selection) {
            var selected = selection[0];
            selected.getImageAsync(function (image, error) {
                if (error) {
                    console.log("Error getting image source from picker: " + error);
                    return;
                }
                _this.imageSeleced.emit(image);
                if (_this.isPersonalProfile) {
                    var ios = selected.ios;
                    if (ios && ios.mediaType === 1 /* Image */) {
                        var opt = PHImageRequestOptions.new();
                        opt.version = 0 /* Current */;
                        PHImageManager.defaultManager().requestImageDataForAssetOptionsResultHandler(ios, opt, function (_, __, ___, info) {
                            var path = info.objectForKey("PHImageFileURLKey").toString();
                            _this.imagePath.emit(path);
                        });
                    }
                }
            });
        })
            .catch(function (e) {
            console.log("Image Picker error: " + e);
        });
    };
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], ImportCardComponent.prototype, "imageSeleced", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], ImportCardComponent.prototype, "imagePath", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], ImportCardComponent.prototype, "isPersonalProfile", void 0);
    ImportCardComponent = __decorate([
        core_1.Component({
            selector: "ImportCard",
            moduleId: module.id,
            template: ""
        }),
        __metadata("design:paramtypes", [])
    ], ImportCardComponent);
    return ImportCardComponent;
}());
exports.ImportCardComponent = ImportCardComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW1wb3J0Y2FyZC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbXBvcnRjYXJkLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUF1RTtBQUV2RSxzREFBd0Q7QUFTeEQ7SUFLRTtRQUpVLGlCQUFZLEdBQUcsSUFBSSxtQkFBWSxFQUFFLENBQUM7UUFDbEMsY0FBUyxHQUFHLElBQUksbUJBQVksRUFBRSxDQUFDO1FBSXZDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUNyQixDQUFDO0lBRUQseUNBQVcsR0FBWDtRQUFBLGlCQWdDQztRQS9CQyxJQUFNLFdBQVcsR0FBRyxXQUFXLENBQUMsTUFBTSxDQUFDO1lBQ3JDLElBQUksRUFBRSxRQUFRO1NBQ2YsQ0FBQyxDQUFDO1FBQ0gsV0FBVzthQUNSLFNBQVMsRUFBRTthQUNYLElBQUksQ0FBQyxjQUFNLE9BQUEsV0FBVyxDQUFDLE9BQU8sRUFBRSxFQUFyQixDQUFxQixDQUFDO2FBQ2pDLElBQUksQ0FBQyxVQUFDLFNBQTRCO1lBQ2pDLElBQU0sUUFBUSxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM5QixRQUFRLENBQUMsYUFBYSxDQUFDLFVBQUMsS0FBaUIsRUFBRSxLQUFVO2dCQUNuRCxJQUFJLEtBQUssRUFBRTtvQkFDVCxPQUFPLENBQUMsR0FBRyxDQUFDLDZDQUEyQyxLQUFPLENBQUMsQ0FBQztvQkFDaEUsT0FBTztpQkFDUjtnQkFDRCxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDOUIsSUFBSSxLQUFJLENBQUMsaUJBQWlCLEVBQUU7b0JBQzFCLElBQU0sR0FBRyxHQUFHLFFBQVEsQ0FBQyxHQUFHLENBQUM7b0JBQ3pCLElBQUksR0FBRyxJQUFJLEdBQUcsQ0FBQyxTQUFTLGtCQUEyQixFQUFFO3dCQUNuRCxJQUFNLEdBQUcsR0FBRyxxQkFBcUIsQ0FBQyxHQUFHLEVBQUUsQ0FBQzt3QkFDeEMsR0FBRyxDQUFDLE9BQU8sa0JBQXVDLENBQUM7d0JBQ25ELGNBQWMsQ0FBQyxjQUFjLEVBQUUsQ0FBQyw0Q0FBNEMsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUNuRixVQUFDLENBQVMsRUFBRSxFQUFVLEVBQUUsR0FBdUIsRUFBRSxJQUE0Qjs0QkFDM0UsSUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDOzRCQUMvRCxLQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDNUIsQ0FBQyxDQUFDLENBQUM7cUJBQ047aUJBQ0Y7WUFDSCxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQzthQUNELEtBQUssQ0FBQyxVQUFBLENBQUM7WUFDTixPQUFPLENBQUMsR0FBRyxDQUFDLHlCQUF1QixDQUFHLENBQUMsQ0FBQztRQUMxQyxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUF4Q1M7UUFBVCxhQUFNLEVBQUU7OzZEQUFtQztJQUNsQztRQUFULGFBQU0sRUFBRTs7MERBQWdDO0lBQ2hDO1FBQVIsWUFBSyxFQUFFOztrRUFBNEI7SUFIekIsbUJBQW1CO1FBTC9CLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsWUFBWTtZQUN0QixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLEVBQUU7U0FDYixDQUFDOztPQUNXLG1CQUFtQixDQTBDL0I7SUFBRCwwQkFBQztDQUFBLEFBMUNELElBMENDO0FBMUNZLGtEQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBPdXRwdXQsIElucHV0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcblxuaW1wb3J0ICogYXMgSW1hZ2VQaWNrZXIgZnJvbSBcIm5hdGl2ZXNjcmlwdC1pbWFnZXBpY2tlclwiO1xuaW1wb3J0IHsgSW1hZ2VBc3NldCB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL2ltYWdlLWFzc2V0L2ltYWdlLWFzc2V0XCI7XG5pbXBvcnQgeyBJbWFnZVNvdXJjZSB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL2ltYWdlLXNvdXJjZS9pbWFnZS1zb3VyY2VcIjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBcIkltcG9ydENhcmRcIixcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbiAgdGVtcGxhdGU6IGBgXG59KVxuZXhwb3J0IGNsYXNzIEltcG9ydENhcmRDb21wb25lbnQge1xuICBAT3V0cHV0KCkgaW1hZ2VTZWxlY2VkID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgaW1hZ2VQYXRoID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBASW5wdXQoKSBpc1BlcnNvbmFsUHJvZmlsZTogYm9vbGVhbjtcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICB0aGlzLnVwbG9hZEltYWdlKCk7XG4gIH1cblxuICB1cGxvYWRJbWFnZSgpOiB2b2lkIHtcbiAgICBjb25zdCBpbWFnZVBpY2tlciA9IEltYWdlUGlja2VyLmNyZWF0ZSh7XG4gICAgICBtb2RlOiBcInNpbmdsZVwiXG4gICAgfSk7XG4gICAgaW1hZ2VQaWNrZXJcbiAgICAgIC5hdXRob3JpemUoKVxuICAgICAgLnRoZW4oKCkgPT4gaW1hZ2VQaWNrZXIucHJlc2VudCgpKVxuICAgICAgLnRoZW4oKHNlbGVjdGlvbjogQXJyYXk8SW1hZ2VBc3NldD4pID0+IHtcbiAgICAgICAgY29uc3Qgc2VsZWN0ZWQgPSBzZWxlY3Rpb25bMF07XG4gICAgICAgIHNlbGVjdGVkLmdldEltYWdlQXN5bmMoKGltYWdlOiBJbWFnZUFzc2V0LCBlcnJvcjogYW55KSA9PiB7XG4gICAgICAgICAgaWYgKGVycm9yKSB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhgRXJyb3IgZ2V0dGluZyBpbWFnZSBzb3VyY2UgZnJvbSBwaWNrZXI6ICR7ZXJyb3J9YCk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgfVxuICAgICAgICAgIHRoaXMuaW1hZ2VTZWxlY2VkLmVtaXQoaW1hZ2UpO1xuICAgICAgICAgIGlmICh0aGlzLmlzUGVyc29uYWxQcm9maWxlKSB7XG4gICAgICAgICAgICBjb25zdCBpb3MgPSBzZWxlY3RlZC5pb3M7XG4gICAgICAgICAgICBpZiAoaW9zICYmIGlvcy5tZWRpYVR5cGUgPT09IFBIQXNzZXRNZWRpYVR5cGUuSW1hZ2UpIHtcbiAgICAgICAgICAgICAgY29uc3Qgb3B0ID0gUEhJbWFnZVJlcXVlc3RPcHRpb25zLm5ldygpO1xuICAgICAgICAgICAgICBvcHQudmVyc2lvbiA9IFBISW1hZ2VSZXF1ZXN0T3B0aW9uc1ZlcnNpb24uQ3VycmVudDtcbiAgICAgICAgICAgICAgUEhJbWFnZU1hbmFnZXIuZGVmYXVsdE1hbmFnZXIoKS5yZXF1ZXN0SW1hZ2VEYXRhRm9yQXNzZXRPcHRpb25zUmVzdWx0SGFuZGxlcihpb3MsIG9wdCxcbiAgICAgICAgICAgICAgICAoXzogTlNEYXRhLCBfXzogc3RyaW5nLCBfX186IFVJSW1hZ2VPcmllbnRhdGlvbiwgaW5mbzogTlNEaWN0aW9uYXJ5PGFueSwgYW55PikgPT4ge1xuICAgICAgICAgICAgICAgICAgY29uc3QgcGF0aCA9IGluZm8ub2JqZWN0Rm9yS2V5KFwiUEhJbWFnZUZpbGVVUkxLZXlcIikudG9TdHJpbmcoKTtcbiAgICAgICAgICAgICAgICAgIHRoaXMuaW1hZ2VQYXRoLmVtaXQocGF0aCk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgIH0pXG4gICAgICAuY2F0Y2goZSA9PiB7XG4gICAgICAgIGNvbnNvbGUubG9nKGBJbWFnZSBQaWNrZXIgZXJyb3I6ICR7ZX1gKTtcbiAgICAgIH0pO1xuICB9XG59XG4iXX0=