import { Component, EventEmitter, Output, Input } from "@angular/core";

import * as ImagePicker from "nativescript-imagepicker";
import { ImageAsset } from "tns-core-modules/image-asset/image-asset";
import { ImageSource } from "tns-core-modules/image-source/image-source";

@Component({
  selector: "ImportCard",
  moduleId: module.id,
  template: ``
})
export class ImportCardComponent {
  @Output() imageSeleced = new EventEmitter();
  @Output() imagePath = new EventEmitter();
  @Input() isPersonalProfile: boolean;

  constructor() {
    this.uploadImage();
  }

  uploadImage(): void {
    const imagePicker = ImagePicker.create({
      mode: "single"
    });
    imagePicker
      .authorize()
      .then(() => imagePicker.present())
      .then((selection: Array<ImageAsset>) => {
        const selected = selection[0];
        selected.getImageAsync((image: ImageAsset, error: any) => {
          if (error) {
            console.log(`Error getting image source from picker: ${error}`);
            return;
          }
          this.imageSeleced.emit(image);
          if (this.isPersonalProfile) {
            const ios = selected.ios;
            if (ios && ios.mediaType === PHAssetMediaType.Image) {
              const opt = PHImageRequestOptions.new();
              opt.version = PHImageRequestOptionsVersion.Current;
              PHImageManager.defaultManager().requestImageDataForAssetOptionsResultHandler(ios, opt,
                (_: NSData, __: string, ___: UIImageOrientation, info: NSDictionary<any, any>) => {
                  const path = info.objectForKey("PHImageFileURLKey").toString();
                  this.imagePath.emit(path);
                });
            }
          }
        });
      })
      .catch(e => {
        console.log(`Image Picker error: ${e}`);
      });
  }
}
