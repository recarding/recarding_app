"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_1 = require("tns-core-modules/platform");
var Camera = require("nativescript-camera");
var image_source_1 = require("tns-core-modules/image-source/image-source");
var fs = require("file-system");
var ScanCardComponent = /** @class */ (function () {
    function ScanCardComponent() {
        this.imageSrc = new core_1.EventEmitter();
        this.base64Img = new core_1.EventEmitter();
        this.imagePath = new core_1.EventEmitter();
    }
    ScanCardComponent.prototype.ngOnInit = function () {
        this.scanImage();
    };
    ScanCardComponent.prototype.scanImage = function () {
        var _this = this;
        if (!platform_1.isIOS) {
            Camera.requestPermissions();
        }
        Camera.takePicture({
            width: 800,
            height: 800,
            keepAspectRatio: true,
            saveToGallery: true,
            cameraFacing: "rear"
        }).then(function (imageAsset) {
            new image_source_1.ImageSource().fromAsset(imageAsset).then(function (imageSource) {
                if (_this.isPersonalProfile) {
                    var folder = fs.knownFolders.documents().path;
                    var fileName = _this.email + ".cards.jpeg";
                    var path = fs.path.join(folder, fileName);
                    _this.imagePath.emit(path);
                }
                var base64 = imageSource.toBase64String("jpeg", 100);
                _this.base64Img.emit(base64);
                _this.imageSrc.emit(imageSource);
            });
        });
    };
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], ScanCardComponent.prototype, "imageSrc", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], ScanCardComponent.prototype, "base64Img", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], ScanCardComponent.prototype, "imagePath", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], ScanCardComponent.prototype, "email", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], ScanCardComponent.prototype, "isPersonalProfile", void 0);
    ScanCardComponent = __decorate([
        core_1.Component({
            selector: "ScanCard",
            moduleId: module.id,
            template: ""
        })
    ], ScanCardComponent);
    return ScanCardComponent;
}());
exports.ScanCardComponent = ScanCardComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2NhbmNhcmQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsic2NhbmNhcmQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQStFO0FBRS9FLHNEQUFrRDtBQUNsRCw0Q0FBOEM7QUFDOUMsMkVBQXlFO0FBQ3pFLGdDQUFrQztBQVFsQztJQU5BO1FBT1ksYUFBUSxHQUFHLElBQUksbUJBQVksRUFBRSxDQUFDO1FBQzlCLGNBQVMsR0FBRyxJQUFJLG1CQUFZLEVBQUUsQ0FBQztRQUMvQixjQUFTLEdBQUcsSUFBSSxtQkFBWSxFQUFFLENBQUM7SUFnQzNDLENBQUM7SUE1QkMsb0NBQVEsR0FBUjtRQUNFLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztJQUNuQixDQUFDO0lBRUQscUNBQVMsR0FBVDtRQUFBLGlCQXVCQztRQXRCQyxJQUFJLENBQUMsZ0JBQUssRUFBRTtZQUNWLE1BQU0sQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1NBQzdCO1FBQ0QsTUFBTSxDQUFDLFdBQVcsQ0FBQztZQUNqQixLQUFLLEVBQUUsR0FBRztZQUNWLE1BQU0sRUFBRSxHQUFHO1lBQ1gsZUFBZSxFQUFFLElBQUk7WUFDckIsYUFBYSxFQUFFLElBQUk7WUFDbkIsWUFBWSxFQUFFLE1BQU07U0FDckIsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLFVBQVU7WUFDaEIsSUFBSSwwQkFBVyxFQUFFLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLFdBQVc7Z0JBQ3RELElBQUksS0FBSSxDQUFDLGlCQUFpQixFQUFFO29CQUMxQixJQUFNLE1BQU0sR0FBRyxFQUFFLENBQUMsWUFBWSxDQUFDLFNBQVMsRUFBRSxDQUFDLElBQUksQ0FBQztvQkFDaEQsSUFBTSxRQUFRLEdBQU0sS0FBSSxDQUFDLEtBQUssZ0JBQWEsQ0FBQztvQkFDNUMsSUFBTSxJQUFJLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLFFBQVEsQ0FBQyxDQUFDO29CQUM1QyxLQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTtpQkFDMUI7Z0JBQ0QsSUFBTSxNQUFNLEdBQUcsV0FBVyxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQ3ZELEtBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUM1QixLQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQTtZQUNqQyxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQWpDUztRQUFULGFBQU0sRUFBRTs7dURBQStCO0lBQzlCO1FBQVQsYUFBTSxFQUFFOzt3REFBZ0M7SUFDL0I7UUFBVCxhQUFNLEVBQUU7O3dEQUFnQztJQUNoQztRQUFSLFlBQUssRUFBRTs7b0RBQWU7SUFDZDtRQUFSLFlBQUssRUFBRTs7Z0VBQTRCO0lBTHpCLGlCQUFpQjtRQU43QixnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLFVBQVU7WUFDcEIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSxFQUFFO1NBQ2IsQ0FBQztPQUVXLGlCQUFpQixDQW1DN0I7SUFBRCx3QkFBQztDQUFBLEFBbkNELElBbUNDO0FBbkNZLDhDQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBPbkluaXQsIE91dHB1dCwgSW5wdXQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuXG5pbXBvcnQgeyBpc0lPUyB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3BsYXRmb3JtXCI7XG5pbXBvcnQgKiBhcyBDYW1lcmEgZnJvbSBcIm5hdGl2ZXNjcmlwdC1jYW1lcmFcIjtcbmltcG9ydCB7IEltYWdlU291cmNlIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvaW1hZ2Utc291cmNlL2ltYWdlLXNvdXJjZVwiO1xuaW1wb3J0ICogYXMgZnMgZnJvbSBcImZpbGUtc3lzdGVtXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJTY2FuQ2FyZFwiLFxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxuICB0ZW1wbGF0ZTogYGBcbn0pXG5cbmV4cG9ydCBjbGFzcyBTY2FuQ2FyZENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIEBPdXRwdXQoKSBpbWFnZVNyYyA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQE91dHB1dCgpIGJhc2U2NEltZyA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQE91dHB1dCgpIGltYWdlUGF0aCA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQElucHV0KCkgZW1haWw6IHN0cmluZztcbiAgQElucHV0KCkgaXNQZXJzb25hbFByb2ZpbGU6IGJvb2xlYW47XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgdGhpcy5zY2FuSW1hZ2UoKTtcbiAgfVxuXG4gIHNjYW5JbWFnZSgpIHtcbiAgICBpZiAoIWlzSU9TKSB7XG4gICAgICBDYW1lcmEucmVxdWVzdFBlcm1pc3Npb25zKCk7XG4gICAgfVxuICAgIENhbWVyYS50YWtlUGljdHVyZSh7XG4gICAgICB3aWR0aDogODAwLFxuICAgICAgaGVpZ2h0OiA4MDAsXG4gICAgICBrZWVwQXNwZWN0UmF0aW86IHRydWUsXG4gICAgICBzYXZlVG9HYWxsZXJ5OiB0cnVlLFxuICAgICAgY2FtZXJhRmFjaW5nOiBcInJlYXJcIlxuICAgIH0pLnRoZW4oaW1hZ2VBc3NldCA9PiB7XG4gICAgICBuZXcgSW1hZ2VTb3VyY2UoKS5mcm9tQXNzZXQoaW1hZ2VBc3NldCkudGhlbihpbWFnZVNvdXJjZSA9PiB7XG4gICAgICAgIGlmICh0aGlzLmlzUGVyc29uYWxQcm9maWxlKSB7XG4gICAgICAgICAgY29uc3QgZm9sZGVyID0gZnMua25vd25Gb2xkZXJzLmRvY3VtZW50cygpLnBhdGg7XG4gICAgICAgICAgY29uc3QgZmlsZU5hbWUgPSBgJHt0aGlzLmVtYWlsfS5jYXJkcy5qcGVnYDtcbiAgICAgICAgICBjb25zdCBwYXRoID0gZnMucGF0aC5qb2luKGZvbGRlciwgZmlsZU5hbWUpO1xuICAgICAgICAgIHRoaXMuaW1hZ2VQYXRoLmVtaXQocGF0aClcbiAgICAgICAgfVxuICAgICAgICBjb25zdCBiYXNlNjQgPSBpbWFnZVNvdXJjZS50b0Jhc2U2NFN0cmluZyhcImpwZWdcIiwgMTAwKTtcbiAgICAgICAgdGhpcy5iYXNlNjRJbWcuZW1pdChiYXNlNjQpO1xuICAgICAgICB0aGlzLmltYWdlU3JjLmVtaXQoaW1hZ2VTb3VyY2UpXG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxufVxuIl19