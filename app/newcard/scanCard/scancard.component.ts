import { Component, EventEmitter, OnInit, Output, Input } from "@angular/core";

import { isIOS } from "tns-core-modules/platform";
import * as Camera from "nativescript-camera";
import { ImageSource } from "tns-core-modules/image-source/image-source";
import * as fs from "file-system";

@Component({
  selector: "ScanCard",
  moduleId: module.id,
  template: ``
})

export class ScanCardComponent implements OnInit {
  @Output() imageSrc = new EventEmitter();
  @Output() base64Img = new EventEmitter();
  @Output() imagePath = new EventEmitter();
  @Input() email: string;
  @Input() isPersonalProfile: boolean;

  ngOnInit(): void {
    this.scanImage();
  }

  scanImage() {
    if (!isIOS) {
      Camera.requestPermissions();
    }
    Camera.takePicture({
      width: 800,
      height: 800,
      keepAspectRatio: true,
      saveToGallery: true,
      cameraFacing: "rear"
    }).then(imageAsset => {
      new ImageSource().fromAsset(imageAsset).then(imageSource => {
        if (this.isPersonalProfile) {
          const folder = fs.knownFolders.documents().path;
          const fileName = `${this.email}.cards.jpeg`;
          const path = fs.path.join(folder, fileName);
          this.imagePath.emit(path)
        }
        const base64 = imageSource.toBase64String("jpeg", 100);
        this.base64Img.emit(base64);
        this.imageSrc.emit(imageSource)
      });
    });
  }
}
