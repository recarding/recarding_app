import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { BarcodeFormat, MLKitScanBarcodesOnDeviceResult } from "nativescript-plugin-firebase/mlkit/barcodescanning";

import { isIOS } from "tns-core-modules/platform/platform";

import * as Camera from "nativescript-camera";
import { ImageSource } from "tns-core-modules/image-source/image-source";

const firebase = require("nativescript-plugin-firebase");

@Component({
  selector: "ScanQR",
  moduleId: module.id,
  template: ``,
})
export class ScanQRComponent implements OnInit {
  @Output()
  scannedQR = new EventEmitter;

  constructor() { }

  ngOnInit(): void {
    this.scanInit()
  }

  public onScanResult(evt) {
    console.log(`onScanResult: ${evt.text} (${evt.format})`);
  }

  scanInit() {
    if (!isIOS) {
      Camera.requestPermissions();
    }
    Camera.takePicture({
      width: 800,
      height: 800,
      keepAspectRatio: true,
      saveToGallery: true,
      cameraFacing: "rear"
    }).then(imageAsset => {
      new ImageSource().fromAsset(imageAsset).then(imageSource => {
        firebase.mlkit.barcodescanning.scanBarcodesOnDevice({
          image: imageSource,
          formats: [BarcodeFormat.QR_CODE]
        })
          .then((result: MLKitScanBarcodesOnDeviceResult) => {
              this.scannedQR.emit(result.barcodes);
            })
          .catch(errorMessage => console.log("ML Kit error: " + errorMessage));
      });
    });
  }
}
