"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var barcodescanning_1 = require("nativescript-plugin-firebase/mlkit/barcodescanning");
var platform_1 = require("tns-core-modules/platform/platform");
var Camera = require("nativescript-camera");
var image_source_1 = require("tns-core-modules/image-source/image-source");
var firebase = require("nativescript-plugin-firebase");
var ScanQRComponent = /** @class */ (function () {
    function ScanQRComponent() {
        this.scannedQR = new core_1.EventEmitter;
    }
    ScanQRComponent.prototype.ngOnInit = function () {
        this.scanInit();
    };
    ScanQRComponent.prototype.onScanResult = function (evt) {
        console.log("onScanResult: " + evt.text + " (" + evt.format + ")");
    };
    ScanQRComponent.prototype.scanInit = function () {
        var _this = this;
        if (!platform_1.isIOS) {
            Camera.requestPermissions();
        }
        Camera.takePicture({
            width: 800,
            height: 800,
            keepAspectRatio: true,
            saveToGallery: true,
            cameraFacing: "rear"
        }).then(function (imageAsset) {
            new image_source_1.ImageSource().fromAsset(imageAsset).then(function (imageSource) {
                firebase.mlkit.barcodescanning.scanBarcodesOnDevice({
                    image: imageSource,
                    formats: [barcodescanning_1.BarcodeFormat.QR_CODE]
                })
                    .then(function (result) {
                    _this.scannedQR.emit(result.barcodes);
                })
                    .catch(function (errorMessage) { return console.log("ML Kit error: " + errorMessage); });
            });
        });
    };
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], ScanQRComponent.prototype, "scannedQR", void 0);
    ScanQRComponent = __decorate([
        core_1.Component({
            selector: "ScanQR",
            moduleId: module.id,
            template: "",
        }),
        __metadata("design:paramtypes", [])
    ], ScanQRComponent);
    return ScanQRComponent;
}());
exports.ScanQRComponent = ScanQRComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2NhblFSLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNjYW5RUi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBd0U7QUFDeEUsc0ZBQW9IO0FBRXBILCtEQUEyRDtBQUUzRCw0Q0FBOEM7QUFDOUMsMkVBQXlFO0FBRXpFLElBQU0sUUFBUSxHQUFHLE9BQU8sQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO0FBT3pEO0lBSUU7UUFGQSxjQUFTLEdBQUcsSUFBSSxtQkFBWSxDQUFDO0lBRWIsQ0FBQztJQUVqQixrQ0FBUSxHQUFSO1FBQ0UsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFBO0lBQ2pCLENBQUM7SUFFTSxzQ0FBWSxHQUFuQixVQUFvQixHQUFHO1FBQ3JCLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQWlCLEdBQUcsQ0FBQyxJQUFJLFVBQUssR0FBRyxDQUFDLE1BQU0sTUFBRyxDQUFDLENBQUM7SUFDM0QsQ0FBQztJQUVELGtDQUFRLEdBQVI7UUFBQSxpQkFzQkM7UUFyQkMsSUFBSSxDQUFDLGdCQUFLLEVBQUU7WUFDVixNQUFNLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztTQUM3QjtRQUNELE1BQU0sQ0FBQyxXQUFXLENBQUM7WUFDakIsS0FBSyxFQUFFLEdBQUc7WUFDVixNQUFNLEVBQUUsR0FBRztZQUNYLGVBQWUsRUFBRSxJQUFJO1lBQ3JCLGFBQWEsRUFBRSxJQUFJO1lBQ25CLFlBQVksRUFBRSxNQUFNO1NBQ3JCLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxVQUFVO1lBQ2hCLElBQUksMEJBQVcsRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxXQUFXO2dCQUN0RCxRQUFRLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxvQkFBb0IsQ0FBQztvQkFDbEQsS0FBSyxFQUFFLFdBQVc7b0JBQ2xCLE9BQU8sRUFBRSxDQUFDLCtCQUFhLENBQUMsT0FBTyxDQUFDO2lCQUNqQyxDQUFDO3FCQUNDLElBQUksQ0FBQyxVQUFDLE1BQXVDO29CQUMxQyxLQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3ZDLENBQUMsQ0FBQztxQkFDSCxLQUFLLENBQUMsVUFBQSxZQUFZLElBQUksT0FBQSxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixHQUFHLFlBQVksQ0FBQyxFQUE1QyxDQUE0QyxDQUFDLENBQUM7WUFDekUsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFsQ0Q7UUFEQyxhQUFNLEVBQUU7O3NEQUNvQjtJQUZsQixlQUFlO1FBTDNCLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsUUFBUTtZQUNsQixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLEVBQUU7U0FDYixDQUFDOztPQUNXLGVBQWUsQ0FxQzNCO0lBQUQsc0JBQUM7Q0FBQSxBQXJDRCxJQXFDQztBQXJDWSwwQ0FBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBCYXJjb2RlRm9ybWF0LCBNTEtpdFNjYW5CYXJjb2Rlc09uRGV2aWNlUmVzdWx0IH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1wbHVnaW4tZmlyZWJhc2UvbWxraXQvYmFyY29kZXNjYW5uaW5nXCI7XG5cbmltcG9ydCB7IGlzSU9TIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvcGxhdGZvcm0vcGxhdGZvcm1cIjtcblxuaW1wb3J0ICogYXMgQ2FtZXJhIGZyb20gXCJuYXRpdmVzY3JpcHQtY2FtZXJhXCI7XG5pbXBvcnQgeyBJbWFnZVNvdXJjZSB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL2ltYWdlLXNvdXJjZS9pbWFnZS1zb3VyY2VcIjtcblxuY29uc3QgZmlyZWJhc2UgPSByZXF1aXJlKFwibmF0aXZlc2NyaXB0LXBsdWdpbi1maXJlYmFzZVwiKTtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBcIlNjYW5RUlwiLFxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxuICB0ZW1wbGF0ZTogYGAsXG59KVxuZXhwb3J0IGNsYXNzIFNjYW5RUkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIEBPdXRwdXQoKVxuICBzY2FubmVkUVIgPSBuZXcgRXZlbnRFbWl0dGVyO1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgdGhpcy5zY2FuSW5pdCgpXG4gIH1cblxuICBwdWJsaWMgb25TY2FuUmVzdWx0KGV2dCkge1xuICAgIGNvbnNvbGUubG9nKGBvblNjYW5SZXN1bHQ6ICR7ZXZ0LnRleHR9ICgke2V2dC5mb3JtYXR9KWApO1xuICB9XG5cbiAgc2NhbkluaXQoKSB7XG4gICAgaWYgKCFpc0lPUykge1xuICAgICAgQ2FtZXJhLnJlcXVlc3RQZXJtaXNzaW9ucygpO1xuICAgIH1cbiAgICBDYW1lcmEudGFrZVBpY3R1cmUoe1xuICAgICAgd2lkdGg6IDgwMCxcbiAgICAgIGhlaWdodDogODAwLFxuICAgICAga2VlcEFzcGVjdFJhdGlvOiB0cnVlLFxuICAgICAgc2F2ZVRvR2FsbGVyeTogdHJ1ZSxcbiAgICAgIGNhbWVyYUZhY2luZzogXCJyZWFyXCJcbiAgICB9KS50aGVuKGltYWdlQXNzZXQgPT4ge1xuICAgICAgbmV3IEltYWdlU291cmNlKCkuZnJvbUFzc2V0KGltYWdlQXNzZXQpLnRoZW4oaW1hZ2VTb3VyY2UgPT4ge1xuICAgICAgICBmaXJlYmFzZS5tbGtpdC5iYXJjb2Rlc2Nhbm5pbmcuc2NhbkJhcmNvZGVzT25EZXZpY2Uoe1xuICAgICAgICAgIGltYWdlOiBpbWFnZVNvdXJjZSxcbiAgICAgICAgICBmb3JtYXRzOiBbQmFyY29kZUZvcm1hdC5RUl9DT0RFXVxuICAgICAgICB9KVxuICAgICAgICAgIC50aGVuKChyZXN1bHQ6IE1MS2l0U2NhbkJhcmNvZGVzT25EZXZpY2VSZXN1bHQpID0+IHtcbiAgICAgICAgICAgICAgdGhpcy5zY2FubmVkUVIuZW1pdChyZXN1bHQuYmFyY29kZXMpO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgICAuY2F0Y2goZXJyb3JNZXNzYWdlID0+IGNvbnNvbGUubG9nKFwiTUwgS2l0IGVycm9yOiBcIiArIGVycm9yTWVzc2FnZSkpO1xuICAgICAgfSk7XG4gICAgfSk7XG4gIH1cbn1cbiJdfQ==