import { ChangeDetectorRef, Component, OnInit, ViewChild, ElementRef, NgZone } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";

import { DrawerTransitionBase, SlideInOnTopTransition } from "nativescript-ui-sidedrawer";
import { RadSideDrawerComponent } from "nativescript-ui-sidedrawer/angular";
import { Subscription } from "rxjs";

import { IContact } from "../shared/helper/app-settings/storage.model";

import { GroupService } from "../shared/helper/app-settings/groups.service";
import { StorageService } from "../shared/helper/app-settings/storage.service";
import { OcrService } from "../shared/helper/ocr.service";

import * as dialogs from "ui/dialogs";

import { ImageSource } from "image-source";

import { MLKitRecognizeTextResult } from "nativescript-plugin-firebase/mlkit/textrecognition";
import { CardExchangeService } from "../shared/helper/cardExchange.service";
import { AuthService } from "../shared/helper/auth.service";

import * as firebase from "nativescript-plugin-firebase";

@Component({
  selector: "NewCard",
  moduleId: module.id,
  templateUrl: "./newcard.component.html",
  styleUrls: ["./newcard-common.css"]
})
export class NewCardComponent implements OnInit {
  @ViewChild("drawer") drawerComponent: RadSideDrawerComponent;

  freshContact: IContact = {
    id: '',
    user_id: '',
    group: '',
    name: {
      displayname: ''
    },
    organization: {
      name: ''
    },
    cardImage: '',
    phoneNumbers: [''],
    emailAddress: '',
    physicalAddress: '',
    urls: [''],
  };
  imagePath: string;
  pickedImage: ImageSource;
  importMethod: string;
  isPersonalProfile: boolean;
  private type: string;
  private sub: Subscription;
  private _sideDrawerTransition: DrawerTransitionBase;

  constructor(
    private routerExtensions: RouterExtensions,
    private activatedRoute: ActivatedRoute,
    private storageService: StorageService,
    private auth: AuthService,
    private cardExchange: CardExchangeService,
    private cdrf: ChangeDetectorRef,
    private groupService: GroupService,
    private zone: NgZone,
    private ocr: OcrService,
  ) { }

  ngOnInit() {
    this._sideDrawerTransition = new SlideInOnTopTransition();
    this.sub = this.activatedRoute.params.subscribe((params) => {
      this.type = params.type;
      this.isPersonalProfile = this.type === 'myprofiles' ? true : false;
      this.importMethod = params.importMethod;
    });
  }

  sideDrawerTransition(): DrawerTransitionBase {
    return this._sideDrawerTransition;
  }

  onDrawerButtonTap(): void {
    this.drawerComponent.sideDrawer.showDrawer();
  }

  cancel() {
    this.routerExtensions.navigateByUrl("/home", { clearHistory: true });
  }

  scannedQR(scanned: object) {
    const scannedObj = JSON.parse(scanned[0]['value'])
    if (!!scannedObj) {
      this.cardExchange.getProfileByID(scannedObj['context']['id'])
        .then((result) => {
          if (result.found) {
            this.freshContact = result.profile;
          } else {
            alert('Profile not found');
          }
        });
    }
  }

  imageUploaded(image): void {
    const imageSource = new ImageSource();
    imageSource.setNativeSource(image);
    this.zone.run(() => {
      const base64 = imageSource.toBase64String("jpeg", 100);
      this.base64Img(base64)
      this.recognizeTextCloud(imageSource)
    })
  }

  base64Img(encodedImg: string) {
    const imageSource = new ImageSource();
    const loadedBase64 = imageSource.loadFromBase64(encodedImg);
    if (loadedBase64) {
      this.freshContact.cardImage = encodedImg;
      this.pickedImage = imageSource;
    }
  }

  chooseGroup(): void {
    const groupObj = this.groupService.getContactGroups();
    const nameArray = Object.keys(groupObj["contact_groups"]).map((name) => name);
    dialogs.action({
      message: "Choose Category",
      cancelButtonText: "Cancel",
      actions: ["New...", ...nameArray]
    })
      .then((result) => {
        if (result === "New...") {
          dialogs.prompt({
            title: "New Contact Group",
            inputType: dialogs.inputType.text,
            okButtonText: "OK",
            cancelButtonText: "Cancel",
            cancelable: true
          }).then((r) => {
            if (r.text !== "Cancel") {
              this.freshContact.group = r.text;
              this.groupService.addNewContactGroup(this.freshContact.group);
            } else {
              this.freshContact.group = "Contact Group";
            }
          });
        } else {
          this.freshContact.group = (result === "Cancel" && this.freshContact.group === "Contact Group") ?
            "Contact Group"
            : result;
        }
      });
  }

  saveImagePath(path) {
    this.freshContact.cardImagePath = path;
  }

  submit(): void {
    const date = new Date();
    this.freshContact.id = `${date}|${this.freshContact.emailAddress}`;

    if (this.type === "myprofiles") {
      const user = this.auth.getactiveUser();
      this.freshContact.user_id = user.user_id.replace(' ', '');
      this.storageService.setUnsyncedProfiles(this.freshContact);
    }

    if (this.freshContact.group !== "Contact Group") {
      this.groupService.addContactToGroup(this.freshContact.id, this.freshContact.group);
    }
    this.storageService.add(this.freshContact, this.type);

    this.routerExtensions.navigateByUrl("home", { clearHistory: true });
  }

  private recognizeTextCloud(imageSource: ImageSource): void {
    firebase.mlkit.textrecognition.recognizeTextCloud({
      image: imageSource,
      modelType: "latest",
      maxResults: 15
    }).then(
      (result: MLKitRecognizeTextResult) => {
        this.ocr.filterCardText(result.text as string)
          .then((scannedCard: IContact) => {
            this.freshContact = { ...this.freshContact, ...scannedCard };
          });
        this.cdrf.detectChanges();
      })
      .catch(errorMessage => console.log("ML Kit error: " + errorMessage));
  }
}
