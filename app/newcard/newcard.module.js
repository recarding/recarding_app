"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
// import { HttpModule } from "@angular/http";
var common_1 = require("nativescript-angular/common");
var http_client_1 = require("nativescript-angular/http-client");
var shared_module_1 = require("../shared/shared.module");
var newcard_routing_module_1 = require("./newcard-routing.module");
var newcard_component_1 = require("./newcard.component");
var storage_service_1 = require("../shared/helper/app-settings/storage.service");
var importcard_component_1 = require("./importCard/importcard.component");
var import_method_component_1 = require("./importmethod/import_method.component");
var scancard_component_1 = require("./scanCard/scancard.component");
var scanQR_component_1 = require("./scanQR/scanQR.component");
var nativescript_barcodescanner_1 = require("nativescript-barcodescanner");
var element_registry_1 = require("nativescript-angular/element-registry");
element_registry_1.registerElement("BarcodeScanner", function () { return require("nativescript-barcodescanner").BarcodeScannerView; });
var NewCardModule = /** @class */ (function () {
    function NewCardModule() {
    }
    NewCardModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.NativeScriptCommonModule,
                http_client_1.NativeScriptHttpClientModule,
                newcard_routing_module_1.NewCardRoutingModule,
                shared_module_1.SharedModule
            ],
            declarations: [
                scanQR_component_1.ScanQRComponent,
                newcard_component_1.NewCardComponent,
                scancard_component_1.ScanCardComponent,
                importcard_component_1.ImportCardComponent,
                import_method_component_1.ImportMethodComponent
            ],
            providers: [
                storage_service_1.StorageService,
                nativescript_barcodescanner_1.BarcodeScanner
            ],
            schemas: [
                core_1.NO_ERRORS_SCHEMA
            ]
        })
    ], NewCardModule);
    return NewCardModule;
}());
exports.NewCardModule = NewCardModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmV3Y2FyZC5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJuZXdjYXJkLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUEyRDtBQUMzRCw4Q0FBOEM7QUFFOUMsc0RBQXVFO0FBQ3ZFLGdFQUFnRjtBQUVoRix5REFBdUQ7QUFDdkQsbUVBQWdFO0FBQ2hFLHlEQUF1RDtBQUV2RCxpRkFBK0U7QUFDL0UsMEVBQXdFO0FBQ3hFLGtGQUErRTtBQUMvRSxvRUFBa0U7QUFDbEUsOERBQTREO0FBRTVELDJFQUEyRDtBQUMzRCwwRUFBd0U7QUFDeEUsa0NBQWUsQ0FBQyxnQkFBZ0IsRUFBRSxjQUFNLE9BQUEsT0FBTyxDQUFDLDZCQUE2QixDQUFDLENBQUMsa0JBQWtCLEVBQXpELENBQXlELENBQUMsQ0FBQztBQXdCbkc7SUFBQTtJQUE2QixDQUFDO0lBQWpCLGFBQWE7UUF0QnpCLGVBQVEsQ0FBQztZQUNOLE9BQU8sRUFBRTtnQkFDTCxpQ0FBd0I7Z0JBQ3hCLDBDQUE0QjtnQkFDNUIsNkNBQW9CO2dCQUNwQiw0QkFBWTthQUNmO1lBQ0QsWUFBWSxFQUFFO2dCQUNWLGtDQUFlO2dCQUNmLG9DQUFnQjtnQkFDaEIsc0NBQWlCO2dCQUNqQiwwQ0FBbUI7Z0JBQ25CLCtDQUFxQjthQUN4QjtZQUNELFNBQVMsRUFBRTtnQkFDVCxnQ0FBYztnQkFDZCw0Q0FBYzthQUNkO1lBQ0YsT0FBTyxFQUFFO2dCQUNMLHVCQUFnQjthQUNuQjtTQUNKLENBQUM7T0FDVyxhQUFhLENBQUk7SUFBRCxvQkFBQztDQUFBLEFBQTlCLElBQThCO0FBQWpCLHNDQUFhIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUsIE5PX0VSUk9SU19TQ0hFTUEgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuLy8gaW1wb3J0IHsgSHR0cE1vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9odHRwXCI7XG5cbmltcG9ydCB7IE5hdGl2ZVNjcmlwdENvbW1vbk1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9jb21tb25cIjtcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdEh0dHBDbGllbnRNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvaHR0cC1jbGllbnRcIjtcblxuaW1wb3J0IHsgU2hhcmVkTW9kdWxlIH0gZnJvbSBcIi4uL3NoYXJlZC9zaGFyZWQubW9kdWxlXCI7XG5pbXBvcnQgeyBOZXdDYXJkUm91dGluZ01vZHVsZSB9IGZyb20gXCIuL25ld2NhcmQtcm91dGluZy5tb2R1bGVcIjtcbmltcG9ydCB7IE5ld0NhcmRDb21wb25lbnQgfSBmcm9tIFwiLi9uZXdjYXJkLmNvbXBvbmVudFwiO1xuXG5pbXBvcnQgeyBTdG9yYWdlU2VydmljZSB9IGZyb20gXCIuLi9zaGFyZWQvaGVscGVyL2FwcC1zZXR0aW5ncy9zdG9yYWdlLnNlcnZpY2VcIjtcbmltcG9ydCB7IEltcG9ydENhcmRDb21wb25lbnQgfSBmcm9tIFwiLi9pbXBvcnRDYXJkL2ltcG9ydGNhcmQuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBJbXBvcnRNZXRob2RDb21wb25lbnQgfSBmcm9tIFwiLi9pbXBvcnRtZXRob2QvaW1wb3J0X21ldGhvZC5jb21wb25lbnRcIjtcbmltcG9ydCB7IFNjYW5DYXJkQ29tcG9uZW50IH0gZnJvbSBcIi4vc2NhbkNhcmQvc2NhbmNhcmQuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBTY2FuUVJDb21wb25lbnQgfSBmcm9tICcuL3NjYW5RUi9zY2FuUVIuY29tcG9uZW50JztcblxuaW1wb3J0IHtCYXJjb2RlU2Nhbm5lcn0gZnJvbSAnbmF0aXZlc2NyaXB0LWJhcmNvZGVzY2FubmVyJztcbmltcG9ydCB7IHJlZ2lzdGVyRWxlbWVudCB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9lbGVtZW50LXJlZ2lzdHJ5XCI7XG5yZWdpc3RlckVsZW1lbnQoXCJCYXJjb2RlU2Nhbm5lclwiLCAoKSA9PiByZXF1aXJlKFwibmF0aXZlc2NyaXB0LWJhcmNvZGVzY2FubmVyXCIpLkJhcmNvZGVTY2FubmVyVmlldyk7XG5cbkBOZ01vZHVsZSh7XG4gICAgaW1wb3J0czogW1xuICAgICAgICBOYXRpdmVTY3JpcHRDb21tb25Nb2R1bGUsXG4gICAgICAgIE5hdGl2ZVNjcmlwdEh0dHBDbGllbnRNb2R1bGUsXG4gICAgICAgIE5ld0NhcmRSb3V0aW5nTW9kdWxlLFxuICAgICAgICBTaGFyZWRNb2R1bGVcbiAgICBdLFxuICAgIGRlY2xhcmF0aW9uczogW1xuICAgICAgICBTY2FuUVJDb21wb25lbnQsXG4gICAgICAgIE5ld0NhcmRDb21wb25lbnQsXG4gICAgICAgIFNjYW5DYXJkQ29tcG9uZW50LFxuICAgICAgICBJbXBvcnRDYXJkQ29tcG9uZW50LFxuICAgICAgICBJbXBvcnRNZXRob2RDb21wb25lbnRcbiAgICBdLFxuICAgIHByb3ZpZGVyczogW1xuICAgICAgU3RvcmFnZVNlcnZpY2UsXG4gICAgICBCYXJjb2RlU2Nhbm5lclxuICAgICBdLFxuICAgIHNjaGVtYXM6IFtcbiAgICAgICAgTk9fRVJST1JTX1NDSEVNQVxuICAgIF1cbn0pXG5leHBvcnQgY2xhc3MgTmV3Q2FyZE1vZHVsZSB7IH1cbiJdfQ==