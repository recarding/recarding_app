"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var router_2 = require("nativescript-angular/router");
var nativescript_ui_sidedrawer_1 = require("nativescript-ui-sidedrawer");
var angular_1 = require("nativescript-ui-sidedrawer/angular");
var groups_service_1 = require("../shared/helper/app-settings/groups.service");
var storage_service_1 = require("../shared/helper/app-settings/storage.service");
var ocr_service_1 = require("../shared/helper/ocr.service");
var dialogs = require("ui/dialogs");
var image_source_1 = require("image-source");
var cardExchange_service_1 = require("../shared/helper/cardExchange.service");
var auth_service_1 = require("../shared/helper/auth.service");
var firebase = require("nativescript-plugin-firebase");
var NewCardComponent = /** @class */ (function () {
    function NewCardComponent(routerExtensions, activatedRoute, storageService, auth, cardExchange, cdrf, groupService, zone, ocr) {
        this.routerExtensions = routerExtensions;
        this.activatedRoute = activatedRoute;
        this.storageService = storageService;
        this.auth = auth;
        this.cardExchange = cardExchange;
        this.cdrf = cdrf;
        this.groupService = groupService;
        this.zone = zone;
        this.ocr = ocr;
        this.freshContact = {
            id: '',
            user_id: '',
            group: '',
            name: {
                displayname: ''
            },
            organization: {
                name: ''
            },
            cardImage: '',
            phoneNumbers: [''],
            emailAddress: '',
            physicalAddress: '',
            urls: [''],
        };
    }
    NewCardComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._sideDrawerTransition = new nativescript_ui_sidedrawer_1.SlideInOnTopTransition();
        this.sub = this.activatedRoute.params.subscribe(function (params) {
            _this.type = params.type;
            _this.isPersonalProfile = _this.type === 'myprofiles' ? true : false;
            _this.importMethod = params.importMethod;
        });
    };
    NewCardComponent.prototype.sideDrawerTransition = function () {
        return this._sideDrawerTransition;
    };
    NewCardComponent.prototype.onDrawerButtonTap = function () {
        this.drawerComponent.sideDrawer.showDrawer();
    };
    NewCardComponent.prototype.cancel = function () {
        this.routerExtensions.navigateByUrl("/home", { clearHistory: true });
    };
    NewCardComponent.prototype.scannedQR = function (scanned) {
        var _this = this;
        var scannedObj = JSON.parse(scanned[0]['value']);
        if (!!scannedObj) {
            this.cardExchange.getProfileByID(scannedObj['context']['id'])
                .then(function (result) {
                if (result.found) {
                    _this.freshContact = result.profile;
                }
                else {
                    alert('Profile not found');
                }
            });
        }
    };
    NewCardComponent.prototype.imageUploaded = function (image) {
        var _this = this;
        var imageSource = new image_source_1.ImageSource();
        imageSource.setNativeSource(image);
        this.zone.run(function () {
            var base64 = imageSource.toBase64String("jpeg", 100);
            _this.base64Img(base64);
            _this.recognizeTextCloud(imageSource);
        });
    };
    NewCardComponent.prototype.base64Img = function (encodedImg) {
        var imageSource = new image_source_1.ImageSource();
        var loadedBase64 = imageSource.loadFromBase64(encodedImg);
        if (loadedBase64) {
            this.freshContact.cardImage = encodedImg;
            this.pickedImage = imageSource;
        }
    };
    NewCardComponent.prototype.chooseGroup = function () {
        var _this = this;
        var groupObj = this.groupService.getContactGroups();
        var nameArray = Object.keys(groupObj["contact_groups"]).map(function (name) { return name; });
        dialogs.action({
            message: "Choose Category",
            cancelButtonText: "Cancel",
            actions: ["New..."].concat(nameArray)
        })
            .then(function (result) {
            if (result === "New...") {
                dialogs.prompt({
                    title: "New Contact Group",
                    inputType: dialogs.inputType.text,
                    okButtonText: "OK",
                    cancelButtonText: "Cancel",
                    cancelable: true
                }).then(function (r) {
                    if (r.text !== "Cancel") {
                        _this.freshContact.group = r.text;
                        _this.groupService.addNewContactGroup(_this.freshContact.group);
                    }
                    else {
                        _this.freshContact.group = "Contact Group";
                    }
                });
            }
            else {
                _this.freshContact.group = (result === "Cancel" && _this.freshContact.group === "Contact Group") ?
                    "Contact Group"
                    : result;
            }
        });
    };
    NewCardComponent.prototype.saveImagePath = function (path) {
        this.freshContact.cardImagePath = path;
    };
    NewCardComponent.prototype.submit = function () {
        var date = new Date();
        this.freshContact.id = date + "|" + this.freshContact.emailAddress;
        if (this.type === "myprofiles") {
            var user = this.auth.getactiveUser();
            this.freshContact.user_id = user.user_id.replace(' ', '');
            this.storageService.setUnsyncedProfiles(this.freshContact);
        }
        if (this.freshContact.group !== "Contact Group") {
            this.groupService.addContactToGroup(this.freshContact.id, this.freshContact.group);
        }
        this.storageService.add(this.freshContact, this.type);
        this.routerExtensions.navigateByUrl("home", { clearHistory: true });
    };
    NewCardComponent.prototype.recognizeTextCloud = function (imageSource) {
        var _this = this;
        firebase.mlkit.textrecognition.recognizeTextCloud({
            image: imageSource,
            modelType: "latest",
            maxResults: 15
        }).then(function (result) {
            _this.ocr.filterCardText(result.text)
                .then(function (scannedCard) {
                _this.freshContact = __assign({}, _this.freshContact, scannedCard);
            });
            _this.cdrf.detectChanges();
        })
            .catch(function (errorMessage) { return console.log("ML Kit error: " + errorMessage); });
    };
    __decorate([
        core_1.ViewChild("drawer"),
        __metadata("design:type", angular_1.RadSideDrawerComponent)
    ], NewCardComponent.prototype, "drawerComponent", void 0);
    NewCardComponent = __decorate([
        core_1.Component({
            selector: "NewCard",
            moduleId: module.id,
            templateUrl: "./newcard.component.html",
            styleUrls: ["./newcard-common.css"]
        }),
        __metadata("design:paramtypes", [router_2.RouterExtensions,
            router_1.ActivatedRoute,
            storage_service_1.StorageService,
            auth_service_1.AuthService,
            cardExchange_service_1.CardExchangeService,
            core_1.ChangeDetectorRef,
            groups_service_1.GroupService,
            core_1.NgZone,
            ocr_service_1.OcrService])
    ], NewCardComponent);
    return NewCardComponent;
}());
exports.NewCardComponent = NewCardComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmV3Y2FyZC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJuZXdjYXJkLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFvRztBQUNwRywwQ0FBaUQ7QUFDakQsc0RBQStEO0FBRS9ELHlFQUEwRjtBQUMxRiw4REFBNEU7QUFLNUUsK0VBQTRFO0FBQzVFLGlGQUErRTtBQUMvRSw0REFBMEQ7QUFFMUQsb0NBQXNDO0FBRXRDLDZDQUEyQztBQUczQyw4RUFBNEU7QUFDNUUsOERBQTREO0FBRTVELHVEQUF5RDtBQVF6RDtJQTJCRSwwQkFDVSxnQkFBa0MsRUFDbEMsY0FBOEIsRUFDOUIsY0FBOEIsRUFDOUIsSUFBaUIsRUFDakIsWUFBaUMsRUFDakMsSUFBdUIsRUFDdkIsWUFBMEIsRUFDMUIsSUFBWSxFQUNaLEdBQWU7UUFSZixxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQ2xDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsU0FBSSxHQUFKLElBQUksQ0FBYTtRQUNqQixpQkFBWSxHQUFaLFlBQVksQ0FBcUI7UUFDakMsU0FBSSxHQUFKLElBQUksQ0FBbUI7UUFDdkIsaUJBQVksR0FBWixZQUFZLENBQWM7UUFDMUIsU0FBSSxHQUFKLElBQUksQ0FBUTtRQUNaLFFBQUcsR0FBSCxHQUFHLENBQVk7UUFqQ3pCLGlCQUFZLEdBQWE7WUFDdkIsRUFBRSxFQUFFLEVBQUU7WUFDTixPQUFPLEVBQUUsRUFBRTtZQUNYLEtBQUssRUFBRSxFQUFFO1lBQ1QsSUFBSSxFQUFFO2dCQUNKLFdBQVcsRUFBRSxFQUFFO2FBQ2hCO1lBQ0QsWUFBWSxFQUFFO2dCQUNaLElBQUksRUFBRSxFQUFFO2FBQ1Q7WUFDRCxTQUFTLEVBQUUsRUFBRTtZQUNiLFlBQVksRUFBRSxDQUFDLEVBQUUsQ0FBQztZQUNsQixZQUFZLEVBQUUsRUFBRTtZQUNoQixlQUFlLEVBQUUsRUFBRTtZQUNuQixJQUFJLEVBQUUsQ0FBQyxFQUFFLENBQUM7U0FDWCxDQUFDO0lBbUJFLENBQUM7SUFFTCxtQ0FBUSxHQUFSO1FBQUEsaUJBT0M7UUFOQyxJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxtREFBc0IsRUFBRSxDQUFDO1FBQzFELElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLFVBQUMsTUFBTTtZQUNyRCxLQUFJLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUM7WUFDeEIsS0FBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUksQ0FBQyxJQUFJLEtBQUssWUFBWSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztZQUNuRSxLQUFJLENBQUMsWUFBWSxHQUFHLE1BQU0sQ0FBQyxZQUFZLENBQUM7UUFDMUMsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsK0NBQW9CLEdBQXBCO1FBQ0UsT0FBTyxJQUFJLENBQUMscUJBQXFCLENBQUM7SUFDcEMsQ0FBQztJQUVELDRDQUFpQixHQUFqQjtRQUNFLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLFVBQVUsRUFBRSxDQUFDO0lBQy9DLENBQUM7SUFFRCxpQ0FBTSxHQUFOO1FBQ0UsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxPQUFPLEVBQUUsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztJQUN2RSxDQUFDO0lBRUQsb0NBQVMsR0FBVCxVQUFVLE9BQWU7UUFBekIsaUJBWUM7UUFYQyxJQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFBO1FBQ2xELElBQUksQ0FBQyxDQUFDLFVBQVUsRUFBRTtZQUNoQixJQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQzFELElBQUksQ0FBQyxVQUFDLE1BQU07Z0JBQ1gsSUFBSSxNQUFNLENBQUMsS0FBSyxFQUFFO29CQUNoQixLQUFJLENBQUMsWUFBWSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUM7aUJBQ3BDO3FCQUFNO29CQUNMLEtBQUssQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO2lCQUM1QjtZQUNILENBQUMsQ0FBQyxDQUFDO1NBQ047SUFDSCxDQUFDO0lBRUQsd0NBQWEsR0FBYixVQUFjLEtBQUs7UUFBbkIsaUJBUUM7UUFQQyxJQUFNLFdBQVcsR0FBRyxJQUFJLDBCQUFXLEVBQUUsQ0FBQztRQUN0QyxXQUFXLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ25DLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO1lBQ1osSUFBTSxNQUFNLEdBQUcsV0FBVyxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDdkQsS0FBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQTtZQUN0QixLQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxDQUFDLENBQUE7UUFDdEMsQ0FBQyxDQUFDLENBQUE7SUFDSixDQUFDO0lBRUQsb0NBQVMsR0FBVCxVQUFVLFVBQWtCO1FBQzFCLElBQU0sV0FBVyxHQUFHLElBQUksMEJBQVcsRUFBRSxDQUFDO1FBQ3RDLElBQU0sWUFBWSxHQUFHLFdBQVcsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDNUQsSUFBSSxZQUFZLEVBQUU7WUFDaEIsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLEdBQUcsVUFBVSxDQUFDO1lBQ3pDLElBQUksQ0FBQyxXQUFXLEdBQUcsV0FBVyxDQUFDO1NBQ2hDO0lBQ0gsQ0FBQztJQUVELHNDQUFXLEdBQVg7UUFBQSxpQkE4QkM7UUE3QkMsSUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQ3RELElBQU0sU0FBUyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBQyxJQUFJLElBQUssT0FBQSxJQUFJLEVBQUosQ0FBSSxDQUFDLENBQUM7UUFDOUUsT0FBTyxDQUFDLE1BQU0sQ0FBQztZQUNiLE9BQU8sRUFBRSxpQkFBaUI7WUFDMUIsZ0JBQWdCLEVBQUUsUUFBUTtZQUMxQixPQUFPLEdBQUcsUUFBUSxTQUFLLFNBQVMsQ0FBQztTQUNsQyxDQUFDO2FBQ0MsSUFBSSxDQUFDLFVBQUMsTUFBTTtZQUNYLElBQUksTUFBTSxLQUFLLFFBQVEsRUFBRTtnQkFDdkIsT0FBTyxDQUFDLE1BQU0sQ0FBQztvQkFDYixLQUFLLEVBQUUsbUJBQW1CO29CQUMxQixTQUFTLEVBQUUsT0FBTyxDQUFDLFNBQVMsQ0FBQyxJQUFJO29CQUNqQyxZQUFZLEVBQUUsSUFBSTtvQkFDbEIsZ0JBQWdCLEVBQUUsUUFBUTtvQkFDMUIsVUFBVSxFQUFFLElBQUk7aUJBQ2pCLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxDQUFDO29CQUNSLElBQUksQ0FBQyxDQUFDLElBQUksS0FBSyxRQUFRLEVBQUU7d0JBQ3ZCLEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUM7d0JBQ2pDLEtBQUksQ0FBQyxZQUFZLENBQUMsa0JBQWtCLENBQUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDL0Q7eUJBQU07d0JBQ0wsS0FBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEdBQUcsZUFBZSxDQUFDO3FCQUMzQztnQkFDSCxDQUFDLENBQUMsQ0FBQzthQUNKO2lCQUFNO2dCQUNMLEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxHQUFHLENBQUMsTUFBTSxLQUFLLFFBQVEsSUFBSSxLQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssS0FBSyxlQUFlLENBQUMsQ0FBQyxDQUFDO29CQUM5RixlQUFlO29CQUNmLENBQUMsQ0FBQyxNQUFNLENBQUM7YUFDWjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHdDQUFhLEdBQWIsVUFBYyxJQUFJO1FBQ2hCLElBQUksQ0FBQyxZQUFZLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztJQUN6QyxDQUFDO0lBRUQsaUNBQU0sR0FBTjtRQUNFLElBQU0sSUFBSSxHQUFHLElBQUksSUFBSSxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLEdBQU0sSUFBSSxTQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsWUFBYyxDQUFDO1FBRW5FLElBQUksSUFBSSxDQUFDLElBQUksS0FBSyxZQUFZLEVBQUU7WUFDOUIsSUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztZQUN2QyxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDMUQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7U0FDNUQ7UUFFRCxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxLQUFLLGVBQWUsRUFBRTtZQUMvQyxJQUFJLENBQUMsWUFBWSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDcEY7UUFDRCxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUV0RCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsYUFBYSxDQUFDLE1BQU0sRUFBRSxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBQ3RFLENBQUM7SUFFTyw2Q0FBa0IsR0FBMUIsVUFBMkIsV0FBd0I7UUFBbkQsaUJBY0M7UUFiQyxRQUFRLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQztZQUNoRCxLQUFLLEVBQUUsV0FBVztZQUNsQixTQUFTLEVBQUUsUUFBUTtZQUNuQixVQUFVLEVBQUUsRUFBRTtTQUNmLENBQUMsQ0FBQyxJQUFJLENBQ0wsVUFBQyxNQUFnQztZQUMvQixLQUFJLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsSUFBYyxDQUFDO2lCQUMzQyxJQUFJLENBQUMsVUFBQyxXQUFxQjtnQkFDMUIsS0FBSSxDQUFDLFlBQVksZ0JBQVEsS0FBSSxDQUFDLFlBQVksRUFBSyxXQUFXLENBQUUsQ0FBQztZQUMvRCxDQUFDLENBQUMsQ0FBQztZQUNMLEtBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDNUIsQ0FBQyxDQUFDO2FBQ0QsS0FBSyxDQUFDLFVBQUEsWUFBWSxJQUFJLE9BQUEsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsR0FBRyxZQUFZLENBQUMsRUFBNUMsQ0FBNEMsQ0FBQyxDQUFDO0lBQ3pFLENBQUM7SUFoS29CO1FBQXBCLGdCQUFTLENBQUMsUUFBUSxDQUFDO2tDQUFrQixnQ0FBc0I7NkRBQUM7SUFEbEQsZ0JBQWdCO1FBTjVCLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsU0FBUztZQUNuQixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsV0FBVyxFQUFFLDBCQUEwQjtZQUN2QyxTQUFTLEVBQUUsQ0FBQyxzQkFBc0IsQ0FBQztTQUNwQyxDQUFDO3lDQTZCNEIseUJBQWdCO1lBQ2xCLHVCQUFjO1lBQ2QsZ0NBQWM7WUFDeEIsMEJBQVc7WUFDSCwwQ0FBbUI7WUFDM0Isd0JBQWlCO1lBQ1QsNkJBQVk7WUFDcEIsYUFBTTtZQUNQLHdCQUFVO09BcENkLGdCQUFnQixDQWtLNUI7SUFBRCx1QkFBQztDQUFBLEFBbEtELElBa0tDO0FBbEtZLDRDQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENoYW5nZURldGVjdG9yUmVmLCBDb21wb25lbnQsIE9uSW5pdCwgVmlld0NoaWxkLCBFbGVtZW50UmVmLCBOZ1pvbmUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xuXG5pbXBvcnQgeyBEcmF3ZXJUcmFuc2l0aW9uQmFzZSwgU2xpZGVJbk9uVG9wVHJhbnNpdGlvbiB9IGZyb20gXCJuYXRpdmVzY3JpcHQtdWktc2lkZWRyYXdlclwiO1xuaW1wb3J0IHsgUmFkU2lkZURyYXdlckNvbXBvbmVudCB9IGZyb20gXCJuYXRpdmVzY3JpcHQtdWktc2lkZWRyYXdlci9hbmd1bGFyXCI7XG5pbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tIFwicnhqc1wiO1xuXG5pbXBvcnQgeyBJQ29udGFjdCB9IGZyb20gXCIuLi9zaGFyZWQvaGVscGVyL2FwcC1zZXR0aW5ncy9zdG9yYWdlLm1vZGVsXCI7XG5cbmltcG9ydCB7IEdyb3VwU2VydmljZSB9IGZyb20gXCIuLi9zaGFyZWQvaGVscGVyL2FwcC1zZXR0aW5ncy9ncm91cHMuc2VydmljZVwiO1xuaW1wb3J0IHsgU3RvcmFnZVNlcnZpY2UgfSBmcm9tIFwiLi4vc2hhcmVkL2hlbHBlci9hcHAtc2V0dGluZ3Mvc3RvcmFnZS5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBPY3JTZXJ2aWNlIH0gZnJvbSBcIi4uL3NoYXJlZC9oZWxwZXIvb2NyLnNlcnZpY2VcIjtcblxuaW1wb3J0ICogYXMgZGlhbG9ncyBmcm9tIFwidWkvZGlhbG9nc1wiO1xuXG5pbXBvcnQgeyBJbWFnZVNvdXJjZSB9IGZyb20gXCJpbWFnZS1zb3VyY2VcIjtcblxuaW1wb3J0IHsgTUxLaXRSZWNvZ25pemVUZXh0UmVzdWx0IH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1wbHVnaW4tZmlyZWJhc2UvbWxraXQvdGV4dHJlY29nbml0aW9uXCI7XG5pbXBvcnQgeyBDYXJkRXhjaGFuZ2VTZXJ2aWNlIH0gZnJvbSBcIi4uL3NoYXJlZC9oZWxwZXIvY2FyZEV4Y2hhbmdlLnNlcnZpY2VcIjtcbmltcG9ydCB7IEF1dGhTZXJ2aWNlIH0gZnJvbSBcIi4uL3NoYXJlZC9oZWxwZXIvYXV0aC5zZXJ2aWNlXCI7XG5cbmltcG9ydCAqIGFzIGZpcmViYXNlIGZyb20gXCJuYXRpdmVzY3JpcHQtcGx1Z2luLWZpcmViYXNlXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJOZXdDYXJkXCIsXG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXG4gIHRlbXBsYXRlVXJsOiBcIi4vbmV3Y2FyZC5jb21wb25lbnQuaHRtbFwiLFxuICBzdHlsZVVybHM6IFtcIi4vbmV3Y2FyZC1jb21tb24uY3NzXCJdXG59KVxuZXhwb3J0IGNsYXNzIE5ld0NhcmRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBAVmlld0NoaWxkKFwiZHJhd2VyXCIpIGRyYXdlckNvbXBvbmVudDogUmFkU2lkZURyYXdlckNvbXBvbmVudDtcblxuICBmcmVzaENvbnRhY3Q6IElDb250YWN0ID0ge1xuICAgIGlkOiAnJyxcbiAgICB1c2VyX2lkOiAnJyxcbiAgICBncm91cDogJycsXG4gICAgbmFtZToge1xuICAgICAgZGlzcGxheW5hbWU6ICcnXG4gICAgfSxcbiAgICBvcmdhbml6YXRpb246IHtcbiAgICAgIG5hbWU6ICcnXG4gICAgfSxcbiAgICBjYXJkSW1hZ2U6ICcnLFxuICAgIHBob25lTnVtYmVyczogWycnXSxcbiAgICBlbWFpbEFkZHJlc3M6ICcnLFxuICAgIHBoeXNpY2FsQWRkcmVzczogJycsXG4gICAgdXJsczogWycnXSxcbiAgfTtcbiAgaW1hZ2VQYXRoOiBzdHJpbmc7XG4gIHBpY2tlZEltYWdlOiBJbWFnZVNvdXJjZTtcbiAgaW1wb3J0TWV0aG9kOiBzdHJpbmc7XG4gIGlzUGVyc29uYWxQcm9maWxlOiBib29sZWFuO1xuICBwcml2YXRlIHR5cGU6IHN0cmluZztcbiAgcHJpdmF0ZSBzdWI6IFN1YnNjcmlwdGlvbjtcbiAgcHJpdmF0ZSBfc2lkZURyYXdlclRyYW5zaXRpb246IERyYXdlclRyYW5zaXRpb25CYXNlO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucyxcbiAgICBwcml2YXRlIGFjdGl2YXRlZFJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcbiAgICBwcml2YXRlIHN0b3JhZ2VTZXJ2aWNlOiBTdG9yYWdlU2VydmljZSxcbiAgICBwcml2YXRlIGF1dGg6IEF1dGhTZXJ2aWNlLFxuICAgIHByaXZhdGUgY2FyZEV4Y2hhbmdlOiBDYXJkRXhjaGFuZ2VTZXJ2aWNlLFxuICAgIHByaXZhdGUgY2RyZjogQ2hhbmdlRGV0ZWN0b3JSZWYsXG4gICAgcHJpdmF0ZSBncm91cFNlcnZpY2U6IEdyb3VwU2VydmljZSxcbiAgICBwcml2YXRlIHpvbmU6IE5nWm9uZSxcbiAgICBwcml2YXRlIG9jcjogT2NyU2VydmljZSxcbiAgKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICB0aGlzLl9zaWRlRHJhd2VyVHJhbnNpdGlvbiA9IG5ldyBTbGlkZUluT25Ub3BUcmFuc2l0aW9uKCk7XG4gICAgdGhpcy5zdWIgPSB0aGlzLmFjdGl2YXRlZFJvdXRlLnBhcmFtcy5zdWJzY3JpYmUoKHBhcmFtcykgPT4ge1xuICAgICAgdGhpcy50eXBlID0gcGFyYW1zLnR5cGU7XG4gICAgICB0aGlzLmlzUGVyc29uYWxQcm9maWxlID0gdGhpcy50eXBlID09PSAnbXlwcm9maWxlcycgPyB0cnVlIDogZmFsc2U7XG4gICAgICB0aGlzLmltcG9ydE1ldGhvZCA9IHBhcmFtcy5pbXBvcnRNZXRob2Q7XG4gICAgfSk7XG4gIH1cblxuICBzaWRlRHJhd2VyVHJhbnNpdGlvbigpOiBEcmF3ZXJUcmFuc2l0aW9uQmFzZSB7XG4gICAgcmV0dXJuIHRoaXMuX3NpZGVEcmF3ZXJUcmFuc2l0aW9uO1xuICB9XG5cbiAgb25EcmF3ZXJCdXR0b25UYXAoKTogdm9pZCB7XG4gICAgdGhpcy5kcmF3ZXJDb21wb25lbnQuc2lkZURyYXdlci5zaG93RHJhd2VyKCk7XG4gIH1cblxuICBjYW5jZWwoKSB7XG4gICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlQnlVcmwoXCIvaG9tZVwiLCB7IGNsZWFySGlzdG9yeTogdHJ1ZSB9KTtcbiAgfVxuXG4gIHNjYW5uZWRRUihzY2FubmVkOiBvYmplY3QpIHtcbiAgICBjb25zdCBzY2FubmVkT2JqID0gSlNPTi5wYXJzZShzY2FubmVkWzBdWyd2YWx1ZSddKVxuICAgIGlmICghIXNjYW5uZWRPYmopIHtcbiAgICAgIHRoaXMuY2FyZEV4Y2hhbmdlLmdldFByb2ZpbGVCeUlEKHNjYW5uZWRPYmpbJ2NvbnRleHQnXVsnaWQnXSlcbiAgICAgICAgLnRoZW4oKHJlc3VsdCkgPT4ge1xuICAgICAgICAgIGlmIChyZXN1bHQuZm91bmQpIHtcbiAgICAgICAgICAgIHRoaXMuZnJlc2hDb250YWN0ID0gcmVzdWx0LnByb2ZpbGU7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGFsZXJ0KCdQcm9maWxlIG5vdCBmb3VuZCcpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfVxuICB9XG5cbiAgaW1hZ2VVcGxvYWRlZChpbWFnZSk6IHZvaWQge1xuICAgIGNvbnN0IGltYWdlU291cmNlID0gbmV3IEltYWdlU291cmNlKCk7XG4gICAgaW1hZ2VTb3VyY2Uuc2V0TmF0aXZlU291cmNlKGltYWdlKTtcbiAgICB0aGlzLnpvbmUucnVuKCgpID0+IHtcbiAgICAgIGNvbnN0IGJhc2U2NCA9IGltYWdlU291cmNlLnRvQmFzZTY0U3RyaW5nKFwianBlZ1wiLCAxMDApO1xuICAgICAgdGhpcy5iYXNlNjRJbWcoYmFzZTY0KVxuICAgICAgdGhpcy5yZWNvZ25pemVUZXh0Q2xvdWQoaW1hZ2VTb3VyY2UpXG4gICAgfSlcbiAgfVxuXG4gIGJhc2U2NEltZyhlbmNvZGVkSW1nOiBzdHJpbmcpIHtcbiAgICBjb25zdCBpbWFnZVNvdXJjZSA9IG5ldyBJbWFnZVNvdXJjZSgpO1xuICAgIGNvbnN0IGxvYWRlZEJhc2U2NCA9IGltYWdlU291cmNlLmxvYWRGcm9tQmFzZTY0KGVuY29kZWRJbWcpO1xuICAgIGlmIChsb2FkZWRCYXNlNjQpIHtcbiAgICAgIHRoaXMuZnJlc2hDb250YWN0LmNhcmRJbWFnZSA9IGVuY29kZWRJbWc7XG4gICAgICB0aGlzLnBpY2tlZEltYWdlID0gaW1hZ2VTb3VyY2U7XG4gICAgfVxuICB9XG5cbiAgY2hvb3NlR3JvdXAoKTogdm9pZCB7XG4gICAgY29uc3QgZ3JvdXBPYmogPSB0aGlzLmdyb3VwU2VydmljZS5nZXRDb250YWN0R3JvdXBzKCk7XG4gICAgY29uc3QgbmFtZUFycmF5ID0gT2JqZWN0LmtleXMoZ3JvdXBPYmpbXCJjb250YWN0X2dyb3Vwc1wiXSkubWFwKChuYW1lKSA9PiBuYW1lKTtcbiAgICBkaWFsb2dzLmFjdGlvbih7XG4gICAgICBtZXNzYWdlOiBcIkNob29zZSBDYXRlZ29yeVwiLFxuICAgICAgY2FuY2VsQnV0dG9uVGV4dDogXCJDYW5jZWxcIixcbiAgICAgIGFjdGlvbnM6IFtcIk5ldy4uLlwiLCAuLi5uYW1lQXJyYXldXG4gICAgfSlcbiAgICAgIC50aGVuKChyZXN1bHQpID0+IHtcbiAgICAgICAgaWYgKHJlc3VsdCA9PT0gXCJOZXcuLi5cIikge1xuICAgICAgICAgIGRpYWxvZ3MucHJvbXB0KHtcbiAgICAgICAgICAgIHRpdGxlOiBcIk5ldyBDb250YWN0IEdyb3VwXCIsXG4gICAgICAgICAgICBpbnB1dFR5cGU6IGRpYWxvZ3MuaW5wdXRUeXBlLnRleHQsXG4gICAgICAgICAgICBva0J1dHRvblRleHQ6IFwiT0tcIixcbiAgICAgICAgICAgIGNhbmNlbEJ1dHRvblRleHQ6IFwiQ2FuY2VsXCIsXG4gICAgICAgICAgICBjYW5jZWxhYmxlOiB0cnVlXG4gICAgICAgICAgfSkudGhlbigocikgPT4ge1xuICAgICAgICAgICAgaWYgKHIudGV4dCAhPT0gXCJDYW5jZWxcIikge1xuICAgICAgICAgICAgICB0aGlzLmZyZXNoQ29udGFjdC5ncm91cCA9IHIudGV4dDtcbiAgICAgICAgICAgICAgdGhpcy5ncm91cFNlcnZpY2UuYWRkTmV3Q29udGFjdEdyb3VwKHRoaXMuZnJlc2hDb250YWN0Lmdyb3VwKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgIHRoaXMuZnJlc2hDb250YWN0Lmdyb3VwID0gXCJDb250YWN0IEdyb3VwXCI7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdGhpcy5mcmVzaENvbnRhY3QuZ3JvdXAgPSAocmVzdWx0ID09PSBcIkNhbmNlbFwiICYmIHRoaXMuZnJlc2hDb250YWN0Lmdyb3VwID09PSBcIkNvbnRhY3QgR3JvdXBcIikgP1xuICAgICAgICAgICAgXCJDb250YWN0IEdyb3VwXCJcbiAgICAgICAgICAgIDogcmVzdWx0O1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgfVxuXG4gIHNhdmVJbWFnZVBhdGgocGF0aCkge1xuICAgIHRoaXMuZnJlc2hDb250YWN0LmNhcmRJbWFnZVBhdGggPSBwYXRoO1xuICB9XG5cbiAgc3VibWl0KCk6IHZvaWQge1xuICAgIGNvbnN0IGRhdGUgPSBuZXcgRGF0ZSgpO1xuICAgIHRoaXMuZnJlc2hDb250YWN0LmlkID0gYCR7ZGF0ZX18JHt0aGlzLmZyZXNoQ29udGFjdC5lbWFpbEFkZHJlc3N9YDtcblxuICAgIGlmICh0aGlzLnR5cGUgPT09IFwibXlwcm9maWxlc1wiKSB7XG4gICAgICBjb25zdCB1c2VyID0gdGhpcy5hdXRoLmdldGFjdGl2ZVVzZXIoKTtcbiAgICAgIHRoaXMuZnJlc2hDb250YWN0LnVzZXJfaWQgPSB1c2VyLnVzZXJfaWQucmVwbGFjZSgnICcsICcnKTtcbiAgICAgIHRoaXMuc3RvcmFnZVNlcnZpY2Uuc2V0VW5zeW5jZWRQcm9maWxlcyh0aGlzLmZyZXNoQ29udGFjdCk7XG4gICAgfVxuXG4gICAgaWYgKHRoaXMuZnJlc2hDb250YWN0Lmdyb3VwICE9PSBcIkNvbnRhY3QgR3JvdXBcIikge1xuICAgICAgdGhpcy5ncm91cFNlcnZpY2UuYWRkQ29udGFjdFRvR3JvdXAodGhpcy5mcmVzaENvbnRhY3QuaWQsIHRoaXMuZnJlc2hDb250YWN0Lmdyb3VwKTtcbiAgICB9XG4gICAgdGhpcy5zdG9yYWdlU2VydmljZS5hZGQodGhpcy5mcmVzaENvbnRhY3QsIHRoaXMudHlwZSk7XG5cbiAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGVCeVVybChcImhvbWVcIiwgeyBjbGVhckhpc3Rvcnk6IHRydWUgfSk7XG4gIH1cblxuICBwcml2YXRlIHJlY29nbml6ZVRleHRDbG91ZChpbWFnZVNvdXJjZTogSW1hZ2VTb3VyY2UpOiB2b2lkIHtcbiAgICBmaXJlYmFzZS5tbGtpdC50ZXh0cmVjb2duaXRpb24ucmVjb2duaXplVGV4dENsb3VkKHtcbiAgICAgIGltYWdlOiBpbWFnZVNvdXJjZSxcbiAgICAgIG1vZGVsVHlwZTogXCJsYXRlc3RcIixcbiAgICAgIG1heFJlc3VsdHM6IDE1XG4gICAgfSkudGhlbihcbiAgICAgIChyZXN1bHQ6IE1MS2l0UmVjb2duaXplVGV4dFJlc3VsdCkgPT4ge1xuICAgICAgICB0aGlzLm9jci5maWx0ZXJDYXJkVGV4dChyZXN1bHQudGV4dCBhcyBzdHJpbmcpXG4gICAgICAgICAgLnRoZW4oKHNjYW5uZWRDYXJkOiBJQ29udGFjdCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5mcmVzaENvbnRhY3QgPSB7IC4uLnRoaXMuZnJlc2hDb250YWN0LCAuLi5zY2FubmVkQ2FyZCB9O1xuICAgICAgICAgIH0pO1xuICAgICAgICB0aGlzLmNkcmYuZGV0ZWN0Q2hhbmdlcygpO1xuICAgICAgfSlcbiAgICAgIC5jYXRjaChlcnJvck1lc3NhZ2UgPT4gY29uc29sZS5sb2coXCJNTCBLaXQgZXJyb3I6IFwiICsgZXJyb3JNZXNzYWdlKSk7XG4gIH1cbn1cbiJdfQ==