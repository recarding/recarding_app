import { Component, OnInit, ViewChild } from "@angular/core";

import { RouterExtensions } from "nativescript-angular/router";

import {
  DrawerTransitionBase,
  SlideInOnTopTransition
} from "nativescript-ui-sidedrawer";
import { RadSideDrawerComponent } from "nativescript-ui-sidedrawer/angular";
import { PushViewModel } from "../shared/helper/push/notification.service";
import { Page } from "tns-core-modules/ui/page/page";

@Component({
  selector: "Home",
  moduleId: module.id,
  templateUrl: "./home.component.html",
  styleUrls: ["./home-common.scss"]
})
export class HomeComponent implements OnInit {
  @ViewChild("drawer") drawerComponent: RadSideDrawerComponent;

  private _sideDrawerTransition: DrawerTransitionBase;

  constructor(
    private routerExtensions: RouterExtensions,
    private push: PushViewModel,
    private page: Page
   ) { 
    page.actionBarHidden = true;
  }

  ngOnInit(): void {
    this._sideDrawerTransition = new SlideInOnTopTransition();
    if (!this.push.doGetAreNotificationsEnabled()) {
      this.push.doRequestConsent();
    }
  }

  navigate(here: string) {
    this.routerExtensions.navigateByUrl(`${here}`, { clearHistory: true });
  }

  get sideDrawerTransition(): DrawerTransitionBase {
    return this._sideDrawerTransition;
  }

  onDrawerButtonTap(): void {
    this.drawerComponent.sideDrawer.showDrawer();
  }
}
