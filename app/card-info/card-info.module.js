"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var common_1 = require("nativescript-angular/common");
var shared_module_1 = require("../shared/shared.module");
var card_info_component_1 = require("./card-info.component");
var card_routing_module_1 = require("./card-routing.module");
var CardInfoModule = /** @class */ (function () {
    function CardInfoModule() {
    }
    CardInfoModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.NativeScriptCommonModule,
                card_routing_module_1.CardRoutingModule,
                http_1.HttpModule,
                shared_module_1.SharedModule
            ],
            declarations: [
                card_info_component_1.CardInfoComponent
            ],
            schemas: [
                core_1.NO_ERRORS_SCHEMA
            ],
            providers: []
        })
    ], CardInfoModule);
    return CardInfoModule;
}());
exports.CardInfoModule = CardInfoModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC1pbmZvLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImNhcmQtaW5mby5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkQ7QUFDM0Qsc0NBQTJDO0FBRTNDLHNEQUF1RTtBQUV2RSx5REFBdUQ7QUFDdkQsNkRBQTBEO0FBQzFELDZEQUEwRDtBQWlCMUQ7SUFBQTtJQUE4QixDQUFDO0lBQWxCLGNBQWM7UUFmMUIsZUFBUSxDQUFDO1lBQ04sT0FBTyxFQUFFO2dCQUNMLGlDQUF3QjtnQkFDeEIsdUNBQWlCO2dCQUNqQixpQkFBVTtnQkFDViw0QkFBWTthQUNmO1lBQ0QsWUFBWSxFQUFFO2dCQUNWLHVDQUFpQjthQUNwQjtZQUNELE9BQU8sRUFBRTtnQkFDTCx1QkFBZ0I7YUFDbkI7WUFDRCxTQUFTLEVBQUUsRUFBRztTQUNqQixDQUFDO09BQ1csY0FBYyxDQUFJO0lBQUQscUJBQUM7Q0FBQSxBQUEvQixJQUErQjtBQUFsQix3Q0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlLCBOT19FUlJPUlNfU0NIRU1BIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IEh0dHBNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvaHR0cFwiO1xuXG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRDb21tb25Nb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvY29tbW9uXCI7XG5cbmltcG9ydCB7IFNoYXJlZE1vZHVsZSB9IGZyb20gXCIuLi9zaGFyZWQvc2hhcmVkLm1vZHVsZVwiO1xuaW1wb3J0IHsgQ2FyZEluZm9Db21wb25lbnQgfSBmcm9tIFwiLi9jYXJkLWluZm8uY29tcG9uZW50XCI7XG5pbXBvcnQgeyBDYXJkUm91dGluZ01vZHVsZSB9IGZyb20gXCIuL2NhcmQtcm91dGluZy5tb2R1bGVcIjtcblxuQE5nTW9kdWxlKHtcbiAgICBpbXBvcnRzOiBbXG4gICAgICAgIE5hdGl2ZVNjcmlwdENvbW1vbk1vZHVsZSxcbiAgICAgICAgQ2FyZFJvdXRpbmdNb2R1bGUsXG4gICAgICAgIEh0dHBNb2R1bGUsXG4gICAgICAgIFNoYXJlZE1vZHVsZVxuICAgIF0sXG4gICAgZGVjbGFyYXRpb25zOiBbXG4gICAgICAgIENhcmRJbmZvQ29tcG9uZW50XG4gICAgXSxcbiAgICBzY2hlbWFzOiBbXG4gICAgICAgIE5PX0VSUk9SU19TQ0hFTUFcbiAgICBdLFxuICAgIHByb3ZpZGVyczogWyBdXG59KVxuZXhwb3J0IGNsYXNzIENhcmRJbmZvTW9kdWxlIHsgfVxuIl19