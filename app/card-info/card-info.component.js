"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var nativescript_ui_sidedrawer_1 = require("nativescript-ui-sidedrawer");
var angular_1 = require("nativescript-ui-sidedrawer/angular");
var storage_service_1 = require("../shared/helper/app-settings/storage.service");
var Dialogs = require("ui/dialogs");
var dialogs_1 = require("nativescript-angular/directives/dialogs");
var share_modal_1 = require("../shared/shareModal/share.modal");
var auth_service_1 = require("../shared/helper/auth.service");
var router_2 = require("nativescript-angular/router");
var page_1 = require("tns-core-modules/ui/page/page");
var CardInfoComponent = /** @class */ (function () {
    function CardInfoComponent(route, storageService, auth, routerExtensions, modal, vcRef, page) {
        this.route = route;
        this.storageService = storageService;
        this.auth = auth;
        this.routerExtensions = routerExtensions;
        this.modal = modal;
        this.vcRef = vcRef;
        this.page = page;
        this.nearByUsers = [];
        page.actionBarHidden = true;
    }
    CardInfoComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._sideDrawerTransition = new nativescript_ui_sidedrawer_1.SlideInOnTopTransition();
        this.sub = this.route.params.subscribe(function (params) {
            _this.type = params.type;
            _this.id = params.id;
            _this.showSwipe = _this.type === "contacts" ? true : false;
        });
        this.contact = this.storageService.getById(this.id, this.type);
    };
    Object.defineProperty(CardInfoComponent.prototype, "sideDrawerTransition", {
        get: function () {
            return this._sideDrawerTransition;
        },
        enumerable: true,
        configurable: true
    });
    CardInfoComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    CardInfoComponent.prototype.onDrawerButtonTap = function () {
        this.drawerComponent.sideDrawer.showDrawer();
    };
    CardInfoComponent.prototype.back = function () {
        var here = this.type === "contact" ? "contacts" : "myprofile";
        this.routerExtensions.navigateByUrl(here, {
            clearHistory: true,
            transition: {
                name: "slideRight",
                duration: 200,
            }
        });
    };
    CardInfoComponent.prototype.editCard = function () {
        this.routerExtensions.navigateByUrl("edit/" + this.type + "/" + this.contact.id);
    };
    CardInfoComponent.prototype.removeCard = function () {
        var _this = this;
        Dialogs.confirm("Remove " + this.contact.name.displayname + "?").then(function (confirmed) {
            if (confirmed) {
                var removed = _this.storageService.remove(_this.contact.id, _this.type);
                if (_this.type === "myprofiles") {
                    _this.storageService.removeFromUnsynced(_this.contact.emailAddress);
                }
                if (removed) {
                }
                else {
                }
                _this.back();
            }
        });
    };
    CardInfoComponent.prototype.onSwipe = function (args) {
        if (args.direction === 4) {
            alert("Card Sent!");
        }
    };
    CardInfoComponent.prototype.shareCard = function () {
        var _this = this;
        var user = this.auth.getactiveUser();
        user.user_id = user.user_id ? user.user_id : 'hello';
        var options = {
            context: {
                userId: user.user_id,
                cardId: this.contact.id
            },
            fullscreen: false,
            viewContainerRef: this.vcRef
        };
        this.modal.showModal(share_modal_1.ShareModalComponent, options)
            .then(function (nearByUsers) {
            _this.nearByUsers = nearByUsers.content.toJSON();
            console.log(JSON.stringify(_this.nearByUsers));
        });
    };
    __decorate([
        core_1.ViewChild("drawer"),
        __metadata("design:type", angular_1.RadSideDrawerComponent)
    ], CardInfoComponent.prototype, "drawerComponent", void 0);
    CardInfoComponent = __decorate([
        core_1.Component({
            selector: "card-info",
            moduleId: module.id,
            templateUrl: "./card-info.component.html",
            styleUrls: ['./card-common.scss'],
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute,
            storage_service_1.StorageService,
            auth_service_1.AuthService,
            router_2.RouterExtensions,
            dialogs_1.ModalDialogService,
            core_1.ViewContainerRef,
            page_1.Page])
    ], CardInfoComponent);
    return CardInfoComponent;
}());
exports.CardInfoComponent = CardInfoComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC1pbmZvLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImNhcmQtaW5mby5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMEY7QUFDMUYsMENBQWlEO0FBR2pELHlFQUEwRjtBQUMxRiw4REFBNEU7QUFJNUUsaUZBQStFO0FBRS9FLG9DQUFzQztBQUV0QyxtRUFBNkU7QUFDN0UsZ0VBQXVFO0FBQ3ZFLDhEQUE0RDtBQUM1RCxzREFBK0Q7QUFDL0Qsc0RBQXFEO0FBU3JEO0lBV0UsMkJBQ1UsS0FBcUIsRUFDckIsY0FBOEIsRUFDOUIsSUFBaUIsRUFDakIsZ0JBQWtDLEVBQ2xDLEtBQXlCLEVBQ3pCLEtBQXVCLEVBQ3ZCLElBQVU7UUFOVixVQUFLLEdBQUwsS0FBSyxDQUFnQjtRQUNyQixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsU0FBSSxHQUFKLElBQUksQ0FBYTtRQUNqQixxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQ2xDLFVBQUssR0FBTCxLQUFLLENBQW9CO1FBQ3pCLFVBQUssR0FBTCxLQUFLLENBQWtCO1FBQ3ZCLFNBQUksR0FBSixJQUFJLENBQU07UUFWWixnQkFBVyxHQUFrQixFQUFFLENBQUM7UUFZdEMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7SUFDOUIsQ0FBQztJQUVELG9DQUFRLEdBQVI7UUFBQSxpQkFRQztRQVBDLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLG1EQUFzQixFQUFFLENBQUM7UUFDMUQsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsVUFBQyxNQUFNO1lBQzVDLEtBQUksQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQztZQUN4QixLQUFJLENBQUMsRUFBRSxHQUFHLE1BQU0sQ0FBQyxFQUFFLENBQUM7WUFDcEIsS0FBSSxDQUFDLFNBQVMsR0FBRyxLQUFJLENBQUMsSUFBSSxLQUFLLFVBQVUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7UUFDM0QsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ2pFLENBQUM7SUFFRCxzQkFBSSxtREFBb0I7YUFBeEI7WUFDRSxPQUFPLElBQUksQ0FBQyxxQkFBcUIsQ0FBQztRQUNwQyxDQUFDOzs7T0FBQTtJQUVELHVDQUFXLEdBQVg7UUFDRSxJQUFJLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ3pCLENBQUM7SUFFRCw2Q0FBaUIsR0FBakI7UUFDRSxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxVQUFVLEVBQUUsQ0FBQztJQUMvQyxDQUFDO0lBRUQsZ0NBQUksR0FBSjtRQUNFLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLEtBQUssU0FBUyxDQUFBLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQztRQUM3RCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRTtZQUN4QyxZQUFZLEVBQUUsSUFBSTtZQUNsQixVQUFVLEVBQUU7Z0JBQ1YsSUFBSSxFQUFFLFlBQVk7Z0JBQ2xCLFFBQVEsRUFBRSxHQUFHO2FBQ2Q7U0FDRixDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsb0NBQVEsR0FBUjtRQUNFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsVUFBUSxJQUFJLENBQUMsSUFBSSxTQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBSSxDQUFDLENBQUM7SUFDOUUsQ0FBQztJQUVELHNDQUFVLEdBQVY7UUFBQSxpQkFhQztRQVpDLE9BQU8sQ0FBQyxPQUFPLENBQUMsWUFBVSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLE1BQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLFNBQVM7WUFDekUsSUFBSSxTQUFTLEVBQUU7Z0JBQ2IsSUFBTSxPQUFPLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLEVBQUUsS0FBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUN2RSxJQUFHLEtBQUksQ0FBQyxJQUFJLEtBQUssWUFBWSxFQUFFO29CQUM3QixLQUFJLENBQUMsY0FBYyxDQUFDLGtCQUFrQixDQUFDLEtBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUE7aUJBQ2xFO2dCQUNELElBQUksT0FBTyxFQUFFO2lCQUNaO3FCQUFNO2lCQUNOO2dCQUNGLEtBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQzthQUNaO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsbUNBQU8sR0FBUCxVQUFRLElBQTJCO1FBQ2pDLElBQUksSUFBSSxDQUFDLFNBQVMsS0FBSyxDQUFDLEVBQUU7WUFDeEIsS0FBSyxDQUFDLFlBQVksQ0FBQyxDQUFDO1NBQ3JCO0lBQ0gsQ0FBQztJQUVELHFDQUFTLEdBQVQ7UUFBQSxpQkFnQkM7UUFmQyxJQUFNLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFBO1FBQ3RDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDO1FBQ3JELElBQU0sT0FBTyxHQUFHO1lBQ2QsT0FBTyxFQUFFO2dCQUNQLE1BQU0sRUFBRSxJQUFJLENBQUMsT0FBTztnQkFDcEIsTUFBTSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRTthQUN4QjtZQUNELFVBQVUsRUFBRSxLQUFLO1lBQ2pCLGdCQUFnQixFQUFFLElBQUksQ0FBQyxLQUFLO1NBQzdCLENBQUM7UUFDRixJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxpQ0FBbUIsRUFBRSxPQUFPLENBQUM7YUFDL0MsSUFBSSxDQUFDLFVBQUMsV0FBVztZQUNoQixLQUFJLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUM7WUFDaEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1FBQ2hELENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQWhHb0I7UUFBcEIsZ0JBQVMsQ0FBQyxRQUFRLENBQUM7a0NBQWtCLGdDQUFzQjs4REFBQztJQURsRCxpQkFBaUI7UUFQN0IsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxXQUFXO1lBQ3JCLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixXQUFXLEVBQUUsNEJBQTRCO1lBQ3pDLFNBQVMsRUFBRSxDQUFDLG9CQUFvQixDQUFDO1NBQ2xDLENBQUM7eUNBY2lCLHVCQUFjO1lBQ0wsZ0NBQWM7WUFDeEIsMEJBQVc7WUFDQyx5QkFBZ0I7WUFDM0IsNEJBQWtCO1lBQ2xCLHVCQUFnQjtZQUNqQixXQUFJO09BbEJULGlCQUFpQixDQWtHN0I7SUFBRCx3QkFBQztDQUFBLEFBbEdELElBa0dDO0FBbEdZLDhDQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25EZXN0cm95LCBPbkluaXQsIFZpZXdDaGlsZCwgVmlld0NvbnRhaW5lclJlZiB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZSB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcbmltcG9ydCB7IFN1YnNjcmlwdGlvbiB9IGZyb20gXCJyeGpzXCI7XG5cbmltcG9ydCB7IERyYXdlclRyYW5zaXRpb25CYXNlLCBTbGlkZUluT25Ub3BUcmFuc2l0aW9uIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC11aS1zaWRlZHJhd2VyXCI7XG5pbXBvcnQgeyBSYWRTaWRlRHJhd2VyQ29tcG9uZW50IH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC11aS1zaWRlZHJhd2VyL2FuZ3VsYXJcIjtcbmltcG9ydCB7IFN3aXBlR2VzdHVyZUV2ZW50RGF0YSB9IGZyb20gXCJ1aS9nZXN0dXJlc1wiO1xuXG5pbXBvcnQgeyBJQ29udGFjdCB9IGZyb20gXCIuLi9zaGFyZWQvaGVscGVyL2FwcC1zZXR0aW5ncy9zdG9yYWdlLm1vZGVsXCI7XG5pbXBvcnQgeyBTdG9yYWdlU2VydmljZSB9IGZyb20gXCIuLi9zaGFyZWQvaGVscGVyL2FwcC1zZXR0aW5ncy9zdG9yYWdlLnNlcnZpY2VcIjtcblxuaW1wb3J0ICogYXMgRGlhbG9ncyBmcm9tIFwidWkvZGlhbG9nc1wiO1xuXG5pbXBvcnQgeyBNb2RhbERpYWxvZ1NlcnZpY2UgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvZGlyZWN0aXZlcy9kaWFsb2dzXCI7XG5pbXBvcnQgeyBTaGFyZU1vZGFsQ29tcG9uZW50IH0gZnJvbSBcIi4uL3NoYXJlZC9zaGFyZU1vZGFsL3NoYXJlLm1vZGFsXCI7XG5pbXBvcnQgeyBBdXRoU2VydmljZSB9IGZyb20gXCIuLi9zaGFyZWQvaGVscGVyL2F1dGguc2VydmljZVwiO1xuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcbmltcG9ydCB7IFBhZ2UgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9wYWdlL3BhZ2VcIjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBcImNhcmQtaW5mb1wiLFxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxuICB0ZW1wbGF0ZVVybDogXCIuL2NhcmQtaW5mby5jb21wb25lbnQuaHRtbFwiLFxuICBzdHlsZVVybHM6IFsnLi9jYXJkLWNvbW1vbi5zY3NzJ10sXG59KVxuXG5leHBvcnQgY2xhc3MgQ2FyZEluZm9Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG4gIEBWaWV3Q2hpbGQoXCJkcmF3ZXJcIikgZHJhd2VyQ29tcG9uZW50OiBSYWRTaWRlRHJhd2VyQ29tcG9uZW50O1xuICBwcml2YXRlIF9zaWRlRHJhd2VyVHJhbnNpdGlvbjogRHJhd2VyVHJhbnNpdGlvbkJhc2U7XG5cbiAgcHJpdmF0ZSBzdWI6IFN1YnNjcmlwdGlvbjtcbiAgcHJpdmF0ZSBjb250YWN0OiBJQ29udGFjdDtcbiAgcHJpdmF0ZSBpZDogc3RyaW5nO1xuICBwcml2YXRlIHR5cGU6IHN0cmluZztcbiAgcHJpdmF0ZSBuZWFyQnlVc2VyczogQXJyYXk8b2JqZWN0PiA9IFtdO1xuICBwcml2YXRlIHNob3dTd2lwZTogYm9vbGVhbjtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcbiAgICBwcml2YXRlIHN0b3JhZ2VTZXJ2aWNlOiBTdG9yYWdlU2VydmljZSxcbiAgICBwcml2YXRlIGF1dGg6IEF1dGhTZXJ2aWNlLFxuICAgIHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucyxcbiAgICBwcml2YXRlIG1vZGFsOiBNb2RhbERpYWxvZ1NlcnZpY2UsXG4gICAgcHJpdmF0ZSB2Y1JlZjogVmlld0NvbnRhaW5lclJlZixcbiAgICBwcml2YXRlIHBhZ2U6IFBhZ2UsXG4gICkgeyBcbiAgICBwYWdlLmFjdGlvbkJhckhpZGRlbiA9IHRydWU7XG4gIH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICB0aGlzLl9zaWRlRHJhd2VyVHJhbnNpdGlvbiA9IG5ldyBTbGlkZUluT25Ub3BUcmFuc2l0aW9uKCk7XG4gICAgdGhpcy5zdWIgPSB0aGlzLnJvdXRlLnBhcmFtcy5zdWJzY3JpYmUoKHBhcmFtcykgPT4ge1xuICAgICAgdGhpcy50eXBlID0gcGFyYW1zLnR5cGU7XG4gICAgICB0aGlzLmlkID0gcGFyYW1zLmlkO1xuICAgICAgdGhpcy5zaG93U3dpcGUgPSB0aGlzLnR5cGUgPT09IFwiY29udGFjdHNcIiA/IHRydWUgOiBmYWxzZTtcbiAgICB9KTtcbiAgICB0aGlzLmNvbnRhY3QgPSB0aGlzLnN0b3JhZ2VTZXJ2aWNlLmdldEJ5SWQodGhpcy5pZCwgdGhpcy50eXBlKTtcbiAgfVxuXG4gIGdldCBzaWRlRHJhd2VyVHJhbnNpdGlvbigpOiBEcmF3ZXJUcmFuc2l0aW9uQmFzZSB7XG4gICAgcmV0dXJuIHRoaXMuX3NpZGVEcmF3ZXJUcmFuc2l0aW9uO1xuICB9XG5cbiAgbmdPbkRlc3Ryb3koKSB7XG4gICAgdGhpcy5zdWIudW5zdWJzY3JpYmUoKTtcbiAgfVxuXG4gIG9uRHJhd2VyQnV0dG9uVGFwKCk6IHZvaWQge1xuICAgIHRoaXMuZHJhd2VyQ29tcG9uZW50LnNpZGVEcmF3ZXIuc2hvd0RyYXdlcigpO1xuICB9XG4gIFxuICBiYWNrKCl7XG4gICAgbGV0IGhlcmUgPSB0aGlzLnR5cGUgPT09IFwiY29udGFjdFwiPyBcImNvbnRhY3RzXCIgOiBcIm15cHJvZmlsZVwiO1xuICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZUJ5VXJsKGhlcmUsIHsgXG4gICAgICBjbGVhckhpc3Rvcnk6IHRydWUsXG4gICAgICB0cmFuc2l0aW9uOiB7XG4gICAgICAgIG5hbWU6IFwic2xpZGVSaWdodFwiLFxuICAgICAgICBkdXJhdGlvbjogMjAwLFxuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgZWRpdENhcmQoKSB7XG4gICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlQnlVcmwoYGVkaXQvJHt0aGlzLnR5cGV9LyR7dGhpcy5jb250YWN0LmlkfWApO1xuICB9XG5cbiAgcmVtb3ZlQ2FyZCgpIHtcbiAgICBEaWFsb2dzLmNvbmZpcm0oYFJlbW92ZSAke3RoaXMuY29udGFjdC5uYW1lLmRpc3BsYXluYW1lfT9gKS50aGVuKChjb25maXJtZWQpID0+IHtcbiAgICAgIGlmIChjb25maXJtZWQpIHtcbiAgICAgICAgY29uc3QgcmVtb3ZlZCA9IHRoaXMuc3RvcmFnZVNlcnZpY2UucmVtb3ZlKHRoaXMuY29udGFjdC5pZCwgdGhpcy50eXBlKTtcbiAgICAgICAgaWYodGhpcy50eXBlID09PSBcIm15cHJvZmlsZXNcIikge1xuICAgICAgICAgIHRoaXMuc3RvcmFnZVNlcnZpY2UucmVtb3ZlRnJvbVVuc3luY2VkKHRoaXMuY29udGFjdC5lbWFpbEFkZHJlc3MpXG4gICAgICAgIH1cbiAgICAgICAgaWYgKHJlbW92ZWQpIHtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgfVxuICAgICAgIHRoaXMuYmFjaygpO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgb25Td2lwZShhcmdzOiBTd2lwZUdlc3R1cmVFdmVudERhdGEpIHtcbiAgICBpZiAoYXJncy5kaXJlY3Rpb24gPT09IDQpIHtcbiAgICAgIGFsZXJ0KFwiQ2FyZCBTZW50IVwiKTtcbiAgICB9XG4gIH1cblxuICBzaGFyZUNhcmQoKSB7XG4gICAgY29uc3QgdXNlciA9IHRoaXMuYXV0aC5nZXRhY3RpdmVVc2VyKClcbiAgICB1c2VyLnVzZXJfaWQgPSB1c2VyLnVzZXJfaWQgPyB1c2VyLnVzZXJfaWQgOiAnaGVsbG8nO1xuICAgIGNvbnN0IG9wdGlvbnMgPSB7XG4gICAgICBjb250ZXh0OiB7XG4gICAgICAgIHVzZXJJZDogdXNlci51c2VyX2lkLFxuICAgICAgICBjYXJkSWQ6IHRoaXMuY29udGFjdC5pZFxuICAgICAgfSxcbiAgICAgIGZ1bGxzY3JlZW46IGZhbHNlLFxuICAgICAgdmlld0NvbnRhaW5lclJlZjogdGhpcy52Y1JlZlxuICAgIH07XG4gICAgdGhpcy5tb2RhbC5zaG93TW9kYWwoU2hhcmVNb2RhbENvbXBvbmVudCwgb3B0aW9ucylcbiAgICAgIC50aGVuKChuZWFyQnlVc2VycykgPT4ge1xuICAgICAgICB0aGlzLm5lYXJCeVVzZXJzID0gbmVhckJ5VXNlcnMuY29udGVudC50b0pTT04oKTtcbiAgICAgICAgY29uc29sZS5sb2coSlNPTi5zdHJpbmdpZnkodGhpcy5uZWFyQnlVc2VycykpO1xuICAgICAgfSk7XG4gIH1cbn1cbiJdfQ==