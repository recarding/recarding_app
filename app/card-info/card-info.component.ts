import { Component, OnDestroy, OnInit, ViewChild, ViewContainerRef } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs";

import { DrawerTransitionBase, SlideInOnTopTransition } from "nativescript-ui-sidedrawer";
import { RadSideDrawerComponent } from "nativescript-ui-sidedrawer/angular";
import { SwipeGestureEventData } from "ui/gestures";

import { IContact } from "../shared/helper/app-settings/storage.model";
import { StorageService } from "../shared/helper/app-settings/storage.service";

import * as Dialogs from "ui/dialogs";

import { ModalDialogService } from "nativescript-angular/directives/dialogs";
import { ShareModalComponent } from "../shared/shareModal/share.modal";
import { AuthService } from "../shared/helper/auth.service";
import { RouterExtensions } from "nativescript-angular/router";
import { Page } from "tns-core-modules/ui/page/page";

@Component({
  selector: "card-info",
  moduleId: module.id,
  templateUrl: "./card-info.component.html",
  styleUrls: ['./card-common.scss'],
})

export class CardInfoComponent implements OnInit, OnDestroy {
  @ViewChild("drawer") drawerComponent: RadSideDrawerComponent;
  private _sideDrawerTransition: DrawerTransitionBase;

  private sub: Subscription;
  private contact: IContact;
  private id: string;
  private type: string;
  private nearByUsers: Array<object> = [];
  private showSwipe: boolean;

  constructor(
    private route: ActivatedRoute,
    private storageService: StorageService,
    private auth: AuthService,
    private routerExtensions: RouterExtensions,
    private modal: ModalDialogService,
    private vcRef: ViewContainerRef,
    private page: Page,
  ) { 
    page.actionBarHidden = true;
  }

  ngOnInit(): void {
    this._sideDrawerTransition = new SlideInOnTopTransition();
    this.sub = this.route.params.subscribe((params) => {
      this.type = params.type;
      this.id = params.id;
      this.showSwipe = this.type === "contacts" ? true : false;
    });
    this.contact = this.storageService.getById(this.id, this.type);
  }

  get sideDrawerTransition(): DrawerTransitionBase {
    return this._sideDrawerTransition;
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  onDrawerButtonTap(): void {
    this.drawerComponent.sideDrawer.showDrawer();
  }
  
  back(){
    let here = this.type === "contact"? "contacts" : "myprofile";
    this.routerExtensions.navigateByUrl(here, { 
      clearHistory: true,
      transition: {
        name: "slideRight",
        duration: 200,
      }
    });
  }

  editCard() {
    this.routerExtensions.navigateByUrl(`edit/${this.type}/${this.contact.id}`);
  }

  removeCard() {
    Dialogs.confirm(`Remove ${this.contact.name.displayname}?`).then((confirmed) => {
      if (confirmed) {
        const removed = this.storageService.remove(this.contact.id, this.type);
        if(this.type === "myprofiles") {
          this.storageService.removeFromUnsynced(this.contact.emailAddress)
        }
        if (removed) {
        } else {
        }
       this.back();
      }
    });
  }

  onSwipe(args: SwipeGestureEventData) {
    if (args.direction === 4) {
      alert("Card Sent!");
    }
  }

  shareCard() {
    const user = this.auth.getactiveUser()
    user.user_id = user.user_id ? user.user_id : 'hello';
    const options = {
      context: {
        userId: user.user_id,
        cardId: this.contact.id
      },
      fullscreen: false,
      viewContainerRef: this.vcRef
    };
    this.modal.showModal(ShareModalComponent, options)
      .then((nearByUsers) => {
        this.nearByUsers = nearByUsers.content.toJSON();
        console.log(JSON.stringify(this.nearByUsers));
      });
  }
}
