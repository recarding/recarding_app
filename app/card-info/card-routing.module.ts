import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { CardInfoComponent } from "./card-info.component";

const routes: Routes = [
    { path: ":type/:id",
      component: CardInfoComponent,
      data: {
            shouldDetach: true,
            title: null
        }
   }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class CardRoutingModule { }
