import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpModule } from "@angular/http";

import { NativeScriptCommonModule } from "nativescript-angular/common";

import { SharedModule } from "../shared/shared.module";
import { CardInfoComponent } from "./card-info.component";
import { CardRoutingModule } from "./card-routing.module";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        CardRoutingModule,
        HttpModule,
        SharedModule
    ],
    declarations: [
        CardInfoComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    providers: [ ]
})
export class CardInfoModule { }
