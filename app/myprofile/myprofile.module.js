"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var common_1 = require("nativescript-angular/common");
var shared_module_1 = require("../shared/shared.module");
var myprofile_routing_module_1 = require("./myprofile-routing.module");
var myprofile_component_1 = require("./myprofile.component");
var MyProfileModule = /** @class */ (function () {
    function MyProfileModule() {
    }
    MyProfileModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.NativeScriptCommonModule,
                myprofile_routing_module_1.MyProfileRoutingModule,
                shared_module_1.SharedModule,
                http_1.HttpModule
            ],
            declarations: [
                myprofile_component_1.MyProfileComponent
            ],
            schemas: [
                core_1.NO_ERRORS_SCHEMA
            ],
            providers: []
        })
    ], MyProfileModule);
    return MyProfileModule;
}());
exports.MyProfileModule = MyProfileModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlwcm9maWxlLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm15cHJvZmlsZS5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkQ7QUFDM0Qsc0NBQTJDO0FBRTNDLHNEQUF1RTtBQUV2RSx5REFBdUQ7QUFDdkQsdUVBQW9FO0FBRXBFLDZEQUEyRDtBQWlCM0Q7SUFBQTtJQUErQixDQUFDO0lBQW5CLGVBQWU7UUFmM0IsZUFBUSxDQUFDO1lBQ1IsT0FBTyxFQUFFO2dCQUNQLGlDQUF3QjtnQkFDeEIsaURBQXNCO2dCQUN0Qiw0QkFBWTtnQkFDWixpQkFBVTthQUNYO1lBQ0QsWUFBWSxFQUFFO2dCQUNaLHdDQUFrQjthQUNuQjtZQUNELE9BQU8sRUFBRTtnQkFDUCx1QkFBZ0I7YUFDakI7WUFDRCxTQUFTLEVBQUUsRUFBRTtTQUNkLENBQUM7T0FDVyxlQUFlLENBQUk7SUFBRCxzQkFBQztDQUFBLEFBQWhDLElBQWdDO0FBQW5CLDBDQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUsIE5PX0VSUk9SU19TQ0hFTUEgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBIdHRwTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2h0dHBcIjtcclxuXHJcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdENvbW1vbk1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9jb21tb25cIjtcclxuXHJcbmltcG9ydCB7IFNoYXJlZE1vZHVsZSB9IGZyb20gXCIuLi9zaGFyZWQvc2hhcmVkLm1vZHVsZVwiO1xyXG5pbXBvcnQgeyBNeVByb2ZpbGVSb3V0aW5nTW9kdWxlIH0gZnJvbSBcIi4vbXlwcm9maWxlLXJvdXRpbmcubW9kdWxlXCI7XHJcblxyXG5pbXBvcnQgeyBNeVByb2ZpbGVDb21wb25lbnQgfSBmcm9tIFwiLi9teXByb2ZpbGUuY29tcG9uZW50XCI7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGltcG9ydHM6IFtcclxuICAgIE5hdGl2ZVNjcmlwdENvbW1vbk1vZHVsZSxcclxuICAgIE15UHJvZmlsZVJvdXRpbmdNb2R1bGUsXHJcbiAgICBTaGFyZWRNb2R1bGUsXHJcbiAgICBIdHRwTW9kdWxlXHJcbiAgXSxcclxuICBkZWNsYXJhdGlvbnM6IFtcclxuICAgIE15UHJvZmlsZUNvbXBvbmVudFxyXG4gIF0sXHJcbiAgc2NoZW1hczogW1xyXG4gICAgTk9fRVJST1JTX1NDSEVNQVxyXG4gIF0sXHJcbiAgcHJvdmlkZXJzOiBbXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTXlQcm9maWxlTW9kdWxlIHsgfVxyXG4iXX0=