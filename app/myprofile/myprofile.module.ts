import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpModule } from "@angular/http";

import { NativeScriptCommonModule } from "nativescript-angular/common";

import { SharedModule } from "../shared/shared.module";
import { MyProfileRoutingModule } from "./myprofile-routing.module";

import { MyProfileComponent } from "./myprofile.component";

@NgModule({
  imports: [
    NativeScriptCommonModule,
    MyProfileRoutingModule,
    SharedModule,
    HttpModule
  ],
  declarations: [
    MyProfileComponent
  ],
  schemas: [
    NO_ERRORS_SCHEMA
  ],
  providers: []
})
export class MyProfileModule { }
