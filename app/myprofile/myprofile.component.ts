import { Component, OnInit, ViewChild } from "@angular/core";

import { DrawerTransitionBase } from "nativescript-ui-sidedrawer";
import { RadSideDrawerComponent } from "nativescript-ui-sidedrawer/angular";
import { SearchBar } from "ui/search-bar";

import "rxjs/add/observable/fromPromise";

import { IContact } from "../shared/helper/app-settings/storage.model";
import { StorageService } from "../shared/helper/app-settings/storage.service";
import { RouterExtensions } from "nativescript-angular/router";
import { Page } from "tns-core-modules/ui/page/page";

@Component({
  selector: "MyProfile",
  moduleId: module.id,
  templateUrl: "./myprofile.component.html",
  styleUrls: ["myprofile.common.scss"]
})
export class MyProfileComponent implements OnInit {
  @ViewChild("drawer") drawerComponent: RadSideDrawerComponent;
  @ViewChild("searchField") searchField: SearchBar;
  myprofiles: Array<IContact> = [];
  showSearch: boolean = false;
  filterInProgress: boolean = false;
  type = "myprofiles";
  private _sideDrawerTransition: DrawerTransitionBase;

  constructor(
    private storageService: StorageService,
    private routerExtensions: RouterExtensions
  ) {}

  ngOnInit(): void {
    this.myprofiles = this.storageService.getContacts("myprofiles");
  }

  addNew() {
    this.routerExtensions.navigateByUrl("/newcard/myprofiles", {
      clearHistory: true
    });
  }

  get sideDrawerTransition(): DrawerTransitionBase {
    return this._sideDrawerTransition;
  }

  onDrawerButtonTap(): void {
    this.drawerComponent.sideDrawer.showDrawer();
  }

  showSearchField() {
    this.showSearch = !this.showSearch;
  }

  sortAlpha() {
    this.myprofiles.sort((a, b) => {
      const nameA = a.name.displayname;
      const nameB = b.name.displayname;
      return nameA < nameB ? -1 : nameA > nameB ? 1 : 0;
    });
  }

  sortDate() {
    this.myprofiles.sort(function(a, b) {
      var createdAtA = a.id.split("|")[0];
      var createdAtB = b.id.split("|")[0];
      return createdAtA < createdAtB ? -1 : createdAtA > createdAtB ? 1 : 0;
    });
  }

  back() {
    this.routerExtensions.navigateByUrl(`/home`, {
      clearHistory: true,
      transition: {
        name: "slideRight",
        duration: 200
      }
    });
  }
}
