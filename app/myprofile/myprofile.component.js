"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var angular_1 = require("nativescript-ui-sidedrawer/angular");
var search_bar_1 = require("ui/search-bar");
require("rxjs/add/observable/fromPromise");
var storage_service_1 = require("../shared/helper/app-settings/storage.service");
var router_1 = require("nativescript-angular/router");
var MyProfileComponent = /** @class */ (function () {
    function MyProfileComponent(storageService, routerExtensions) {
        this.storageService = storageService;
        this.routerExtensions = routerExtensions;
        this.myprofiles = [];
        this.showSearch = false;
        this.filterInProgress = false;
        this.type = "myprofiles";
    }
    MyProfileComponent.prototype.ngOnInit = function () {
        this.myprofiles = this.storageService.getContacts("myprofiles");
    };
    MyProfileComponent.prototype.addNew = function () {
        this.routerExtensions.navigateByUrl("/newcard/myprofiles", {
            clearHistory: true
        });
    };
    Object.defineProperty(MyProfileComponent.prototype, "sideDrawerTransition", {
        get: function () {
            return this._sideDrawerTransition;
        },
        enumerable: true,
        configurable: true
    });
    MyProfileComponent.prototype.onDrawerButtonTap = function () {
        this.drawerComponent.sideDrawer.showDrawer();
    };
    MyProfileComponent.prototype.showSearchField = function () {
        this.showSearch = !this.showSearch;
    };
    MyProfileComponent.prototype.sortAlpha = function () {
        this.myprofiles.sort(function (a, b) {
            var nameA = a.name.displayname;
            var nameB = b.name.displayname;
            return nameA < nameB ? -1 : nameA > nameB ? 1 : 0;
        });
    };
    MyProfileComponent.prototype.sortDate = function () {
        this.myprofiles.sort(function (a, b) {
            var createdAtA = a.id.split("|")[0];
            var createdAtB = b.id.split("|")[0];
            return createdAtA < createdAtB ? -1 : createdAtA > createdAtB ? 1 : 0;
        });
    };
    MyProfileComponent.prototype.back = function () {
        this.routerExtensions.navigateByUrl("/home", {
            clearHistory: true,
            transition: {
                name: "slideRight",
                duration: 200
            }
        });
    };
    __decorate([
        core_1.ViewChild("drawer"),
        __metadata("design:type", angular_1.RadSideDrawerComponent)
    ], MyProfileComponent.prototype, "drawerComponent", void 0);
    __decorate([
        core_1.ViewChild("searchField"),
        __metadata("design:type", search_bar_1.SearchBar)
    ], MyProfileComponent.prototype, "searchField", void 0);
    MyProfileComponent = __decorate([
        core_1.Component({
            selector: "MyProfile",
            moduleId: module.id,
            templateUrl: "./myprofile.component.html",
            styleUrls: ["myprofile.common.scss"]
        }),
        __metadata("design:paramtypes", [storage_service_1.StorageService,
            router_1.RouterExtensions])
    ], MyProfileComponent);
    return MyProfileComponent;
}());
exports.MyProfileComponent = MyProfileComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlwcm9maWxlLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm15cHJvZmlsZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBNkQ7QUFHN0QsOERBQTRFO0FBQzVFLDRDQUEwQztBQUUxQywyQ0FBeUM7QUFHekMsaUZBQStFO0FBQy9FLHNEQUErRDtBQVMvRDtJQVNFLDRCQUNVLGNBQThCLEVBQzlCLGdCQUFrQztRQURsQyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQVI1QyxlQUFVLEdBQW9CLEVBQUUsQ0FBQztRQUNqQyxlQUFVLEdBQVksS0FBSyxDQUFDO1FBQzVCLHFCQUFnQixHQUFZLEtBQUssQ0FBQztRQUNsQyxTQUFJLEdBQUcsWUFBWSxDQUFDO0lBTWpCLENBQUM7SUFFSixxQ0FBUSxHQUFSO1FBQ0UsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUNsRSxDQUFDO0lBRUQsbUNBQU0sR0FBTjtRQUNFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMscUJBQXFCLEVBQUU7WUFDekQsWUFBWSxFQUFFLElBQUk7U0FDbkIsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELHNCQUFJLG9EQUFvQjthQUF4QjtZQUNFLE9BQU8sSUFBSSxDQUFDLHFCQUFxQixDQUFDO1FBQ3BDLENBQUM7OztPQUFBO0lBRUQsOENBQWlCLEdBQWpCO1FBQ0UsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDL0MsQ0FBQztJQUVELDRDQUFlLEdBQWY7UUFDRSxJQUFJLENBQUMsVUFBVSxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQztJQUNyQyxDQUFDO0lBRUQsc0NBQVMsR0FBVDtRQUNFLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFVBQUMsQ0FBQyxFQUFFLENBQUM7WUFDeEIsSUFBTSxLQUFLLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUM7WUFDakMsSUFBTSxLQUFLLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUM7WUFDakMsT0FBTyxLQUFLLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDcEQsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQscUNBQVEsR0FBUjtRQUNFLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVMsQ0FBQyxFQUFFLENBQUM7WUFDaEMsSUFBSSxVQUFVLEdBQUcsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDcEMsSUFBSSxVQUFVLEdBQUcsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDcEMsT0FBTyxVQUFVLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDeEUsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsaUNBQUksR0FBSjtRQUNFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsT0FBTyxFQUFFO1lBQzNDLFlBQVksRUFBRSxJQUFJO1lBQ2xCLFVBQVUsRUFBRTtnQkFDVixJQUFJLEVBQUUsWUFBWTtnQkFDbEIsUUFBUSxFQUFFLEdBQUc7YUFDZDtTQUNGLENBQUMsQ0FBQztJQUNMLENBQUM7SUEzRG9CO1FBQXBCLGdCQUFTLENBQUMsUUFBUSxDQUFDO2tDQUFrQixnQ0FBc0I7K0RBQUM7SUFDbkM7UUFBekIsZ0JBQVMsQ0FBQyxhQUFhLENBQUM7a0NBQWMsc0JBQVM7MkRBQUM7SUFGdEMsa0JBQWtCO1FBTjlCLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsV0FBVztZQUNyQixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsV0FBVyxFQUFFLDRCQUE0QjtZQUN6QyxTQUFTLEVBQUUsQ0FBQyx1QkFBdUIsQ0FBQztTQUNyQyxDQUFDO3lDQVcwQixnQ0FBYztZQUNaLHlCQUFnQjtPQVhqQyxrQkFBa0IsQ0E2RDlCO0lBQUQseUJBQUM7Q0FBQSxBQTdERCxJQTZEQztBQTdEWSxnREFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgVmlld0NoaWxkIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuXHJcbmltcG9ydCB7IERyYXdlclRyYW5zaXRpb25CYXNlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC11aS1zaWRlZHJhd2VyXCI7XHJcbmltcG9ydCB7IFJhZFNpZGVEcmF3ZXJDb21wb25lbnQgfSBmcm9tIFwibmF0aXZlc2NyaXB0LXVpLXNpZGVkcmF3ZXIvYW5ndWxhclwiO1xyXG5pbXBvcnQgeyBTZWFyY2hCYXIgfSBmcm9tIFwidWkvc2VhcmNoLWJhclwiO1xyXG5cclxuaW1wb3J0IFwicnhqcy9hZGQvb2JzZXJ2YWJsZS9mcm9tUHJvbWlzZVwiO1xyXG5cclxuaW1wb3J0IHsgSUNvbnRhY3QgfSBmcm9tIFwiLi4vc2hhcmVkL2hlbHBlci9hcHAtc2V0dGluZ3Mvc3RvcmFnZS5tb2RlbFwiO1xyXG5pbXBvcnQgeyBTdG9yYWdlU2VydmljZSB9IGZyb20gXCIuLi9zaGFyZWQvaGVscGVyL2FwcC1zZXR0aW5ncy9zdG9yYWdlLnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHsgUGFnZSB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL3BhZ2UvcGFnZVwiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6IFwiTXlQcm9maWxlXCIsXHJcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICB0ZW1wbGF0ZVVybDogXCIuL215cHJvZmlsZS5jb21wb25lbnQuaHRtbFwiLFxyXG4gIHN0eWxlVXJsczogW1wibXlwcm9maWxlLmNvbW1vbi5zY3NzXCJdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBNeVByb2ZpbGVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIEBWaWV3Q2hpbGQoXCJkcmF3ZXJcIikgZHJhd2VyQ29tcG9uZW50OiBSYWRTaWRlRHJhd2VyQ29tcG9uZW50O1xyXG4gIEBWaWV3Q2hpbGQoXCJzZWFyY2hGaWVsZFwiKSBzZWFyY2hGaWVsZDogU2VhcmNoQmFyO1xyXG4gIG15cHJvZmlsZXM6IEFycmF5PElDb250YWN0PiA9IFtdO1xyXG4gIHNob3dTZWFyY2g6IGJvb2xlYW4gPSBmYWxzZTtcclxuICBmaWx0ZXJJblByb2dyZXNzOiBib29sZWFuID0gZmFsc2U7XHJcbiAgdHlwZSA9IFwibXlwcm9maWxlc1wiO1xyXG4gIHByaXZhdGUgX3NpZGVEcmF3ZXJUcmFuc2l0aW9uOiBEcmF3ZXJUcmFuc2l0aW9uQmFzZTtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIHN0b3JhZ2VTZXJ2aWNlOiBTdG9yYWdlU2VydmljZSxcclxuICAgIHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9uc1xyXG4gICkge31cclxuXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICB0aGlzLm15cHJvZmlsZXMgPSB0aGlzLnN0b3JhZ2VTZXJ2aWNlLmdldENvbnRhY3RzKFwibXlwcm9maWxlc1wiKTtcclxuICB9XHJcblxyXG4gIGFkZE5ldygpIHtcclxuICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZUJ5VXJsKFwiL25ld2NhcmQvbXlwcm9maWxlc1wiLCB7XHJcbiAgICAgIGNsZWFySGlzdG9yeTogdHJ1ZVxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBnZXQgc2lkZURyYXdlclRyYW5zaXRpb24oKTogRHJhd2VyVHJhbnNpdGlvbkJhc2Uge1xyXG4gICAgcmV0dXJuIHRoaXMuX3NpZGVEcmF3ZXJUcmFuc2l0aW9uO1xyXG4gIH1cclxuXHJcbiAgb25EcmF3ZXJCdXR0b25UYXAoKTogdm9pZCB7XHJcbiAgICB0aGlzLmRyYXdlckNvbXBvbmVudC5zaWRlRHJhd2VyLnNob3dEcmF3ZXIoKTtcclxuICB9XHJcblxyXG4gIHNob3dTZWFyY2hGaWVsZCgpIHtcclxuICAgIHRoaXMuc2hvd1NlYXJjaCA9ICF0aGlzLnNob3dTZWFyY2g7XHJcbiAgfVxyXG5cclxuICBzb3J0QWxwaGEoKSB7XHJcbiAgICB0aGlzLm15cHJvZmlsZXMuc29ydCgoYSwgYikgPT4ge1xyXG4gICAgICBjb25zdCBuYW1lQSA9IGEubmFtZS5kaXNwbGF5bmFtZTtcclxuICAgICAgY29uc3QgbmFtZUIgPSBiLm5hbWUuZGlzcGxheW5hbWU7XHJcbiAgICAgIHJldHVybiBuYW1lQSA8IG5hbWVCID8gLTEgOiBuYW1lQSA+IG5hbWVCID8gMSA6IDA7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHNvcnREYXRlKCkge1xyXG4gICAgdGhpcy5teXByb2ZpbGVzLnNvcnQoZnVuY3Rpb24oYSwgYikge1xyXG4gICAgICB2YXIgY3JlYXRlZEF0QSA9IGEuaWQuc3BsaXQoXCJ8XCIpWzBdO1xyXG4gICAgICB2YXIgY3JlYXRlZEF0QiA9IGIuaWQuc3BsaXQoXCJ8XCIpWzBdO1xyXG4gICAgICByZXR1cm4gY3JlYXRlZEF0QSA8IGNyZWF0ZWRBdEIgPyAtMSA6IGNyZWF0ZWRBdEEgPiBjcmVhdGVkQXRCID8gMSA6IDA7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGJhY2soKSB7XHJcbiAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGVCeVVybChgL2hvbWVgLCB7XHJcbiAgICAgIGNsZWFySGlzdG9yeTogdHJ1ZSxcclxuICAgICAgdHJhbnNpdGlvbjoge1xyXG4gICAgICAgIG5hbWU6IFwic2xpZGVSaWdodFwiLFxyXG4gICAgICAgIGR1cmF0aW9uOiAyMDBcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG59XHJcbiJdfQ==