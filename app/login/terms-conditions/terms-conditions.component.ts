import { Component } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Page } from "tns-core-modules/ui/page/page";

@Component({
  selector: "ToC",
  moduleId: module.id,
  templateUrl: "./terms-conditions.component.html",
  styleUrls: ["terms-conditions.common.scss"]
})
export class TermsConditionsComponent {
  text = {
    content:
      `Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
      sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
      In hac habitasse platea dictumst quisque sagittis purus sit amet. Tristique senectus et netus et malesuada. 
      At volutpat diam ut venenatis tellus in. Mattis molestie a iaculis at erat pellentesque adipiscing commodo elit. 
      Adipiscing diam donec adipiscing tristique. Enim facilisis gravida neque convallis a cras semper auctor neque. 
      Porttitor rhoncus dolor purus non enim praesent elementum. Vel eros donec ac odio tempor orci dapibus. Auctor 
      neque vitae tempus quam pellentesque nec nam aliquam. Ipsum dolor sit amet consectetur adipiscing elit duis 
      tristique sollicitudin. Lectus mauris ultrices eros in cursus turpis.Mi eget mauris pharetra et ultrices neque ornare. 
      Quisque sagittis purus sit amet volutpat consequat mauris. Purus ut faucibus pulvinar elementum integer. Orci nulla 
      pellentesque dignissim enim sit. Donec ultrices tincidunt arcu non sodales neque. Dui id ornare arcu odio ut sem nulla. 
      Aenean vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant morbi. Nisi lacus sed viverra tellus. 
      Luctus venenatis lectus magna fringilla urna porttitor. Feugiat nibh sed pulvinar proin gravida hendrerit. Viverra nibh 
      cras pulvinar mattis nunc sed blandit libero volutpat.`
  };
  constructor(private router: RouterExtensions, private page: Page) {
    page.actionBarHidden = true;
  }
  back = () => {
    this.router.backToPreviousPage();
  };
}
