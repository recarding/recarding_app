"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var auth_service_1 = require("../../shared/helper/auth.service");
var login_service_1 = require("../login.service");
var dialogs_1 = require("tns-core-modules/ui/dialogs");
var SignUpComponent = /** @class */ (function () {
    function SignUpComponent(routerExtensions, authService, logInService) {
        this.routerExtensions = routerExtensions;
        this.authService = authService;
        this.logInService = logInService;
        this.cancelSignUp = new core_1.EventEmitter();
    }
    SignUpComponent.prototype.signUp = function () {
        var _this = this;
        var date = new Date().getTime();
        var newUsr = {
            email: this.email,
            fullName: this.fullname,
            password: this.password,
            user_id: "" + date + this.fullname,
            loc: {
                type: "Point",
                coordinates: [0, 0]
            }
        };
        this.logInService.signUpUser(newUsr).then(function (res) {
            var result = res.content.toJSON();
            if (result.success) {
                var params = {
                    email: _this.email,
                    password: _this.password
                };
                _this.logInService.signInUser(params).then(function (re) {
                    var response = re.content.toJSON();
                    if (response.success) {
                        _this.authService.setactiveUser(newUsr);
                        _this.authService.storeAuthToken(response.token);
                        _this.routerExtensions.navigateByUrl("/home", {
                            clearHistory: true
                        });
                    }
                    else {
                        dialogs_1.alert(result.msg ? result.msg : "Oops");
                    }
                });
            }
            else {
                switch (result.err.code) {
                    case 11000: {
                        dialogs_1.alert({
                            title: "ReCarding",
                            message: "User already exists",
                            okButtonText: "Close"
                        });
                        break;
                    }
                    default: {
                        dialogs_1.alert("Error during sign up. Please try again");
                        break;
                    }
                }
            }
        });
    };
    SignUpComponent.prototype.showToC = function () {
        this.routerExtensions.navigateByUrl("/login/toc");
    };
    SignUpComponent.prototype.cancel = function () {
        this.cancelSignUp.emit();
    };
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], SignUpComponent.prototype, "cancelSignUp", void 0);
    SignUpComponent = __decorate([
        core_1.Component({
            selector: "SignUp",
            moduleId: module.id,
            templateUrl: "./signup.component.html",
            styleUrls: ["signup.common.scss"]
        }),
        __metadata("design:paramtypes", [router_1.RouterExtensions,
            auth_service_1.AuthService,
            login_service_1.LogInService])
    ], SignUpComponent);
    return SignUpComponent;
}());
exports.SignUpComponent = SignUpComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2lnbnVwLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNpZ251cC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBZ0U7QUFDaEUsc0RBQStEO0FBRS9ELGlFQUErRDtBQUMvRCxrREFBZ0Q7QUFHaEQsdURBQW9EO0FBUXBEO0lBT0UseUJBQ1UsZ0JBQWtDLEVBQ2xDLFdBQXdCLEVBQ3hCLFlBQTBCO1FBRjFCLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDbEMsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsaUJBQVksR0FBWixZQUFZLENBQWM7UUFUMUIsaUJBQVksR0FBRyxJQUFJLG1CQUFZLEVBQUUsQ0FBQztJQVV6QyxDQUFDO0lBRUosZ0NBQU0sR0FBTjtRQUFBLGlCQWdEQztRQS9DQyxJQUFNLElBQUksR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQ2xDLElBQU0sTUFBTSxHQUFVO1lBQ3BCLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7WUFDdkIsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO1lBQ3ZCLE9BQU8sRUFBRSxLQUFHLElBQUksR0FBRyxJQUFJLENBQUMsUUFBVTtZQUNsQyxHQUFHLEVBQUU7Z0JBQ0gsSUFBSSxFQUFFLE9BQU87Z0JBQ2IsV0FBVyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUNwQjtTQUNGLENBQUM7UUFDRixJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHO1lBQzNDLElBQU0sTUFBTSxHQUFHLEdBQUcsQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUM7WUFDcEMsSUFBSSxNQUFNLENBQUMsT0FBTyxFQUFFO2dCQUNsQixJQUFNLE1BQU0sR0FBRztvQkFDYixLQUFLLEVBQUUsS0FBSSxDQUFDLEtBQUs7b0JBQ2pCLFFBQVEsRUFBRSxLQUFJLENBQUMsUUFBUTtpQkFDeEIsQ0FBQztnQkFDRixLQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxFQUFFO29CQUMxQyxJQUFNLFFBQVEsR0FBRyxFQUFFLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDO29CQUNyQyxJQUFJLFFBQVEsQ0FBQyxPQUFPLEVBQUU7d0JBQ3BCLEtBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3dCQUN2QyxLQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7d0JBQ2hELEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsT0FBTyxFQUFFOzRCQUMzQyxZQUFZLEVBQUUsSUFBSTt5QkFDbkIsQ0FBQyxDQUFDO3FCQUNKO3lCQUFNO3dCQUNMLGVBQUssQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQztxQkFDekM7Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7YUFDSjtpQkFBTTtnQkFDTCxRQUFRLE1BQU0sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFO29CQUN2QixLQUFLLEtBQUssQ0FBQyxDQUFDO3dCQUNWLGVBQUssQ0FBQzs0QkFDSixLQUFLLEVBQUUsV0FBVzs0QkFDbEIsT0FBTyxFQUFFLHFCQUFxQjs0QkFDOUIsWUFBWSxFQUFFLE9BQU87eUJBQ3RCLENBQUMsQ0FBQzt3QkFDSCxNQUFNO3FCQUNQO29CQUNELE9BQU8sQ0FBQyxDQUFDO3dCQUNQLGVBQUssQ0FBQyx3Q0FBd0MsQ0FBQyxDQUFDO3dCQUNoRCxNQUFNO3FCQUNQO2lCQUNGO2FBQ0Y7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxpQ0FBTyxHQUFQO1FBQ0UsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBQ0QsZ0NBQU0sR0FBTjtRQUNFLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDM0IsQ0FBQztJQW5FUztRQUFULGFBQU0sRUFBRTs7eURBQW1DO0lBRGpDLGVBQWU7UUFOM0IsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxRQUFRO1lBQ2xCLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixXQUFXLEVBQUUseUJBQXlCO1lBQ3RDLFNBQVMsRUFBRSxDQUFDLG9CQUFvQixDQUFDO1NBQ2xDLENBQUM7eUNBUzRCLHlCQUFnQjtZQUNyQiwwQkFBVztZQUNWLDRCQUFZO09BVnpCLGVBQWUsQ0FxRTNCO0lBQUQsc0JBQUM7Q0FBQSxBQXJFRCxJQXFFQztBQXJFWSwwQ0FBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBPdXRwdXQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcblxuaW1wb3J0IHsgQXV0aFNlcnZpY2UgfSBmcm9tIFwiLi4vLi4vc2hhcmVkL2hlbHBlci9hdXRoLnNlcnZpY2VcIjtcbmltcG9ydCB7IExvZ0luU2VydmljZSB9IGZyb20gXCIuLi9sb2dpbi5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBJVXNlciB9IGZyb20gXCIuLi8uLi9zaGFyZWQvaGVscGVyL2FwcC1zZXR0aW5ncy9zdG9yYWdlLm1vZGVsXCI7XG5cbmltcG9ydCB7IGFsZXJ0IH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvZGlhbG9nc1wiO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6IFwiU2lnblVwXCIsXG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXG4gIHRlbXBsYXRlVXJsOiBcIi4vc2lnbnVwLmNvbXBvbmVudC5odG1sXCIsXG4gIHN0eWxlVXJsczogW1wic2lnbnVwLmNvbW1vbi5zY3NzXCJdXG59KVxuZXhwb3J0IGNsYXNzIFNpZ25VcENvbXBvbmVudCB7XG4gIEBPdXRwdXQoKSBjYW5jZWxTaWduVXAgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG5cbiAgcHJpdmF0ZSBmdWxsbmFtZTogc3RyaW5nO1xuICBwcml2YXRlIGVtYWlsOiBzdHJpbmc7XG4gIHByaXZhdGUgcGFzc3dvcmQ6IHN0cmluZztcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnMsXG4gICAgcHJpdmF0ZSBhdXRoU2VydmljZTogQXV0aFNlcnZpY2UsXG4gICAgcHJpdmF0ZSBsb2dJblNlcnZpY2U6IExvZ0luU2VydmljZVxuICApIHt9XG5cbiAgc2lnblVwKCkge1xuICAgIGNvbnN0IGRhdGUgPSBuZXcgRGF0ZSgpLmdldFRpbWUoKTtcbiAgICBjb25zdCBuZXdVc3I6IElVc2VyID0ge1xuICAgICAgZW1haWw6IHRoaXMuZW1haWwsXG4gICAgICBmdWxsTmFtZTogdGhpcy5mdWxsbmFtZSxcbiAgICAgIHBhc3N3b3JkOiB0aGlzLnBhc3N3b3JkLFxuICAgICAgdXNlcl9pZDogYCR7ZGF0ZX0ke3RoaXMuZnVsbG5hbWV9YCxcbiAgICAgIGxvYzoge1xuICAgICAgICB0eXBlOiBcIlBvaW50XCIsXG4gICAgICAgIGNvb3JkaW5hdGVzOiBbMCwgMF1cbiAgICAgIH1cbiAgICB9O1xuICAgIHRoaXMubG9nSW5TZXJ2aWNlLnNpZ25VcFVzZXIobmV3VXNyKS50aGVuKHJlcyA9PiB7XG4gICAgICBjb25zdCByZXN1bHQgPSByZXMuY29udGVudC50b0pTT04oKTtcbiAgICAgIGlmIChyZXN1bHQuc3VjY2Vzcykge1xuICAgICAgICBjb25zdCBwYXJhbXMgPSB7XG4gICAgICAgICAgZW1haWw6IHRoaXMuZW1haWwsXG4gICAgICAgICAgcGFzc3dvcmQ6IHRoaXMucGFzc3dvcmRcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5sb2dJblNlcnZpY2Uuc2lnbkluVXNlcihwYXJhbXMpLnRoZW4ocmUgPT4ge1xuICAgICAgICAgIGNvbnN0IHJlc3BvbnNlID0gcmUuY29udGVudC50b0pTT04oKTtcbiAgICAgICAgICBpZiAocmVzcG9uc2Uuc3VjY2Vzcykge1xuICAgICAgICAgICAgdGhpcy5hdXRoU2VydmljZS5zZXRhY3RpdmVVc2VyKG5ld1Vzcik7XG4gICAgICAgICAgICB0aGlzLmF1dGhTZXJ2aWNlLnN0b3JlQXV0aFRva2VuKHJlc3BvbnNlLnRva2VuKTtcbiAgICAgICAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZUJ5VXJsKFwiL2hvbWVcIiwge1xuICAgICAgICAgICAgICBjbGVhckhpc3Rvcnk6IHRydWVcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBhbGVydChyZXN1bHQubXNnID8gcmVzdWx0Lm1zZyA6IFwiT29wc1wiKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgc3dpdGNoIChyZXN1bHQuZXJyLmNvZGUpIHtcbiAgICAgICAgICBjYXNlIDExMDAwOiB7XG4gICAgICAgICAgICBhbGVydCh7XG4gICAgICAgICAgICAgIHRpdGxlOiBcIlJlQ2FyZGluZ1wiLFxuICAgICAgICAgICAgICBtZXNzYWdlOiBcIlVzZXIgYWxyZWFkeSBleGlzdHNcIixcbiAgICAgICAgICAgICAgb2tCdXR0b25UZXh0OiBcIkNsb3NlXCJcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgfVxuICAgICAgICAgIGRlZmF1bHQ6IHtcbiAgICAgICAgICAgIGFsZXJ0KFwiRXJyb3IgZHVyaW5nIHNpZ24gdXAuIFBsZWFzZSB0cnkgYWdhaW5cIik7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIHNob3dUb0MoKSB7XG4gICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlQnlVcmwoXCIvbG9naW4vdG9jXCIpO1xuICB9XG4gIGNhbmNlbCgpIHtcbiAgICB0aGlzLmNhbmNlbFNpZ25VcC5lbWl0KCk7XG4gIH1cbn1cbiJdfQ==