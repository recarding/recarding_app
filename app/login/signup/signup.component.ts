import { Component, EventEmitter, Output } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";

import { AuthService } from "../../shared/helper/auth.service";
import { LogInService } from "../login.service";
import { IUser } from "../../shared/helper/app-settings/storage.model";

import { alert } from "tns-core-modules/ui/dialogs";

@Component({
  selector: "SignUp",
  moduleId: module.id,
  templateUrl: "./signup.component.html",
  styleUrls: ["signup.common.scss"]
})
export class SignUpComponent {
  @Output() cancelSignUp = new EventEmitter();

  private fullname: string;
  private email: string;
  private password: string;

  constructor(
    private routerExtensions: RouterExtensions,
    private authService: AuthService,
    private logInService: LogInService
  ) {}

  signUp() {
    const date = new Date().getTime();
    const newUsr: IUser = {
      email: this.email,
      fullName: this.fullname,
      password: this.password,
      user_id: `${date}${this.fullname}`,
      loc: {
        type: "Point",
        coordinates: [0, 0]
      }
    };
    this.logInService.signUpUser(newUsr).then(res => {
      const result = res.content.toJSON();
      if (result.success) {
        const params = {
          email: this.email,
          password: this.password
        };
        this.logInService.signInUser(params).then(re => {
          const response = re.content.toJSON();
          if (response.success) {
            this.authService.setactiveUser(newUsr);
            this.authService.storeAuthToken(response.token);
            this.routerExtensions.navigateByUrl("/home", {
              clearHistory: true
            });
          } else {
            alert(result.msg ? result.msg : "Oops");
          }
        });
      } else {
        switch (result.err.code) {
          case 11000: {
            alert({
              title: "ReCarding",
              message: "User already exists",
              okButtonText: "Close"
            });
            break;
          }
          default: {
            alert("Error during sign up. Please try again");
            break;
          }
        }
      }
    });
  }

  showToC() {
    this.routerExtensions.navigateByUrl("/login/toc");
  }
  cancel() {
    this.cancelSignUp.emit();
  }
}
