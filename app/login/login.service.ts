import * as httpModule from "http";

import { TnsOAuthClient, ITnsOAuthTokenResult } from "nativescript-oauth2";

export class LogInService {
  private client: TnsOAuthClient = null;

  public signUpUser(user: object): Promise<any> {
    return httpModule.request({
      url: "http://localhost:5000/user/new_user",
      method: "POST",
      headers: { "Content-Type": "application/json" },
      content: JSON.stringify(user)
    });
  }

  public signInUser(user: object): Promise<any> {
    return httpModule.request({
      url: "http://localhost:5000/user/login",
      method: "POST",
      headers: { "Content-Type": "application/json" },
      content: JSON.stringify(user)
    });
  }

  public tnsOauthLogin(providerType): Promise<ITnsOAuthTokenResult> {
    this.client = new TnsOAuthClient(providerType);

    return new Promise<ITnsOAuthTokenResult>((resolve, reject) => {
      this.client.loginWithCompletion(
        (tokenResult: ITnsOAuthTokenResult, error) => {
          if (error) {
            reject(error);
          } else {
            resolve(tokenResult);
          }
        }
      );
    });
  }

  public tnsOauthLogout() {
    if (this.client) {
      this.client.logout();
    }
  }
}
