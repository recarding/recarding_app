"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var dialogs_1 = require("tns-core-modules/ui/dialogs");
var auth_service_1 = require("../shared/helper/auth.service");
var login_service_1 = require("./login.service");
var page_1 = require("tns-core-modules/ui/page/page");
var LoginComponent = /** @class */ (function () {
    function LoginComponent(routerExtensions, logInService, authService, page) {
        this.routerExtensions = routerExtensions;
        this.logInService = logInService;
        this.authService = authService;
        this.page = page;
        this.default = true;
        this.signIn = false;
        this.signUp = false;
        page.actionBarHidden = true;
    }
    LoginComponent.prototype.submit = function () {
        this.routerExtensions.navigateByUrl("/home", { clearHistory: true });
    };
    LoginComponent.prototype.forgotPassword = function () {
        dialogs_1.prompt({
            title: "Forgot Password",
            message: "Enter the email address you used to register for APP NAME to reset your password.",
            defaultText: "",
            okButtonText: "Ok",
            cancelButtonText: "Cancel"
        }).then(function (data) {
            if (data.result) {
                // Call the backend to reset the password
                dialogs_1.alert({
                    title: "ReCarding",
                    message: "Please check your email for instructions on choosing a new password.",
                    okButtonText: "Ok"
                });
            }
        });
    };
    LoginComponent.prototype.facebookLogin = function () {
        var _this = this;
        this.logInService
            .tnsOauthLogin("facebook")
            .then(function (resultToken) {
            var token = _this.authService.getAuthToken() || "";
            if (token !== "") {
                _this.authService.removeAuthToken();
            }
            _this.authService.storeFacebookToken(resultToken);
            _this.routerExtensions.navigateByUrl("/home", { clearHistory: true });
        })
            .catch(function (er) {
            dialogs_1.alert("Authentication Error");
        });
    };
    LoginComponent.prototype.showDefault = function () {
        this.default = true;
        this.signUp = false;
        this.signIn = false;
    };
    LoginComponent.prototype.showSignUp = function () {
        this.default = !this.default;
        this.signUp = !this.signUp;
    };
    LoginComponent.prototype.showSignIn = function () {
        this.default = !this.default;
        this.signIn = !this.signIn;
    };
    LoginComponent = __decorate([
        core_1.Component({
            selector: "Home",
            moduleId: module.id,
            templateUrl: "./login.component.html",
            styleUrls: ["login-common.scss"]
        }),
        __metadata("design:paramtypes", [router_1.RouterExtensions,
            login_service_1.LogInService,
            auth_service_1.AuthService,
            page_1.Page])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibG9naW4uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTBDO0FBRTFDLHNEQUErRDtBQUUvRCx1REFBNEQ7QUFDNUQsOERBQTREO0FBQzVELGlEQUErQztBQUcvQyxzREFBcUQ7QUFRckQ7SUFLRSx3QkFDVSxnQkFBa0MsRUFDbEMsWUFBMEIsRUFDMUIsV0FBd0IsRUFDeEIsSUFBVTtRQUhWLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDbEMsaUJBQVksR0FBWixZQUFZLENBQWM7UUFDMUIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsU0FBSSxHQUFKLElBQUksQ0FBTTtRQVJwQixZQUFPLEdBQVksSUFBSSxDQUFDO1FBQ3hCLFdBQU0sR0FBWSxLQUFLLENBQUM7UUFDeEIsV0FBTSxHQUFZLEtBQUssQ0FBQztRQVF0QixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztJQUM5QixDQUFDO0lBRUQsK0JBQU0sR0FBTjtRQUNFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsT0FBTyxFQUFFLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7SUFDdkUsQ0FBQztJQUVELHVDQUFjLEdBQWQ7UUFDRSxnQkFBTSxDQUFDO1lBQ0wsS0FBSyxFQUFFLGlCQUFpQjtZQUN4QixPQUFPLEVBQ0wsbUZBQW1GO1lBQ3JGLFdBQVcsRUFBRSxFQUFFO1lBQ2YsWUFBWSxFQUFFLElBQUk7WUFDbEIsZ0JBQWdCLEVBQUUsUUFBUTtTQUMzQixDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsSUFBSTtZQUNWLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtnQkFDZix5Q0FBeUM7Z0JBQ3pDLGVBQUssQ0FBQztvQkFDSixLQUFLLEVBQUUsV0FBVztvQkFDbEIsT0FBTyxFQUFFLHNFQUFzRTtvQkFDL0UsWUFBWSxFQUFFLElBQUk7aUJBQ25CLENBQUMsQ0FBQzthQUNKO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsc0NBQWEsR0FBYjtRQUFBLGlCQWNDO1FBYkMsSUFBSSxDQUFDLFlBQVk7YUFDZCxhQUFhLENBQUMsVUFBVSxDQUFDO2FBQ3pCLElBQUksQ0FBQyxVQUFDLFdBQWlDO1lBQ3RDLElBQU0sS0FBSyxHQUFHLEtBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxFQUFFLElBQUksRUFBRSxDQUFDO1lBQ3BELElBQUksS0FBSyxLQUFLLEVBQUUsRUFBRTtnQkFDaEIsS0FBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLEVBQUUsQ0FBQzthQUNwQztZQUNELEtBQUksQ0FBQyxXQUFXLENBQUMsa0JBQWtCLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDakQsS0FBSSxDQUFDLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxPQUFPLEVBQUUsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztRQUN2RSxDQUFDLENBQUM7YUFDRCxLQUFLLENBQUMsVUFBQSxFQUFFO1lBQ1AsZUFBSyxDQUFDLHNCQUFzQixDQUFDLENBQUM7UUFDaEMsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsb0NBQVcsR0FBWDtRQUNFLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ3BCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ3BCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO0lBQ3RCLENBQUM7SUFDRCxtQ0FBVSxHQUFWO1FBQ0UsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDN0IsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDN0IsQ0FBQztJQUNELG1DQUFVLEdBQVY7UUFDRSxJQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUM3QixJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUM3QixDQUFDO0lBbEVVLGNBQWM7UUFOMUIsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxNQUFNO1lBQ2hCLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixXQUFXLEVBQUUsd0JBQXdCO1lBQ3JDLFNBQVMsRUFBRSxDQUFDLG1CQUFtQixDQUFDO1NBQ2pDLENBQUM7eUNBTzRCLHlCQUFnQjtZQUNwQiw0QkFBWTtZQUNiLDBCQUFXO1lBQ2xCLFdBQUk7T0FUVCxjQUFjLENBbUUxQjtJQUFELHFCQUFDO0NBQUEsQUFuRUQsSUFtRUM7QUFuRVksd0NBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuXG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xuXG5pbXBvcnQgeyBhbGVydCwgcHJvbXB0IH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvZGlhbG9nc1wiO1xuaW1wb3J0IHsgQXV0aFNlcnZpY2UgfSBmcm9tIFwiLi4vc2hhcmVkL2hlbHBlci9hdXRoLnNlcnZpY2VcIjtcbmltcG9ydCB7IExvZ0luU2VydmljZSB9IGZyb20gXCIuL2xvZ2luLnNlcnZpY2VcIjtcblxuaW1wb3J0IHsgSVRuc09BdXRoVG9rZW5SZXN1bHQgfSBmcm9tIFwibmF0aXZlc2NyaXB0LW9hdXRoMlwiO1xuaW1wb3J0IHsgUGFnZSB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL3BhZ2UvcGFnZVwiO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6IFwiSG9tZVwiLFxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxuICB0ZW1wbGF0ZVVybDogXCIuL2xvZ2luLmNvbXBvbmVudC5odG1sXCIsXG4gIHN0eWxlVXJsczogW1wibG9naW4tY29tbW9uLnNjc3NcIl1cbn0pXG5leHBvcnQgY2xhc3MgTG9naW5Db21wb25lbnQge1xuICBkZWZhdWx0OiBib29sZWFuID0gdHJ1ZTtcbiAgc2lnbkluOiBib29sZWFuID0gZmFsc2U7XG4gIHNpZ25VcDogYm9vbGVhbiA9IGZhbHNlO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucyxcbiAgICBwcml2YXRlIGxvZ0luU2VydmljZTogTG9nSW5TZXJ2aWNlLFxuICAgIHByaXZhdGUgYXV0aFNlcnZpY2U6IEF1dGhTZXJ2aWNlLFxuICAgIHByaXZhdGUgcGFnZTogUGFnZVxuICApIHtcbiAgICBwYWdlLmFjdGlvbkJhckhpZGRlbiA9IHRydWU7XG4gIH1cblxuICBzdWJtaXQoKSB7XG4gICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlQnlVcmwoXCIvaG9tZVwiLCB7IGNsZWFySGlzdG9yeTogdHJ1ZSB9KTtcbiAgfVxuXG4gIGZvcmdvdFBhc3N3b3JkKCkge1xuICAgIHByb21wdCh7XG4gICAgICB0aXRsZTogXCJGb3Jnb3QgUGFzc3dvcmRcIixcbiAgICAgIG1lc3NhZ2U6XG4gICAgICAgIFwiRW50ZXIgdGhlIGVtYWlsIGFkZHJlc3MgeW91IHVzZWQgdG8gcmVnaXN0ZXIgZm9yIEFQUCBOQU1FIHRvIHJlc2V0IHlvdXIgcGFzc3dvcmQuXCIsXG4gICAgICBkZWZhdWx0VGV4dDogXCJcIixcbiAgICAgIG9rQnV0dG9uVGV4dDogXCJPa1wiLFxuICAgICAgY2FuY2VsQnV0dG9uVGV4dDogXCJDYW5jZWxcIlxuICAgIH0pLnRoZW4oZGF0YSA9PiB7XG4gICAgICBpZiAoZGF0YS5yZXN1bHQpIHtcbiAgICAgICAgLy8gQ2FsbCB0aGUgYmFja2VuZCB0byByZXNldCB0aGUgcGFzc3dvcmRcbiAgICAgICAgYWxlcnQoe1xuICAgICAgICAgIHRpdGxlOiBcIlJlQ2FyZGluZ1wiLFxuICAgICAgICAgIG1lc3NhZ2U6IGBQbGVhc2UgY2hlY2sgeW91ciBlbWFpbCBmb3IgaW5zdHJ1Y3Rpb25zIG9uIGNob29zaW5nIGEgbmV3IHBhc3N3b3JkLmAsXG4gICAgICAgICAgb2tCdXR0b25UZXh0OiBcIk9rXCJcbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICBmYWNlYm9va0xvZ2luKCkge1xuICAgIHRoaXMubG9nSW5TZXJ2aWNlXG4gICAgICAudG5zT2F1dGhMb2dpbihcImZhY2Vib29rXCIpXG4gICAgICAudGhlbigocmVzdWx0VG9rZW46IElUbnNPQXV0aFRva2VuUmVzdWx0KSA9PiB7XG4gICAgICAgIGNvbnN0IHRva2VuID0gdGhpcy5hdXRoU2VydmljZS5nZXRBdXRoVG9rZW4oKSB8fCBcIlwiO1xuICAgICAgICBpZiAodG9rZW4gIT09IFwiXCIpIHtcbiAgICAgICAgICB0aGlzLmF1dGhTZXJ2aWNlLnJlbW92ZUF1dGhUb2tlbigpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuYXV0aFNlcnZpY2Uuc3RvcmVGYWNlYm9va1Rva2VuKHJlc3VsdFRva2VuKTtcbiAgICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlQnlVcmwoXCIvaG9tZVwiLCB7IGNsZWFySGlzdG9yeTogdHJ1ZSB9KTtcbiAgICAgIH0pXG4gICAgICAuY2F0Y2goZXIgPT4ge1xuICAgICAgICBhbGVydChcIkF1dGhlbnRpY2F0aW9uIEVycm9yXCIpO1xuICAgICAgfSk7XG4gIH1cblxuICBzaG93RGVmYXVsdCgpIHtcbiAgICB0aGlzLmRlZmF1bHQgPSB0cnVlO1xuICAgIHRoaXMuc2lnblVwID0gZmFsc2U7XG4gICAgdGhpcy5zaWduSW4gPSBmYWxzZTtcbiAgfVxuICBzaG93U2lnblVwKCkge1xuICAgIHRoaXMuZGVmYXVsdCA9ICF0aGlzLmRlZmF1bHQ7XG4gICAgdGhpcy5zaWduVXAgPSAhdGhpcy5zaWduVXA7XG4gIH1cbiAgc2hvd1NpZ25JbigpIHtcbiAgICB0aGlzLmRlZmF1bHQgPSAhdGhpcy5kZWZhdWx0O1xuICAgIHRoaXMuc2lnbkluID0gIXRoaXMuc2lnbkluO1xuICB9XG59XG4iXX0=