import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";

import { SharedModule } from "../shared/shared.module";
import { LoginRoutingModule } from "./login-routing.module";
import { LoginComponent } from "./login.component";

import { LogInService } from "./login.service";
import { SignInComponent } from "./signin/signin.component";
import { SignUpComponent } from "./signup/signup.component";
import { TermsConditionsComponent } from "./terms-conditions/terms-conditions.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        LoginRoutingModule,
        SharedModule
    ],
    declarations: [
        LoginComponent,
        SignInComponent,
        SignUpComponent,
        TermsConditionsComponent
    ],
    providers: [
        LogInService
     ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class LoginModule { }
