import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { LoginComponent } from "./login.component";
import { TermsConditionsComponent } from "./terms-conditions/terms-conditions.component";

const routes: Routes = [
    { path: "", component: LoginComponent },
    { path: "toc", component: TermsConditionsComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class LoginRoutingModule { }
