import { Component } from "@angular/core";

import { RouterExtensions } from "nativescript-angular/router";

import { alert, prompt } from "tns-core-modules/ui/dialogs";
import { AuthService } from "../shared/helper/auth.service";
import { LogInService } from "./login.service";

import { ITnsOAuthTokenResult } from "nativescript-oauth2";
import { Page } from "tns-core-modules/ui/page/page";

@Component({
  selector: "Home",
  moduleId: module.id,
  templateUrl: "./login.component.html",
  styleUrls: ["login-common.scss"]
})
export class LoginComponent {
  default: boolean = true;
  signIn: boolean = false;
  signUp: boolean = false;

  constructor(
    private routerExtensions: RouterExtensions,
    private logInService: LogInService,
    private authService: AuthService,
    private page: Page
  ) {
    page.actionBarHidden = true;
  }

  submit() {
    this.routerExtensions.navigateByUrl("/home", { clearHistory: true });
  }

  forgotPassword() {
    prompt({
      title: "Forgot Password",
      message:
        "Enter the email address you used to register for APP NAME to reset your password.",
      defaultText: "",
      okButtonText: "Ok",
      cancelButtonText: "Cancel"
    }).then(data => {
      if (data.result) {
        // Call the backend to reset the password
        alert({
          title: "ReCarding",
          message: `Please check your email for instructions on choosing a new password.`,
          okButtonText: "Ok"
        });
      }
    });
  }

  facebookLogin() {
    this.logInService
      .tnsOauthLogin("facebook")
      .then((resultToken: ITnsOAuthTokenResult) => {
        const token = this.authService.getAuthToken() || "";
        if (token !== "") {
          this.authService.removeAuthToken();
        }
        this.authService.storeFacebookToken(resultToken);
        this.routerExtensions.navigateByUrl("/home", { clearHistory: true });
      })
      .catch(er => {
        alert("Authentication Error");
      });
  }

  showDefault() {
    this.default = true;
    this.signUp = false;
    this.signIn = false;
  }
  showSignUp() {
    this.default = !this.default;
    this.signUp = !this.signUp;
  }
  showSignIn() {
    this.default = !this.default;
    this.signIn = !this.signIn;
  }
}
