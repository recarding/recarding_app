"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var httpModule = require("http");
var nativescript_oauth2_1 = require("nativescript-oauth2");
var LogInService = /** @class */ (function () {
    function LogInService() {
        this.client = null;
    }
    LogInService.prototype.signUpUser = function (user) {
        return httpModule.request({
            url: "http://localhost:5000/user/new_user",
            method: "POST",
            headers: { "Content-Type": "application/json" },
            content: JSON.stringify(user)
        });
    };
    LogInService.prototype.signInUser = function (user) {
        return httpModule.request({
            url: "http://localhost:5000/user/login",
            method: "POST",
            headers: { "Content-Type": "application/json" },
            content: JSON.stringify(user)
        });
    };
    LogInService.prototype.tnsOauthLogin = function (providerType) {
        var _this = this;
        this.client = new nativescript_oauth2_1.TnsOAuthClient(providerType);
        return new Promise(function (resolve, reject) {
            _this.client.loginWithCompletion(function (tokenResult, error) {
                if (error) {
                    reject(error);
                }
                else {
                    resolve(tokenResult);
                }
            });
        });
    };
    LogInService.prototype.tnsOauthLogout = function () {
        if (this.client) {
            this.client.logout();
        }
    };
    return LogInService;
}());
exports.LogInService = LogInService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImxvZ2luLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxpQ0FBbUM7QUFFbkMsMkRBQTJFO0FBRTNFO0lBQUE7UUFDVSxXQUFNLEdBQW1CLElBQUksQ0FBQztJQXlDeEMsQ0FBQztJQXZDUSxpQ0FBVSxHQUFqQixVQUFrQixJQUFZO1FBQzVCLE9BQU8sVUFBVSxDQUFDLE9BQU8sQ0FBQztZQUN4QixHQUFHLEVBQUUscUNBQXFDO1lBQzFDLE1BQU0sRUFBRSxNQUFNO1lBQ2QsT0FBTyxFQUFFLEVBQUUsY0FBYyxFQUFFLGtCQUFrQixFQUFFO1lBQy9DLE9BQU8sRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQztTQUM5QixDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU0saUNBQVUsR0FBakIsVUFBa0IsSUFBWTtRQUM1QixPQUFPLFVBQVUsQ0FBQyxPQUFPLENBQUM7WUFDeEIsR0FBRyxFQUFFLGtDQUFrQztZQUN2QyxNQUFNLEVBQUUsTUFBTTtZQUNkLE9BQU8sRUFBRSxFQUFFLGNBQWMsRUFBRSxrQkFBa0IsRUFBRTtZQUMvQyxPQUFPLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUM7U0FDOUIsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVNLG9DQUFhLEdBQXBCLFVBQXFCLFlBQVk7UUFBakMsaUJBY0M7UUFiQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksb0NBQWMsQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUUvQyxPQUFPLElBQUksT0FBTyxDQUF1QixVQUFDLE9BQU8sRUFBRSxNQUFNO1lBQ3ZELEtBQUksQ0FBQyxNQUFNLENBQUMsbUJBQW1CLENBQzdCLFVBQUMsV0FBaUMsRUFBRSxLQUFLO2dCQUN2QyxJQUFJLEtBQUssRUFBRTtvQkFDVCxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ2Y7cUJBQU07b0JBQ0wsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDO2lCQUN0QjtZQUNILENBQUMsQ0FDRixDQUFDO1FBQ0osQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU0scUNBQWMsR0FBckI7UUFDRSxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDZixJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDO1NBQ3RCO0lBQ0gsQ0FBQztJQUNILG1CQUFDO0FBQUQsQ0FBQyxBQTFDRCxJQTBDQztBQTFDWSxvQ0FBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIGh0dHBNb2R1bGUgZnJvbSBcImh0dHBcIjtcblxuaW1wb3J0IHsgVG5zT0F1dGhDbGllbnQsIElUbnNPQXV0aFRva2VuUmVzdWx0IH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1vYXV0aDJcIjtcblxuZXhwb3J0IGNsYXNzIExvZ0luU2VydmljZSB7XG4gIHByaXZhdGUgY2xpZW50OiBUbnNPQXV0aENsaWVudCA9IG51bGw7XG5cbiAgcHVibGljIHNpZ25VcFVzZXIodXNlcjogb2JqZWN0KTogUHJvbWlzZTxhbnk+IHtcbiAgICByZXR1cm4gaHR0cE1vZHVsZS5yZXF1ZXN0KHtcbiAgICAgIHVybDogXCJodHRwOi8vbG9jYWxob3N0OjUwMDAvdXNlci9uZXdfdXNlclwiLFxuICAgICAgbWV0aG9kOiBcIlBPU1RcIixcbiAgICAgIGhlYWRlcnM6IHsgXCJDb250ZW50LVR5cGVcIjogXCJhcHBsaWNhdGlvbi9qc29uXCIgfSxcbiAgICAgIGNvbnRlbnQ6IEpTT04uc3RyaW5naWZ5KHVzZXIpXG4gICAgfSk7XG4gIH1cblxuICBwdWJsaWMgc2lnbkluVXNlcih1c2VyOiBvYmplY3QpOiBQcm9taXNlPGFueT4ge1xuICAgIHJldHVybiBodHRwTW9kdWxlLnJlcXVlc3Qoe1xuICAgICAgdXJsOiBcImh0dHA6Ly9sb2NhbGhvc3Q6NTAwMC91c2VyL2xvZ2luXCIsXG4gICAgICBtZXRob2Q6IFwiUE9TVFwiLFxuICAgICAgaGVhZGVyczogeyBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL2pzb25cIiB9LFxuICAgICAgY29udGVudDogSlNPTi5zdHJpbmdpZnkodXNlcilcbiAgICB9KTtcbiAgfVxuXG4gIHB1YmxpYyB0bnNPYXV0aExvZ2luKHByb3ZpZGVyVHlwZSk6IFByb21pc2U8SVRuc09BdXRoVG9rZW5SZXN1bHQ+IHtcbiAgICB0aGlzLmNsaWVudCA9IG5ldyBUbnNPQXV0aENsaWVudChwcm92aWRlclR5cGUpO1xuXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlPElUbnNPQXV0aFRva2VuUmVzdWx0PigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICB0aGlzLmNsaWVudC5sb2dpbldpdGhDb21wbGV0aW9uKFxuICAgICAgICAodG9rZW5SZXN1bHQ6IElUbnNPQXV0aFRva2VuUmVzdWx0LCBlcnJvcikgPT4ge1xuICAgICAgICAgIGlmIChlcnJvcikge1xuICAgICAgICAgICAgcmVqZWN0KGVycm9yKTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcmVzb2x2ZSh0b2tlblJlc3VsdCk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICApO1xuICAgIH0pO1xuICB9XG5cbiAgcHVibGljIHRuc09hdXRoTG9nb3V0KCkge1xuICAgIGlmICh0aGlzLmNsaWVudCkge1xuICAgICAgdGhpcy5jbGllbnQubG9nb3V0KCk7XG4gICAgfVxuICB9XG59XG4iXX0=