import { Component, EventEmitter, Output } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";

import { AuthService } from "../../shared/helper/auth.service";
import { LogInService } from "../login.service";

@Component({
  selector: "SignIn",
  moduleId: module.id,
  templateUrl: "./signin.component.html",
  styleUrls: ["signin.common.scss"]

})

export class SignInComponent {

  @Output() cancelSignUp = new EventEmitter();

  private email: string;
  private password: string;

  constructor(private routerExtensions: RouterExtensions,
    private authService: AuthService,
    private logInService: LogInService
  ) { }

  signIn() {
    const user = {
      email: this.email,
      password: this.password
    };
    if (user.email && user.password) {
      this.logInService.signInUser(user)
        .then((res) => {
          const result = res.content.toJSON();
          if (result.success) {
            const fb_token = this.authService.getAuthToken() || "";
            if (fb_token !== "") {
              this.authService.removeFacebookToken();
            }            
            this.authService.setactiveUser(result.user);
            this.authService.storeAuthToken(result.token);
            this.routerExtensions.navigateByUrl("/home", { clearHistory: true });
          } else {
            alert(result.msg ? result.msg : "Oops");
          }
        })
        .catch((err) => {
          console.error({ err })
          alert("Something unexpected happened");
        });
    }
  }

  cancel() {
    this.cancelSignUp.emit();
  }
}
