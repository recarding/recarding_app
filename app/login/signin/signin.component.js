"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var auth_service_1 = require("../../shared/helper/auth.service");
var login_service_1 = require("../login.service");
var SignInComponent = /** @class */ (function () {
    function SignInComponent(routerExtensions, authService, logInService) {
        this.routerExtensions = routerExtensions;
        this.authService = authService;
        this.logInService = logInService;
        this.cancelSignUp = new core_1.EventEmitter();
    }
    SignInComponent.prototype.signIn = function () {
        var _this = this;
        var user = {
            email: this.email,
            password: this.password
        };
        if (user.email && user.password) {
            this.logInService.signInUser(user)
                .then(function (res) {
                var result = res.content.toJSON();
                if (result.success) {
                    var fb_token = _this.authService.getAuthToken() || "";
                    if (fb_token !== "") {
                        _this.authService.removeFacebookToken();
                    }
                    _this.authService.setactiveUser(result.user);
                    _this.authService.storeAuthToken(result.token);
                    _this.routerExtensions.navigateByUrl("/home", { clearHistory: true });
                }
                else {
                    alert(result.msg ? result.msg : "Oops");
                }
            })
                .catch(function (err) {
                console.error({ err: err });
                alert("Something unexpected happened");
            });
        }
    };
    SignInComponent.prototype.cancel = function () {
        this.cancelSignUp.emit();
    };
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], SignInComponent.prototype, "cancelSignUp", void 0);
    SignInComponent = __decorate([
        core_1.Component({
            selector: "SignIn",
            moduleId: module.id,
            templateUrl: "./signin.component.html",
            styleUrls: ["signin.common.scss"]
        }),
        __metadata("design:paramtypes", [router_1.RouterExtensions,
            auth_service_1.AuthService,
            login_service_1.LogInService])
    ], SignInComponent);
    return SignInComponent;
}());
exports.SignInComponent = SignInComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2lnbmluLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNpZ25pbi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBZ0U7QUFDaEUsc0RBQStEO0FBRS9ELGlFQUErRDtBQUMvRCxrREFBZ0Q7QUFVaEQ7SUFPRSx5QkFBb0IsZ0JBQWtDLEVBQzVDLFdBQXdCLEVBQ3hCLFlBQTBCO1FBRmhCLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDNUMsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsaUJBQVksR0FBWixZQUFZLENBQWM7UUFQMUIsaUJBQVksR0FBRyxJQUFJLG1CQUFZLEVBQUUsQ0FBQztJQVF4QyxDQUFDO0lBRUwsZ0NBQU0sR0FBTjtRQUFBLGlCQTBCQztRQXpCQyxJQUFNLElBQUksR0FBRztZQUNYLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7U0FDeEIsQ0FBQztRQUNGLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQy9CLElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQztpQkFDL0IsSUFBSSxDQUFDLFVBQUMsR0FBRztnQkFDUixJQUFNLE1BQU0sR0FBRyxHQUFHLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDO2dCQUNwQyxJQUFJLE1BQU0sQ0FBQyxPQUFPLEVBQUU7b0JBQ2xCLElBQU0sUUFBUSxHQUFHLEtBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxFQUFFLElBQUksRUFBRSxDQUFDO29CQUN2RCxJQUFJLFFBQVEsS0FBSyxFQUFFLEVBQUU7d0JBQ25CLEtBQUksQ0FBQyxXQUFXLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztxQkFDeEM7b0JBQ0QsS0FBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUM1QyxLQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQzlDLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsT0FBTyxFQUFFLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7aUJBQ3RFO3FCQUFNO29CQUNMLEtBQUssQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQztpQkFDekM7WUFDSCxDQUFDLENBQUM7aUJBQ0QsS0FBSyxDQUFDLFVBQUMsR0FBRztnQkFDVCxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUUsR0FBRyxLQUFBLEVBQUUsQ0FBQyxDQUFBO2dCQUN0QixLQUFLLENBQUMsK0JBQStCLENBQUMsQ0FBQztZQUN6QyxDQUFDLENBQUMsQ0FBQztTQUNOO0lBQ0gsQ0FBQztJQUVELGdDQUFNLEdBQU47UUFDRSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQzNCLENBQUM7SUF4Q1M7UUFBVCxhQUFNLEVBQUU7O3lEQUFtQztJQUZqQyxlQUFlO1FBUjNCLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsUUFBUTtZQUNsQixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsV0FBVyxFQUFFLHlCQUF5QjtZQUN0QyxTQUFTLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQztTQUVsQyxDQUFDO3lDQVNzQyx5QkFBZ0I7WUFDL0IsMEJBQVc7WUFDViw0QkFBWTtPQVR6QixlQUFlLENBMkMzQjtJQUFELHNCQUFDO0NBQUEsQUEzQ0QsSUEyQ0M7QUEzQ1ksMENBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgT3V0cHV0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XG5cbmltcG9ydCB7IEF1dGhTZXJ2aWNlIH0gZnJvbSBcIi4uLy4uL3NoYXJlZC9oZWxwZXIvYXV0aC5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBMb2dJblNlcnZpY2UgfSBmcm9tIFwiLi4vbG9naW4uc2VydmljZVwiO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6IFwiU2lnbkluXCIsXG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXG4gIHRlbXBsYXRlVXJsOiBcIi4vc2lnbmluLmNvbXBvbmVudC5odG1sXCIsXG4gIHN0eWxlVXJsczogW1wic2lnbmluLmNvbW1vbi5zY3NzXCJdXG5cbn0pXG5cbmV4cG9ydCBjbGFzcyBTaWduSW5Db21wb25lbnQge1xuXG4gIEBPdXRwdXQoKSBjYW5jZWxTaWduVXAgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG5cbiAgcHJpdmF0ZSBlbWFpbDogc3RyaW5nO1xuICBwcml2YXRlIHBhc3N3b3JkOiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLFxuICAgIHByaXZhdGUgYXV0aFNlcnZpY2U6IEF1dGhTZXJ2aWNlLFxuICAgIHByaXZhdGUgbG9nSW5TZXJ2aWNlOiBMb2dJblNlcnZpY2VcbiAgKSB7IH1cblxuICBzaWduSW4oKSB7XG4gICAgY29uc3QgdXNlciA9IHtcbiAgICAgIGVtYWlsOiB0aGlzLmVtYWlsLFxuICAgICAgcGFzc3dvcmQ6IHRoaXMucGFzc3dvcmRcbiAgICB9O1xuICAgIGlmICh1c2VyLmVtYWlsICYmIHVzZXIucGFzc3dvcmQpIHtcbiAgICAgIHRoaXMubG9nSW5TZXJ2aWNlLnNpZ25JblVzZXIodXNlcilcbiAgICAgICAgLnRoZW4oKHJlcykgPT4ge1xuICAgICAgICAgIGNvbnN0IHJlc3VsdCA9IHJlcy5jb250ZW50LnRvSlNPTigpO1xuICAgICAgICAgIGlmIChyZXN1bHQuc3VjY2Vzcykge1xuICAgICAgICAgICAgY29uc3QgZmJfdG9rZW4gPSB0aGlzLmF1dGhTZXJ2aWNlLmdldEF1dGhUb2tlbigpIHx8IFwiXCI7XG4gICAgICAgICAgICBpZiAoZmJfdG9rZW4gIT09IFwiXCIpIHtcbiAgICAgICAgICAgICAgdGhpcy5hdXRoU2VydmljZS5yZW1vdmVGYWNlYm9va1Rva2VuKCk7XG4gICAgICAgICAgICB9ICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLmF1dGhTZXJ2aWNlLnNldGFjdGl2ZVVzZXIocmVzdWx0LnVzZXIpO1xuICAgICAgICAgICAgdGhpcy5hdXRoU2VydmljZS5zdG9yZUF1dGhUb2tlbihyZXN1bHQudG9rZW4pO1xuICAgICAgICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlQnlVcmwoXCIvaG9tZVwiLCB7IGNsZWFySGlzdG9yeTogdHJ1ZSB9KTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgYWxlcnQocmVzdWx0Lm1zZyA/IHJlc3VsdC5tc2cgOiBcIk9vcHNcIik7XG4gICAgICAgICAgfVxuICAgICAgICB9KVxuICAgICAgICAuY2F0Y2goKGVycikgPT4ge1xuICAgICAgICAgIGNvbnNvbGUuZXJyb3IoeyBlcnIgfSlcbiAgICAgICAgICBhbGVydChcIlNvbWV0aGluZyB1bmV4cGVjdGVkIGhhcHBlbmVkXCIpO1xuICAgICAgICB9KTtcbiAgICB9XG4gIH1cblxuICBjYW5jZWwoKSB7XG4gICAgdGhpcy5jYW5jZWxTaWduVXAuZW1pdCgpO1xuICB9XG59XG4iXX0=