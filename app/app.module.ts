import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";

import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

import { StorageService } from "./shared/helper/app-settings/storage.service";
import { SyncService } from "./shared/helper/app-settings/sync.service";
import { AuthService } from "./shared/helper/auth.service";
import { OcrService } from "./shared/helper/ocr.service";
import { ChecksService } from "./shared/helper/app-settings/checks.service";

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        NativeScriptHttpModule,
        NativeScriptRouterModule
    ],
    declarations: [
        AppComponent
    ],
    providers: [
        ChecksService,
        StorageService,
        AuthService,
        SyncService,
        OcrService
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
