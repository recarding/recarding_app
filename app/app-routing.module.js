"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var routes = [
    { path: "", redirectTo: "/myprofile", pathMatch: "full" },
    { path: "card", loadChildren: "./card-info/card-info.module#CardInfoModule" },
    {
        path: "contacts",
        loadChildren: "./contacts/contacts.module#ContactsModule"
    },
    { path: "home", loadChildren: "./home/home.module#HomeModule" },
    { path: "login", loadChildren: "./login/login.module#LoginModule" },
    {
        path: "myprofile",
        loadChildren: "./myprofile/myprofile.module#MyProfileModule"
    },
    { path: "newcard", loadChildren: "./newcard/newcard.module#NewCardModule" },
    {
        path: "settings",
        loadChildren: "./settings/settings.module#SettingsModule"
    },
    {
        path: "edit",
        loadChildren: "./editcontact/editcontact.module#EditContactModule"
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.NativeScriptRouterModule.forRoot(routes)],
            exports: [router_1.NativeScriptRouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLXJvdXRpbmcubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYXBwLXJvdXRpbmcubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQXlDO0FBRXpDLHNEQUF1RTtBQUV2RSxJQUFNLE1BQU0sR0FBVztJQUNyQixFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFO0lBQ3pELEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsNkNBQTZDLEVBQUU7SUFDN0U7UUFDRSxJQUFJLEVBQUUsVUFBVTtRQUNoQixZQUFZLEVBQUUsMkNBQTJDO0tBQzFEO0lBQ0QsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSwrQkFBK0IsRUFBRTtJQUMvRCxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsWUFBWSxFQUFFLGtDQUFrQyxFQUFFO0lBQ25FO1FBQ0UsSUFBSSxFQUFFLFdBQVc7UUFDakIsWUFBWSxFQUFFLDhDQUE4QztLQUM3RDtJQUNELEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsd0NBQXdDLEVBQUU7SUFDM0U7UUFDRSxJQUFJLEVBQUUsVUFBVTtRQUNoQixZQUFZLEVBQUUsMkNBQTJDO0tBQzFEO0lBQ0Q7UUFDRSxJQUFJLEVBQUUsTUFBTTtRQUNaLFlBQVksRUFBRSxvREFBb0Q7S0FDbkU7Q0FDRixDQUFDO0FBTUY7SUFBQTtJQUErQixDQUFDO0lBQW5CLGdCQUFnQjtRQUo1QixlQUFRLENBQUM7WUFDUixPQUFPLEVBQUUsQ0FBQyxpQ0FBd0IsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDbkQsT0FBTyxFQUFFLENBQUMsaUNBQXdCLENBQUM7U0FDcEMsQ0FBQztPQUNXLGdCQUFnQixDQUFHO0lBQUQsdUJBQUM7Q0FBQSxBQUFoQyxJQUFnQztBQUFuQiw0Q0FBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IFJvdXRlcyB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHsgTmF0aXZlU2NyaXB0Um91dGVyTW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xyXG5cclxuY29uc3Qgcm91dGVzOiBSb3V0ZXMgPSBbXHJcbiAgeyBwYXRoOiBcIlwiLCByZWRpcmVjdFRvOiBcIi9teXByb2ZpbGVcIiwgcGF0aE1hdGNoOiBcImZ1bGxcIiB9LFxyXG4gIHsgcGF0aDogXCJjYXJkXCIsIGxvYWRDaGlsZHJlbjogXCIuL2NhcmQtaW5mby9jYXJkLWluZm8ubW9kdWxlI0NhcmRJbmZvTW9kdWxlXCIgfSxcclxuICB7XHJcbiAgICBwYXRoOiBcImNvbnRhY3RzXCIsXHJcbiAgICBsb2FkQ2hpbGRyZW46IFwiLi9jb250YWN0cy9jb250YWN0cy5tb2R1bGUjQ29udGFjdHNNb2R1bGVcIlxyXG4gIH0sXHJcbiAgeyBwYXRoOiBcImhvbWVcIiwgbG9hZENoaWxkcmVuOiBcIi4vaG9tZS9ob21lLm1vZHVsZSNIb21lTW9kdWxlXCIgfSxcclxuICB7IHBhdGg6IFwibG9naW5cIiwgbG9hZENoaWxkcmVuOiBcIi4vbG9naW4vbG9naW4ubW9kdWxlI0xvZ2luTW9kdWxlXCIgfSxcclxuICB7XHJcbiAgICBwYXRoOiBcIm15cHJvZmlsZVwiLFxyXG4gICAgbG9hZENoaWxkcmVuOiBcIi4vbXlwcm9maWxlL215cHJvZmlsZS5tb2R1bGUjTXlQcm9maWxlTW9kdWxlXCJcclxuICB9LFxyXG4gIHsgcGF0aDogXCJuZXdjYXJkXCIsIGxvYWRDaGlsZHJlbjogXCIuL25ld2NhcmQvbmV3Y2FyZC5tb2R1bGUjTmV3Q2FyZE1vZHVsZVwiIH0sXHJcbiAge1xyXG4gICAgcGF0aDogXCJzZXR0aW5nc1wiLFxyXG4gICAgbG9hZENoaWxkcmVuOiBcIi4vc2V0dGluZ3Mvc2V0dGluZ3MubW9kdWxlI1NldHRpbmdzTW9kdWxlXCJcclxuICB9LFxyXG4gIHtcclxuICAgIHBhdGg6IFwiZWRpdFwiLFxyXG4gICAgbG9hZENoaWxkcmVuOiBcIi4vZWRpdGNvbnRhY3QvZWRpdGNvbnRhY3QubW9kdWxlI0VkaXRDb250YWN0TW9kdWxlXCJcclxuICB9XHJcbl07XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGltcG9ydHM6IFtOYXRpdmVTY3JpcHRSb3V0ZXJNb2R1bGUuZm9yUm9vdChyb3V0ZXMpXSxcclxuICBleHBvcnRzOiBbTmF0aXZlU2NyaXB0Um91dGVyTW9kdWxlXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQXBwUm91dGluZ01vZHVsZSB7fVxyXG4iXX0=