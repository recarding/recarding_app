import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpModule } from "@angular/http";

import { NativeScriptCommonModule } from "nativescript-angular/common";

import { SharedModule } from "../shared/shared.module";
import { EditRoutingModule } from "./editcontact-routing.module";

import { EditContactComponent } from "./editcontact.component";

@NgModule({
  imports: [
    NativeScriptCommonModule,
    HttpModule,
    SharedModule,
    EditRoutingModule
  ],
  declarations: [
    EditContactComponent
  ],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class EditContactModule { }
