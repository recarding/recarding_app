import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { EditContactComponent } from "./editcontact.component";

const routes: Routes = [
    { path: ":type/:id",
      component: EditContactComponent,
      data: {
            shouldDetach: true,
            title: null
        }
   }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class EditRoutingModule { }
