"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var common_1 = require("nativescript-angular/common");
var shared_module_1 = require("../shared/shared.module");
var editcontact_routing_module_1 = require("./editcontact-routing.module");
var editcontact_component_1 = require("./editcontact.component");
var EditContactModule = /** @class */ (function () {
    function EditContactModule() {
    }
    EditContactModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.NativeScriptCommonModule,
                http_1.HttpModule,
                shared_module_1.SharedModule,
                editcontact_routing_module_1.EditRoutingModule
            ],
            declarations: [
                editcontact_component_1.EditContactComponent
            ],
            schemas: [
                core_1.NO_ERRORS_SCHEMA
            ]
        })
    ], EditContactModule);
    return EditContactModule;
}());
exports.EditContactModule = EditContactModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWRpdGNvbnRhY3QubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZWRpdGNvbnRhY3QubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTJEO0FBQzNELHNDQUEyQztBQUUzQyxzREFBdUU7QUFFdkUseURBQXVEO0FBQ3ZELDJFQUFpRTtBQUVqRSxpRUFBK0Q7QUFnQi9EO0lBQUE7SUFBaUMsQ0FBQztJQUFyQixpQkFBaUI7UUFkN0IsZUFBUSxDQUFDO1lBQ1IsT0FBTyxFQUFFO2dCQUNQLGlDQUF3QjtnQkFDeEIsaUJBQVU7Z0JBQ1YsNEJBQVk7Z0JBQ1osOENBQWlCO2FBQ2xCO1lBQ0QsWUFBWSxFQUFFO2dCQUNaLDRDQUFvQjthQUNyQjtZQUNELE9BQU8sRUFBRTtnQkFDUCx1QkFBZ0I7YUFDakI7U0FDRixDQUFDO09BQ1csaUJBQWlCLENBQUk7SUFBRCx3QkFBQztDQUFBLEFBQWxDLElBQWtDO0FBQXJCLDhDQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlLCBOT19FUlJPUlNfU0NIRU1BIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IEh0dHBNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvaHR0cFwiO1xuXG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRDb21tb25Nb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvY29tbW9uXCI7XG5cbmltcG9ydCB7IFNoYXJlZE1vZHVsZSB9IGZyb20gXCIuLi9zaGFyZWQvc2hhcmVkLm1vZHVsZVwiO1xuaW1wb3J0IHsgRWRpdFJvdXRpbmdNb2R1bGUgfSBmcm9tIFwiLi9lZGl0Y29udGFjdC1yb3V0aW5nLm1vZHVsZVwiO1xuXG5pbXBvcnQgeyBFZGl0Q29udGFjdENvbXBvbmVudCB9IGZyb20gXCIuL2VkaXRjb250YWN0LmNvbXBvbmVudFwiO1xuXG5ATmdNb2R1bGUoe1xuICBpbXBvcnRzOiBbXG4gICAgTmF0aXZlU2NyaXB0Q29tbW9uTW9kdWxlLFxuICAgIEh0dHBNb2R1bGUsXG4gICAgU2hhcmVkTW9kdWxlLFxuICAgIEVkaXRSb3V0aW5nTW9kdWxlXG4gIF0sXG4gIGRlY2xhcmF0aW9uczogW1xuICAgIEVkaXRDb250YWN0Q29tcG9uZW50XG4gIF0sXG4gIHNjaGVtYXM6IFtcbiAgICBOT19FUlJPUlNfU0NIRU1BXG4gIF1cbn0pXG5leHBvcnQgY2xhc3MgRWRpdENvbnRhY3RNb2R1bGUgeyB9XG4iXX0=