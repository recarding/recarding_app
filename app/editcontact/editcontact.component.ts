import { Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";

import { DrawerTransitionBase } from "nativescript-ui-sidedrawer";
import { RadSideDrawerComponent } from "nativescript-ui-sidedrawer/angular";

import { Subscription } from "rxjs";
import "rxjs/add/observable/fromPromise";

import { IContact } from "../shared/helper/app-settings/storage.model";
import { StorageService } from "../shared/helper/app-settings/storage.service";

@Component({
  selector: "EditContact",
  moduleId: module.id,
  templateUrl: "./editcontact.component.html"
})

export class EditContactComponent implements OnInit, OnDestroy {

  @ViewChild("drawer") drawerComponent: RadSideDrawerComponent;
  id: string;
  type: string;
  _sideDrawerTransition: DrawerTransitionBase;
  sub: Subscription;
  contact: IContact = {
    id: '',
    user_id: '',
    group: '',
    name: {
      displayname: ''
    },
    organization: {
      name: ''
    },
    cardImage: '',
    phoneNumbers: [''],
    emailAddress: '',
    physicalAddress: '',
    urls: [''],
  };

  constructor(
    private route: ActivatedRoute,
    private storageService: StorageService,
    private router: RouterExtensions
  ) { }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe((params) => {
      this.type = params.type;
      this.id = params.id;
    });
    this.contact = this.storageService.getById(this.id, this.type);
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  updateCard() {
    const updated = this.storageService.update(this.contact, this.type);
    if (updated) {
      //alert success
      this.router.navigateByUrl("home", { clearHistory: true });
    } else {
      //alert error
      this.router.navigateByUrl("home", { clearHistory: true });
    }
  }

  cancel() {
    this.router.navigateByUrl("home", { clearHistory: true });
  }

  get sideDrawerTransition(): DrawerTransitionBase {
    return this._sideDrawerTransition;
  }

  onDrawerButtonTap(): void {
    this.drawerComponent.sideDrawer.showDrawer();
  }

}
