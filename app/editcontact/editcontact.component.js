"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var router_2 = require("nativescript-angular/router");
var angular_1 = require("nativescript-ui-sidedrawer/angular");
require("rxjs/add/observable/fromPromise");
var storage_service_1 = require("../shared/helper/app-settings/storage.service");
var EditContactComponent = /** @class */ (function () {
    function EditContactComponent(route, storageService, router) {
        this.route = route;
        this.storageService = storageService;
        this.router = router;
        this.contact = {
            id: '',
            user_id: '',
            group: '',
            name: {
                displayname: ''
            },
            organization: {
                name: ''
            },
            cardImage: '',
            phoneNumbers: [''],
            emailAddress: '',
            physicalAddress: '',
            urls: [''],
        };
    }
    EditContactComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            _this.type = params.type;
            _this.id = params.id;
        });
        this.contact = this.storageService.getById(this.id, this.type);
    };
    EditContactComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    EditContactComponent.prototype.updateCard = function () {
        var updated = this.storageService.update(this.contact, this.type);
        if (updated) {
            //alert success
            this.router.navigateByUrl("home", { clearHistory: true });
        }
        else {
            //alert error
            this.router.navigateByUrl("home", { clearHistory: true });
        }
    };
    EditContactComponent.prototype.cancel = function () {
        this.router.navigateByUrl("home", { clearHistory: true });
    };
    Object.defineProperty(EditContactComponent.prototype, "sideDrawerTransition", {
        get: function () {
            return this._sideDrawerTransition;
        },
        enumerable: true,
        configurable: true
    });
    EditContactComponent.prototype.onDrawerButtonTap = function () {
        this.drawerComponent.sideDrawer.showDrawer();
    };
    __decorate([
        core_1.ViewChild("drawer"),
        __metadata("design:type", angular_1.RadSideDrawerComponent)
    ], EditContactComponent.prototype, "drawerComponent", void 0);
    EditContactComponent = __decorate([
        core_1.Component({
            selector: "EditContact",
            moduleId: module.id,
            templateUrl: "./editcontact.component.html"
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute,
            storage_service_1.StorageService,
            router_2.RouterExtensions])
    ], EditContactComponent);
    return EditContactComponent;
}());
exports.EditContactComponent = EditContactComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWRpdGNvbnRhY3QuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZWRpdGNvbnRhY3QuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQXdFO0FBQ3hFLDBDQUFpRDtBQUNqRCxzREFBK0Q7QUFHL0QsOERBQTRFO0FBRzVFLDJDQUF5QztBQUd6QyxpRkFBK0U7QUFRL0U7SUF3QkUsOEJBQ1UsS0FBcUIsRUFDckIsY0FBOEIsRUFDOUIsTUFBd0I7UUFGeEIsVUFBSyxHQUFMLEtBQUssQ0FBZ0I7UUFDckIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLFdBQU0sR0FBTixNQUFNLENBQWtCO1FBcEJsQyxZQUFPLEdBQWE7WUFDbEIsRUFBRSxFQUFFLEVBQUU7WUFDTixPQUFPLEVBQUUsRUFBRTtZQUNYLEtBQUssRUFBRSxFQUFFO1lBQ1QsSUFBSSxFQUFFO2dCQUNKLFdBQVcsRUFBRSxFQUFFO2FBQ2hCO1lBQ0QsWUFBWSxFQUFFO2dCQUNaLElBQUksRUFBRSxFQUFFO2FBQ1Q7WUFDRCxTQUFTLEVBQUUsRUFBRTtZQUNiLFlBQVksRUFBRSxDQUFDLEVBQUUsQ0FBQztZQUNsQixZQUFZLEVBQUUsRUFBRTtZQUNoQixlQUFlLEVBQUUsRUFBRTtZQUNuQixJQUFJLEVBQUUsQ0FBQyxFQUFFLENBQUM7U0FDWCxDQUFDO0lBTUUsQ0FBQztJQUVMLHVDQUFRLEdBQVI7UUFBQSxpQkFNQztRQUxDLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLFVBQUMsTUFBTTtZQUM1QyxLQUFJLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUM7WUFDeEIsS0FBSSxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUMsRUFBRSxDQUFDO1FBQ3RCLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNqRSxDQUFDO0lBQ0QsMENBQVcsR0FBWDtRQUNFLElBQUksQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDekIsQ0FBQztJQUVELHlDQUFVLEdBQVY7UUFDRSxJQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNwRSxJQUFJLE9BQU8sRUFBRTtZQUNYLGVBQWU7WUFDZixJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEVBQUUsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztTQUMzRDthQUFNO1lBQ0wsYUFBYTtZQUNiLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLE1BQU0sRUFBRSxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1NBQzNEO0lBQ0gsQ0FBQztJQUVELHFDQUFNLEdBQU47UUFDRSxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEVBQUUsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztJQUM1RCxDQUFDO0lBRUQsc0JBQUksc0RBQW9CO2FBQXhCO1lBQ0UsT0FBTyxJQUFJLENBQUMscUJBQXFCLENBQUM7UUFDcEMsQ0FBQzs7O09BQUE7SUFFRCxnREFBaUIsR0FBakI7UUFDRSxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxVQUFVLEVBQUUsQ0FBQztJQUMvQyxDQUFDO0lBNURvQjtRQUFwQixnQkFBUyxDQUFDLFFBQVEsQ0FBQztrQ0FBa0IsZ0NBQXNCO2lFQUFDO0lBRmxELG9CQUFvQjtRQU5oQyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLGFBQWE7WUFDdkIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFdBQVcsRUFBRSw4QkFBOEI7U0FDNUMsQ0FBQzt5Q0EyQmlCLHVCQUFjO1lBQ0wsZ0NBQWM7WUFDdEIseUJBQWdCO09BM0J2QixvQkFBb0IsQ0FnRWhDO0lBQUQsMkJBQUM7Q0FBQSxBQWhFRCxJQWdFQztBQWhFWSxvREFBb0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uRGVzdHJveSwgT25Jbml0LCBWaWV3Q2hpbGQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xuXG5pbXBvcnQgeyBEcmF3ZXJUcmFuc2l0aW9uQmFzZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtdWktc2lkZWRyYXdlclwiO1xuaW1wb3J0IHsgUmFkU2lkZURyYXdlckNvbXBvbmVudCB9IGZyb20gXCJuYXRpdmVzY3JpcHQtdWktc2lkZWRyYXdlci9hbmd1bGFyXCI7XG5cbmltcG9ydCB7IFN1YnNjcmlwdGlvbiB9IGZyb20gXCJyeGpzXCI7XG5pbXBvcnQgXCJyeGpzL2FkZC9vYnNlcnZhYmxlL2Zyb21Qcm9taXNlXCI7XG5cbmltcG9ydCB7IElDb250YWN0IH0gZnJvbSBcIi4uL3NoYXJlZC9oZWxwZXIvYXBwLXNldHRpbmdzL3N0b3JhZ2UubW9kZWxcIjtcbmltcG9ydCB7IFN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSBcIi4uL3NoYXJlZC9oZWxwZXIvYXBwLXNldHRpbmdzL3N0b3JhZ2Uuc2VydmljZVwiO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6IFwiRWRpdENvbnRhY3RcIixcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbiAgdGVtcGxhdGVVcmw6IFwiLi9lZGl0Y29udGFjdC5jb21wb25lbnQuaHRtbFwiXG59KVxuXG5leHBvcnQgY2xhc3MgRWRpdENvbnRhY3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG5cbiAgQFZpZXdDaGlsZChcImRyYXdlclwiKSBkcmF3ZXJDb21wb25lbnQ6IFJhZFNpZGVEcmF3ZXJDb21wb25lbnQ7XG4gIGlkOiBzdHJpbmc7XG4gIHR5cGU6IHN0cmluZztcbiAgX3NpZGVEcmF3ZXJUcmFuc2l0aW9uOiBEcmF3ZXJUcmFuc2l0aW9uQmFzZTtcbiAgc3ViOiBTdWJzY3JpcHRpb247XG4gIGNvbnRhY3Q6IElDb250YWN0ID0ge1xuICAgIGlkOiAnJyxcbiAgICB1c2VyX2lkOiAnJyxcbiAgICBncm91cDogJycsXG4gICAgbmFtZToge1xuICAgICAgZGlzcGxheW5hbWU6ICcnXG4gICAgfSxcbiAgICBvcmdhbml6YXRpb246IHtcbiAgICAgIG5hbWU6ICcnXG4gICAgfSxcbiAgICBjYXJkSW1hZ2U6ICcnLFxuICAgIHBob25lTnVtYmVyczogWycnXSxcbiAgICBlbWFpbEFkZHJlc3M6ICcnLFxuICAgIHBoeXNpY2FsQWRkcmVzczogJycsXG4gICAgdXJsczogWycnXSxcbiAgfTtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcbiAgICBwcml2YXRlIHN0b3JhZ2VTZXJ2aWNlOiBTdG9yYWdlU2VydmljZSxcbiAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyRXh0ZW5zaW9uc1xuICApIHsgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMuc3ViID0gdGhpcy5yb3V0ZS5wYXJhbXMuc3Vic2NyaWJlKChwYXJhbXMpID0+IHtcbiAgICAgIHRoaXMudHlwZSA9IHBhcmFtcy50eXBlO1xuICAgICAgdGhpcy5pZCA9IHBhcmFtcy5pZDtcbiAgICB9KTtcbiAgICB0aGlzLmNvbnRhY3QgPSB0aGlzLnN0b3JhZ2VTZXJ2aWNlLmdldEJ5SWQodGhpcy5pZCwgdGhpcy50eXBlKTtcbiAgfVxuICBuZ09uRGVzdHJveSgpIHtcbiAgICB0aGlzLnN1Yi51bnN1YnNjcmliZSgpO1xuICB9XG5cbiAgdXBkYXRlQ2FyZCgpIHtcbiAgICBjb25zdCB1cGRhdGVkID0gdGhpcy5zdG9yYWdlU2VydmljZS51cGRhdGUodGhpcy5jb250YWN0LCB0aGlzLnR5cGUpO1xuICAgIGlmICh1cGRhdGVkKSB7XG4gICAgICAvL2FsZXJ0IHN1Y2Nlc3NcbiAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlQnlVcmwoXCJob21lXCIsIHsgY2xlYXJIaXN0b3J5OiB0cnVlIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICAvL2FsZXJ0IGVycm9yXG4gICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZUJ5VXJsKFwiaG9tZVwiLCB7IGNsZWFySGlzdG9yeTogdHJ1ZSB9KTtcbiAgICB9XG4gIH1cblxuICBjYW5jZWwoKSB7XG4gICAgdGhpcy5yb3V0ZXIubmF2aWdhdGVCeVVybChcImhvbWVcIiwgeyBjbGVhckhpc3Rvcnk6IHRydWUgfSk7XG4gIH1cblxuICBnZXQgc2lkZURyYXdlclRyYW5zaXRpb24oKTogRHJhd2VyVHJhbnNpdGlvbkJhc2Uge1xuICAgIHJldHVybiB0aGlzLl9zaWRlRHJhd2VyVHJhbnNpdGlvbjtcbiAgfVxuXG4gIG9uRHJhd2VyQnV0dG9uVGFwKCk6IHZvaWQge1xuICAgIHRoaXMuZHJhd2VyQ29tcG9uZW50LnNpZGVEcmF3ZXIuc2hvd0RyYXdlcigpO1xuICB9XG5cbn1cbiJdfQ==