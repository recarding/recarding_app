import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

const routes: Routes = [
  { path: "", redirectTo: "/myprofile", pathMatch: "full" },
  { path: "card", loadChildren: "./card-info/card-info.module#CardInfoModule" },
  {
    path: "contacts",
    loadChildren: "./contacts/contacts.module#ContactsModule"
  },
  { path: "home", loadChildren: "./home/home.module#HomeModule" },
  { path: "login", loadChildren: "./login/login.module#LoginModule" },
  {
    path: "myprofile",
    loadChildren: "./myprofile/myprofile.module#MyProfileModule"
  },
  { path: "newcard", loadChildren: "./newcard/newcard.module#NewCardModule" },
  {
    path: "settings",
    loadChildren: "./settings/settings.module#SettingsModule"
  },
  {
    path: "edit",
    loadChildren: "./editcontact/editcontact.module#EditContactModule"
  }
];

@NgModule({
  imports: [NativeScriptRouterModule.forRoot(routes)],
  exports: [NativeScriptRouterModule]
})
export class AppRoutingModule {}
