import { configureTnsOAuth } from "nativescript-oauth2";
import {
  TnsOaProvider,
  TnsOaProviderOptionsFacebook,
  TnsOaProviderFacebook
} from "nativescript-oauth2/providers";

export function configureOAuthProviders() {
  const facebookProvider = configureOAuthProviderFacebook();

  configureTnsOAuth([facebookProvider]);
}

export function configureOAuthProviderFacebook(): TnsOaProvider {
  const facebookProviderOptions: TnsOaProviderOptionsFacebook = {
    openIdSupport: "oid-none",
    clientId: "2206863099342237",
    clientSecret: "713da952f231c336b68c7ca008b0943a",
    redirectUri: "https://www.facebook.com/connect/login_success.html",
    scopes: ["email"]
  };
  const facebookProvider = new TnsOaProviderFacebook(facebookProviderOptions);
  return facebookProvider;
}
