"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var application = require("application");
var checks_service_1 = require("./shared/helper/app-settings/checks.service");
var storage_service_1 = require("./shared/helper/app-settings/storage.service");
var sync_service_1 = require("./shared/helper/app-settings/sync.service");
var auth_service_1 = require("./shared/helper/auth.service");
var router_1 = require("nativescript-angular/router");
var AppComponent = /** @class */ (function () {
    function AppComponent(storageService, routerExtensions, checksService, authService, syncService) {
        this.storageService = storageService;
        this.routerExtensions = routerExtensions;
        this.checksService = checksService;
        this.authService = authService;
        this.syncService = syncService;
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        var token = this.authService.getAuthToken() || "";
        var fb_token = this.authService.getFacebookToken() || "";
        var isValid = this.authService.navigateIfTokenExpired(token);
        if (isValid) {
            this.routerExtensions.navigateByUrl("/home", { clearHistory: true });
        }
        if (fb_token !== "") {
            this.checksService.checkFacebookToken(fb_token)
                .then(function (res) { return JSON.parse(res); })
                .then(function (res) {
                if (res.email) {
                    _this.routerExtensions.navigateByUrl("/home", { clearHistory: true });
                }
            });
        }
        application.on(application.suspendEvent, function (args) {
            if (args.ios) {
                _this.connectivityStatus()
                    .then(function (connected) {
                    if (connected) {
                        var unSynced = _this.storageService.getUnsyncedCards();
                        var a = JSON.parse(unSynced);
                        if (a.cards.length) {
                            var task = _this.syncService.syncNewCards(a);
                            task.on("complete", function (event) {
                                _this.storageService.clearUnSynced();
                                _this.checksService.setFlagToShowSuccessToast();
                            });
                            task.on("error", function (event) {
                                console.error(event);
                            });
                        }
                    }
                });
            }
        });
        application.on(application.resumeEvent, function (args) {
            if (args.ios) {
                var showSuccess = _this.checksService.getFlagToShowSuccessToast();
                if (showSuccess) {
                    _this.checksService.unsetFlagToShowSuccessToast();
                }
            }
        });
    };
    AppComponent.prototype.connectivityStatus = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.checksService.checkConnectivityStatus()
                .then(function (res) {
                resolve(res);
            })
                .catch(function (e) {
                reject();
            });
        });
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: "ns-app",
            template: "\n  <page-router-outlet></page-router-outlet>\n",
        }),
        __metadata("design:paramtypes", [storage_service_1.StorageService,
            router_1.RouterExtensions,
            checks_service_1.ChecksService,
            auth_service_1.AuthService,
            sync_service_1.SyncService])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwcC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBa0Q7QUFFbEQseUNBQTJDO0FBRTNDLDhFQUE0RTtBQUM1RSxnRkFBOEU7QUFDOUUsMEVBQXdFO0FBQ3hFLDZEQUEyRDtBQUUzRCxzREFBK0Q7QUFTL0Q7SUFFRSxzQkFDVSxjQUE4QixFQUM5QixnQkFBa0MsRUFDbEMsYUFBNEIsRUFDNUIsV0FBd0IsRUFDeEIsV0FBd0I7UUFKeEIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDbEMsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDNUIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7SUFDOUIsQ0FBQztJQUVMLCtCQUFRLEdBQVI7UUFBQSxpQkE4Q0M7UUE3Q0MsSUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLEVBQUUsSUFBSSxFQUFFLENBQUM7UUFDcEQsSUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLEVBQUUsQ0FBQztRQUUzRCxJQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLHNCQUFzQixDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9ELElBQUksT0FBTyxFQUFFO1lBQ1gsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxPQUFPLEVBQUUsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztTQUN0RTtRQUNELElBQUksUUFBUSxLQUFLLEVBQUUsRUFBRTtZQUNuQixJQUFJLENBQUMsYUFBYSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQztpQkFDNUMsSUFBSSxDQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBZixDQUFlLENBQUM7aUJBQzlCLElBQUksQ0FBQyxVQUFDLEdBQUc7Z0JBQ1IsSUFBSSxHQUFHLENBQUMsS0FBSyxFQUFFO29CQUNiLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsT0FBTyxFQUFFLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7aUJBQ3RFO1lBQ0gsQ0FBQyxDQUFDLENBQUM7U0FDTjtRQUNELFdBQVcsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDLFlBQVksRUFBRSxVQUFDLElBQUk7WUFDNUMsSUFBSSxJQUFJLENBQUMsR0FBRyxFQUFFO2dCQUNaLEtBQUksQ0FBQyxrQkFBa0IsRUFBRTtxQkFDdEIsSUFBSSxDQUFDLFVBQUMsU0FBUztvQkFDZCxJQUFJLFNBQVMsRUFBRTt3QkFDYixJQUFNLFFBQVEsR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixFQUFFLENBQUM7d0JBQ3hELElBQU0sQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUE7d0JBQzlCLElBQUksQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUU7NEJBQ2xCLElBQU0sSUFBSSxHQUFHLEtBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFBOzRCQUM3QyxJQUFJLENBQUMsRUFBRSxDQUFDLFVBQVUsRUFBRSxVQUFDLEtBQUs7Z0NBQ3hCLEtBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxFQUFFLENBQUM7Z0NBQ3BDLEtBQUksQ0FBQyxhQUFhLENBQUMseUJBQXlCLEVBQUUsQ0FBQzs0QkFDakQsQ0FBQyxDQUFDLENBQUM7NEJBQ0gsSUFBSSxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsVUFBQSxLQUFLO2dDQUNwQixPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDOzRCQUN2QixDQUFDLENBQUMsQ0FBQzt5QkFDSjtxQkFDRjtnQkFDSCxDQUFDLENBQUMsQ0FBQzthQUNOO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDSCxXQUFXLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsVUFBQyxJQUFJO1lBQzNDLElBQUksSUFBSSxDQUFDLEdBQUcsRUFBRTtnQkFDWixJQUFNLFdBQVcsR0FBRyxLQUFJLENBQUMsYUFBYSxDQUFDLHlCQUF5QixFQUFFLENBQUM7Z0JBQ25FLElBQUksV0FBVyxFQUFFO29CQUNmLEtBQUksQ0FBQyxhQUFhLENBQUMsMkJBQTJCLEVBQUUsQ0FBQztpQkFDbEQ7YUFDRjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELHlDQUFrQixHQUFsQjtRQUFBLGlCQVVDO1FBVEMsT0FBTyxJQUFJLE9BQU8sQ0FBQyxVQUFDLE9BQU8sRUFBRSxNQUFNO1lBQ2pDLEtBQUksQ0FBQyxhQUFhLENBQUMsdUJBQXVCLEVBQUU7aUJBQ3pDLElBQUksQ0FBQyxVQUFDLEdBQUc7Z0JBQ1IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ2YsQ0FBQyxDQUFDO2lCQUNELEtBQUssQ0FBQyxVQUFDLENBQUM7Z0JBQ1AsTUFBTSxFQUFFLENBQUM7WUFDWCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQXBFVSxZQUFZO1FBUHhCLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsUUFBUTtZQUNsQixRQUFRLEVBQUUsaURBRVg7U0FDQSxDQUFDO3lDQUswQixnQ0FBYztZQUNaLHlCQUFnQjtZQUNuQiw4QkFBYTtZQUNmLDBCQUFXO1lBQ1gsMEJBQVc7T0FQdkIsWUFBWSxDQXFFeEI7SUFBRCxtQkFBQztDQUFBLEFBckVELElBcUVDO0FBckVZLG9DQUFZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5cclxuaW1wb3J0ICogYXMgYXBwbGljYXRpb24gZnJvbSBcImFwcGxpY2F0aW9uXCI7XHJcblxyXG5pbXBvcnQgeyBDaGVja3NTZXJ2aWNlIH0gZnJvbSBcIi4vc2hhcmVkL2hlbHBlci9hcHAtc2V0dGluZ3MvY2hlY2tzLnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgU3RvcmFnZVNlcnZpY2UgfSBmcm9tIFwiLi9zaGFyZWQvaGVscGVyL2FwcC1zZXR0aW5ncy9zdG9yYWdlLnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgU3luY1NlcnZpY2UgfSBmcm9tIFwiLi9zaGFyZWQvaGVscGVyL2FwcC1zZXR0aW5ncy9zeW5jLnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgQXV0aFNlcnZpY2UgfSBmcm9tIFwiLi9zaGFyZWQvaGVscGVyL2F1dGguc2VydmljZVwiO1xyXG5cclxuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiBcIm5zLWFwcFwiLFxyXG4gIHRlbXBsYXRlOiBgXHJcbiAgPHBhZ2Utcm91dGVyLW91dGxldD48L3BhZ2Utcm91dGVyLW91dGxldD5cclxuYCxcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBBcHBDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgc3RvcmFnZVNlcnZpY2U6IFN0b3JhZ2VTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLFxyXG4gICAgcHJpdmF0ZSBjaGVja3NTZXJ2aWNlOiBDaGVja3NTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBhdXRoU2VydmljZTogQXV0aFNlcnZpY2UsXHJcbiAgICBwcml2YXRlIHN5bmNTZXJ2aWNlOiBTeW5jU2VydmljZVxyXG4gICkgeyB9XHJcblxyXG4gIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgY29uc3QgdG9rZW4gPSB0aGlzLmF1dGhTZXJ2aWNlLmdldEF1dGhUb2tlbigpIHx8IFwiXCI7XHJcbiAgICBjb25zdCBmYl90b2tlbiA9IHRoaXMuYXV0aFNlcnZpY2UuZ2V0RmFjZWJvb2tUb2tlbigpIHx8IFwiXCI7XHJcblxyXG4gICAgY29uc3QgaXNWYWxpZCA9IHRoaXMuYXV0aFNlcnZpY2UubmF2aWdhdGVJZlRva2VuRXhwaXJlZCh0b2tlbik7XHJcbiAgICBpZiAoaXNWYWxpZCkge1xyXG4gICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGVCeVVybChcIi9ob21lXCIsIHsgY2xlYXJIaXN0b3J5OiB0cnVlIH0pO1xyXG4gICAgfVxyXG4gICAgaWYgKGZiX3Rva2VuICE9PSBcIlwiKSB7XHJcbiAgICAgIHRoaXMuY2hlY2tzU2VydmljZS5jaGVja0ZhY2Vib29rVG9rZW4oZmJfdG9rZW4pXHJcbiAgICAgICAgLnRoZW4oKHJlcykgPT4gSlNPTi5wYXJzZShyZXMpKVxyXG4gICAgICAgIC50aGVuKChyZXMpID0+IHtcclxuICAgICAgICAgIGlmIChyZXMuZW1haWwpIHtcclxuICAgICAgICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlQnlVcmwoXCIvaG9tZVwiLCB7IGNsZWFySGlzdG9yeTogdHJ1ZSB9KTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuICAgIGFwcGxpY2F0aW9uLm9uKGFwcGxpY2F0aW9uLnN1c3BlbmRFdmVudCwgKGFyZ3MpID0+IHtcclxuICAgICAgaWYgKGFyZ3MuaW9zKSB7XHJcbiAgICAgICAgdGhpcy5jb25uZWN0aXZpdHlTdGF0dXMoKVxyXG4gICAgICAgICAgLnRoZW4oKGNvbm5lY3RlZCkgPT4ge1xyXG4gICAgICAgICAgICBpZiAoY29ubmVjdGVkKSB7XHJcbiAgICAgICAgICAgICAgY29uc3QgdW5TeW5jZWQgPSB0aGlzLnN0b3JhZ2VTZXJ2aWNlLmdldFVuc3luY2VkQ2FyZHMoKTtcclxuICAgICAgICAgICAgICBjb25zdCBhID0gSlNPTi5wYXJzZSh1blN5bmNlZClcclxuICAgICAgICAgICAgICBpZiAoYS5jYXJkcy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHRhc2sgPSB0aGlzLnN5bmNTZXJ2aWNlLnN5bmNOZXdDYXJkcyhhKVxyXG4gICAgICAgICAgICAgICAgdGFzay5vbihcImNvbXBsZXRlXCIsIChldmVudCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICB0aGlzLnN0b3JhZ2VTZXJ2aWNlLmNsZWFyVW5TeW5jZWQoKTtcclxuICAgICAgICAgICAgICAgICAgdGhpcy5jaGVja3NTZXJ2aWNlLnNldEZsYWdUb1Nob3dTdWNjZXNzVG9hc3QoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgdGFzay5vbihcImVycm9yXCIsIGV2ZW50ID0+IHtcclxuICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihldmVudCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0pO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICAgIGFwcGxpY2F0aW9uLm9uKGFwcGxpY2F0aW9uLnJlc3VtZUV2ZW50LCAoYXJncykgPT4ge1xyXG4gICAgICBpZiAoYXJncy5pb3MpIHtcclxuICAgICAgICBjb25zdCBzaG93U3VjY2VzcyA9IHRoaXMuY2hlY2tzU2VydmljZS5nZXRGbGFnVG9TaG93U3VjY2Vzc1RvYXN0KCk7XHJcbiAgICAgICAgaWYgKHNob3dTdWNjZXNzKSB7XHJcbiAgICAgICAgICB0aGlzLmNoZWNrc1NlcnZpY2UudW5zZXRGbGFnVG9TaG93U3VjY2Vzc1RvYXN0KCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGNvbm5lY3Rpdml0eVN0YXR1cygpOiBQcm9taXNlPGFueT4ge1xyXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuICAgICAgdGhpcy5jaGVja3NTZXJ2aWNlLmNoZWNrQ29ubmVjdGl2aXR5U3RhdHVzKClcclxuICAgICAgICAudGhlbigocmVzKSA9PiB7XHJcbiAgICAgICAgICByZXNvbHZlKHJlcyk7XHJcbiAgICAgICAgfSlcclxuICAgICAgICAuY2F0Y2goKGUpID0+IHtcclxuICAgICAgICAgIHJlamVjdCgpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSk7XHJcbiAgfVxyXG59XHJcbiJdfQ==