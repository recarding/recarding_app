import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpModule } from "@angular/http";

import { NativeScriptCommonModule } from "nativescript-angular/common";

import { SharedModule } from "../shared/shared.module";
import { ContactsRoutingModule } from "./contacts-routing.module";

import { ContactsComponent } from "./contacts.component";

@NgModule({
  imports: [
    NativeScriptCommonModule,
    ContactsRoutingModule,
    HttpModule,
    SharedModule
  ],
  declarations: [
    ContactsComponent,
  ],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class ContactsModule { }
