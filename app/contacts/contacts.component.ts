import { Component, OnInit, ViewChild } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { SearchBar } from "ui/search-bar";

import { DrawerTransitionBase } from "nativescript-ui-sidedrawer";
import { RadSideDrawerComponent } from "nativescript-ui-sidedrawer/angular";

import { Subscription } from "rxjs";
import "rxjs/add/observable/fromPromise";

import { IContact } from "../shared/helper/app-settings/storage.model";
import { StorageService } from "../shared/helper/app-settings/storage.service";
import { GroupService } from "../shared/helper/app-settings/groups.service";
import * as dialogs from "ui/dialogs";

@Component({
  selector: "Contacts",
  moduleId: module.id,
  templateUrl: "./contacts.component.html",
  styleUrls: ["./contacts.component.scss"]
})
export class ContactsComponent implements OnInit {
  @ViewChild("drawer") drawerComponent: RadSideDrawerComponent;
  @ViewChild("searchField") searchField: SearchBar;
  contactList: Array<IContact>;
  contacts: Array<IContact>;
  showSearch: boolean = false;
  showGroupsDropdown: boolean = true;
  filterInProgress: boolean = false;
  numberOfGroups: number;
  groupObj: object;
  type = "contact";
  private _sideDrawerTransition: DrawerTransitionBase;
  private sub: Subscription;

  constructor(
    private routerExtensions: RouterExtensions,
    private storageService: StorageService,
    private groupService: GroupService
  ) {}

  ngOnInit(): void {
    this.contactList = this.storageService.getContacts("contact");
    this.contacts = this.contactList;
    this.groupObj = this.groupService.getContactGroups();
    this.numberOfGroups = Object.keys(this.groupObj["contact_groups"]).length;
  }

  get sideDrawerTransition(): DrawerTransitionBase {
    return this._sideDrawerTransition;
  }

  back() {
    this.routerExtensions.navigateByUrl(`/home`, { clearHistory: true });
  }

  onDrawerButtonTap(): void {
    this.drawerComponent.sideDrawer.showDrawer();
  }

  showSearchField() {
    this.showSearch = !this.showSearch;
    this.showGroupsDropdown = false;
  }

  filterContacts(e) {
    const textField = <SearchBar>e.object;
    if (!this.filterInProgress && textField.text !== undefined) {
      this.filterInProgress = true;
      setTimeout(() => {
        if (textField.text === "") {
          this.contactList = this.contacts;
          this.filterInProgress = false;
        } else {
          const result = this.filterItems(textField.text);
          this.contactList = result;
          this.filterInProgress = false;
        }
        this.showSearch = !this.showSearch;
        this.showGroupsDropdown = false;
      }, 100);
    }
  }

  filterItems(query) {
    if (this.contactList.length === 0) {
      this.contactList = this.contacts;
    }

    return this.contactList.filter(el => {
      return (
        el.name.displayname.toLowerCase().indexOf(query.toLowerCase()) > -1
      );
    });
  }

  dropdownSelectGroup() {
    this.chooseGroup().then(result => {
      if (result === "Cancel") {
        return;
      }
      if (result === "All") {
        this.contactList = this.contacts;
      } else {
        this.contactList = this.contacts.filter(
          contact => result === contact.group
        );
      }
    });
  }

  chooseGroup() {
    const nameArray = Object.keys(this.groupObj["contact_groups"]).map(
      name => name
    );
    return dialogs.action({
      message: "Choose Category",
      cancelButtonText: "Cancel",
      actions: ["All", ...nameArray]
    });
  }
}
