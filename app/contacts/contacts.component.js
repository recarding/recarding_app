"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var search_bar_1 = require("ui/search-bar");
var angular_1 = require("nativescript-ui-sidedrawer/angular");
require("rxjs/add/observable/fromPromise");
var storage_service_1 = require("../shared/helper/app-settings/storage.service");
var groups_service_1 = require("../shared/helper/app-settings/groups.service");
var dialogs = require("ui/dialogs");
var ContactsComponent = /** @class */ (function () {
    function ContactsComponent(routerExtensions, storageService, groupService) {
        this.routerExtensions = routerExtensions;
        this.storageService = storageService;
        this.groupService = groupService;
        this.showSearch = false;
        this.showGroupsDropdown = true;
        this.filterInProgress = false;
        this.type = "contact";
    }
    ContactsComponent.prototype.ngOnInit = function () {
        this.contactList = this.storageService.getContacts("contact");
        this.contacts = this.contactList;
        this.groupObj = this.groupService.getContactGroups();
        this.numberOfGroups = Object.keys(this.groupObj["contact_groups"]).length;
    };
    Object.defineProperty(ContactsComponent.prototype, "sideDrawerTransition", {
        get: function () {
            return this._sideDrawerTransition;
        },
        enumerable: true,
        configurable: true
    });
    ContactsComponent.prototype.back = function () {
        this.routerExtensions.navigateByUrl("/home", { clearHistory: true });
    };
    ContactsComponent.prototype.onDrawerButtonTap = function () {
        this.drawerComponent.sideDrawer.showDrawer();
    };
    ContactsComponent.prototype.showSearchField = function () {
        this.showSearch = !this.showSearch;
        this.showGroupsDropdown = false;
    };
    ContactsComponent.prototype.filterContacts = function (e) {
        var _this = this;
        var textField = e.object;
        if (!this.filterInProgress && textField.text !== undefined) {
            this.filterInProgress = true;
            setTimeout(function () {
                if (textField.text === "") {
                    _this.contactList = _this.contacts;
                    _this.filterInProgress = false;
                }
                else {
                    var result = _this.filterItems(textField.text);
                    _this.contactList = result;
                    _this.filterInProgress = false;
                }
                _this.showSearch = !_this.showSearch;
                _this.showGroupsDropdown = false;
            }, 100);
        }
    };
    ContactsComponent.prototype.filterItems = function (query) {
        if (this.contactList.length === 0) {
            this.contactList = this.contacts;
        }
        return this.contactList.filter(function (el) {
            return (el.name.displayname.toLowerCase().indexOf(query.toLowerCase()) > -1);
        });
    };
    ContactsComponent.prototype.dropdownSelectGroup = function () {
        var _this = this;
        this.chooseGroup().then(function (result) {
            if (result === "Cancel") {
                return;
            }
            if (result === "All") {
                _this.contactList = _this.contacts;
            }
            else {
                _this.contactList = _this.contacts.filter(function (contact) { return result === contact.group; });
            }
        });
    };
    ContactsComponent.prototype.chooseGroup = function () {
        var nameArray = Object.keys(this.groupObj["contact_groups"]).map(function (name) { return name; });
        return dialogs.action({
            message: "Choose Category",
            cancelButtonText: "Cancel",
            actions: ["All"].concat(nameArray)
        });
    };
    __decorate([
        core_1.ViewChild("drawer"),
        __metadata("design:type", angular_1.RadSideDrawerComponent)
    ], ContactsComponent.prototype, "drawerComponent", void 0);
    __decorate([
        core_1.ViewChild("searchField"),
        __metadata("design:type", search_bar_1.SearchBar)
    ], ContactsComponent.prototype, "searchField", void 0);
    ContactsComponent = __decorate([
        core_1.Component({
            selector: "Contacts",
            moduleId: module.id,
            templateUrl: "./contacts.component.html",
            styleUrls: ["./contacts.component.scss"]
        }),
        __metadata("design:paramtypes", [router_1.RouterExtensions,
            storage_service_1.StorageService,
            groups_service_1.GroupService])
    ], ContactsComponent);
    return ContactsComponent;
}());
exports.ContactsComponent = ContactsComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFjdHMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29udGFjdHMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTZEO0FBQzdELHNEQUErRDtBQUMvRCw0Q0FBMEM7QUFHMUMsOERBQTRFO0FBRzVFLDJDQUF5QztBQUd6QyxpRkFBK0U7QUFDL0UsK0VBQTRFO0FBQzVFLG9DQUFzQztBQVF0QztJQWNFLDJCQUNVLGdCQUFrQyxFQUNsQyxjQUE4QixFQUM5QixZQUEwQjtRQUYxQixxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQ2xDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQVpwQyxlQUFVLEdBQVksS0FBSyxDQUFDO1FBQzVCLHVCQUFrQixHQUFZLElBQUksQ0FBQztRQUNuQyxxQkFBZ0IsR0FBWSxLQUFLLENBQUM7UUFHbEMsU0FBSSxHQUFHLFNBQVMsQ0FBQztJQVFkLENBQUM7SUFFSixvQ0FBUSxHQUFSO1FBQ0UsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUM5RCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDakMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDckQsSUFBSSxDQUFDLGNBQWMsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztJQUM1RSxDQUFDO0lBRUQsc0JBQUksbURBQW9CO2FBQXhCO1lBQ0UsT0FBTyxJQUFJLENBQUMscUJBQXFCLENBQUM7UUFDcEMsQ0FBQzs7O09BQUE7SUFFRCxnQ0FBSSxHQUFKO1FBQ0UsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxPQUFPLEVBQUUsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztJQUN2RSxDQUFDO0lBRUQsNkNBQWlCLEdBQWpCO1FBQ0UsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDL0MsQ0FBQztJQUVELDJDQUFlLEdBQWY7UUFDRSxJQUFJLENBQUMsVUFBVSxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUNuQyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO0lBQ2xDLENBQUM7SUFFRCwwQ0FBYyxHQUFkLFVBQWUsQ0FBQztRQUFoQixpQkFpQkM7UUFoQkMsSUFBTSxTQUFTLEdBQWMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztRQUN0QyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixJQUFJLFNBQVMsQ0FBQyxJQUFJLEtBQUssU0FBUyxFQUFFO1lBQzFELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7WUFDN0IsVUFBVSxDQUFDO2dCQUNULElBQUksU0FBUyxDQUFDLElBQUksS0FBSyxFQUFFLEVBQUU7b0JBQ3pCLEtBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQztvQkFDakMsS0FBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQztpQkFDL0I7cUJBQU07b0JBQ0wsSUFBTSxNQUFNLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ2hELEtBQUksQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDO29CQUMxQixLQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO2lCQUMvQjtnQkFDRCxLQUFJLENBQUMsVUFBVSxHQUFHLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQztnQkFDbkMsS0FBSSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztZQUNsQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7U0FDVDtJQUNILENBQUM7SUFFRCx1Q0FBVyxHQUFYLFVBQVksS0FBSztRQUNmLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO1lBQ2pDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztTQUNsQztRQUVELE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsVUFBQSxFQUFFO1lBQy9CLE9BQU8sQ0FDTCxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQ3BFLENBQUM7UUFDSixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCwrQ0FBbUIsR0FBbkI7UUFBQSxpQkFhQztRQVpDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQSxNQUFNO1lBQzVCLElBQUksTUFBTSxLQUFLLFFBQVEsRUFBRTtnQkFDdkIsT0FBTzthQUNSO1lBQ0QsSUFBSSxNQUFNLEtBQUssS0FBSyxFQUFFO2dCQUNwQixLQUFJLENBQUMsV0FBVyxHQUFHLEtBQUksQ0FBQyxRQUFRLENBQUM7YUFDbEM7aUJBQU07Z0JBQ0wsS0FBSSxDQUFDLFdBQVcsR0FBRyxLQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FDckMsVUFBQSxPQUFPLElBQUksT0FBQSxNQUFNLEtBQUssT0FBTyxDQUFDLEtBQUssRUFBeEIsQ0FBd0IsQ0FDcEMsQ0FBQzthQUNIO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsdUNBQVcsR0FBWDtRQUNFLElBQU0sU0FBUyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUNoRSxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksRUFBSixDQUFJLENBQ2IsQ0FBQztRQUNGLE9BQU8sT0FBTyxDQUFDLE1BQU0sQ0FBQztZQUNwQixPQUFPLEVBQUUsaUJBQWlCO1lBQzFCLGdCQUFnQixFQUFFLFFBQVE7WUFDMUIsT0FBTyxHQUFHLEtBQUssU0FBSyxTQUFTLENBQUM7U0FDL0IsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQWxHb0I7UUFBcEIsZ0JBQVMsQ0FBQyxRQUFRLENBQUM7a0NBQWtCLGdDQUFzQjs4REFBQztJQUNuQztRQUF6QixnQkFBUyxDQUFDLGFBQWEsQ0FBQztrQ0FBYyxzQkFBUzswREFBQztJQUZ0QyxpQkFBaUI7UUFON0IsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxVQUFVO1lBQ3BCLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixXQUFXLEVBQUUsMkJBQTJCO1lBQ3hDLFNBQVMsRUFBRSxDQUFDLDJCQUEyQixDQUFDO1NBQ3pDLENBQUM7eUNBZ0I0Qix5QkFBZ0I7WUFDbEIsZ0NBQWM7WUFDaEIsNkJBQVk7T0FqQnpCLGlCQUFpQixDQW9HN0I7SUFBRCx3QkFBQztDQUFBLEFBcEdELElBb0dDO0FBcEdZLDhDQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBWaWV3Q2hpbGQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQgeyBTZWFyY2hCYXIgfSBmcm9tIFwidWkvc2VhcmNoLWJhclwiO1xyXG5cclxuaW1wb3J0IHsgRHJhd2VyVHJhbnNpdGlvbkJhc2UgfSBmcm9tIFwibmF0aXZlc2NyaXB0LXVpLXNpZGVkcmF3ZXJcIjtcclxuaW1wb3J0IHsgUmFkU2lkZURyYXdlckNvbXBvbmVudCB9IGZyb20gXCJuYXRpdmVzY3JpcHQtdWktc2lkZWRyYXdlci9hbmd1bGFyXCI7XHJcblxyXG5pbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tIFwicnhqc1wiO1xyXG5pbXBvcnQgXCJyeGpzL2FkZC9vYnNlcnZhYmxlL2Zyb21Qcm9taXNlXCI7XHJcblxyXG5pbXBvcnQgeyBJQ29udGFjdCB9IGZyb20gXCIuLi9zaGFyZWQvaGVscGVyL2FwcC1zZXR0aW5ncy9zdG9yYWdlLm1vZGVsXCI7XHJcbmltcG9ydCB7IFN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSBcIi4uL3NoYXJlZC9oZWxwZXIvYXBwLXNldHRpbmdzL3N0b3JhZ2Uuc2VydmljZVwiO1xyXG5pbXBvcnQgeyBHcm91cFNlcnZpY2UgfSBmcm9tIFwiLi4vc2hhcmVkL2hlbHBlci9hcHAtc2V0dGluZ3MvZ3JvdXBzLnNlcnZpY2VcIjtcclxuaW1wb3J0ICogYXMgZGlhbG9ncyBmcm9tIFwidWkvZGlhbG9nc1wiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6IFwiQ29udGFjdHNcIixcclxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gIHRlbXBsYXRlVXJsOiBcIi4vY29udGFjdHMuY29tcG9uZW50Lmh0bWxcIixcclxuICBzdHlsZVVybHM6IFtcIi4vY29udGFjdHMuY29tcG9uZW50LnNjc3NcIl1cclxufSlcclxuZXhwb3J0IGNsYXNzIENvbnRhY3RzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBAVmlld0NoaWxkKFwiZHJhd2VyXCIpIGRyYXdlckNvbXBvbmVudDogUmFkU2lkZURyYXdlckNvbXBvbmVudDtcclxuICBAVmlld0NoaWxkKFwic2VhcmNoRmllbGRcIikgc2VhcmNoRmllbGQ6IFNlYXJjaEJhcjtcclxuICBjb250YWN0TGlzdDogQXJyYXk8SUNvbnRhY3Q+O1xyXG4gIGNvbnRhY3RzOiBBcnJheTxJQ29udGFjdD47XHJcbiAgc2hvd1NlYXJjaDogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIHNob3dHcm91cHNEcm9wZG93bjogYm9vbGVhbiA9IHRydWU7XHJcbiAgZmlsdGVySW5Qcm9ncmVzczogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIG51bWJlck9mR3JvdXBzOiBudW1iZXI7XHJcbiAgZ3JvdXBPYmo6IG9iamVjdDtcclxuICB0eXBlID0gXCJjb250YWN0XCI7XHJcbiAgcHJpdmF0ZSBfc2lkZURyYXdlclRyYW5zaXRpb246IERyYXdlclRyYW5zaXRpb25CYXNlO1xyXG4gIHByaXZhdGUgc3ViOiBTdWJzY3JpcHRpb247XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLFxyXG4gICAgcHJpdmF0ZSBzdG9yYWdlU2VydmljZTogU3RvcmFnZVNlcnZpY2UsXHJcbiAgICBwcml2YXRlIGdyb3VwU2VydmljZTogR3JvdXBTZXJ2aWNlXHJcbiAgKSB7fVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIHRoaXMuY29udGFjdExpc3QgPSB0aGlzLnN0b3JhZ2VTZXJ2aWNlLmdldENvbnRhY3RzKFwiY29udGFjdFwiKTtcclxuICAgIHRoaXMuY29udGFjdHMgPSB0aGlzLmNvbnRhY3RMaXN0O1xyXG4gICAgdGhpcy5ncm91cE9iaiA9IHRoaXMuZ3JvdXBTZXJ2aWNlLmdldENvbnRhY3RHcm91cHMoKTtcclxuICAgIHRoaXMubnVtYmVyT2ZHcm91cHMgPSBPYmplY3Qua2V5cyh0aGlzLmdyb3VwT2JqW1wiY29udGFjdF9ncm91cHNcIl0pLmxlbmd0aDtcclxuICB9XHJcblxyXG4gIGdldCBzaWRlRHJhd2VyVHJhbnNpdGlvbigpOiBEcmF3ZXJUcmFuc2l0aW9uQmFzZSB7XHJcbiAgICByZXR1cm4gdGhpcy5fc2lkZURyYXdlclRyYW5zaXRpb247XHJcbiAgfVxyXG5cclxuICBiYWNrKCkge1xyXG4gICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlQnlVcmwoYC9ob21lYCwgeyBjbGVhckhpc3Rvcnk6IHRydWUgfSk7XHJcbiAgfVxyXG5cclxuICBvbkRyYXdlckJ1dHRvblRhcCgpOiB2b2lkIHtcclxuICAgIHRoaXMuZHJhd2VyQ29tcG9uZW50LnNpZGVEcmF3ZXIuc2hvd0RyYXdlcigpO1xyXG4gIH1cclxuXHJcbiAgc2hvd1NlYXJjaEZpZWxkKCkge1xyXG4gICAgdGhpcy5zaG93U2VhcmNoID0gIXRoaXMuc2hvd1NlYXJjaDtcclxuICAgIHRoaXMuc2hvd0dyb3Vwc0Ryb3Bkb3duID0gZmFsc2U7XHJcbiAgfVxyXG5cclxuICBmaWx0ZXJDb250YWN0cyhlKSB7XHJcbiAgICBjb25zdCB0ZXh0RmllbGQgPSA8U2VhcmNoQmFyPmUub2JqZWN0O1xyXG4gICAgaWYgKCF0aGlzLmZpbHRlckluUHJvZ3Jlc3MgJiYgdGV4dEZpZWxkLnRleHQgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICB0aGlzLmZpbHRlckluUHJvZ3Jlc3MgPSB0cnVlO1xyXG4gICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICBpZiAodGV4dEZpZWxkLnRleHQgPT09IFwiXCIpIHtcclxuICAgICAgICAgIHRoaXMuY29udGFjdExpc3QgPSB0aGlzLmNvbnRhY3RzO1xyXG4gICAgICAgICAgdGhpcy5maWx0ZXJJblByb2dyZXNzID0gZmFsc2U7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIGNvbnN0IHJlc3VsdCA9IHRoaXMuZmlsdGVySXRlbXModGV4dEZpZWxkLnRleHQpO1xyXG4gICAgICAgICAgdGhpcy5jb250YWN0TGlzdCA9IHJlc3VsdDtcclxuICAgICAgICAgIHRoaXMuZmlsdGVySW5Qcm9ncmVzcyA9IGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnNob3dTZWFyY2ggPSAhdGhpcy5zaG93U2VhcmNoO1xyXG4gICAgICAgIHRoaXMuc2hvd0dyb3Vwc0Ryb3Bkb3duID0gZmFsc2U7XHJcbiAgICAgIH0sIDEwMCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBmaWx0ZXJJdGVtcyhxdWVyeSkge1xyXG4gICAgaWYgKHRoaXMuY29udGFjdExpc3QubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgIHRoaXMuY29udGFjdExpc3QgPSB0aGlzLmNvbnRhY3RzO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiB0aGlzLmNvbnRhY3RMaXN0LmZpbHRlcihlbCA9PiB7XHJcbiAgICAgIHJldHVybiAoXHJcbiAgICAgICAgZWwubmFtZS5kaXNwbGF5bmFtZS50b0xvd2VyQ2FzZSgpLmluZGV4T2YocXVlcnkudG9Mb3dlckNhc2UoKSkgPiAtMVxyXG4gICAgICApO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBkcm9wZG93blNlbGVjdEdyb3VwKCkge1xyXG4gICAgdGhpcy5jaG9vc2VHcm91cCgpLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgaWYgKHJlc3VsdCA9PT0gXCJDYW5jZWxcIikge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG4gICAgICBpZiAocmVzdWx0ID09PSBcIkFsbFwiKSB7XHJcbiAgICAgICAgdGhpcy5jb250YWN0TGlzdCA9IHRoaXMuY29udGFjdHM7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5jb250YWN0TGlzdCA9IHRoaXMuY29udGFjdHMuZmlsdGVyKFxyXG4gICAgICAgICAgY29udGFjdCA9PiByZXN1bHQgPT09IGNvbnRhY3QuZ3JvdXBcclxuICAgICAgICApO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGNob29zZUdyb3VwKCkge1xyXG4gICAgY29uc3QgbmFtZUFycmF5ID0gT2JqZWN0LmtleXModGhpcy5ncm91cE9ialtcImNvbnRhY3RfZ3JvdXBzXCJdKS5tYXAoXHJcbiAgICAgIG5hbWUgPT4gbmFtZVxyXG4gICAgKTtcclxuICAgIHJldHVybiBkaWFsb2dzLmFjdGlvbih7XHJcbiAgICAgIG1lc3NhZ2U6IFwiQ2hvb3NlIENhdGVnb3J5XCIsXHJcbiAgICAgIGNhbmNlbEJ1dHRvblRleHQ6IFwiQ2FuY2VsXCIsXHJcbiAgICAgIGFjdGlvbnM6IFtcIkFsbFwiLCAuLi5uYW1lQXJyYXldXHJcbiAgICB9KTtcclxuICB9XHJcbn1cclxuIl19