import {
  Component,
  EventEmitter,
  Output,
  OnInit,
  AfterViewInit
} from "@angular/core";

@Component({
  selector: "float-menu",
  moduleId: module.id,
  templateUrl: "./bottom-menu.component.html",
  styleUrls: ["./bottom-menu.component.scss"]
})
export class BottomMenuComponent implements OnInit {
  @Output() newPressed = new EventEmitter();
  @Output() sortAlpha = new EventEmitter();
  @Output() sortDate = new EventEmitter();
  alpha = false;

  ngOnInit() {
    setTimeout(() => {
      this.alpha = true;
    }, 10);
  }

  sortBy(wayOfSort: string) {
    if (wayOfSort === "alpha") {
      this.alpha = true;
      this.sortAlpha.emit();
    } else {
      this.alpha = false;
      this.sortDate.emit();
    }
  }

  emitNew() {
    this.newPressed.emit();
  }
}
