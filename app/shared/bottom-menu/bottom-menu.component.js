"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var BottomMenuComponent = /** @class */ (function () {
    function BottomMenuComponent() {
        this.newPressed = new core_1.EventEmitter();
        this.sortAlpha = new core_1.EventEmitter();
        this.sortDate = new core_1.EventEmitter();
        this.alpha = false;
    }
    BottomMenuComponent.prototype.ngOnInit = function () {
        var _this = this;
        setTimeout(function () {
            _this.alpha = true;
        }, 10);
    };
    BottomMenuComponent.prototype.sortBy = function (wayOfSort) {
        if (wayOfSort === "alpha") {
            this.alpha = true;
            this.sortAlpha.emit();
        }
        else {
            this.alpha = false;
            this.sortDate.emit();
        }
    };
    BottomMenuComponent.prototype.emitNew = function () {
        this.newPressed.emit();
    };
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], BottomMenuComponent.prototype, "newPressed", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], BottomMenuComponent.prototype, "sortAlpha", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], BottomMenuComponent.prototype, "sortDate", void 0);
    BottomMenuComponent = __decorate([
        core_1.Component({
            selector: "float-menu",
            moduleId: module.id,
            templateUrl: "./bottom-menu.component.html",
            styleUrls: ["./bottom-menu.component.scss"]
        })
    ], BottomMenuComponent);
    return BottomMenuComponent;
}());
exports.BottomMenuComponent = BottomMenuComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm90dG9tLW1lbnUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYm90dG9tLW1lbnUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBTXVCO0FBUXZCO0lBTkE7UUFPWSxlQUFVLEdBQUcsSUFBSSxtQkFBWSxFQUFFLENBQUM7UUFDaEMsY0FBUyxHQUFHLElBQUksbUJBQVksRUFBRSxDQUFDO1FBQy9CLGFBQVEsR0FBRyxJQUFJLG1CQUFZLEVBQUUsQ0FBQztRQUN4QyxVQUFLLEdBQUcsS0FBSyxDQUFDO0lBcUJoQixDQUFDO0lBbkJDLHNDQUFRLEdBQVI7UUFBQSxpQkFJQztRQUhDLFVBQVUsQ0FBQztZQUNULEtBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ3BCLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztJQUNULENBQUM7SUFFRCxvQ0FBTSxHQUFOLFVBQU8sU0FBaUI7UUFDdEIsSUFBSSxTQUFTLEtBQUssT0FBTyxFQUFFO1lBQ3pCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1lBQ2xCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDdkI7YUFBTTtZQUNMLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1lBQ25CLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDdEI7SUFDSCxDQUFDO0lBRUQscUNBQU8sR0FBUDtRQUNFLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDekIsQ0FBQztJQXZCUztRQUFULGFBQU0sRUFBRTs7MkRBQWlDO0lBQ2hDO1FBQVQsYUFBTSxFQUFFOzswREFBZ0M7SUFDL0I7UUFBVCxhQUFNLEVBQUU7O3lEQUErQjtJQUg3QixtQkFBbUI7UUFOL0IsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxZQUFZO1lBQ3RCLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixXQUFXLEVBQUUsOEJBQThCO1lBQzNDLFNBQVMsRUFBRSxDQUFDLDhCQUE4QixDQUFDO1NBQzVDLENBQUM7T0FDVyxtQkFBbUIsQ0F5Qi9CO0lBQUQsMEJBQUM7Q0FBQSxBQXpCRCxJQXlCQztBQXpCWSxrREFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xuICBDb21wb25lbnQsXG4gIEV2ZW50RW1pdHRlcixcbiAgT3V0cHV0LFxuICBPbkluaXQsXG4gIEFmdGVyVmlld0luaXRcbn0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBcImZsb2F0LW1lbnVcIixcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbiAgdGVtcGxhdGVVcmw6IFwiLi9ib3R0b20tbWVudS5jb21wb25lbnQuaHRtbFwiLFxuICBzdHlsZVVybHM6IFtcIi4vYm90dG9tLW1lbnUuY29tcG9uZW50LnNjc3NcIl1cbn0pXG5leHBvcnQgY2xhc3MgQm90dG9tTWVudUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIEBPdXRwdXQoKSBuZXdQcmVzc2VkID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgc29ydEFscGhhID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgc29ydERhdGUgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIGFscGhhID0gZmFsc2U7XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICB0aGlzLmFscGhhID0gdHJ1ZTtcbiAgICB9LCAxMCk7XG4gIH1cblxuICBzb3J0Qnkod2F5T2ZTb3J0OiBzdHJpbmcpIHtcbiAgICBpZiAod2F5T2ZTb3J0ID09PSBcImFscGhhXCIpIHtcbiAgICAgIHRoaXMuYWxwaGEgPSB0cnVlO1xuICAgICAgdGhpcy5zb3J0QWxwaGEuZW1pdCgpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmFscGhhID0gZmFsc2U7XG4gICAgICB0aGlzLnNvcnREYXRlLmVtaXQoKTtcbiAgICB9XG4gIH1cblxuICBlbWl0TmV3KCkge1xuICAgIHRoaXMubmV3UHJlc3NlZC5lbWl0KCk7XG4gIH1cbn1cbiJdfQ==