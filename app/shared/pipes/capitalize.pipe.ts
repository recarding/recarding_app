import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'toUpperCase' })
export class CapitalizePipe implements PipeTransform {
    transform(str: string): string {
        return str.toUpperCase();
    }
}