"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var image_source_1 = require("tns-core-modules/image-source/image-source");
var ImageConvertPipe = /** @class */ (function () {
    function ImageConvertPipe() {
    }
    ImageConvertPipe.prototype.transform = function (encodedImg) {
        var imageSource = new image_source_1.ImageSource();
        var loadedBase64 = imageSource.loadFromBase64(encodedImg);
        if (loadedBase64) {
            return imageSource;
        }
    };
    ImageConvertPipe = __decorate([
        core_1.Pipe({ name: 'createSource' })
    ], ImageConvertPipe);
    return ImageConvertPipe;
}());
exports.ImageConvertPipe = ImageConvertPipe;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW1hZ2UucGlwZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImltYWdlLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBb0Q7QUFDcEQsMkVBQXlFO0FBR3pFO0lBQUE7SUFRQSxDQUFDO0lBUEcsb0NBQVMsR0FBVCxVQUFVLFVBQWtCO1FBQ3hCLElBQU0sV0FBVyxHQUFHLElBQUksMEJBQVcsRUFBRSxDQUFDO1FBQ3RDLElBQU0sWUFBWSxHQUFHLFdBQVcsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDNUQsSUFBSSxZQUFZLEVBQUU7WUFDZCxPQUFPLFdBQVcsQ0FBQztTQUN0QjtJQUNMLENBQUM7SUFQUSxnQkFBZ0I7UUFENUIsV0FBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLGNBQWMsRUFBRSxDQUFDO09BQ2xCLGdCQUFnQixDQVE1QjtJQUFELHVCQUFDO0NBQUEsQUFSRCxJQVFDO0FBUlksNENBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSW1hZ2VTb3VyY2UgfSBmcm9tICd0bnMtY29yZS1tb2R1bGVzL2ltYWdlLXNvdXJjZS9pbWFnZS1zb3VyY2UnO1xuXG5AUGlwZSh7IG5hbWU6ICdjcmVhdGVTb3VyY2UnIH0pXG5leHBvcnQgY2xhc3MgSW1hZ2VDb252ZXJ0UGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0ge1xuICAgIHRyYW5zZm9ybShlbmNvZGVkSW1nOiBzdHJpbmcpOiBJbWFnZVNvdXJjZSB7XG4gICAgICAgIGNvbnN0IGltYWdlU291cmNlID0gbmV3IEltYWdlU291cmNlKCk7XG4gICAgICAgIGNvbnN0IGxvYWRlZEJhc2U2NCA9IGltYWdlU291cmNlLmxvYWRGcm9tQmFzZTY0KGVuY29kZWRJbWcpO1xuICAgICAgICBpZiAobG9hZGVkQmFzZTY0KSB7XG4gICAgICAgICAgICByZXR1cm4gaW1hZ2VTb3VyY2U7XG4gICAgICAgIH1cbiAgICB9XG59Il19