import { Pipe, PipeTransform } from '@angular/core';
import { ImageSource } from 'tns-core-modules/image-source/image-source';

@Pipe({ name: 'createSource' })
export class ImageConvertPipe implements PipeTransform {
    transform(encodedImg: string): ImageSource {
        const imageSource = new ImageSource();
        const loadedBase64 = imageSource.loadFromBase64(encodedImg);
        if (loadedBase64) {
            return imageSource;
        }
    }
}