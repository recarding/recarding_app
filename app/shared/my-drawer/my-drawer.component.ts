import { Component, Input, OnInit } from "@angular/core";
import { AuthService } from "../helper/auth.service";
import { RouterExtensions } from "nativescript-angular/router";

@Component({
    selector: "MyDrawer",
    moduleId: module.id,
    templateUrl: "./my-drawer.component.html",
    styleUrls: ["./my-drawer.component.scss"]
})
export class MyDrawerComponent implements OnInit {

    @Input() selectedPage: string;
    user: any;

    constructor( 
        private auth :AuthService,
        private routerExtensions: RouterExtensions
    ) {}
    ngOnInit(): void {
        this.user = this.auth.getactiveUser()
    }

    isPageSelected(pageTitle: string): boolean {
        return pageTitle === this.selectedPage;
    }

    signOut() {
        this.auth.removeAuthToken();
        this.auth.removeFacebookToken();
        this.routerExtensions.navigateByUrl("/login", { clearHistory: true });
    }
}
