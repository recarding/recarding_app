"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var dialogs_1 = require("nativescript-angular/directives/dialogs");
var storage_service_1 = require("../../shared/helper/app-settings/storage.service");
var share_modal_1 = require("../../shared/shareModal/share.modal");
var Dialogs = require("ui/dialogs");
var TNSPhone = require("nativescript-phone");
var page_1 = require("tns-core-modules/ui/page/page");
var router_2 = require("nativescript-angular/router");
var ContactPreviewComponent = /** @class */ (function () {
    function ContactPreviewComponent(router, routerExtensions, modal, storageService, vcRef, page) {
        this.router = router;
        this.routerExtensions = routerExtensions;
        this.modal = modal;
        this.storageService = storageService;
        this.vcRef = vcRef;
        this.page = page;
        this.togglePreviewOptions = false;
        this.showPreviewOptions = false;
    }
    ContactPreviewComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        if (this.type === "contact") {
            setTimeout(function () {
                _this.previewtOptions = _this.page.getViewById("previewtOptions" + _this.index);
                _this.previewtOptions["visibility"] = "collapse";
            }, 10);
        }
    };
    ContactPreviewComponent.prototype.onTouch = function (args) {
        if (this.type === "myprofiles") {
            this.routerExtensions.navigateByUrl("card/myprofiles/" + this.contact.id, { clearHistory: true });
        }
        else {
            if (args.action === "down") {
                this.start = new Date().getMilliseconds();
            }
            if (args.action === "up") {
                this.end = new Date().getMilliseconds();
                var duration = Math.abs(this.start - this.end);
                if (duration > 100) {
                    this.showPreviewOptions = !this.showPreviewOptions;
                    this.previewtOptions = this.page.getViewById("previewtOptions" + this.index);
                    this.previewtOptions["visibility"] = this.previewtOptions.isCollapsed
                        ? "visible"
                        : "hidden";
                }
                else {
                    this.routerExtensions.navigateByUrl("card/contact/" + this.contact.id, { clearHistory: true });
                }
            }
        }
    };
    ContactPreviewComponent.prototype.callNumber = function () {
        TNSPhone.dial(this.contact.phoneNumbers[0], false);
        this.togglePreviewOptions = !this.togglePreviewOptions;
    };
    ContactPreviewComponent.prototype.shareCard = function () {
        var options = {
            context: {
                myID: this.contact.id,
                contactID: ""
            },
            fullscreen: false,
            viewContainerRef: this.vcRef
        };
        this.modal.showModal(share_modal_1.ShareModalComponent, options).then(function (nearByUsers) {
            console.log(JSON.stringify(nearByUsers));
        });
    };
    ContactPreviewComponent.prototype.removeContact = function () {
        var _this = this;
        console.log('DELETE PRESSED');
        Dialogs.confirm("Remove " + this.contact.name.displayname + "?").then(function (confirmed) {
            if (confirmed) {
                var removed = _this.storageService.remove(_this.contact.id, "contact");
                if (removed) {
                }
                else {
                }
                _this.router.navigate(["home"]);
            }
        });
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], ContactPreviewComponent.prototype, "index", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], ContactPreviewComponent.prototype, "contact", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], ContactPreviewComponent.prototype, "type", void 0);
    ContactPreviewComponent = __decorate([
        core_1.Component({
            selector: "Contact-Preview",
            moduleId: module.id,
            templateUrl: "./contact-preview.component.html",
            styleUrls: ["./contact-preview.component.scss"],
            changeDetection: core_1.ChangeDetectionStrategy.OnPush
        }),
        __metadata("design:paramtypes", [router_1.Router,
            router_2.RouterExtensions,
            dialogs_1.ModalDialogService,
            storage_service_1.StorageService,
            core_1.ViewContainerRef,
            page_1.Page])
    ], ContactPreviewComponent);
    return ContactPreviewComponent;
}());
exports.ContactPreviewComponent = ContactPreviewComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFjdC1wcmV2aWV3LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImNvbnRhY3QtcHJldmlldy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FNdUI7QUFDdkIsMENBQXlDO0FBRXpDLG1FQUE2RTtBQUc3RSxvRkFBa0Y7QUFDbEYsbUVBQTBFO0FBRTFFLG9DQUFzQztBQUV0Qyw2Q0FBK0M7QUFFL0Msc0RBQStEO0FBQy9ELHNEQUErRDtBQVMvRDtJQVVFLGlDQUNVLE1BQWMsRUFDZCxnQkFBa0MsRUFDbEMsS0FBeUIsRUFDekIsY0FBOEIsRUFDOUIsS0FBdUIsRUFDdkIsSUFBVTtRQUxWLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQ2xDLFVBQUssR0FBTCxLQUFLLENBQW9CO1FBQ3pCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixVQUFLLEdBQUwsS0FBSyxDQUFrQjtRQUN2QixTQUFJLEdBQUosSUFBSSxDQUFNO1FBWloseUJBQW9CLEdBQVksS0FBSyxDQUFDO1FBSXZDLHVCQUFrQixHQUFHLEtBQUssQ0FBQztJQVMvQixDQUFDO0lBRUosaURBQWUsR0FBZjtRQUFBLGlCQVNDO1FBUkMsSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLFNBQVMsRUFBRTtZQUMzQixVQUFVLENBQUM7Z0JBQ1QsS0FBSSxDQUFDLGVBQWUsR0FBRyxLQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FDMUMsb0JBQWtCLEtBQUksQ0FBQyxLQUFPLENBQy9CLENBQUM7Z0JBQ0YsS0FBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsR0FBRyxVQUFVLENBQUM7WUFDbEQsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1NBQ1I7SUFDSCxDQUFDO0lBRUQseUNBQU8sR0FBUCxVQUFRLElBQTJCO1FBQ2pDLElBQUksSUFBSSxDQUFDLElBQUksS0FBSyxZQUFZLEVBQUU7WUFDOUIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGFBQWEsQ0FDakMscUJBQW1CLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBSSxFQUNwQyxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsQ0FDdkIsQ0FBQztTQUNIO2FBQU07WUFDTCxJQUFJLElBQUksQ0FBQyxNQUFNLEtBQUssTUFBTSxFQUFFO2dCQUMxQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksSUFBSSxFQUFFLENBQUMsZUFBZSxFQUFFLENBQUM7YUFDM0M7WUFDRCxJQUFJLElBQUksQ0FBQyxNQUFNLEtBQUssSUFBSSxFQUFFO2dCQUN4QixJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksSUFBSSxFQUFFLENBQUMsZUFBZSxFQUFFLENBQUM7Z0JBQ3hDLElBQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ2pELElBQUksUUFBUSxHQUFHLEdBQUcsRUFBRTtvQkFDbEIsSUFBSSxDQUFDLGtCQUFrQixHQUFHLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDO29CQUNuRCxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUMxQyxvQkFBa0IsSUFBSSxDQUFDLEtBQU8sQ0FDL0IsQ0FBQztvQkFDRixJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVzt3QkFDbkUsQ0FBQyxDQUFDLFNBQVM7d0JBQ1gsQ0FBQyxDQUFDLFFBQVEsQ0FBQztpQkFDZDtxQkFBTTtvQkFDTCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsYUFBYSxDQUNqQyxrQkFBZ0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFJLEVBQ2pDLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxDQUN2QixDQUFDO2lCQUNIO2FBQ0Y7U0FDRjtJQUNILENBQUM7SUFFRCw0Q0FBVSxHQUFWO1FBQ0UsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUNuRCxJQUFJLENBQUMsb0JBQW9CLEdBQUcsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUM7SUFDekQsQ0FBQztJQUVELDJDQUFTLEdBQVQ7UUFDRSxJQUFNLE9BQU8sR0FBRztZQUNkLE9BQU8sRUFBRTtnQkFDUCxJQUFJLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUNyQixTQUFTLEVBQUUsRUFBRTthQUNkO1lBQ0QsVUFBVSxFQUFFLEtBQUs7WUFDakIsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLEtBQUs7U0FDN0IsQ0FBQztRQUNGLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLGlDQUFtQixFQUFFLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLFdBQVc7WUFDakUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7UUFDM0MsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsK0NBQWEsR0FBYjtRQUFBLGlCQWdCQztRQWZDLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQTtRQUM3QixPQUFPLENBQUMsT0FBTyxDQUFDLFlBQVUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxNQUFHLENBQUMsQ0FBQyxJQUFJLENBQzlELFVBQUEsU0FBUztZQUNQLElBQUksU0FBUyxFQUFFO2dCQUNiLElBQU0sT0FBTyxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUN4QyxLQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsRUFDZixTQUFTLENBQ1YsQ0FBQztnQkFDRixJQUFJLE9BQU8sRUFBRTtpQkFDWjtxQkFBTTtpQkFDTjtnQkFDRCxLQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7YUFDaEM7UUFDSCxDQUFDLENBQ0YsQ0FBQztJQUNKLENBQUM7SUEvRlE7UUFBUixZQUFLLEVBQUU7OzBEQUFlO0lBQ2Q7UUFBUixZQUFLLEVBQUU7OzREQUFtQjtJQUNsQjtRQUFSLFlBQUssRUFBRTs7eURBQWM7SUFIWCx1QkFBdUI7UUFQbkMsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxpQkFBaUI7WUFDM0IsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFdBQVcsRUFBRSxrQ0FBa0M7WUFDL0MsU0FBUyxFQUFFLENBQUMsa0NBQWtDLENBQUM7WUFDL0MsZUFBZSxFQUFFLDhCQUF1QixDQUFDLE1BQU07U0FDaEQsQ0FBQzt5Q0FZa0IsZUFBTTtZQUNJLHlCQUFnQjtZQUMzQiw0QkFBa0I7WUFDVCxnQ0FBYztZQUN2Qix1QkFBZ0I7WUFDakIsV0FBSTtPQWhCVCx1QkFBdUIsQ0FpR25DO0lBQUQsOEJBQUM7Q0FBQSxBQWpHRCxJQWlHQztBQWpHWSwwREFBdUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xuICBDb21wb25lbnQsXG4gIElucHV0LFxuICBWaWV3Q29udGFpbmVyUmVmLFxuICBBZnRlclZpZXdJbml0LFxuICBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneVxufSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xuXG5pbXBvcnQgeyBNb2RhbERpYWxvZ1NlcnZpY2UgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvZGlyZWN0aXZlcy9kaWFsb2dzXCI7XG5cbmltcG9ydCB7IElDb250YWN0IH0gZnJvbSBcIi4uLy4uL3NoYXJlZC9oZWxwZXIvYXBwLXNldHRpbmdzL3N0b3JhZ2UubW9kZWxcIjtcbmltcG9ydCB7IFN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSBcIi4uLy4uL3NoYXJlZC9oZWxwZXIvYXBwLXNldHRpbmdzL3N0b3JhZ2Uuc2VydmljZVwiO1xuaW1wb3J0IHsgU2hhcmVNb2RhbENvbXBvbmVudCB9IGZyb20gXCIuLi8uLi9zaGFyZWQvc2hhcmVNb2RhbC9zaGFyZS5tb2RhbFwiO1xuXG5pbXBvcnQgKiBhcyBEaWFsb2dzIGZyb20gXCJ1aS9kaWFsb2dzXCI7XG5cbmltcG9ydCAqIGFzIFROU1Bob25lIGZyb20gXCJuYXRpdmVzY3JpcHQtcGhvbmVcIjtcbmltcG9ydCB7IFRvdWNoR2VzdHVyZUV2ZW50RGF0YSB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL2dlc3R1cmVzL2dlc3R1cmVzXCI7XG5pbXBvcnQgeyBQYWdlLCBWaWV3QmFzZSB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL3BhZ2UvcGFnZVwiO1xuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBcIkNvbnRhY3QtUHJldmlld1wiLFxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxuICB0ZW1wbGF0ZVVybDogXCIuL2NvbnRhY3QtcHJldmlldy5jb21wb25lbnQuaHRtbFwiLFxuICBzdHlsZVVybHM6IFtcIi4vY29udGFjdC1wcmV2aWV3LmNvbXBvbmVudC5zY3NzXCJdLFxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaFxufSlcbmV4cG9ydCBjbGFzcyBDb250YWN0UHJldmlld0NvbXBvbmVudCBpbXBsZW1lbnRzIEFmdGVyVmlld0luaXQge1xuICBASW5wdXQoKSBpbmRleDogbnVtYmVyO1xuICBASW5wdXQoKSBjb250YWN0OiBJQ29udGFjdDtcbiAgQElucHV0KCkgdHlwZTogc3RyaW5nO1xuICBwcml2YXRlIHRvZ2dsZVByZXZpZXdPcHRpb25zOiBib29sZWFuID0gZmFsc2U7XG4gIHByaXZhdGUgc3RhcnQ6IG51bWJlcjtcbiAgcHJpdmF0ZSBlbmQ6IG51bWJlcjtcbiAgcHJpdmF0ZSBwcmV2aWV3dE9wdGlvbnM6IFZpZXdCYXNlO1xuICBwdWJsaWMgc2hvd1ByZXZpZXdPcHRpb25zID0gZmFsc2U7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcbiAgICBwcml2YXRlIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnMsXG4gICAgcHJpdmF0ZSBtb2RhbDogTW9kYWxEaWFsb2dTZXJ2aWNlLFxuICAgIHByaXZhdGUgc3RvcmFnZVNlcnZpY2U6IFN0b3JhZ2VTZXJ2aWNlLFxuICAgIHByaXZhdGUgdmNSZWY6IFZpZXdDb250YWluZXJSZWYsXG4gICAgcHJpdmF0ZSBwYWdlOiBQYWdlXG4gICkge31cblxuICBuZ0FmdGVyVmlld0luaXQoKSB7XG4gICAgaWYgKHRoaXMudHlwZSA9PT0gXCJjb250YWN0XCIpIHtcbiAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICB0aGlzLnByZXZpZXd0T3B0aW9ucyA9IHRoaXMucGFnZS5nZXRWaWV3QnlJZChcbiAgICAgICAgICBgcHJldmlld3RPcHRpb25zJHt0aGlzLmluZGV4fWBcbiAgICAgICAgKTtcbiAgICAgICAgdGhpcy5wcmV2aWV3dE9wdGlvbnNbXCJ2aXNpYmlsaXR5XCJdID0gXCJjb2xsYXBzZVwiO1xuICAgICAgfSwgMTApO1xuICAgIH1cbiAgfVxuXG4gIG9uVG91Y2goYXJnczogVG91Y2hHZXN0dXJlRXZlbnREYXRhKSB7XG4gICAgaWYgKHRoaXMudHlwZSA9PT0gXCJteXByb2ZpbGVzXCIpIHtcbiAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZUJ5VXJsKFxuICAgICAgICBgY2FyZC9teXByb2ZpbGVzLyR7dGhpcy5jb250YWN0LmlkfWAsXG4gICAgICAgIHsgY2xlYXJIaXN0b3J5OiB0cnVlIH1cbiAgICAgICk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGlmIChhcmdzLmFjdGlvbiA9PT0gXCJkb3duXCIpIHtcbiAgICAgICAgdGhpcy5zdGFydCA9IG5ldyBEYXRlKCkuZ2V0TWlsbGlzZWNvbmRzKCk7XG4gICAgICB9XG4gICAgICBpZiAoYXJncy5hY3Rpb24gPT09IFwidXBcIikge1xuICAgICAgICB0aGlzLmVuZCA9IG5ldyBEYXRlKCkuZ2V0TWlsbGlzZWNvbmRzKCk7XG4gICAgICAgIGNvbnN0IGR1cmF0aW9uID0gTWF0aC5hYnModGhpcy5zdGFydCAtIHRoaXMuZW5kKTtcbiAgICAgICAgaWYgKGR1cmF0aW9uID4gMTAwKSB7XG4gICAgICAgICAgdGhpcy5zaG93UHJldmlld09wdGlvbnMgPSAhdGhpcy5zaG93UHJldmlld09wdGlvbnM7XG4gICAgICAgICAgdGhpcy5wcmV2aWV3dE9wdGlvbnMgPSB0aGlzLnBhZ2UuZ2V0Vmlld0J5SWQoXG4gICAgICAgICAgICBgcHJldmlld3RPcHRpb25zJHt0aGlzLmluZGV4fWBcbiAgICAgICAgICApO1xuICAgICAgICAgIHRoaXMucHJldmlld3RPcHRpb25zW1widmlzaWJpbGl0eVwiXSA9IHRoaXMucHJldmlld3RPcHRpb25zLmlzQ29sbGFwc2VkXG4gICAgICAgICAgICA/IFwidmlzaWJsZVwiXG4gICAgICAgICAgICA6IFwiaGlkZGVuXCI7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlQnlVcmwoXG4gICAgICAgICAgICBgY2FyZC9jb250YWN0LyR7dGhpcy5jb250YWN0LmlkfWAsXG4gICAgICAgICAgICB7IGNsZWFySGlzdG9yeTogdHJ1ZSB9XG4gICAgICAgICAgKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIGNhbGxOdW1iZXIoKSB7XG4gICAgVE5TUGhvbmUuZGlhbCh0aGlzLmNvbnRhY3QucGhvbmVOdW1iZXJzWzBdLCBmYWxzZSk7XG4gICAgdGhpcy50b2dnbGVQcmV2aWV3T3B0aW9ucyA9ICF0aGlzLnRvZ2dsZVByZXZpZXdPcHRpb25zO1xuICB9XG5cbiAgc2hhcmVDYXJkKCkge1xuICAgIGNvbnN0IG9wdGlvbnMgPSB7XG4gICAgICBjb250ZXh0OiB7XG4gICAgICAgIG15SUQ6IHRoaXMuY29udGFjdC5pZCxcbiAgICAgICAgY29udGFjdElEOiBcIlwiXG4gICAgICB9LFxuICAgICAgZnVsbHNjcmVlbjogZmFsc2UsXG4gICAgICB2aWV3Q29udGFpbmVyUmVmOiB0aGlzLnZjUmVmXG4gICAgfTtcbiAgICB0aGlzLm1vZGFsLnNob3dNb2RhbChTaGFyZU1vZGFsQ29tcG9uZW50LCBvcHRpb25zKS50aGVuKG5lYXJCeVVzZXJzID0+IHtcbiAgICAgIGNvbnNvbGUubG9nKEpTT04uc3RyaW5naWZ5KG5lYXJCeVVzZXJzKSk7XG4gICAgfSk7XG4gIH1cblxuICByZW1vdmVDb250YWN0KCkge1xuICAgIGNvbnNvbGUubG9nKCdERUxFVEUgUFJFU1NFRCcpXG4gICAgRGlhbG9ncy5jb25maXJtKGBSZW1vdmUgJHt0aGlzLmNvbnRhY3QubmFtZS5kaXNwbGF5bmFtZX0/YCkudGhlbihcbiAgICAgIGNvbmZpcm1lZCA9PiB7XG4gICAgICAgIGlmIChjb25maXJtZWQpIHtcbiAgICAgICAgICBjb25zdCByZW1vdmVkID0gdGhpcy5zdG9yYWdlU2VydmljZS5yZW1vdmUoXG4gICAgICAgICAgICB0aGlzLmNvbnRhY3QuaWQsXG4gICAgICAgICAgICBcImNvbnRhY3RcIlxuICAgICAgICAgICk7XG4gICAgICAgICAgaWYgKHJlbW92ZWQpIHtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIH1cbiAgICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbXCJob21lXCJdKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICk7XG4gIH1cbn1cbiJdfQ==