import {
  Component,
  Input,
  ViewContainerRef,
  AfterViewInit,
  ChangeDetectionStrategy
} from "@angular/core";
import { Router } from "@angular/router";

import { ModalDialogService } from "nativescript-angular/directives/dialogs";

import { IContact } from "../../shared/helper/app-settings/storage.model";
import { StorageService } from "../../shared/helper/app-settings/storage.service";
import { ShareModalComponent } from "../../shared/shareModal/share.modal";

import * as Dialogs from "ui/dialogs";

import * as TNSPhone from "nativescript-phone";
import { TouchGestureEventData } from "tns-core-modules/ui/gestures/gestures";
import { Page, ViewBase } from "tns-core-modules/ui/page/page";
import { RouterExtensions } from "nativescript-angular/router";

@Component({
  selector: "Contact-Preview",
  moduleId: module.id,
  templateUrl: "./contact-preview.component.html",
  styleUrls: ["./contact-preview.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContactPreviewComponent implements AfterViewInit {
  @Input() index: number;
  @Input() contact: IContact;
  @Input() type: string;
  private togglePreviewOptions: boolean = false;
  private start: number;
  private end: number;
  private previewtOptions: ViewBase;
  public showPreviewOptions = false;

  constructor(
    private router: Router,
    private routerExtensions: RouterExtensions,
    private modal: ModalDialogService,
    private storageService: StorageService,
    private vcRef: ViewContainerRef,
    private page: Page
  ) {}

  ngAfterViewInit() {
    if (this.type === "contact") {
      setTimeout(() => {
        this.previewtOptions = this.page.getViewById(
          `previewtOptions${this.index}`
        );
        this.previewtOptions["visibility"] = "collapse";
      }, 10);
    }
  }

  onTouch(args: TouchGestureEventData) {
    if (this.type === "myprofiles") {
      this.routerExtensions.navigateByUrl(
        `card/myprofiles/${this.contact.id}`,
        { clearHistory: true }
      );
    } else {
      if (args.action === "down") {
        this.start = new Date().getMilliseconds();
      }
      if (args.action === "up") {
        this.end = new Date().getMilliseconds();
        const duration = Math.abs(this.start - this.end);
        if (duration > 100) {
          this.showPreviewOptions = !this.showPreviewOptions;
          this.previewtOptions = this.page.getViewById(
            `previewtOptions${this.index}`
          );
          this.previewtOptions["visibility"] = this.previewtOptions.isCollapsed
            ? "visible"
            : "hidden";
        } else {
          this.routerExtensions.navigateByUrl(
            `card/contact/${this.contact.id}`,
            { clearHistory: true }
          );
        }
      }
    }
  }

  callNumber() {
    TNSPhone.dial(this.contact.phoneNumbers[0], false);
    this.togglePreviewOptions = !this.togglePreviewOptions;
  }

  shareCard() {
    const options = {
      context: {
        myID: this.contact.id,
        contactID: ""
      },
      fullscreen: false,
      viewContainerRef: this.vcRef
    };
    this.modal.showModal(ShareModalComponent, options).then(nearByUsers => {
      console.log(JSON.stringify(nearByUsers));
    });
  }

  removeContact() {
    console.log('DELETE PRESSED')
    Dialogs.confirm(`Remove ${this.contact.name.displayname}?`).then(
      confirmed => {
        if (confirmed) {
          const removed = this.storageService.remove(
            this.contact.id,
            "contact"
          );
          if (removed) {
          } else {
          }
          this.router.navigate(["home"]);
        }
      }
    );
  }
}
