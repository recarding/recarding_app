import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";

import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";

import { TNSCheckBoxModule } from "nativescript-checkbox/angular";

import { MyDrawerItemComponent } from "./my-drawer-item/my-drawer-item.component";
import { MyDrawerComponent } from "./my-drawer/my-drawer.component";

import { GroupService } from "./helper/app-settings/groups.service";
import { StorageService } from "./helper/app-settings/storage.service";
import { CardExchangeService } from "./helper/cardExchange.service";
import { PushViewModel } from "./helper/push/notification.service";

import { ModalDialogService } from "nativescript-angular/modal-dialog";
import { BottomMenuComponent } from "./bottom-menu/bottom-menu.component";
import { ShareModalComponent } from "./shareModal/share.modal";
import { PrepareShareComponent } from "./shareModal/prepare-send/prepare-send.component";
import { TargetUserItemComponent } from "./shareModal/target-user-item/target-user-item.component";
import { ContactPreviewComponent } from "./contact-preview/contact-preview.component";
import { ImageConvertPipe } from "./pipes/image.pipe";
import { CapitalizePipe } from "./pipes/capitalize.pipe";

const COMPONENTS = [
  ContactPreviewComponent,
  BottomMenuComponent,
  MyDrawerComponent,
  MyDrawerItemComponent,
  ShareModalComponent,
  TargetUserItemComponent,
  PrepareShareComponent
];

const PIPES = [ImageConvertPipe, CapitalizePipe];

const SERVICES = [
  StorageService,
  ModalDialogService,
  CardExchangeService,
  GroupService
];

const MODULES = [
  NativeScriptUISideDrawerModule,
  NativeScriptFormsModule,
  NativeScriptCommonModule,
  TNSCheckBoxModule
];

const MODELS = [PushViewModel];

@NgModule({
  imports: [...MODULES],
  declarations: [...COMPONENTS, ...PIPES],
  entryComponents: [ShareModalComponent],
  exports: [...COMPONENTS, ...PIPES, ...MODULES],
  providers: [...SERVICES, ...MODELS],
  schemas: [NO_ERRORS_SCHEMA]
})
export class SharedModule {}
