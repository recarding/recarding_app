"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var observable_1 = require("tns-core-modules/data/observable");
var messaging_1 = require("nativescript-plugin-firebase/messaging");
var dialogs_1 = require("tns-core-modules/ui/dialogs");
var platform = require("tns-core-modules/platform");
var applicationSettings = require("tns-core-modules/application-settings");
var getCircularReplacer = function () {
    var seen = new WeakSet();
    return function (key, value) {
        if (typeof value === "object" && value !== null) {
            if (seen.has(value)) {
                return;
            }
            seen.add(value);
        }
        return value;
    };
};
var PushViewModel = /** @class */ (function (_super) {
    __extends(PushViewModel, _super);
    function PushViewModel() {
        var _this = _super.call(this) || this;
        if (applicationSettings.getBoolean(PushViewModel.APP_REGISTERED_FOR_NOTIFICATIONS, false)) {
            _this.doRegisterPushHandlers();
        }
        return _this;
    }
    PushViewModel.prototype.doRequestConsent = function () {
        var _this = this;
        dialogs_1.confirm({
            title: "We'd like to send notifications",
            message: "Notifications are a big part of this app. Do you agree? Please do, we won't spam you. Promised.",
            okButtonText: "Yep!",
            cancelButtonText: "Maybe later"
        }).then(function (pushAllowed) {
            if (pushAllowed) {
                applicationSettings.setBoolean(PushViewModel.APP_REGISTERED_FOR_NOTIFICATIONS, true);
                _this.doRegisterForPushNotifications();
            }
        });
    };
    PushViewModel.prototype.doGetCurrentPushToken = function () {
        messaging_1.messaging
            .getCurrentPushToken()
            .then(function (token) {
            // may be null/undefined if not known yet
            dialogs_1.alert({
                title: "Current Push Token",
                message: !token
                    ? "Not received yet (note that on iOS this does not work on a simulator)"
                    : token + "\n\nSee the console log if you want to copy-paste it.",
                okButtonText: "OK, thx"
            });
        })
            .catch(function (err) { return console.log("Error in doGetCurrentPushToken: " + err); });
    };
    PushViewModel.prototype.doRegisterForInteractivePush = function () {
        if (!platform.isIOS) {
            console.log("##### Interactive push messaging is currently iOS-only!");
            console.log("##### Also, please make sure you don't include the 'click_action' notification property when pusing to Android.");
            return;
        }
        var model = new messaging_1.messaging.PushNotificationModel();
        model.iosSettings = new messaging_1.messaging.IosPushSettings();
        model.iosSettings.badge = false;
        model.iosSettings.alert = true;
        model.iosSettings.interactiveSettings = new messaging_1.messaging.IosInteractivePushSettings();
        model.iosSettings.interactiveSettings.actions = [
            {
                identifier: "OPEN_ACTION",
                title: "Open the app (if closed)",
                options: messaging_1.messaging.IosInteractiveNotificationActionOptions.foreground
            },
            {
                identifier: "AUTH",
                title: "Open the app, but only if device is not locked with a passcode",
                options: messaging_1.messaging.IosInteractiveNotificationActionOptions.foreground |
                    messaging_1.messaging.IosInteractiveNotificationActionOptions
                        .authenticationRequired
            },
            {
                identifier: "INPUT_ACTION",
                title: "Tap to reply without opening the app",
                type: "input",
                submitLabel: "Fire!",
                placeholder: "Load the gun..."
            },
            {
                identifier: "INPUT_ACTION",
                title: "Tap to reply and open the app",
                options: messaging_1.messaging.IosInteractiveNotificationActionOptions.foreground,
                type: "input",
                submitLabel: "OK, send it",
                placeholder: "Type here, baby!"
            },
            {
                identifier: "DELETE_ACTION",
                title: "Delete without opening the app",
                options: messaging_1.messaging.IosInteractiveNotificationActionOptions.destructive
            }
        ];
        model.iosSettings.interactiveSettings.categories = [
            {
                identifier: "GENERAL"
            }
        ];
        model.onNotificationActionTakenCallback = function (actionIdentifier, message, inputText) {
            console.log("onNotificationActionTakenCallback fired! \n\r Message: " + JSON.stringify(message) + ", \n\r Action taken: " + actionIdentifier);
            dialogs_1.alert({
                title: "Interactive push action",
                message: "Message: " + JSON.stringify(message) + ", \n\r Action taken: " + actionIdentifier +
                    (inputText ? ", \n\r Input text: " + inputText : ""),
                okButtonText: "Nice!"
            });
        };
        messaging_1.messaging.registerForInteractivePush(model);
        console.log("Registered for interactive push");
        dialogs_1.alert({
            title: "Registered for interactive push",
            okButtonText: "Thx!"
        });
    };
    // You could add these handlers in 'init', but if you want you can do it seperately as well.
    // The benefit being your user will not be confronted with the "Allow notifications" consent popup when 'init' runs.
    PushViewModel.prototype.doRegisterPushHandlers = function () {
        // note that this will implicitly register for push notifications, so there's no need to call 'registerForPushNotifications'
        messaging_1.messaging.addOnPushTokenReceivedCallback(function (token) {
            // you can use this token to send to your own backend server,
            // so you can send notifications to this specific device
            console.log("Firebase plugin received a push token: " + token);
            // var pasteboard = utils.ios.getter(UIPasteboard, UIPasteboard.generalPasteboard);
            // pasteboard.setValueForPasteboardType(token, kUTTypePlainText);
        });
        messaging_1.messaging
            .addOnMessageReceivedCallback(function (message) {
            console.log("Push message received in push-view-model: " +
                JSON.stringify(message, getCircularReplacer()));
            setTimeout(function () {
                dialogs_1.alert({
                    title: "Push message!",
                    message: message !== undefined && message.title !== undefined
                        ? message.title
                        : "",
                    okButtonText: "Sw33t"
                });
            }, 500);
        })
            .then(function () {
            console.log("Added addOnMessageReceivedCallback");
        }, function (err) {
            console.log("Failed to add addOnMessageReceivedCallback: " + err);
        });
    };
    PushViewModel.prototype.doUnregisterForPushNotifications = function () {
        messaging_1.messaging.unregisterForPushNotifications().then(function () {
            dialogs_1.alert({
                title: "Unregistered",
                message: "If you were registered, that is.",
                okButtonText: "Got it, thanks!"
            });
        });
    };
    PushViewModel.prototype.doRegisterForPushNotifications = function () {
        messaging_1.messaging
            .registerForPushNotifications({
            onPushTokenReceivedCallback: function (token) {
                console.log("Firebase plugin received a push token: " + token);
            },
            onMessageReceivedCallback: function (message) {
                console.log("Push message received in push-view-model: " +
                    JSON.stringify(message, getCircularReplacer()));
                setTimeout(function () {
                    dialogs_1.alert({
                        title: "Push message!",
                        message: message !== undefined && message.title !== undefined
                            ? message.title
                            : "",
                        okButtonText: "Sw33t"
                    });
                }, 500);
            },
            showNotifications: true,
            showNotificationsWhenInForeground: false
        })
            .then(function () { return console.log("Registered for push"); });
    };
    PushViewModel.prototype.doSubscribeToTopic = function () {
        messaging_1.messaging.subscribeToTopic("demo").then(function () {
            dialogs_1.alert({
                title: "Subscribed",
                message: ".. to the 'demo' topic",
                okButtonText: "Okay, interesting"
            });
        }, function (error) {
            dialogs_1.alert({
                title: "Subscribe error",
                message: error,
                okButtonText: "OK"
            });
        });
    };
    PushViewModel.prototype.doUnsubscribeFromTopic = function () {
        messaging_1.messaging.unsubscribeFromTopic("demo").then(function () {
            dialogs_1.alert({
                title: "Unsubscribed",
                message: ".. from the 'demo' topic",
                okButtonText: "Okay, very interesting"
            });
        }, function (error) {
            dialogs_1.alert({
                title: "Unsubscribe error",
                message: error,
                okButtonText: "OK"
            });
        });
    };
    PushViewModel.prototype.doGetAreNotificationsEnabled = function () {
        return messaging_1.messaging.areNotificationsEnabled();
    };
    PushViewModel.APP_REGISTERED_FOR_NOTIFICATIONS = "APP_REGISTERED_FOR_NOTIFICATIONS";
    return PushViewModel;
}(observable_1.Observable));
exports.PushViewModel = PushViewModel;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLnNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJub3RpZmljYXRpb24uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLCtEQUE4RDtBQUM5RCxvRUFBNEU7QUFDNUUsdURBQTZEO0FBQzdELG9EQUFzRDtBQUN0RCwyRUFBNkU7QUFFN0UsSUFBTSxtQkFBbUIsR0FBRztJQUMxQixJQUFNLElBQUksR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO0lBQzNCLE9BQU8sVUFBQyxHQUFHLEVBQUUsS0FBSztRQUNoQixJQUFJLE9BQU8sS0FBSyxLQUFLLFFBQVEsSUFBSSxLQUFLLEtBQUssSUFBSSxFQUFFO1lBQy9DLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBRTtnQkFDbkIsT0FBTzthQUNSO1lBQ0QsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNqQjtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQyxDQUFDO0FBQ0osQ0FBQyxDQUFDO0FBRUY7SUFBbUMsaUNBQVU7SUFJM0M7UUFBQSxZQUNFLGlCQUFPLFNBU1I7UUFSQyxJQUNFLG1CQUFtQixDQUFDLFVBQVUsQ0FDNUIsYUFBYSxDQUFDLGdDQUFnQyxFQUM5QyxLQUFLLENBQ04sRUFDRDtZQUNBLEtBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1NBQy9COztJQUNILENBQUM7SUFFTSx3Q0FBZ0IsR0FBdkI7UUFBQSxpQkFnQkM7UUFmQyxpQkFBTyxDQUFDO1lBQ04sS0FBSyxFQUFFLGlDQUFpQztZQUN4QyxPQUFPLEVBQ0wsaUdBQWlHO1lBQ25HLFlBQVksRUFBRSxNQUFNO1lBQ3BCLGdCQUFnQixFQUFFLGFBQWE7U0FDaEMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLFdBQVc7WUFDakIsSUFBSSxXQUFXLEVBQUU7Z0JBQ2YsbUJBQW1CLENBQUMsVUFBVSxDQUM1QixhQUFhLENBQUMsZ0NBQWdDLEVBQzlDLElBQUksQ0FDTCxDQUFDO2dCQUNGLEtBQUksQ0FBQyw4QkFBOEIsRUFBRSxDQUFDO2FBQ3ZDO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU0sNkNBQXFCLEdBQTVCO1FBQ0UscUJBQVM7YUFDTixtQkFBbUIsRUFBRTthQUNyQixJQUFJLENBQUMsVUFBQSxLQUFLO1lBQ1QseUNBQXlDO1lBQ3pDLGVBQUssQ0FBQztnQkFDSixLQUFLLEVBQUUsb0JBQW9CO2dCQUMzQixPQUFPLEVBQUUsQ0FBQyxLQUFLO29CQUNiLENBQUMsQ0FBQyx1RUFBdUU7b0JBQ3pFLENBQUMsQ0FBQyxLQUFLLEdBQUcsdURBQXVEO2dCQUNuRSxZQUFZLEVBQUUsU0FBUzthQUN4QixDQUFDLENBQUM7UUFDTCxDQUFDLENBQUM7YUFDRCxLQUFLLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxPQUFPLENBQUMsR0FBRyxDQUFDLGtDQUFrQyxHQUFHLEdBQUcsQ0FBQyxFQUFyRCxDQUFxRCxDQUFDLENBQUM7SUFDekUsQ0FBQztJQUVNLG9EQUE0QixHQUFuQztRQUNFLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFO1lBQ25CLE9BQU8sQ0FBQyxHQUFHLENBQUMseURBQXlELENBQUMsQ0FBQztZQUN2RSxPQUFPLENBQUMsR0FBRyxDQUNULGlIQUFpSCxDQUNsSCxDQUFDO1lBQ0YsT0FBTztTQUNSO1FBRUQsSUFBTSxLQUFLLEdBQUcsSUFBSSxxQkFBUyxDQUFDLHFCQUFxQixFQUFFLENBQUM7UUFDcEQsS0FBSyxDQUFDLFdBQVcsR0FBRyxJQUFJLHFCQUFTLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDcEQsS0FBSyxDQUFDLFdBQVcsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ2hDLEtBQUssQ0FBQyxXQUFXLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztRQUMvQixLQUFLLENBQUMsV0FBVyxDQUFDLG1CQUFtQixHQUFHLElBQUkscUJBQVMsQ0FBQywwQkFBMEIsRUFBRSxDQUFDO1FBQ25GLEtBQUssQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsT0FBTyxHQUFHO1lBQzlDO2dCQUNFLFVBQVUsRUFBRSxhQUFhO2dCQUN6QixLQUFLLEVBQUUsMEJBQTBCO2dCQUNqQyxPQUFPLEVBQUUscUJBQVMsQ0FBQyx1Q0FBdUMsQ0FBQyxVQUFVO2FBQ3RFO1lBQ0Q7Z0JBQ0UsVUFBVSxFQUFFLE1BQU07Z0JBQ2xCLEtBQUssRUFBRSxnRUFBZ0U7Z0JBQ3ZFLE9BQU8sRUFDTCxxQkFBUyxDQUFDLHVDQUF1QyxDQUFDLFVBQVU7b0JBQzVELHFCQUFTLENBQUMsdUNBQXVDO3lCQUM5QyxzQkFBc0I7YUFDNUI7WUFDRDtnQkFDRSxVQUFVLEVBQUUsY0FBYztnQkFDMUIsS0FBSyxFQUFFLHNDQUFzQztnQkFDN0MsSUFBSSxFQUFFLE9BQU87Z0JBQ2IsV0FBVyxFQUFFLE9BQU87Z0JBQ3BCLFdBQVcsRUFBRSxpQkFBaUI7YUFDL0I7WUFDRDtnQkFDRSxVQUFVLEVBQUUsY0FBYztnQkFDMUIsS0FBSyxFQUFFLCtCQUErQjtnQkFDdEMsT0FBTyxFQUFFLHFCQUFTLENBQUMsdUNBQXVDLENBQUMsVUFBVTtnQkFDckUsSUFBSSxFQUFFLE9BQU87Z0JBQ2IsV0FBVyxFQUFFLGFBQWE7Z0JBQzFCLFdBQVcsRUFBRSxrQkFBa0I7YUFDaEM7WUFDRDtnQkFDRSxVQUFVLEVBQUUsZUFBZTtnQkFDM0IsS0FBSyxFQUFFLGdDQUFnQztnQkFDdkMsT0FBTyxFQUFFLHFCQUFTLENBQUMsdUNBQXVDLENBQUMsV0FBVzthQUN2RTtTQUNGLENBQUM7UUFFRixLQUFLLENBQUMsV0FBVyxDQUFDLG1CQUFtQixDQUFDLFVBQVUsR0FBRztZQUNqRDtnQkFDRSxVQUFVLEVBQUUsU0FBUzthQUN0QjtTQUNGLENBQUM7UUFFRixLQUFLLENBQUMsaUNBQWlDLEdBQUcsVUFDeEMsZ0JBQXdCLEVBQ3hCLE9BQWdCLEVBQ2hCLFNBQWtCO1lBRWxCLE9BQU8sQ0FBQyxHQUFHLENBQ1QsNERBQTBELElBQUksQ0FBQyxTQUFTLENBQ3RFLE9BQU8sQ0FDUiw2QkFBd0IsZ0JBQWtCLENBQzVDLENBQUM7WUFFRixlQUFLLENBQUM7Z0JBQ0osS0FBSyxFQUFFLHlCQUF5QjtnQkFDaEMsT0FBTyxFQUNMLGNBQVksSUFBSSxDQUFDLFNBQVMsQ0FDeEIsT0FBTyxDQUNSLDZCQUF3QixnQkFBa0I7b0JBQzNDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyx3QkFBc0IsU0FBVyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQ3RELFlBQVksRUFBRSxPQUFPO2FBQ3RCLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQztRQUVGLHFCQUFTLENBQUMsMEJBQTBCLENBQUMsS0FBSyxDQUFDLENBQUM7UUFFNUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFDO1FBQy9DLGVBQUssQ0FBQztZQUNKLEtBQUssRUFBRSxpQ0FBaUM7WUFDeEMsWUFBWSxFQUFFLE1BQU07U0FDckIsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELDRGQUE0RjtJQUM1RixvSEFBb0g7SUFDN0csOENBQXNCLEdBQTdCO1FBQ0UsNEhBQTRIO1FBQzVILHFCQUFTLENBQUMsOEJBQThCLENBQUMsVUFBQSxLQUFLO1lBQzVDLDZEQUE2RDtZQUM3RCx3REFBd0Q7WUFDeEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyx5Q0FBeUMsR0FBRyxLQUFLLENBQUMsQ0FBQztZQUMvRCxtRkFBbUY7WUFDbkYsaUVBQWlFO1FBQ25FLENBQUMsQ0FBQyxDQUFDO1FBQ0gscUJBQVM7YUFDTiw0QkFBNEIsQ0FBQyxVQUFBLE9BQU87WUFDbkMsT0FBTyxDQUFDLEdBQUcsQ0FDVCw0Q0FBNEM7Z0JBQzFDLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxFQUFFLG1CQUFtQixFQUFFLENBQUMsQ0FDakQsQ0FBQztZQUVGLFVBQVUsQ0FBQztnQkFDVCxlQUFLLENBQUM7b0JBQ0osS0FBSyxFQUFFLGVBQWU7b0JBQ3RCLE9BQU8sRUFDTCxPQUFPLEtBQUssU0FBUyxJQUFJLE9BQU8sQ0FBQyxLQUFLLEtBQUssU0FBUzt3QkFDbEQsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLO3dCQUNmLENBQUMsQ0FBQyxFQUFFO29CQUNSLFlBQVksRUFBRSxPQUFPO2lCQUN0QixDQUFDLENBQUM7WUFDTCxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDVixDQUFDLENBQUM7YUFDRCxJQUFJLENBQ0g7WUFDRSxPQUFPLENBQUMsR0FBRyxDQUFDLG9DQUFvQyxDQUFDLENBQUM7UUFDcEQsQ0FBQyxFQUNELFVBQUEsR0FBRztZQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsOENBQThDLEdBQUcsR0FBRyxDQUFDLENBQUM7UUFDcEUsQ0FBQyxDQUNGLENBQUM7SUFDTixDQUFDO0lBRU0sd0RBQWdDLEdBQXZDO1FBQ0UscUJBQVMsQ0FBQyw4QkFBOEIsRUFBRSxDQUFDLElBQUksQ0FBQztZQUM5QyxlQUFLLENBQUM7Z0JBQ0osS0FBSyxFQUFFLGNBQWM7Z0JBQ3JCLE9BQU8sRUFBRSxrQ0FBa0M7Z0JBQzNDLFlBQVksRUFBRSxpQkFBaUI7YUFDaEMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU0sc0RBQThCLEdBQXJDO1FBQ0UscUJBQVM7YUFDTiw0QkFBNEIsQ0FBQztZQUM1QiwyQkFBMkIsRUFBRSxVQUFDLEtBQWE7Z0JBQ3pDLE9BQU8sQ0FBQyxHQUFHLENBQUMseUNBQXlDLEdBQUcsS0FBSyxDQUFDLENBQUM7WUFDakUsQ0FBQztZQUVELHlCQUF5QixFQUFFLFVBQUMsT0FBZ0I7Z0JBQzFDLE9BQU8sQ0FBQyxHQUFHLENBQ1QsNENBQTRDO29CQUMxQyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxDQUFDLENBQ2pELENBQUM7Z0JBRUYsVUFBVSxDQUFDO29CQUNULGVBQUssQ0FBQzt3QkFDSixLQUFLLEVBQUUsZUFBZTt3QkFDdEIsT0FBTyxFQUNMLE9BQU8sS0FBSyxTQUFTLElBQUksT0FBTyxDQUFDLEtBQUssS0FBSyxTQUFTOzRCQUNsRCxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUs7NEJBQ2YsQ0FBQyxDQUFDLEVBQUU7d0JBQ1IsWUFBWSxFQUFFLE9BQU87cUJBQ3RCLENBQUMsQ0FBQztnQkFDTCxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDVixDQUFDO1lBQ0QsaUJBQWlCLEVBQUUsSUFBSTtZQUN2QixpQ0FBaUMsRUFBRSxLQUFLO1NBQ3pDLENBQUM7YUFDRCxJQUFJLENBQUMsY0FBTSxPQUFBLE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsRUFBbEMsQ0FBa0MsQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFFTSwwQ0FBa0IsR0FBekI7UUFDRSxxQkFBUyxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FDckM7WUFDRSxlQUFLLENBQUM7Z0JBQ0osS0FBSyxFQUFFLFlBQVk7Z0JBQ25CLE9BQU8sRUFBRSx3QkFBd0I7Z0JBQ2pDLFlBQVksRUFBRSxtQkFBbUI7YUFDbEMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxFQUNELFVBQUEsS0FBSztZQUNILGVBQUssQ0FBQztnQkFDSixLQUFLLEVBQUUsaUJBQWlCO2dCQUN4QixPQUFPLEVBQUUsS0FBSztnQkFDZCxZQUFZLEVBQUUsSUFBSTthQUNuQixDQUFDLENBQUM7UUFDTCxDQUFDLENBQ0YsQ0FBQztJQUNKLENBQUM7SUFFTSw4Q0FBc0IsR0FBN0I7UUFDRSxxQkFBUyxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FDekM7WUFDRSxlQUFLLENBQUM7Z0JBQ0osS0FBSyxFQUFFLGNBQWM7Z0JBQ3JCLE9BQU8sRUFBRSwwQkFBMEI7Z0JBQ25DLFlBQVksRUFBRSx3QkFBd0I7YUFDdkMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxFQUNELFVBQUEsS0FBSztZQUNILGVBQUssQ0FBQztnQkFDSixLQUFLLEVBQUUsbUJBQW1CO2dCQUMxQixPQUFPLEVBQUUsS0FBSztnQkFDZCxZQUFZLEVBQUUsSUFBSTthQUNuQixDQUFDLENBQUM7UUFDTCxDQUFDLENBQ0YsQ0FBQztJQUNKLENBQUM7SUFFTSxvREFBNEIsR0FBbkM7UUFDRSxPQUFPLHFCQUFTLENBQUMsdUJBQXVCLEVBQUUsQ0FBQztJQUM3QyxDQUFDO0lBL1BjLDhDQUFnQyxHQUM3QyxrQ0FBa0MsQ0FBQztJQStQdkMsb0JBQUM7Q0FBQSxBQWpRRCxDQUFtQyx1QkFBVSxHQWlRNUM7QUFqUVksc0NBQWEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvZGF0YS9vYnNlcnZhYmxlXCI7XG5pbXBvcnQgeyBtZXNzYWdpbmcsIE1lc3NhZ2UgfSBmcm9tIFwibmF0aXZlc2NyaXB0LXBsdWdpbi1maXJlYmFzZS9tZXNzYWdpbmdcIjtcbmltcG9ydCB7IGFsZXJ0LCBjb25maXJtIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvZGlhbG9nc1wiO1xuaW1wb3J0ICogYXMgcGxhdGZvcm0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvcGxhdGZvcm1cIjtcbmltcG9ydCAqIGFzIGFwcGxpY2F0aW9uU2V0dGluZ3MgZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvYXBwbGljYXRpb24tc2V0dGluZ3NcIjtcblxuY29uc3QgZ2V0Q2lyY3VsYXJSZXBsYWNlciA9ICgpID0+IHtcbiAgY29uc3Qgc2VlbiA9IG5ldyBXZWFrU2V0KCk7XG4gIHJldHVybiAoa2V5LCB2YWx1ZSkgPT4ge1xuICAgIGlmICh0eXBlb2YgdmFsdWUgPT09IFwib2JqZWN0XCIgJiYgdmFsdWUgIT09IG51bGwpIHtcbiAgICAgIGlmIChzZWVuLmhhcyh2YWx1ZSkpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgc2Vlbi5hZGQodmFsdWUpO1xuICAgIH1cbiAgICByZXR1cm4gdmFsdWU7XG4gIH07XG59O1xuXG5leHBvcnQgY2xhc3MgUHVzaFZpZXdNb2RlbCBleHRlbmRzIE9ic2VydmFibGUge1xuICBwcml2YXRlIHN0YXRpYyBBUFBfUkVHSVNURVJFRF9GT1JfTk9USUZJQ0FUSU9OUyA9XG4gICAgXCJBUFBfUkVHSVNURVJFRF9GT1JfTk9USUZJQ0FUSU9OU1wiO1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHN1cGVyKCk7XG4gICAgaWYgKFxuICAgICAgYXBwbGljYXRpb25TZXR0aW5ncy5nZXRCb29sZWFuKFxuICAgICAgICBQdXNoVmlld01vZGVsLkFQUF9SRUdJU1RFUkVEX0ZPUl9OT1RJRklDQVRJT05TLFxuICAgICAgICBmYWxzZVxuICAgICAgKVxuICAgICkge1xuICAgICAgdGhpcy5kb1JlZ2lzdGVyUHVzaEhhbmRsZXJzKCk7XG4gICAgfVxuICB9XG5cbiAgcHVibGljIGRvUmVxdWVzdENvbnNlbnQoKTogdm9pZCB7XG4gICAgY29uZmlybSh7XG4gICAgICB0aXRsZTogXCJXZSdkIGxpa2UgdG8gc2VuZCBub3RpZmljYXRpb25zXCIsXG4gICAgICBtZXNzYWdlOlxuICAgICAgICBcIk5vdGlmaWNhdGlvbnMgYXJlIGEgYmlnIHBhcnQgb2YgdGhpcyBhcHAuIERvIHlvdSBhZ3JlZT8gUGxlYXNlIGRvLCB3ZSB3b24ndCBzcGFtIHlvdS4gUHJvbWlzZWQuXCIsXG4gICAgICBva0J1dHRvblRleHQ6IFwiWWVwIVwiLFxuICAgICAgY2FuY2VsQnV0dG9uVGV4dDogXCJNYXliZSBsYXRlclwiXG4gICAgfSkudGhlbihwdXNoQWxsb3dlZCA9PiB7XG4gICAgICBpZiAocHVzaEFsbG93ZWQpIHtcbiAgICAgICAgYXBwbGljYXRpb25TZXR0aW5ncy5zZXRCb29sZWFuKFxuICAgICAgICAgIFB1c2hWaWV3TW9kZWwuQVBQX1JFR0lTVEVSRURfRk9SX05PVElGSUNBVElPTlMsXG4gICAgICAgICAgdHJ1ZVxuICAgICAgICApO1xuICAgICAgICB0aGlzLmRvUmVnaXN0ZXJGb3JQdXNoTm90aWZpY2F0aW9ucygpO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgcHVibGljIGRvR2V0Q3VycmVudFB1c2hUb2tlbigpOiB2b2lkIHtcbiAgICBtZXNzYWdpbmdcbiAgICAgIC5nZXRDdXJyZW50UHVzaFRva2VuKClcbiAgICAgIC50aGVuKHRva2VuID0+IHtcbiAgICAgICAgLy8gbWF5IGJlIG51bGwvdW5kZWZpbmVkIGlmIG5vdCBrbm93biB5ZXRcbiAgICAgICAgYWxlcnQoe1xuICAgICAgICAgIHRpdGxlOiBcIkN1cnJlbnQgUHVzaCBUb2tlblwiLFxuICAgICAgICAgIG1lc3NhZ2U6ICF0b2tlblxuICAgICAgICAgICAgPyBcIk5vdCByZWNlaXZlZCB5ZXQgKG5vdGUgdGhhdCBvbiBpT1MgdGhpcyBkb2VzIG5vdCB3b3JrIG9uIGEgc2ltdWxhdG9yKVwiXG4gICAgICAgICAgICA6IHRva2VuICsgXCJcXG5cXG5TZWUgdGhlIGNvbnNvbGUgbG9nIGlmIHlvdSB3YW50IHRvIGNvcHktcGFzdGUgaXQuXCIsXG4gICAgICAgICAgb2tCdXR0b25UZXh0OiBcIk9LLCB0aHhcIlxuICAgICAgICB9KTtcbiAgICAgIH0pXG4gICAgICAuY2F0Y2goZXJyID0+IGNvbnNvbGUubG9nKFwiRXJyb3IgaW4gZG9HZXRDdXJyZW50UHVzaFRva2VuOiBcIiArIGVycikpO1xuICB9XG5cbiAgcHVibGljIGRvUmVnaXN0ZXJGb3JJbnRlcmFjdGl2ZVB1c2goKTogdm9pZCB7XG4gICAgaWYgKCFwbGF0Zm9ybS5pc0lPUykge1xuICAgICAgY29uc29sZS5sb2coXCIjIyMjIyBJbnRlcmFjdGl2ZSBwdXNoIG1lc3NhZ2luZyBpcyBjdXJyZW50bHkgaU9TLW9ubHkhXCIpO1xuICAgICAgY29uc29sZS5sb2coXG4gICAgICAgIFwiIyMjIyMgQWxzbywgcGxlYXNlIG1ha2Ugc3VyZSB5b3UgZG9uJ3QgaW5jbHVkZSB0aGUgJ2NsaWNrX2FjdGlvbicgbm90aWZpY2F0aW9uIHByb3BlcnR5IHdoZW4gcHVzaW5nIHRvIEFuZHJvaWQuXCJcbiAgICAgICk7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgY29uc3QgbW9kZWwgPSBuZXcgbWVzc2FnaW5nLlB1c2hOb3RpZmljYXRpb25Nb2RlbCgpO1xuICAgIG1vZGVsLmlvc1NldHRpbmdzID0gbmV3IG1lc3NhZ2luZy5Jb3NQdXNoU2V0dGluZ3MoKTtcbiAgICBtb2RlbC5pb3NTZXR0aW5ncy5iYWRnZSA9IGZhbHNlO1xuICAgIG1vZGVsLmlvc1NldHRpbmdzLmFsZXJ0ID0gdHJ1ZTtcbiAgICBtb2RlbC5pb3NTZXR0aW5ncy5pbnRlcmFjdGl2ZVNldHRpbmdzID0gbmV3IG1lc3NhZ2luZy5Jb3NJbnRlcmFjdGl2ZVB1c2hTZXR0aW5ncygpO1xuICAgIG1vZGVsLmlvc1NldHRpbmdzLmludGVyYWN0aXZlU2V0dGluZ3MuYWN0aW9ucyA9IFtcbiAgICAgIHtcbiAgICAgICAgaWRlbnRpZmllcjogXCJPUEVOX0FDVElPTlwiLFxuICAgICAgICB0aXRsZTogXCJPcGVuIHRoZSBhcHAgKGlmIGNsb3NlZClcIixcbiAgICAgICAgb3B0aW9uczogbWVzc2FnaW5nLklvc0ludGVyYWN0aXZlTm90aWZpY2F0aW9uQWN0aW9uT3B0aW9ucy5mb3JlZ3JvdW5kXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBpZGVudGlmaWVyOiBcIkFVVEhcIixcbiAgICAgICAgdGl0bGU6IFwiT3BlbiB0aGUgYXBwLCBidXQgb25seSBpZiBkZXZpY2UgaXMgbm90IGxvY2tlZCB3aXRoIGEgcGFzc2NvZGVcIixcbiAgICAgICAgb3B0aW9uczpcbiAgICAgICAgICBtZXNzYWdpbmcuSW9zSW50ZXJhY3RpdmVOb3RpZmljYXRpb25BY3Rpb25PcHRpb25zLmZvcmVncm91bmQgfFxuICAgICAgICAgIG1lc3NhZ2luZy5Jb3NJbnRlcmFjdGl2ZU5vdGlmaWNhdGlvbkFjdGlvbk9wdGlvbnNcbiAgICAgICAgICAgIC5hdXRoZW50aWNhdGlvblJlcXVpcmVkXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBpZGVudGlmaWVyOiBcIklOUFVUX0FDVElPTlwiLFxuICAgICAgICB0aXRsZTogXCJUYXAgdG8gcmVwbHkgd2l0aG91dCBvcGVuaW5nIHRoZSBhcHBcIixcbiAgICAgICAgdHlwZTogXCJpbnB1dFwiLFxuICAgICAgICBzdWJtaXRMYWJlbDogXCJGaXJlIVwiLFxuICAgICAgICBwbGFjZWhvbGRlcjogXCJMb2FkIHRoZSBndW4uLi5cIlxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgaWRlbnRpZmllcjogXCJJTlBVVF9BQ1RJT05cIixcbiAgICAgICAgdGl0bGU6IFwiVGFwIHRvIHJlcGx5IGFuZCBvcGVuIHRoZSBhcHBcIixcbiAgICAgICAgb3B0aW9uczogbWVzc2FnaW5nLklvc0ludGVyYWN0aXZlTm90aWZpY2F0aW9uQWN0aW9uT3B0aW9ucy5mb3JlZ3JvdW5kLFxuICAgICAgICB0eXBlOiBcImlucHV0XCIsXG4gICAgICAgIHN1Ym1pdExhYmVsOiBcIk9LLCBzZW5kIGl0XCIsXG4gICAgICAgIHBsYWNlaG9sZGVyOiBcIlR5cGUgaGVyZSwgYmFieSFcIlxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgaWRlbnRpZmllcjogXCJERUxFVEVfQUNUSU9OXCIsXG4gICAgICAgIHRpdGxlOiBcIkRlbGV0ZSB3aXRob3V0IG9wZW5pbmcgdGhlIGFwcFwiLFxuICAgICAgICBvcHRpb25zOiBtZXNzYWdpbmcuSW9zSW50ZXJhY3RpdmVOb3RpZmljYXRpb25BY3Rpb25PcHRpb25zLmRlc3RydWN0aXZlXG4gICAgICB9XG4gICAgXTtcblxuICAgIG1vZGVsLmlvc1NldHRpbmdzLmludGVyYWN0aXZlU2V0dGluZ3MuY2F0ZWdvcmllcyA9IFtcbiAgICAgIHtcbiAgICAgICAgaWRlbnRpZmllcjogXCJHRU5FUkFMXCJcbiAgICAgIH1cbiAgICBdO1xuXG4gICAgbW9kZWwub25Ob3RpZmljYXRpb25BY3Rpb25UYWtlbkNhbGxiYWNrID0gKFxuICAgICAgYWN0aW9uSWRlbnRpZmllcjogc3RyaW5nLFxuICAgICAgbWVzc2FnZTogTWVzc2FnZSxcbiAgICAgIGlucHV0VGV4dD86IHN0cmluZ1xuICAgICkgPT4ge1xuICAgICAgY29uc29sZS5sb2coXG4gICAgICAgIGBvbk5vdGlmaWNhdGlvbkFjdGlvblRha2VuQ2FsbGJhY2sgZmlyZWQhIFxcblxcciBNZXNzYWdlOiAke0pTT04uc3RyaW5naWZ5KFxuICAgICAgICAgIG1lc3NhZ2VcbiAgICAgICAgKX0sIFxcblxcciBBY3Rpb24gdGFrZW46ICR7YWN0aW9uSWRlbnRpZmllcn1gXG4gICAgICApO1xuXG4gICAgICBhbGVydCh7XG4gICAgICAgIHRpdGxlOiBcIkludGVyYWN0aXZlIHB1c2ggYWN0aW9uXCIsXG4gICAgICAgIG1lc3NhZ2U6XG4gICAgICAgICAgYE1lc3NhZ2U6ICR7SlNPTi5zdHJpbmdpZnkoXG4gICAgICAgICAgICBtZXNzYWdlXG4gICAgICAgICAgKX0sIFxcblxcciBBY3Rpb24gdGFrZW46ICR7YWN0aW9uSWRlbnRpZmllcn1gICtcbiAgICAgICAgICAoaW5wdXRUZXh0ID8gYCwgXFxuXFxyIElucHV0IHRleHQ6ICR7aW5wdXRUZXh0fWAgOiBcIlwiKSxcbiAgICAgICAgb2tCdXR0b25UZXh0OiBcIk5pY2UhXCJcbiAgICAgIH0pO1xuICAgIH07XG5cbiAgICBtZXNzYWdpbmcucmVnaXN0ZXJGb3JJbnRlcmFjdGl2ZVB1c2gobW9kZWwpO1xuXG4gICAgY29uc29sZS5sb2coXCJSZWdpc3RlcmVkIGZvciBpbnRlcmFjdGl2ZSBwdXNoXCIpO1xuICAgIGFsZXJ0KHtcbiAgICAgIHRpdGxlOiBcIlJlZ2lzdGVyZWQgZm9yIGludGVyYWN0aXZlIHB1c2hcIixcbiAgICAgIG9rQnV0dG9uVGV4dDogXCJUaHghXCJcbiAgICB9KTtcbiAgfVxuXG4gIC8vIFlvdSBjb3VsZCBhZGQgdGhlc2UgaGFuZGxlcnMgaW4gJ2luaXQnLCBidXQgaWYgeW91IHdhbnQgeW91IGNhbiBkbyBpdCBzZXBlcmF0ZWx5IGFzIHdlbGwuXG4gIC8vIFRoZSBiZW5lZml0IGJlaW5nIHlvdXIgdXNlciB3aWxsIG5vdCBiZSBjb25mcm9udGVkIHdpdGggdGhlIFwiQWxsb3cgbm90aWZpY2F0aW9uc1wiIGNvbnNlbnQgcG9wdXAgd2hlbiAnaW5pdCcgcnVucy5cbiAgcHVibGljIGRvUmVnaXN0ZXJQdXNoSGFuZGxlcnMoKTogdm9pZCB7XG4gICAgLy8gbm90ZSB0aGF0IHRoaXMgd2lsbCBpbXBsaWNpdGx5IHJlZ2lzdGVyIGZvciBwdXNoIG5vdGlmaWNhdGlvbnMsIHNvIHRoZXJlJ3Mgbm8gbmVlZCB0byBjYWxsICdyZWdpc3RlckZvclB1c2hOb3RpZmljYXRpb25zJ1xuICAgIG1lc3NhZ2luZy5hZGRPblB1c2hUb2tlblJlY2VpdmVkQ2FsbGJhY2sodG9rZW4gPT4ge1xuICAgICAgLy8geW91IGNhbiB1c2UgdGhpcyB0b2tlbiB0byBzZW5kIHRvIHlvdXIgb3duIGJhY2tlbmQgc2VydmVyLFxuICAgICAgLy8gc28geW91IGNhbiBzZW5kIG5vdGlmaWNhdGlvbnMgdG8gdGhpcyBzcGVjaWZpYyBkZXZpY2VcbiAgICAgIGNvbnNvbGUubG9nKFwiRmlyZWJhc2UgcGx1Z2luIHJlY2VpdmVkIGEgcHVzaCB0b2tlbjogXCIgKyB0b2tlbik7XG4gICAgICAvLyB2YXIgcGFzdGVib2FyZCA9IHV0aWxzLmlvcy5nZXR0ZXIoVUlQYXN0ZWJvYXJkLCBVSVBhc3RlYm9hcmQuZ2VuZXJhbFBhc3RlYm9hcmQpO1xuICAgICAgLy8gcGFzdGVib2FyZC5zZXRWYWx1ZUZvclBhc3RlYm9hcmRUeXBlKHRva2VuLCBrVVRUeXBlUGxhaW5UZXh0KTtcbiAgICB9KTtcbiAgICBtZXNzYWdpbmdcbiAgICAgIC5hZGRPbk1lc3NhZ2VSZWNlaXZlZENhbGxiYWNrKG1lc3NhZ2UgPT4ge1xuICAgICAgICBjb25zb2xlLmxvZyhcbiAgICAgICAgICBcIlB1c2ggbWVzc2FnZSByZWNlaXZlZCBpbiBwdXNoLXZpZXctbW9kZWw6IFwiICtcbiAgICAgICAgICAgIEpTT04uc3RyaW5naWZ5KG1lc3NhZ2UsIGdldENpcmN1bGFyUmVwbGFjZXIoKSlcbiAgICAgICAgKTtcblxuICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICBhbGVydCh7XG4gICAgICAgICAgICB0aXRsZTogXCJQdXNoIG1lc3NhZ2UhXCIsXG4gICAgICAgICAgICBtZXNzYWdlOlxuICAgICAgICAgICAgICBtZXNzYWdlICE9PSB1bmRlZmluZWQgJiYgbWVzc2FnZS50aXRsZSAhPT0gdW5kZWZpbmVkXG4gICAgICAgICAgICAgICAgPyBtZXNzYWdlLnRpdGxlXG4gICAgICAgICAgICAgICAgOiBcIlwiLFxuICAgICAgICAgICAgb2tCdXR0b25UZXh0OiBcIlN3MzN0XCJcbiAgICAgICAgICB9KTtcbiAgICAgICAgfSwgNTAwKTtcbiAgICAgIH0pXG4gICAgICAudGhlbihcbiAgICAgICAgKCkgPT4ge1xuICAgICAgICAgIGNvbnNvbGUubG9nKFwiQWRkZWQgYWRkT25NZXNzYWdlUmVjZWl2ZWRDYWxsYmFja1wiKTtcbiAgICAgICAgfSxcbiAgICAgICAgZXJyID0+IHtcbiAgICAgICAgICBjb25zb2xlLmxvZyhcIkZhaWxlZCB0byBhZGQgYWRkT25NZXNzYWdlUmVjZWl2ZWRDYWxsYmFjazogXCIgKyBlcnIpO1xuICAgICAgICB9XG4gICAgICApO1xuICB9XG5cbiAgcHVibGljIGRvVW5yZWdpc3RlckZvclB1c2hOb3RpZmljYXRpb25zKCk6IHZvaWQge1xuICAgIG1lc3NhZ2luZy51bnJlZ2lzdGVyRm9yUHVzaE5vdGlmaWNhdGlvbnMoKS50aGVuKCgpID0+IHtcbiAgICAgIGFsZXJ0KHtcbiAgICAgICAgdGl0bGU6IFwiVW5yZWdpc3RlcmVkXCIsXG4gICAgICAgIG1lc3NhZ2U6IFwiSWYgeW91IHdlcmUgcmVnaXN0ZXJlZCwgdGhhdCBpcy5cIixcbiAgICAgICAgb2tCdXR0b25UZXh0OiBcIkdvdCBpdCwgdGhhbmtzIVwiXG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIHB1YmxpYyBkb1JlZ2lzdGVyRm9yUHVzaE5vdGlmaWNhdGlvbnMoKTogdm9pZCB7XG4gICAgbWVzc2FnaW5nXG4gICAgICAucmVnaXN0ZXJGb3JQdXNoTm90aWZpY2F0aW9ucyh7XG4gICAgICAgIG9uUHVzaFRva2VuUmVjZWl2ZWRDYWxsYmFjazogKHRva2VuOiBzdHJpbmcpOiB2b2lkID0+IHtcbiAgICAgICAgICBjb25zb2xlLmxvZyhcIkZpcmViYXNlIHBsdWdpbiByZWNlaXZlZCBhIHB1c2ggdG9rZW46IFwiICsgdG9rZW4pO1xuICAgICAgICB9LFxuXG4gICAgICAgIG9uTWVzc2FnZVJlY2VpdmVkQ2FsbGJhY2s6IChtZXNzYWdlOiBNZXNzYWdlKSA9PiB7XG4gICAgICAgICAgY29uc29sZS5sb2coXG4gICAgICAgICAgICBcIlB1c2ggbWVzc2FnZSByZWNlaXZlZCBpbiBwdXNoLXZpZXctbW9kZWw6IFwiICtcbiAgICAgICAgICAgICAgSlNPTi5zdHJpbmdpZnkobWVzc2FnZSwgZ2V0Q2lyY3VsYXJSZXBsYWNlcigpKVxuICAgICAgICAgICk7XG5cbiAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgIGFsZXJ0KHtcbiAgICAgICAgICAgICAgdGl0bGU6IFwiUHVzaCBtZXNzYWdlIVwiLFxuICAgICAgICAgICAgICBtZXNzYWdlOlxuICAgICAgICAgICAgICAgIG1lc3NhZ2UgIT09IHVuZGVmaW5lZCAmJiBtZXNzYWdlLnRpdGxlICE9PSB1bmRlZmluZWRcbiAgICAgICAgICAgICAgICAgID8gbWVzc2FnZS50aXRsZVxuICAgICAgICAgICAgICAgICAgOiBcIlwiLFxuICAgICAgICAgICAgICBva0J1dHRvblRleHQ6IFwiU3czM3RcIlxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfSwgNTAwKTtcbiAgICAgICAgfSxcbiAgICAgICAgc2hvd05vdGlmaWNhdGlvbnM6IHRydWUsXG4gICAgICAgIHNob3dOb3RpZmljYXRpb25zV2hlbkluRm9yZWdyb3VuZDogZmFsc2VcbiAgICAgIH0pXG4gICAgICAudGhlbigoKSA9PiBjb25zb2xlLmxvZyhcIlJlZ2lzdGVyZWQgZm9yIHB1c2hcIikpO1xuICB9XG5cbiAgcHVibGljIGRvU3Vic2NyaWJlVG9Ub3BpYygpOiB2b2lkIHtcbiAgICBtZXNzYWdpbmcuc3Vic2NyaWJlVG9Ub3BpYyhcImRlbW9cIikudGhlbihcbiAgICAgICgpID0+IHtcbiAgICAgICAgYWxlcnQoe1xuICAgICAgICAgIHRpdGxlOiBcIlN1YnNjcmliZWRcIixcbiAgICAgICAgICBtZXNzYWdlOiBcIi4uIHRvIHRoZSAnZGVtbycgdG9waWNcIixcbiAgICAgICAgICBva0J1dHRvblRleHQ6IFwiT2theSwgaW50ZXJlc3RpbmdcIlxuICAgICAgICB9KTtcbiAgICAgIH0sXG4gICAgICBlcnJvciA9PiB7XG4gICAgICAgIGFsZXJ0KHtcbiAgICAgICAgICB0aXRsZTogXCJTdWJzY3JpYmUgZXJyb3JcIixcbiAgICAgICAgICBtZXNzYWdlOiBlcnJvcixcbiAgICAgICAgICBva0J1dHRvblRleHQ6IFwiT0tcIlxuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICApO1xuICB9XG5cbiAgcHVibGljIGRvVW5zdWJzY3JpYmVGcm9tVG9waWMoKTogdm9pZCB7XG4gICAgbWVzc2FnaW5nLnVuc3Vic2NyaWJlRnJvbVRvcGljKFwiZGVtb1wiKS50aGVuKFxuICAgICAgKCkgPT4ge1xuICAgICAgICBhbGVydCh7XG4gICAgICAgICAgdGl0bGU6IFwiVW5zdWJzY3JpYmVkXCIsXG4gICAgICAgICAgbWVzc2FnZTogXCIuLiBmcm9tIHRoZSAnZGVtbycgdG9waWNcIixcbiAgICAgICAgICBva0J1dHRvblRleHQ6IFwiT2theSwgdmVyeSBpbnRlcmVzdGluZ1wiXG4gICAgICAgIH0pO1xuICAgICAgfSxcbiAgICAgIGVycm9yID0+IHtcbiAgICAgICAgYWxlcnQoe1xuICAgICAgICAgIHRpdGxlOiBcIlVuc3Vic2NyaWJlIGVycm9yXCIsXG4gICAgICAgICAgbWVzc2FnZTogZXJyb3IsXG4gICAgICAgICAgb2tCdXR0b25UZXh0OiBcIk9LXCJcbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgKTtcbiAgfVxuXG4gIHB1YmxpYyBkb0dldEFyZU5vdGlmaWNhdGlvbnNFbmFibGVkKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiBtZXNzYWdpbmcuYXJlTm90aWZpY2F0aW9uc0VuYWJsZWQoKTtcbiAgfVxufVxuIl19