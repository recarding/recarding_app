import * as http from "http";
import {platformNames} from "platform";
import {device} from "platform";

export class CardExchangeService {
  uri: string;
  nativePlatformLocalhost;
  constructor() {

   if(device.os === platformNames.ios){
    /*localhost for ios*/
    this.nativePlatformLocalhost= "localhost";
  }
 else if(device.os === platformNames.android){
   /*localhost for android*/
   this.nativePlatformLocalhost= "10.0.2.2";
   }
  }

  fetchNearbyUsers(geolocation: object): Promise<any> {
    return http.request({
      url: "http://localhost:5000/user/fetch_near_users",
      method: "POST",
      headers: { "Content-Type": "application/json" },
      content: JSON.stringify(geolocation)
    });
  }

  getProfileByID(id: string): Promise<any> {
    const url = `http://${this.nativePlatformLocalhost}:5000/user/${id}`
    return http.getJSON(url.replace(/ /g, "%20"));
  }

  sendTargetUsers(users: Object) {
    return http.request({
      url: "http://localhost:5000/cards/push",
      method: "POST",
      headers: { "Content-Type": "application/json" },
      content: JSON.stringify(users)
    });
  }
}
