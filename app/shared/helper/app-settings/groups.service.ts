import { Injectable } from "@angular/core";
import { IGroups } from "./groups.model";

import * as appSettings from "application-settings";

@Injectable()
export class GroupService {
  groups: IGroups;

  getContactGroups(): object {
    const groups = appSettings.getString("contact_groups");
    if (groups) {
      return JSON.parse(groups);
    } else {
      const firstObject = {contact_groups: {}};
      appSettings.setString("contact_groups", JSON.stringify(firstObject));

      return firstObject;
    }
  }

  addNewContactGroup(groupName: string): void {
    const groups = appSettings.getString("contact_groups");
    if (groups != null) {
      this.groups = JSON.parse(groups);
      this.groups.contact_groups[groupName] = [];
    } else {
      this.groups = {contact_groups: {}};
      this.groups.contact_groups[groupName] = [];
    }
    appSettings.setString("contact_groups", JSON.stringify(this.groups));
  }

  addContactToGroup(unique_id: string, groupName: string): void {
    const groups = appSettings.getString("contact_groups");
    if (groups) {
      const groupObj = JSON.parse(groups);
      groupObj.contact_groups[groupName].push(unique_id);
      appSettings.setString("contact_groups", JSON.stringify(groupObj));
    }
  }
}
