import { Injectable } from "@angular/core";
import { IContact } from "./storage.model";

import * as appSettings from "application-settings";

@Injectable()
export class StorageService {
  contacts: Array<IContact>;

  getContacts(type: string): Array<IContact> {
    const list = appSettings.getString(type);
    if (list) {
      const sortedList = JSON.parse(list);
      sortedList.sort((a, b) => {
        if (a.name.family < b.name.family) { return -1; }
        if (a.name.family > b.name.family) { return 1; }

        return 0;
      });

      return sortedList;
    }

    return [];
  }

  add(contact: IContact, type: string): void {
    this.contacts = this.getContacts(type);
    this.contacts.push(contact);
    appSettings.setString(type, JSON.stringify(this.contacts));
  }

  getById(id: string, type: string): IContact {
    this.contacts = this.getContacts(type);
    const index = this.contacts.findIndex((contact) => contact.id === id);

    return this.contacts[index];
  }

  update(contact: IContact, type: string): boolean {
    const removed = this.remove(contact.id, type);
    if (removed) {
      this.add(contact, type);

      return true;
    } else {

      return false;
    }
  }

  remove(id: string, type: string): boolean {
    try {
      this.contacts = this.getContacts(type);
      const index = this.contacts.findIndex((contact) => contact.id === id);
      this.contacts.splice(index, 1);
      appSettings.setString(type, JSON.stringify(this.contacts));

      return true;
    } catch (e) {
      console.error(e);

      return false;
    }
  }

  setUnsyncedProfiles(freshProfile: IContact): void {
    const newUnsynced = {
      email: freshProfile.emailAddress,
      file: freshProfile.cardImagePath
    }
    const unSyncedString = appSettings.getString("unSyncedCards") || `{"cards":[]}`;
    const unSynced = JSON.parse(unSyncedString);
    unSynced.cards.push(newUnsynced);
    appSettings.setString("unSyncedCards", JSON.stringify(unSynced));
  }

  removeFromUnsynced(email: string): void{
    const unSyncedString = appSettings.getString("unSyncedCards");
    const unSynced = JSON.parse(unSyncedString);
    if (unSynced.cards.length) {
      unSynced.cards.forEach((card, index) => {
        if (card.email === email) {
          unSynced.cards.splice(index, 1);
        }
      });
      this.clearUnSynced()
      unSynced.cards.forEach(card => {
        this.setUnsyncedProfiles(card);
      })
    }
  }

  getUnsyncedCards(): string {
    return appSettings.getString("unSyncedCards") || `{"cards":[]}`;
  }

  clearUnSynced(): void {
    appSettings.setString("unSyncedCards", `{"cards":[]}`);
  }

}
