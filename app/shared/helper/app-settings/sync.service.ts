import { Injectable } from "@angular/core";

import { AuthService } from "../auth.service";
const http = require("tns-core-modules/http");

import * as bgHttp from "nativescript-background-http";
const session = bgHttp.session("image-upload");

import { IUnsycedCards } from './storage.model';

@Injectable()
export class SyncService {
  serverUrl: string = "http://0.0.0.0:5000/";
  token: string = this.authService.getAuthToken() || "";
  public tasks: bgHttp.Task[] = [];
  public events: { eventTitle: string, eventData: any }[] = [];


  constructor(
    private authService: AuthService
  ) { }

  getUserProfile(token: string): Promise<any> {
    return http.request({
      url: `${this.serverUrl}user/`,
      method: "GET",
      headers: { "Content-Type": "application/json" },
    });
  }

  syncNewCards(files: IUnsycedCards) {
    const request = {
      url: `${this.serverUrl}cards/upload`,
      method: "POST",
      headers: {
        "Content-Type": "application/octet-stream",
        "File-Name": "files"
      },
      description: '',
      // androidAutoDeleteAfterUpload: false,
      // androidNotificationTitle: 'NativeScript HTTP background',
    };
    let params = []
    files.cards.forEach(card => {
      let fileName = card.file.slice(card.file.lastIndexOf('/') + 1)
      params.push({ name: card.email, fileName, mimeType: 'image/jpeg' })
    });
    return session.multipartUpload(params, request);
  }
}
