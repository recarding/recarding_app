"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var appSettings = require("application-settings");
var GroupService = /** @class */ (function () {
    function GroupService() {
    }
    GroupService.prototype.getContactGroups = function () {
        var groups = appSettings.getString("contact_groups");
        if (groups) {
            return JSON.parse(groups);
        }
        else {
            var firstObject = { contact_groups: {} };
            appSettings.setString("contact_groups", JSON.stringify(firstObject));
            return firstObject;
        }
    };
    GroupService.prototype.addNewContactGroup = function (groupName) {
        var groups = appSettings.getString("contact_groups");
        if (groups != null) {
            this.groups = JSON.parse(groups);
            this.groups.contact_groups[groupName] = [];
        }
        else {
            this.groups = { contact_groups: {} };
            this.groups.contact_groups[groupName] = [];
        }
        appSettings.setString("contact_groups", JSON.stringify(this.groups));
    };
    GroupService.prototype.addContactToGroup = function (unique_id, groupName) {
        var groups = appSettings.getString("contact_groups");
        if (groups) {
            var groupObj = JSON.parse(groups);
            groupObj.contact_groups[groupName].push(unique_id);
            appSettings.setString("contact_groups", JSON.stringify(groupObj));
        }
    };
    GroupService = __decorate([
        core_1.Injectable()
    ], GroupService);
    return GroupService;
}());
exports.GroupService = GroupService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ3JvdXBzLnNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJncm91cHMuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUEyQztBQUczQyxrREFBb0Q7QUFHcEQ7SUFBQTtJQW1DQSxDQUFDO0lBaENDLHVDQUFnQixHQUFoQjtRQUNFLElBQU0sTUFBTSxHQUFHLFdBQVcsQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUN2RCxJQUFJLE1BQU0sRUFBRTtZQUNWLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUMzQjthQUFNO1lBQ0wsSUFBTSxXQUFXLEdBQUcsRUFBQyxjQUFjLEVBQUUsRUFBRSxFQUFDLENBQUM7WUFDekMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7WUFFckUsT0FBTyxXQUFXLENBQUM7U0FDcEI7SUFDSCxDQUFDO0lBRUQseUNBQWtCLEdBQWxCLFVBQW1CLFNBQWlCO1FBQ2xDLElBQU0sTUFBTSxHQUFHLFdBQVcsQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUN2RCxJQUFJLE1BQU0sSUFBSSxJQUFJLEVBQUU7WUFDbEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ2pDLElBQUksQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztTQUM1QzthQUFNO1lBQ0wsSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFDLGNBQWMsRUFBRSxFQUFFLEVBQUMsQ0FBQztZQUNuQyxJQUFJLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFLENBQUM7U0FDNUM7UUFDRCxXQUFXLENBQUMsU0FBUyxDQUFDLGdCQUFnQixFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7SUFDdkUsQ0FBQztJQUVELHdDQUFpQixHQUFqQixVQUFrQixTQUFpQixFQUFFLFNBQWlCO1FBQ3BELElBQU0sTUFBTSxHQUFHLFdBQVcsQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUN2RCxJQUFJLE1BQU0sRUFBRTtZQUNWLElBQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDcEMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDbkQsV0FBVyxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7U0FDbkU7SUFDSCxDQUFDO0lBbENVLFlBQVk7UUFEeEIsaUJBQVUsRUFBRTtPQUNBLFlBQVksQ0FtQ3hCO0lBQUQsbUJBQUM7Q0FBQSxBQW5DRCxJQW1DQztBQW5DWSxvQ0FBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgSUdyb3VwcyB9IGZyb20gXCIuL2dyb3Vwcy5tb2RlbFwiO1xuXG5pbXBvcnQgKiBhcyBhcHBTZXR0aW5ncyBmcm9tIFwiYXBwbGljYXRpb24tc2V0dGluZ3NcIjtcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIEdyb3VwU2VydmljZSB7XG4gIGdyb3VwczogSUdyb3VwcztcblxuICBnZXRDb250YWN0R3JvdXBzKCk6IG9iamVjdCB7XG4gICAgY29uc3QgZ3JvdXBzID0gYXBwU2V0dGluZ3MuZ2V0U3RyaW5nKFwiY29udGFjdF9ncm91cHNcIik7XG4gICAgaWYgKGdyb3Vwcykge1xuICAgICAgcmV0dXJuIEpTT04ucGFyc2UoZ3JvdXBzKTtcbiAgICB9IGVsc2Uge1xuICAgICAgY29uc3QgZmlyc3RPYmplY3QgPSB7Y29udGFjdF9ncm91cHM6IHt9fTtcbiAgICAgIGFwcFNldHRpbmdzLnNldFN0cmluZyhcImNvbnRhY3RfZ3JvdXBzXCIsIEpTT04uc3RyaW5naWZ5KGZpcnN0T2JqZWN0KSk7XG5cbiAgICAgIHJldHVybiBmaXJzdE9iamVjdDtcbiAgICB9XG4gIH1cblxuICBhZGROZXdDb250YWN0R3JvdXAoZ3JvdXBOYW1lOiBzdHJpbmcpOiB2b2lkIHtcbiAgICBjb25zdCBncm91cHMgPSBhcHBTZXR0aW5ncy5nZXRTdHJpbmcoXCJjb250YWN0X2dyb3Vwc1wiKTtcbiAgICBpZiAoZ3JvdXBzICE9IG51bGwpIHtcbiAgICAgIHRoaXMuZ3JvdXBzID0gSlNPTi5wYXJzZShncm91cHMpO1xuICAgICAgdGhpcy5ncm91cHMuY29udGFjdF9ncm91cHNbZ3JvdXBOYW1lXSA9IFtdO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmdyb3VwcyA9IHtjb250YWN0X2dyb3Vwczoge319O1xuICAgICAgdGhpcy5ncm91cHMuY29udGFjdF9ncm91cHNbZ3JvdXBOYW1lXSA9IFtdO1xuICAgIH1cbiAgICBhcHBTZXR0aW5ncy5zZXRTdHJpbmcoXCJjb250YWN0X2dyb3Vwc1wiLCBKU09OLnN0cmluZ2lmeSh0aGlzLmdyb3VwcykpO1xuICB9XG5cbiAgYWRkQ29udGFjdFRvR3JvdXAodW5pcXVlX2lkOiBzdHJpbmcsIGdyb3VwTmFtZTogc3RyaW5nKTogdm9pZCB7XG4gICAgY29uc3QgZ3JvdXBzID0gYXBwU2V0dGluZ3MuZ2V0U3RyaW5nKFwiY29udGFjdF9ncm91cHNcIik7XG4gICAgaWYgKGdyb3Vwcykge1xuICAgICAgY29uc3QgZ3JvdXBPYmogPSBKU09OLnBhcnNlKGdyb3Vwcyk7XG4gICAgICBncm91cE9iai5jb250YWN0X2dyb3Vwc1tncm91cE5hbWVdLnB1c2godW5pcXVlX2lkKTtcbiAgICAgIGFwcFNldHRpbmdzLnNldFN0cmluZyhcImNvbnRhY3RfZ3JvdXBzXCIsIEpTT04uc3RyaW5naWZ5KGdyb3VwT2JqKSk7XG4gICAgfVxuICB9XG59XG4iXX0=