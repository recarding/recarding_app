"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var auth_service_1 = require("../auth.service");
var http = require("tns-core-modules/http");
var bgHttp = require("nativescript-background-http");
var session = bgHttp.session("image-upload");
var SyncService = /** @class */ (function () {
    function SyncService(authService) {
        this.authService = authService;
        this.serverUrl = "http://0.0.0.0:5000/";
        this.token = this.authService.getAuthToken() || "";
        this.tasks = [];
        this.events = [];
    }
    SyncService.prototype.getUserProfile = function (token) {
        return http.request({
            url: this.serverUrl + "user/",
            method: "GET",
            headers: { "Content-Type": "application/json" },
        });
    };
    SyncService.prototype.syncNewCards = function (files) {
        var request = {
            url: this.serverUrl + "cards/upload",
            method: "POST",
            headers: {
                "Content-Type": "application/octet-stream",
                "File-Name": "files"
            },
            description: '',
        };
        var params = [];
        files.cards.forEach(function (card) {
            var fileName = card.file.slice(card.file.lastIndexOf('/') + 1);
            params.push({ name: card.email, fileName: fileName, mimeType: 'image/jpeg' });
        });
        return session.multipartUpload(params, request);
    };
    SyncService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [auth_service_1.AuthService])
    ], SyncService);
    return SyncService;
}());
exports.SyncService = SyncService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3luYy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsic3luYy5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTJDO0FBRTNDLGdEQUE4QztBQUM5QyxJQUFNLElBQUksR0FBRyxPQUFPLENBQUMsdUJBQXVCLENBQUMsQ0FBQztBQUU5QyxxREFBdUQ7QUFDdkQsSUFBTSxPQUFPLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQztBQUsvQztJQU9FLHFCQUNVLFdBQXdCO1FBQXhCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBUGxDLGNBQVMsR0FBVyxzQkFBc0IsQ0FBQztRQUMzQyxVQUFLLEdBQVcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLEVBQUUsSUFBSSxFQUFFLENBQUM7UUFDL0MsVUFBSyxHQUFrQixFQUFFLENBQUM7UUFDMUIsV0FBTSxHQUE2QyxFQUFFLENBQUM7SUFLekQsQ0FBQztJQUVMLG9DQUFjLEdBQWQsVUFBZSxLQUFhO1FBQzFCLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQztZQUNsQixHQUFHLEVBQUssSUFBSSxDQUFDLFNBQVMsVUFBTztZQUM3QixNQUFNLEVBQUUsS0FBSztZQUNiLE9BQU8sRUFBRSxFQUFFLGNBQWMsRUFBRSxrQkFBa0IsRUFBRTtTQUNoRCxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsa0NBQVksR0FBWixVQUFhLEtBQW9CO1FBQy9CLElBQU0sT0FBTyxHQUFHO1lBQ2QsR0FBRyxFQUFLLElBQUksQ0FBQyxTQUFTLGlCQUFjO1lBQ3BDLE1BQU0sRUFBRSxNQUFNO1lBQ2QsT0FBTyxFQUFFO2dCQUNQLGNBQWMsRUFBRSwwQkFBMEI7Z0JBQzFDLFdBQVcsRUFBRSxPQUFPO2FBQ3JCO1lBQ0QsV0FBVyxFQUFFLEVBQUU7U0FHaEIsQ0FBQztRQUNGLElBQUksTUFBTSxHQUFHLEVBQUUsQ0FBQTtRQUNmLEtBQUssQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTtZQUN0QixJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQTtZQUM5RCxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsUUFBUSxVQUFBLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxDQUFDLENBQUE7UUFDckUsQ0FBQyxDQUFDLENBQUM7UUFDSCxPQUFPLE9BQU8sQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0lBQ2xELENBQUM7SUFyQ1UsV0FBVztRQUR2QixpQkFBVSxFQUFFO3lDQVNZLDBCQUFXO09BUnZCLFdBQVcsQ0FzQ3ZCO0lBQUQsa0JBQUM7Q0FBQSxBQXRDRCxJQXNDQztBQXRDWSxrQ0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuXG5pbXBvcnQgeyBBdXRoU2VydmljZSB9IGZyb20gXCIuLi9hdXRoLnNlcnZpY2VcIjtcbmNvbnN0IGh0dHAgPSByZXF1aXJlKFwidG5zLWNvcmUtbW9kdWxlcy9odHRwXCIpO1xuXG5pbXBvcnQgKiBhcyBiZ0h0dHAgZnJvbSBcIm5hdGl2ZXNjcmlwdC1iYWNrZ3JvdW5kLWh0dHBcIjtcbmNvbnN0IHNlc3Npb24gPSBiZ0h0dHAuc2Vzc2lvbihcImltYWdlLXVwbG9hZFwiKTtcblxuaW1wb3J0IHsgSVVuc3ljZWRDYXJkcyB9IGZyb20gJy4vc3RvcmFnZS5tb2RlbCc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBTeW5jU2VydmljZSB7XG4gIHNlcnZlclVybDogc3RyaW5nID0gXCJodHRwOi8vMC4wLjAuMDo1MDAwL1wiO1xuICB0b2tlbjogc3RyaW5nID0gdGhpcy5hdXRoU2VydmljZS5nZXRBdXRoVG9rZW4oKSB8fCBcIlwiO1xuICBwdWJsaWMgdGFza3M6IGJnSHR0cC5UYXNrW10gPSBbXTtcbiAgcHVibGljIGV2ZW50czogeyBldmVudFRpdGxlOiBzdHJpbmcsIGV2ZW50RGF0YTogYW55IH1bXSA9IFtdO1xuXG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBhdXRoU2VydmljZTogQXV0aFNlcnZpY2VcbiAgKSB7IH1cblxuICBnZXRVc2VyUHJvZmlsZSh0b2tlbjogc3RyaW5nKTogUHJvbWlzZTxhbnk+IHtcbiAgICByZXR1cm4gaHR0cC5yZXF1ZXN0KHtcbiAgICAgIHVybDogYCR7dGhpcy5zZXJ2ZXJVcmx9dXNlci9gLFxuICAgICAgbWV0aG9kOiBcIkdFVFwiLFxuICAgICAgaGVhZGVyczogeyBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL2pzb25cIiB9LFxuICAgIH0pO1xuICB9XG5cbiAgc3luY05ld0NhcmRzKGZpbGVzOiBJVW5zeWNlZENhcmRzKSB7XG4gICAgY29uc3QgcmVxdWVzdCA9IHtcbiAgICAgIHVybDogYCR7dGhpcy5zZXJ2ZXJVcmx9Y2FyZHMvdXBsb2FkYCxcbiAgICAgIG1ldGhvZDogXCJQT1NUXCIsXG4gICAgICBoZWFkZXJzOiB7XG4gICAgICAgIFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24vb2N0ZXQtc3RyZWFtXCIsXG4gICAgICAgIFwiRmlsZS1OYW1lXCI6IFwiZmlsZXNcIlxuICAgICAgfSxcbiAgICAgIGRlc2NyaXB0aW9uOiAnJyxcbiAgICAgIC8vIGFuZHJvaWRBdXRvRGVsZXRlQWZ0ZXJVcGxvYWQ6IGZhbHNlLFxuICAgICAgLy8gYW5kcm9pZE5vdGlmaWNhdGlvblRpdGxlOiAnTmF0aXZlU2NyaXB0IEhUVFAgYmFja2dyb3VuZCcsXG4gICAgfTtcbiAgICBsZXQgcGFyYW1zID0gW11cbiAgICBmaWxlcy5jYXJkcy5mb3JFYWNoKGNhcmQgPT4ge1xuICAgICAgbGV0IGZpbGVOYW1lID0gY2FyZC5maWxlLnNsaWNlKGNhcmQuZmlsZS5sYXN0SW5kZXhPZignLycpICsgMSlcbiAgICAgIHBhcmFtcy5wdXNoKHsgbmFtZTogY2FyZC5lbWFpbCwgZmlsZU5hbWUsIG1pbWVUeXBlOiAnaW1hZ2UvanBlZycgfSlcbiAgICB9KTtcbiAgICByZXR1cm4gc2Vzc2lvbi5tdWx0aXBhcnRVcGxvYWQocGFyYW1zLCByZXF1ZXN0KTtcbiAgfVxufVxuIl19