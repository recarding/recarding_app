export interface IContact {
  user_id: string;
  id: string;
  group?: string;
  name: IName;
  organization?: IJob;
  cardImage?: string;
  cardImagePath?:string;
  phoneNumbers?: Array<string>;
  emailAddress?: string;
  physicalAddress?: string;
  urls?: Array<string>;
  custom?: ICustom;
}

interface IName {
  given?: string;
  middle?: string;
  family?: string;
  displayname?: string;
}

interface IJob {
  name?: string;
  jobTitle?: string;
  department?: string;
}

interface ICustom {
  key: string;
  value: any;
}

export interface IUser {
  email: string;
  fullName: string;
  password: string;
  user_id: string;
  loc: {
    type: string,
    coordinates: Array<number>
  }
}

export interface IUnsycedCards {
  cards: [{
    email: string;
    file: string;
  }]
}