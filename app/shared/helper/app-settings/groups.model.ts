export interface IGroups {
  contact_groups?: {
    [key: string]: Array<string>;
  };
}
