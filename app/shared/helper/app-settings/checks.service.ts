import { IContact } from "./storage.model";

import * as httpModule from "http";

import * as appSettings from "application-settings";
import * as connectivity from "tns-core-modules/connectivity";

export class ChecksService {
  contacts: Array<IContact>;
  appStatus: string = "idle";
  facebookbURLStart: string = "https://graph.facebook.com/v3.1/me?access_token=";
  facebookURLEnd: string = "&debug=all&fields=id%2Cname%2Cemail&format=json&method=get&pretty=0&suppress_http_code=1";

  checkConnectivityStatus() {
    return this.assertConnected(connectivity.getConnectionType());
  }
  assertConnected(connectionType: number): Promise<boolean> {
    return new Promise((resolve, reject) => {
      switch (connectionType) {
        case connectivity.connectionType.none:
          reject(false);
        case connectivity.connectionType.wifi:
        case connectivity.connectionType.mobile:
          resolve(true);
      }
    });
  }

  setFlagToShowSuccessToast() {
    appSettings.setBoolean("successFlag", true);
  }
  unsetFlagToShowSuccessToast() {
    appSettings.setBoolean("successFlag", false);
  }
  getFlagToShowSuccessToast() {
    return appSettings.getBoolean("successFlag");
  }

  checkFacebookToken(token: string) {
    return httpModule.getString(`${this.facebookbURLStart}${token}${this.facebookURLEnd}`);
  }
}
