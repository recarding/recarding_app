"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var httpModule = require("http");
var appSettings = require("application-settings");
var connectivity = require("tns-core-modules/connectivity");
var ChecksService = /** @class */ (function () {
    function ChecksService() {
        this.appStatus = "idle";
        this.facebookbURLStart = "https://graph.facebook.com/v3.1/me?access_token=";
        this.facebookURLEnd = "&debug=all&fields=id%2Cname%2Cemail&format=json&method=get&pretty=0&suppress_http_code=1";
    }
    ChecksService.prototype.checkConnectivityStatus = function () {
        return this.assertConnected(connectivity.getConnectionType());
    };
    ChecksService.prototype.assertConnected = function (connectionType) {
        return new Promise(function (resolve, reject) {
            switch (connectionType) {
                case connectivity.connectionType.none:
                    reject(false);
                case connectivity.connectionType.wifi:
                case connectivity.connectionType.mobile:
                    resolve(true);
            }
        });
    };
    ChecksService.prototype.setFlagToShowSuccessToast = function () {
        appSettings.setBoolean("successFlag", true);
    };
    ChecksService.prototype.unsetFlagToShowSuccessToast = function () {
        appSettings.setBoolean("successFlag", false);
    };
    ChecksService.prototype.getFlagToShowSuccessToast = function () {
        return appSettings.getBoolean("successFlag");
    };
    ChecksService.prototype.checkFacebookToken = function (token) {
        return httpModule.getString("" + this.facebookbURLStart + token + this.facebookURLEnd);
    };
    return ChecksService;
}());
exports.ChecksService = ChecksService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hlY2tzLnNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJjaGVja3Muc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBLGlDQUFtQztBQUVuQyxrREFBb0Q7QUFDcEQsNERBQThEO0FBRTlEO0lBQUE7UUFFRSxjQUFTLEdBQVcsTUFBTSxDQUFDO1FBQzNCLHNCQUFpQixHQUFXLGtEQUFrRCxDQUFDO1FBQy9FLG1CQUFjLEdBQVcsMEZBQTBGLENBQUM7SUE4QnRILENBQUM7SUE1QkMsK0NBQXVCLEdBQXZCO1FBQ0UsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLENBQUM7SUFDaEUsQ0FBQztJQUNELHVDQUFlLEdBQWYsVUFBZ0IsY0FBc0I7UUFDcEMsT0FBTyxJQUFJLE9BQU8sQ0FBQyxVQUFDLE9BQU8sRUFBRSxNQUFNO1lBQ2pDLFFBQVEsY0FBYyxFQUFFO2dCQUN0QixLQUFLLFlBQVksQ0FBQyxjQUFjLENBQUMsSUFBSTtvQkFDbkMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNoQixLQUFLLFlBQVksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDO2dCQUN0QyxLQUFLLFlBQVksQ0FBQyxjQUFjLENBQUMsTUFBTTtvQkFDckMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ2pCO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsaURBQXlCLEdBQXpCO1FBQ0UsV0FBVyxDQUFDLFVBQVUsQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDOUMsQ0FBQztJQUNELG1EQUEyQixHQUEzQjtRQUNFLFdBQVcsQ0FBQyxVQUFVLENBQUMsYUFBYSxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQy9DLENBQUM7SUFDRCxpREFBeUIsR0FBekI7UUFDRSxPQUFPLFdBQVcsQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDL0MsQ0FBQztJQUVELDBDQUFrQixHQUFsQixVQUFtQixLQUFhO1FBQzlCLE9BQU8sVUFBVSxDQUFDLFNBQVMsQ0FBQyxLQUFHLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLEdBQUcsSUFBSSxDQUFDLGNBQWdCLENBQUMsQ0FBQztJQUN6RixDQUFDO0lBQ0gsb0JBQUM7QUFBRCxDQUFDLEFBbENELElBa0NDO0FBbENZLHNDQUFhIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSUNvbnRhY3QgfSBmcm9tIFwiLi9zdG9yYWdlLm1vZGVsXCI7XG5cbmltcG9ydCAqIGFzIGh0dHBNb2R1bGUgZnJvbSBcImh0dHBcIjtcblxuaW1wb3J0ICogYXMgYXBwU2V0dGluZ3MgZnJvbSBcImFwcGxpY2F0aW9uLXNldHRpbmdzXCI7XG5pbXBvcnQgKiBhcyBjb25uZWN0aXZpdHkgZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvY29ubmVjdGl2aXR5XCI7XG5cbmV4cG9ydCBjbGFzcyBDaGVja3NTZXJ2aWNlIHtcbiAgY29udGFjdHM6IEFycmF5PElDb250YWN0PjtcbiAgYXBwU3RhdHVzOiBzdHJpbmcgPSBcImlkbGVcIjtcbiAgZmFjZWJvb2tiVVJMU3RhcnQ6IHN0cmluZyA9IFwiaHR0cHM6Ly9ncmFwaC5mYWNlYm9vay5jb20vdjMuMS9tZT9hY2Nlc3NfdG9rZW49XCI7XG4gIGZhY2Vib29rVVJMRW5kOiBzdHJpbmcgPSBcIiZkZWJ1Zz1hbGwmZmllbGRzPWlkJTJDbmFtZSUyQ2VtYWlsJmZvcm1hdD1qc29uJm1ldGhvZD1nZXQmcHJldHR5PTAmc3VwcHJlc3NfaHR0cF9jb2RlPTFcIjtcblxuICBjaGVja0Nvbm5lY3Rpdml0eVN0YXR1cygpIHtcbiAgICByZXR1cm4gdGhpcy5hc3NlcnRDb25uZWN0ZWQoY29ubmVjdGl2aXR5LmdldENvbm5lY3Rpb25UeXBlKCkpO1xuICB9XG4gIGFzc2VydENvbm5lY3RlZChjb25uZWN0aW9uVHlwZTogbnVtYmVyKTogUHJvbWlzZTxib29sZWFuPiB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIHN3aXRjaCAoY29ubmVjdGlvblR5cGUpIHtcbiAgICAgICAgY2FzZSBjb25uZWN0aXZpdHkuY29ubmVjdGlvblR5cGUubm9uZTpcbiAgICAgICAgICByZWplY3QoZmFsc2UpO1xuICAgICAgICBjYXNlIGNvbm5lY3Rpdml0eS5jb25uZWN0aW9uVHlwZS53aWZpOlxuICAgICAgICBjYXNlIGNvbm5lY3Rpdml0eS5jb25uZWN0aW9uVHlwZS5tb2JpbGU6XG4gICAgICAgICAgcmVzb2x2ZSh0cnVlKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIHNldEZsYWdUb1Nob3dTdWNjZXNzVG9hc3QoKSB7XG4gICAgYXBwU2V0dGluZ3Muc2V0Qm9vbGVhbihcInN1Y2Nlc3NGbGFnXCIsIHRydWUpO1xuICB9XG4gIHVuc2V0RmxhZ1RvU2hvd1N1Y2Nlc3NUb2FzdCgpIHtcbiAgICBhcHBTZXR0aW5ncy5zZXRCb29sZWFuKFwic3VjY2Vzc0ZsYWdcIiwgZmFsc2UpO1xuICB9XG4gIGdldEZsYWdUb1Nob3dTdWNjZXNzVG9hc3QoKSB7XG4gICAgcmV0dXJuIGFwcFNldHRpbmdzLmdldEJvb2xlYW4oXCJzdWNjZXNzRmxhZ1wiKTtcbiAgfVxuXG4gIGNoZWNrRmFjZWJvb2tUb2tlbih0b2tlbjogc3RyaW5nKSB7XG4gICAgcmV0dXJuIGh0dHBNb2R1bGUuZ2V0U3RyaW5nKGAke3RoaXMuZmFjZWJvb2tiVVJMU3RhcnR9JHt0b2tlbn0ke3RoaXMuZmFjZWJvb2tVUkxFbmR9YCk7XG4gIH1cbn1cbiJdfQ==