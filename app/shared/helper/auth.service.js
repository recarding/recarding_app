"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var appSettings = require("application-settings");
var jwt = require("jwt-decode");
var AuthService = /** @class */ (function () {
    function AuthService() {
    }
    AuthService.prototype.setactiveUser = function (user) {
        appSettings.setString("activeUser", JSON.stringify(user));
    };
    AuthService.prototype.getactiveUser = function () {
        var userString = appSettings.getString("activeUser") || "{}";
        return JSON.parse(userString);
    };
    AuthService.prototype.storeFacebookToken = function (token) {
        appSettings.setString("fb_token", "" + token);
    };
    AuthService.prototype.getFacebookToken = function () {
        return appSettings.getString("fb_token");
    };
    AuthService.prototype.removeFacebookToken = function () {
        appSettings.remove("fb_token");
    };
    AuthService.prototype.storeAuthToken = function (token) {
        appSettings.setString("authToken", token);
    };
    AuthService.prototype.getAuthToken = function () {
        return appSettings.getString("authToken");
    };
    AuthService.prototype.removeAuthToken = function () {
        appSettings.remove("authToken");
    };
    AuthService.prototype.navigateIfTokenExpired = function (token) {
        if (token) {
            return this.checkTokenHasExpired(token);
        }
        else {
            return false;
        }
    };
    AuthService.prototype.checkTokenHasExpired = function (token) {
        var decoded = jwt(token);
        if (decoded.exp === undefined) {
            return false;
        }
        var now = new Date();
        var date = new Date(0);
        date.setUTCSeconds(decoded.exp);
        return date < now ? false : true;
    };
    return AuthService;
}());
exports.AuthService = AuthService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYXV0aC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsa0RBQW9EO0FBQ3BELGdDQUFrQztBQUlsQztJQUFBO0lBdURBLENBQUM7SUFyREMsbUNBQWEsR0FBYixVQUFjLElBQVc7UUFDdkIsV0FBVyxDQUFDLFNBQVMsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO0lBQzVELENBQUM7SUFFRCxtQ0FBYSxHQUFiO1FBQ0UsSUFBTSxVQUFVLEdBQUcsV0FBVyxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsSUFBSSxJQUFJLENBQUM7UUFDL0QsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ2hDLENBQUM7SUFDRCx3Q0FBa0IsR0FBbEIsVUFBbUIsS0FBMkI7UUFDNUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxVQUFVLEVBQUUsS0FBRyxLQUFPLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBRUQsc0NBQWdCLEdBQWhCO1FBQ0UsT0FBTyxXQUFXLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFFRCx5Q0FBbUIsR0FBbkI7UUFDRSxXQUFXLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ2pDLENBQUM7SUFFRCxvQ0FBYyxHQUFkLFVBQWUsS0FBYTtRQUMxQixXQUFXLENBQUMsU0FBUyxDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBRUQsa0NBQVksR0FBWjtRQUNFLE9BQU8sV0FBVyxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBRUQscUNBQWUsR0FBZjtRQUNFLFdBQVcsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDbEMsQ0FBQztJQUVELDRDQUFzQixHQUF0QixVQUF1QixLQUFhO1FBQ2xDLElBQUksS0FBSyxFQUFFO1lBQ1QsT0FBTyxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDekM7YUFBTTtZQUNMLE9BQU8sS0FBSyxDQUFDO1NBQ2Q7SUFDSCxDQUFDO0lBRUQsMENBQW9CLEdBQXBCLFVBQXFCLEtBQWE7UUFDaEMsSUFBTSxPQUFPLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRTNCLElBQUksT0FBTyxDQUFDLEdBQUcsS0FBSyxTQUFTLEVBQUU7WUFDN0IsT0FBTyxLQUFLLENBQUM7U0FDZDtRQUNELElBQU0sR0FBRyxHQUFHLElBQUksSUFBSSxFQUFFLENBQUM7UUFDdkIsSUFBTSxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDekIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7UUFFaEMsT0FBTyxJQUFJLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUNuQyxDQUFDO0lBRUgsa0JBQUM7QUFBRCxDQUFDLEFBdkRELElBdURDO0FBdkRZLGtDQUFXIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgYXBwU2V0dGluZ3MgZnJvbSBcImFwcGxpY2F0aW9uLXNldHRpbmdzXCI7XG5pbXBvcnQgKiBhcyBqd3QgZnJvbSBcImp3dC1kZWNvZGVcIjtcbmltcG9ydCB7IElVc2VyIH0gZnJvbSBcIi4vYXBwLXNldHRpbmdzL3N0b3JhZ2UubW9kZWxcIjtcbmltcG9ydCB7IElUbnNPQXV0aFRva2VuUmVzdWx0IH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1vYXV0aDJcIjtcblxuZXhwb3J0IGNsYXNzIEF1dGhTZXJ2aWNlIHtcblxuICBzZXRhY3RpdmVVc2VyKHVzZXI6IElVc2VyKSB7XG4gICAgYXBwU2V0dGluZ3Muc2V0U3RyaW5nKFwiYWN0aXZlVXNlclwiLCBKU09OLnN0cmluZ2lmeSh1c2VyKSk7XG4gIH1cblxuICBnZXRhY3RpdmVVc2VyKCk6IElVc2VyIHtcbiAgICBjb25zdCB1c2VyU3RyaW5nID0gYXBwU2V0dGluZ3MuZ2V0U3RyaW5nKFwiYWN0aXZlVXNlclwiKSB8fCBcInt9XCI7XG4gICAgcmV0dXJuIEpTT04ucGFyc2UodXNlclN0cmluZyk7XG4gIH1cbiAgc3RvcmVGYWNlYm9va1Rva2VuKHRva2VuOiBJVG5zT0F1dGhUb2tlblJlc3VsdCkge1xuICAgIGFwcFNldHRpbmdzLnNldFN0cmluZyhcImZiX3Rva2VuXCIsIGAke3Rva2VufWApO1xuICB9XG5cbiAgZ2V0RmFjZWJvb2tUb2tlbigpIHtcbiAgICByZXR1cm4gYXBwU2V0dGluZ3MuZ2V0U3RyaW5nKFwiZmJfdG9rZW5cIik7XG4gIH1cblxuICByZW1vdmVGYWNlYm9va1Rva2VuKCk6IHZvaWQge1xuICAgIGFwcFNldHRpbmdzLnJlbW92ZShcImZiX3Rva2VuXCIpO1xuICB9XG5cbiAgc3RvcmVBdXRoVG9rZW4odG9rZW46IHN0cmluZykge1xuICAgIGFwcFNldHRpbmdzLnNldFN0cmluZyhcImF1dGhUb2tlblwiLCB0b2tlbik7XG4gIH1cblxuICBnZXRBdXRoVG9rZW4oKSB7XG4gICAgcmV0dXJuIGFwcFNldHRpbmdzLmdldFN0cmluZyhcImF1dGhUb2tlblwiKTtcbiAgfVxuXG4gIHJlbW92ZUF1dGhUb2tlbigpIHtcbiAgICBhcHBTZXR0aW5ncy5yZW1vdmUoXCJhdXRoVG9rZW5cIik7XG4gIH1cblxuICBuYXZpZ2F0ZUlmVG9rZW5FeHBpcmVkKHRva2VuOiBzdHJpbmcpIHtcbiAgICBpZiAodG9rZW4pIHtcbiAgICAgIHJldHVybiB0aGlzLmNoZWNrVG9rZW5IYXNFeHBpcmVkKHRva2VuKTtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbiAgfVxuXG4gIGNoZWNrVG9rZW5IYXNFeHBpcmVkKHRva2VuOiBzdHJpbmcpOiBib29sZWFuIHtcbiAgICBjb25zdCBkZWNvZGVkID0gand0KHRva2VuKTtcblxuICAgIGlmIChkZWNvZGVkLmV4cCA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICAgIGNvbnN0IG5vdyA9IG5ldyBEYXRlKCk7XG4gICAgY29uc3QgZGF0ZSA9IG5ldyBEYXRlKDApO1xuICAgIGRhdGUuc2V0VVRDU2Vjb25kcyhkZWNvZGVkLmV4cCk7XG5cbiAgICByZXR1cm4gZGF0ZSA8IG5vdyA/IGZhbHNlIDogdHJ1ZTtcbiAgfVxuXG59XG4iXX0=