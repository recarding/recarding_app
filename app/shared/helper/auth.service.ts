import * as appSettings from "application-settings";
import * as jwt from "jwt-decode";
import { IUser } from "./app-settings/storage.model";
import { ITnsOAuthTokenResult } from "nativescript-oauth2";

export class AuthService {

  setactiveUser(user: IUser) {
    appSettings.setString("activeUser", JSON.stringify(user));
  }

  getactiveUser(): IUser {
    const userString = appSettings.getString("activeUser") || "{}";
    return JSON.parse(userString);
  }
  storeFacebookToken(token: ITnsOAuthTokenResult) {
    appSettings.setString("fb_token", `${token}`);
  }

  getFacebookToken() {
    return appSettings.getString("fb_token");
  }

  removeFacebookToken(): void {
    appSettings.remove("fb_token");
  }

  storeAuthToken(token: string) {
    appSettings.setString("authToken", token);
  }

  getAuthToken() {
    return appSettings.getString("authToken");
  }

  removeAuthToken() {
    appSettings.remove("authToken");
  }

  navigateIfTokenExpired(token: string) {
    if (token) {
      return this.checkTokenHasExpired(token);
    } else {
      return false;
    }
  }

  checkTokenHasExpired(token: string): boolean {
    const decoded = jwt(token);

    if (decoded.exp === undefined) {
      return false;
    }
    const now = new Date();
    const date = new Date(0);
    date.setUTCSeconds(decoded.exp);

    return date < now ? false : true;
  }

}
