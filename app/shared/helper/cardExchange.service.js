"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var http = require("http");
var platform_1 = require("platform");
var platform_2 = require("platform");
var CardExchangeService = /** @class */ (function () {
    function CardExchangeService() {
        if (platform_2.device.os === platform_1.platformNames.ios) {
            /*localhost for ios*/
            this.nativePlatformLocalhost = "localhost";
        }
        else if (platform_2.device.os === platform_1.platformNames.android) {
            /*localhost for android*/
            this.nativePlatformLocalhost = "10.0.2.2";
        }
    }
    CardExchangeService.prototype.fetchNearbyUsers = function (geolocation) {
        return http.request({
            url: "http://localhost:5000/user/fetch_near_users",
            method: "POST",
            headers: { "Content-Type": "application/json" },
            content: JSON.stringify(geolocation)
        });
    };
    CardExchangeService.prototype.getProfileByID = function (id) {
        var url = "http://" + this.nativePlatformLocalhost + ":5000/user/" + id;
        return http.getJSON(url.replace(/ /g, "%20"));
    };
    CardExchangeService.prototype.sendTargetUsers = function (users) {
        return http.request({
            url: "http://localhost:5000/cards/push",
            method: "POST",
            headers: { "Content-Type": "application/json" },
            content: JSON.stringify(users)
        });
    };
    return CardExchangeService;
}());
exports.CardExchangeService = CardExchangeService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZEV4Y2hhbmdlLnNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJjYXJkRXhjaGFuZ2Uuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLDJCQUE2QjtBQUM3QixxQ0FBdUM7QUFDdkMscUNBQWdDO0FBRWhDO0lBR0U7UUFFQyxJQUFHLGlCQUFNLENBQUMsRUFBRSxLQUFLLHdCQUFhLENBQUMsR0FBRyxFQUFDO1lBQ2xDLHFCQUFxQjtZQUNyQixJQUFJLENBQUMsdUJBQXVCLEdBQUUsV0FBVyxDQUFDO1NBQzNDO2FBQ0csSUFBRyxpQkFBTSxDQUFDLEVBQUUsS0FBSyx3QkFBYSxDQUFDLE9BQU8sRUFBQztZQUMxQyx5QkFBeUI7WUFDekIsSUFBSSxDQUFDLHVCQUF1QixHQUFFLFVBQVUsQ0FBQztTQUN4QztJQUNGLENBQUM7SUFFRCw4Q0FBZ0IsR0FBaEIsVUFBaUIsV0FBbUI7UUFDbEMsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO1lBQ2xCLEdBQUcsRUFBRSw2Q0FBNkM7WUFDbEQsTUFBTSxFQUFFLE1BQU07WUFDZCxPQUFPLEVBQUUsRUFBRSxjQUFjLEVBQUUsa0JBQWtCLEVBQUU7WUFDL0MsT0FBTyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDO1NBQ3JDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCw0Q0FBYyxHQUFkLFVBQWUsRUFBVTtRQUN2QixJQUFNLEdBQUcsR0FBRyxZQUFVLElBQUksQ0FBQyx1QkFBdUIsbUJBQWMsRUFBSSxDQUFBO1FBQ3BFLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRCw2Q0FBZSxHQUFmLFVBQWdCLEtBQWE7UUFDM0IsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO1lBQ2xCLEdBQUcsRUFBRSxrQ0FBa0M7WUFDdkMsTUFBTSxFQUFFLE1BQU07WUFDZCxPQUFPLEVBQUUsRUFBRSxjQUFjLEVBQUUsa0JBQWtCLEVBQUU7WUFDL0MsT0FBTyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDO1NBQy9CLENBQUMsQ0FBQztJQUNMLENBQUM7SUFDSCwwQkFBQztBQUFELENBQUMsQUFyQ0QsSUFxQ0M7QUFyQ1ksa0RBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgaHR0cCBmcm9tIFwiaHR0cFwiO1xuaW1wb3J0IHtwbGF0Zm9ybU5hbWVzfSBmcm9tIFwicGxhdGZvcm1cIjtcbmltcG9ydCB7ZGV2aWNlfSBmcm9tIFwicGxhdGZvcm1cIjtcblxuZXhwb3J0IGNsYXNzIENhcmRFeGNoYW5nZVNlcnZpY2Uge1xuICB1cmk6IHN0cmluZztcbiAgbmF0aXZlUGxhdGZvcm1Mb2NhbGhvc3Q7XG4gIGNvbnN0cnVjdG9yKCkge1xuXG4gICBpZihkZXZpY2Uub3MgPT09IHBsYXRmb3JtTmFtZXMuaW9zKXtcbiAgICAvKmxvY2FsaG9zdCBmb3IgaW9zKi9cbiAgICB0aGlzLm5hdGl2ZVBsYXRmb3JtTG9jYWxob3N0PSBcImxvY2FsaG9zdFwiO1xuICB9XG4gZWxzZSBpZihkZXZpY2Uub3MgPT09IHBsYXRmb3JtTmFtZXMuYW5kcm9pZCl7XG4gICAvKmxvY2FsaG9zdCBmb3IgYW5kcm9pZCovXG4gICB0aGlzLm5hdGl2ZVBsYXRmb3JtTG9jYWxob3N0PSBcIjEwLjAuMi4yXCI7XG4gICB9XG4gIH1cblxuICBmZXRjaE5lYXJieVVzZXJzKGdlb2xvY2F0aW9uOiBvYmplY3QpOiBQcm9taXNlPGFueT4ge1xuICAgIHJldHVybiBodHRwLnJlcXVlc3Qoe1xuICAgICAgdXJsOiBcImh0dHA6Ly9sb2NhbGhvc3Q6NTAwMC91c2VyL2ZldGNoX25lYXJfdXNlcnNcIixcbiAgICAgIG1ldGhvZDogXCJQT1NUXCIsXG4gICAgICBoZWFkZXJzOiB7IFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24vanNvblwiIH0sXG4gICAgICBjb250ZW50OiBKU09OLnN0cmluZ2lmeShnZW9sb2NhdGlvbilcbiAgICB9KTtcbiAgfVxuXG4gIGdldFByb2ZpbGVCeUlEKGlkOiBzdHJpbmcpOiBQcm9taXNlPGFueT4ge1xuICAgIGNvbnN0IHVybCA9IGBodHRwOi8vJHt0aGlzLm5hdGl2ZVBsYXRmb3JtTG9jYWxob3N0fTo1MDAwL3VzZXIvJHtpZH1gXG4gICAgcmV0dXJuIGh0dHAuZ2V0SlNPTih1cmwucmVwbGFjZSgvIC9nLCBcIiUyMFwiKSk7XG4gIH1cblxuICBzZW5kVGFyZ2V0VXNlcnModXNlcnM6IE9iamVjdCkge1xuICAgIHJldHVybiBodHRwLnJlcXVlc3Qoe1xuICAgICAgdXJsOiBcImh0dHA6Ly9sb2NhbGhvc3Q6NTAwMC9jYXJkcy9wdXNoXCIsXG4gICAgICBtZXRob2Q6IFwiUE9TVFwiLFxuICAgICAgaGVhZGVyczogeyBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL2pzb25cIiB9LFxuICAgICAgY29udGVudDogSlNPTi5zdHJpbmdpZnkodXNlcnMpXG4gICAgfSk7XG4gIH1cbn1cbiJdfQ==