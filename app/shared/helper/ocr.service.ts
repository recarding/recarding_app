import { IContact } from "./app-settings/storage.model"

export class OcrService {

  private scannedCard: IContact;
  constructor() { }

  filterCardText(recognizedText: string) {
    return new Promise((resolve,reject) => {
      const emailRegEx = /(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/m;
      const scannedEmail = emailRegEx.exec(recognizedText)
      
      const spaceBeeforeDigits = /\s(?=[0-9])/gm
      const textNospace = recognizedText.replace(spaceBeeforeDigits, '');
      const phoneRegEx = /A\+?(?:0{0,2}[46]*){1}7{1}[0-9]{8}/gm;
      const scannedPhone = phoneRegEx.exec(textNospace);
      
      const urlRegex = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/gim;
      const scannedURL =urlRegex.exec(recognizedText)
      
      const mostFrequent = this.findMostFrequent(recognizedText.replace(/\\n/, ' ').split(' '));      
      this.scannedCard = {
        id: '',
        user_id:'',
        name: {
          displayname: mostFrequent
        },
        organization: {
          name: scannedURL[0].split('.').length > 2? scannedURL[0].split('.')[1]:scannedURL[0].split('.')[0]
        },
        urls: [scannedURL? scannedURL[0] : '' ],
        phoneNumbers: [scannedPhone ? scannedPhone[0] : ''],
        emailAddress: scannedEmail ? scannedEmail[0] : ''
      }
      resolve(this.scannedCard);
    })
  }

  private findMostFrequent(wordsArray: Array<string>) {
    const counts = {};
    let compare = 0;
    let mostFrequent;
    (function (array) {
      for (let i = 0, len = array.length; i < len; i++) {
        const word = array[i];
        if (counts[word] === undefined) {
          counts[word] = 1;
        } else {
          counts[word] = counts[word] + 1;
        }
        if (compare < counts[word]) {
          compare = counts[word];
          mostFrequent = wordsArray[i];
        }
      }
      return mostFrequent;
    })(wordsArray);
    return mostFrequent
  }

  private similarity(s1, s2) {
    var longer = s1;
    var shorter = s2;
    if (s1.length < s2.length) {
      longer = s2;
      shorter = s1;
    }
    var longerLength = longer.length;
    if (longerLength == 0) {
      return 1.0;
    }
    return (longerLength - this.editDistance(longer, shorter)) / parseFloat(longerLength);
  }

  private editDistance(s1, s2) {
    s1 = s1.toLowerCase();
    s2 = s2.toLowerCase();

    var costs = new Array();
    for (var i = 0; i <= s1.length; i++) {
      var lastValue = i;
      for (var j = 0; j <= s2.length; j++) {
        if (i == 0)
          costs[j] = j;
        else {
          if (j > 0) {
            var newValue = costs[j - 1];
            if (s1.charAt(i - 1) != s2.charAt(j - 1))
              newValue = Math.min(Math.min(newValue, lastValue),
                costs[j]) + 1;
            costs[j - 1] = lastValue;
            lastValue = newValue;
          }
        }
      }
      if (i > 0)
        costs[s2.length] = lastValue;
    }
    return costs[s2.length];
  }
}
