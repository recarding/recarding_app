"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var TargetUserItemComponent = /** @class */ (function () {
    function TargetUserItemComponent() {
        this.selected = new core_1.EventEmitter();
    }
    TargetUserItemComponent.prototype.ngOnInit = function () { };
    TargetUserItemComponent.prototype.toggleSelectUser = function () {
        this.selected.emit(this.index);
    };
    __decorate([
        core_1.Input("index"),
        __metadata("design:type", Object)
    ], TargetUserItemComponent.prototype, "index", void 0);
    __decorate([
        core_1.Input("user"),
        __metadata("design:type", Object)
    ], TargetUserItemComponent.prototype, "user", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], TargetUserItemComponent.prototype, "selected", void 0);
    TargetUserItemComponent = __decorate([
        core_1.Component({
            selector: "target-user-item",
            moduleId: module.id,
            template: "\n  <CheckBox \n    class=\"fetchedUsersStyle\"\n    [text]=\"user.fullName\" \n    (tap)=\"toggleSelectUser()\" \n    boxType=\"circle\" \n    checked=\"false\"\n    style=\"\n    margin-bottom:10pt;\n    font-size:20pt;\n    \"\n  >\n  </CheckBox>\n  "
        }),
        __metadata("design:paramtypes", [])
    ], TargetUserItemComponent);
    return TargetUserItemComponent;
}());
exports.TargetUserItemComponent = TargetUserItemComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFyZ2V0LXVzZXItaXRlbS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ0YXJnZXQtdXNlci1pdGVtLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFzRztBQXFCdEc7SUFLRTtRQUZVLGFBQVEsR0FBRyxJQUFJLG1CQUFZLEVBQUUsQ0FBQztJQUV4QixDQUFDO0lBRWpCLDBDQUFRLEdBQVIsY0FBWSxDQUFDO0lBRWIsa0RBQWdCLEdBQWhCO1FBQ0UsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFBO0lBQ2hDLENBQUM7SUFWZTtRQUFmLFlBQUssQ0FBQyxPQUFPLENBQUM7OzBEQUFlO0lBQ2Y7UUFBZCxZQUFLLENBQUMsTUFBTSxDQUFDOzt5REFBYztJQUNsQjtRQUFULGFBQU0sRUFBRTs7NkRBQStCO0lBSDdCLHVCQUF1QjtRQW5CbkMsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxrQkFBa0I7WUFDNUIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSwrUEFhVDtTQUNGLENBQUM7O09BRVcsdUJBQXVCLENBYW5DO0lBQUQsOEJBQUM7Q0FBQSxBQWJELElBYUM7QUFiWSwwREFBdUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIFZpZXdDaGlsZCwgRWxlbWVudFJlZiwgT3V0cHV0LCBFdmVudEVtaXR0ZXIgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6IFwidGFyZ2V0LXVzZXItaXRlbVwiLFxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxuICB0ZW1wbGF0ZTogYFxuICA8Q2hlY2tCb3ggXG4gICAgY2xhc3M9XCJmZXRjaGVkVXNlcnNTdHlsZVwiXG4gICAgW3RleHRdPVwidXNlci5mdWxsTmFtZVwiIFxuICAgICh0YXApPVwidG9nZ2xlU2VsZWN0VXNlcigpXCIgXG4gICAgYm94VHlwZT1cImNpcmNsZVwiIFxuICAgIGNoZWNrZWQ9XCJmYWxzZVwiXG4gICAgc3R5bGU9XCJcbiAgICBtYXJnaW4tYm90dG9tOjEwcHQ7XG4gICAgZm9udC1zaXplOjIwcHQ7XG4gICAgXCJcbiAgPlxuICA8L0NoZWNrQm94PlxuICBgXG59KVxuXG5leHBvcnQgY2xhc3MgVGFyZ2V0VXNlckl0ZW1Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBASW5wdXQoXCJpbmRleFwiKSBpbmRleDogb2JqZWN0O1xuICBASW5wdXQoXCJ1c2VyXCIpIHVzZXI6IG9iamVjdDtcbiAgQE91dHB1dCgpIHNlbGVjdGVkID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgbmdPbkluaXQoKSB7fVxuXG4gIHRvZ2dsZVNlbGVjdFVzZXIoKSB7XG4gICAgdGhpcy5zZWxlY3RlZC5lbWl0KHRoaXMuaW5kZXgpXG4gIH1cblxufVxuIl19