import { Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "target-user-item",
  moduleId: module.id,
  template: `
  <CheckBox 
    class="fetchedUsersStyle"
    [text]="user.fullName" 
    (tap)="toggleSelectUser()" 
    boxType="circle" 
    checked="false"
    style="
    margin-bottom:10pt;
    font-size:20pt;
    "
  >
  </CheckBox>
  `
})

export class TargetUserItemComponent implements OnInit {
  @Input("index") index: object;
  @Input("user") user: object;
  @Output() selected = new EventEmitter();

  constructor() { }

  ngOnInit() {}

  toggleSelectUser() {
    this.selected.emit(this.index)
  }

}
