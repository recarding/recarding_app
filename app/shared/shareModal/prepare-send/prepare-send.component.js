"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("ui/page");
var pageCommon = require('ui/page/page-common').Page;
var PrepareShareComponent = /** @class */ (function () {
    function PrepareShareComponent(page) {
        this.page = page;
        this.showQrCode = new core_1.EventEmitter();
        this.sendToNearBy = new core_1.EventEmitter();
        this.sendViaMail = new core_1.EventEmitter();
    }
    PrepareShareComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], PrepareShareComponent.prototype, "showQrCode", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], PrepareShareComponent.prototype, "sendToNearBy", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], PrepareShareComponent.prototype, "sendViaMail", void 0);
    PrepareShareComponent = __decorate([
        core_1.Component({
            selector: "Prepare-share",
            moduleId: module.id,
            templateUrl: "./prepare-send.component.html",
            styleUrls: ["./prepare-send.component.scss"]
        }),
        __metadata("design:paramtypes", [page_1.Page])
    ], PrepareShareComponent);
    return PrepareShareComponent;
}());
exports.PrepareShareComponent = PrepareShareComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJlcGFyZS1zZW5kLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInByZXBhcmUtc2VuZC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBd0U7QUFDeEUsZ0NBQXNDO0FBRXRDLElBQU0sVUFBVSxHQUFHLE9BQU8sQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLElBQUksQ0FBQztBQVF2RDtJQUtFLCtCQUFvQixJQUFVO1FBQVYsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUpwQixlQUFVLEdBQUcsSUFBSSxtQkFBWSxFQUFFLENBQUM7UUFDaEMsaUJBQVksR0FBRyxJQUFJLG1CQUFZLEVBQUUsQ0FBQztRQUNsQyxnQkFBVyxHQUFHLElBQUksbUJBQVksRUFBRSxDQUFDO0lBRVYsQ0FBQztJQUNsQyx3Q0FBUSxHQUFSO0lBRUEsQ0FBQztJQVBTO1FBQVQsYUFBTSxFQUFFOzs2REFBaUM7SUFDaEM7UUFBVCxhQUFNLEVBQUU7OytEQUFtQztJQUNsQztRQUFULGFBQU0sRUFBRTs7OERBQWtDO0lBSGhDLHFCQUFxQjtRQU5qQyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLGVBQWU7WUFDekIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFdBQVcsRUFBRSwrQkFBK0I7WUFDNUMsU0FBUyxFQUFFLENBQUMsK0JBQStCLENBQUM7U0FDN0MsQ0FBQzt5Q0FNMEIsV0FBSTtPQUxuQixxQkFBcUIsQ0FTakM7SUFBRCw0QkFBQztDQUFBLEFBVEQsSUFTQztBQVRZLHNEQUFxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBQYWdlLCBDb2xvciB9IGZyb20gJ3VpL3BhZ2UnO1xuaW1wb3J0ICogYXMgdXRpbHMgZnJvbSAndXRpbHMvdXRpbHMnO1xuY29uc3QgcGFnZUNvbW1vbiA9IHJlcXVpcmUoJ3VpL3BhZ2UvcGFnZS1jb21tb24nKS5QYWdlO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6IFwiUHJlcGFyZS1zaGFyZVwiLFxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxuICB0ZW1wbGF0ZVVybDogXCIuL3ByZXBhcmUtc2VuZC5jb21wb25lbnQuaHRtbFwiLFxuICBzdHlsZVVybHM6IFtcIi4vcHJlcGFyZS1zZW5kLmNvbXBvbmVudC5zY3NzXCJdXG59KVxuZXhwb3J0IGNsYXNzIFByZXBhcmVTaGFyZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIEBPdXRwdXQoKSBzaG93UXJDb2RlID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgc2VuZFRvTmVhckJ5ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgc2VuZFZpYU1haWwgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBwYWdlOiBQYWdlKSB7fVxuICBuZ09uSW5pdCgpIHtcbiAgXG4gIH1cbn1cbiJdfQ==