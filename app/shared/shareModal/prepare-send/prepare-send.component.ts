import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { Page, Color } from 'ui/page';
import * as utils from 'utils/utils';
const pageCommon = require('ui/page/page-common').Page;

@Component({
  selector: "Prepare-share",
  moduleId: module.id,
  templateUrl: "./prepare-send.component.html",
  styleUrls: ["./prepare-send.component.scss"]
})
export class PrepareShareComponent implements OnInit {
  @Output() showQrCode = new EventEmitter();
  @Output() sendToNearBy = new EventEmitter();
  @Output() sendViaMail = new EventEmitter();

  constructor(private page: Page) {}
  ngOnInit() {
  
  }
}
