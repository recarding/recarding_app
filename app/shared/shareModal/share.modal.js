"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var dialogs_1 = require("nativescript-angular/directives/dialogs");
var dialogs = require("tns-core-modules/ui/dialogs");
// const zxing = require("nativescript-zxing");
var geolocation = require("nativescript-geolocation");
var enums_1 = require("ui/enums");
var cardExchange_service_1 = require("../helper/cardExchange.service");
var auth_service_1 = require("../helper/auth.service");
var modal;
(function (modal) {
    modal[modal["opened"] = 0] = "opened";
    modal[modal["fetchingUsers"] = 1] = "fetchingUsers";
    modal[modal["fetchedUsers"] = 2] = "fetchedUsers";
    modal[modal["noUsersFound"] = 3] = "noUsersFound";
})(modal || (modal = {}));
var ShareModalComponent = /** @class */ (function () {
    function ShareModalComponent(params, cardExchangeService, auth) {
        this.params = params;
        this.cardExchangeService = cardExchangeService;
        this.auth = auth;
        this.selectedTargetUsers = {};
    }
    ShareModalComponent.prototype.ngOnInit = function () {
        this.modal_state = modal["opened"];
    };
    ShareModalComponent.prototype.sendNearBy = function () {
        var _this = this;
        this.modal_state = modal["fetchingUsers"];
        geolocation.enableLocationRequest();
        geolocation.isEnabled().then(function (isEnabled) {
            if (isEnabled) {
                geolocation
                    .getCurrentLocation({
                    desiredAccuracy: enums_1.Accuracy.high,
                    timeout: 20000
                })
                    .then(function (currentLocation) {
                    var coords = {
                        long: currentLocation.longitude,
                        lat: currentLocation.latitude
                    };
                    _this.cardExchangeService.fetchNearbyUsers(coords).then(function (res) {
                        if (!!res) {
                            var users = res.content.toJSON();
                            _this.fetchedUsers = users;
                            _this.modal_state = modal["fetchedUsers"];
                        }
                        else {
                            _this.modal_state = modal["noUsersFound"];
                        }
                    });
                });
            }
            else {
                alert("Please enable your location settings");
            }
        });
    };
    ShareModalComponent.prototype.toggleSelectUser = function (index) {
        var string_index = index.toString();
        if (!!this.selectedTargetUsers[string_index]) {
            delete this.selectedTargetUsers[string_index];
        }
        else {
            this.selectedTargetUsers[string_index] = this.fetchedUsers[index];
        }
    };
    ShareModalComponent.prototype.sendCardToTargets = function () {
        var _this = this;
        var activeUser = this.auth.getactiveUser();
        var pushObject = {
            activeUserEmail: activeUser.email,
            targets: this.selectedTargetUsers
        };
        this.cardExchangeService.sendTargetUsers(pushObject).then(function (res) {
            var result = res.content.toJSON();
            if (result.success) {
                dialogs
                    .alert({
                    title: "Success!",
                    message: "Your card has been shared",
                    okButtonText: "Ok"
                })
                    .then(function () {
                    _this.params.closeCallback();
                });
            }
            else {
                dialogs
                    .alert({
                    title: "Error",
                    message: "Something went wrong",
                    okButtonText: "Ok"
                })
                    .then(function () {
                    _this.params.closeCallback();
                });
            }
        });
    };
    // showQrCode() {
    //   const zx = new zxing();
    //   this.qrCode = zx.createBarcode({
    //     encode: JSON.stringify(this.params),
    //     height: 200,
    //     width: 200,
    //     format: zxing.QR_CODE
    //   });
    // }
    ShareModalComponent.prototype.close = function (response) {
        this.params.closeCallback(response);
    };
    ShareModalComponent = __decorate([
        core_1.Component({
            selector: "share-modal",
            moduleId: module.id,
            templateUrl: "./share.modal.html",
            styleUrls: ["./modal.scss"]
        }),
        __metadata("design:paramtypes", [dialogs_1.ModalDialogParams,
            cardExchange_service_1.CardExchangeService,
            auth_service_1.AuthService])
    ], ShareModalComponent);
    return ShareModalComponent;
}());
exports.ShareModalComponent = ShareModalComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hhcmUubW9kYWwuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzaGFyZS5tb2RhbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUF5RTtBQUN6RSxtRUFBNEU7QUFDNUUscURBQXVEO0FBRXZELCtDQUErQztBQUUvQyxzREFBd0Q7QUFDeEQsa0NBQW9DO0FBRXBDLHVFQUFxRTtBQUNyRSx1REFBcUQ7QUFFckQsSUFBSyxLQUtKO0FBTEQsV0FBSyxLQUFLO0lBQ1IscUNBQVUsQ0FBQTtJQUNWLG1EQUFpQixDQUFBO0lBQ2pCLGlEQUFnQixDQUFBO0lBQ2hCLGlEQUFnQixDQUFBO0FBQ2xCLENBQUMsRUFMSSxLQUFLLEtBQUwsS0FBSyxRQUtUO0FBUUQ7SUFNRSw2QkFDVSxNQUF5QixFQUN6QixtQkFBd0MsRUFDeEMsSUFBaUI7UUFGakIsV0FBTSxHQUFOLE1BQU0sQ0FBbUI7UUFDekIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxTQUFJLEdBQUosSUFBSSxDQUFhO1FBTjNCLHdCQUFtQixHQUFXLEVBQUUsQ0FBQztJQU85QixDQUFDO0lBRUosc0NBQVEsR0FBUjtRQUNFLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3JDLENBQUM7SUFFRCx3Q0FBVSxHQUFWO1FBQUEsaUJBNkJDO1FBNUJDLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQzFDLFdBQVcsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO1FBQ3BDLFdBQVcsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQSxTQUFTO1lBQ3BDLElBQUksU0FBUyxFQUFFO2dCQUNiLFdBQVc7cUJBQ1Isa0JBQWtCLENBQUM7b0JBQ2xCLGVBQWUsRUFBRSxnQkFBUSxDQUFDLElBQUk7b0JBQzlCLE9BQU8sRUFBRSxLQUFLO2lCQUNmLENBQUM7cUJBQ0QsSUFBSSxDQUFDLFVBQUEsZUFBZTtvQkFDbkIsSUFBTSxNQUFNLEdBQUc7d0JBQ2IsSUFBSSxFQUFFLGVBQWUsQ0FBQyxTQUFTO3dCQUMvQixHQUFHLEVBQUUsZUFBZSxDQUFDLFFBQVE7cUJBQzlCLENBQUM7b0JBQ0YsS0FBSSxDQUFDLG1CQUFtQixDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUc7d0JBQ3hELElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRTs0QkFDVCxJQUFNLEtBQUssR0FBRyxHQUFHLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDOzRCQUNuQyxLQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQzs0QkFDMUIsS0FBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUMsY0FBYyxDQUFDLENBQUM7eUJBQzFDOzZCQUFNOzRCQUNMLEtBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDLGNBQWMsQ0FBQyxDQUFDO3lCQUMxQztvQkFDSCxDQUFDLENBQUMsQ0FBQztnQkFDTCxDQUFDLENBQUMsQ0FBQzthQUNOO2lCQUFNO2dCQUNMLEtBQUssQ0FBQyxzQ0FBc0MsQ0FBQyxDQUFDO2FBQy9DO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsOENBQWdCLEdBQWhCLFVBQWlCLEtBQWE7UUFDNUIsSUFBSSxZQUFZLEdBQUcsS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ3BDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxZQUFZLENBQUMsRUFBRTtZQUM1QyxPQUFPLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxZQUFZLENBQUMsQ0FBQztTQUMvQzthQUFNO1lBQ0wsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFlBQVksQ0FBQyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDbkU7SUFDSCxDQUFDO0lBRUQsK0NBQWlCLEdBQWpCO1FBQUEsaUJBOEJDO1FBN0JDLElBQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDN0MsSUFBTSxVQUFVLEdBQUc7WUFDakIsZUFBZSxFQUFFLFVBQVUsQ0FBQyxLQUFLO1lBQ2pDLE9BQU8sRUFBRSxJQUFJLENBQUMsbUJBQW1CO1NBQ2xDLENBQUM7UUFDRixJQUFJLENBQUMsbUJBQW1CLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUc7WUFDM0QsSUFBTSxNQUFNLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUNwQyxJQUFJLE1BQU0sQ0FBQyxPQUFPLEVBQUU7Z0JBQ2xCLE9BQU87cUJBQ0osS0FBSyxDQUFDO29CQUNMLEtBQUssRUFBRSxVQUFVO29CQUNqQixPQUFPLEVBQUUsMkJBQTJCO29CQUNwQyxZQUFZLEVBQUUsSUFBSTtpQkFDbkIsQ0FBQztxQkFDRCxJQUFJLENBQUM7b0JBQ0osS0FBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLEVBQUUsQ0FBQztnQkFDOUIsQ0FBQyxDQUFDLENBQUM7YUFDTjtpQkFBTTtnQkFDTCxPQUFPO3FCQUNKLEtBQUssQ0FBQztvQkFDTCxLQUFLLEVBQUUsT0FBTztvQkFDZCxPQUFPLEVBQUUsc0JBQXNCO29CQUMvQixZQUFZLEVBQUUsSUFBSTtpQkFDbkIsQ0FBQztxQkFDRCxJQUFJLENBQUM7b0JBQ0osS0FBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLEVBQUUsQ0FBQztnQkFDOUIsQ0FBQyxDQUFDLENBQUM7YUFDTjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELGlCQUFpQjtJQUNqQiw0QkFBNEI7SUFDNUIscUNBQXFDO0lBQ3JDLDJDQUEyQztJQUMzQyxtQkFBbUI7SUFDbkIsa0JBQWtCO0lBQ2xCLDRCQUE0QjtJQUM1QixRQUFRO0lBQ1IsSUFBSTtJQUNKLG1DQUFLLEdBQUwsVUFBTSxRQUFnQjtRQUNwQixJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUN0QyxDQUFDO0lBbkdVLG1CQUFtQjtRQU4vQixnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLGFBQWE7WUFDdkIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFdBQVcsRUFBRSxvQkFBb0I7WUFDakMsU0FBUyxFQUFFLENBQUMsY0FBYyxDQUFDO1NBQzVCLENBQUM7eUNBUWtCLDJCQUFpQjtZQUNKLDBDQUFtQjtZQUNsQywwQkFBVztPQVRoQixtQkFBbUIsQ0FvRy9CO0lBQUQsMEJBQUM7Q0FBQSxBQXBHRCxJQW9HQztBQXBHWSxrREFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgVmlld0NoaWxkLCBFbGVtZW50UmVmIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IE1vZGFsRGlhbG9nUGFyYW1zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL2RpcmVjdGl2ZXMvZGlhbG9nc1wiO1xuaW1wb3J0ICogYXMgZGlhbG9ncyBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9kaWFsb2dzXCI7XG5cbi8vIGNvbnN0IHp4aW5nID0gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC16eGluZ1wiKTtcblxuaW1wb3J0ICogYXMgZ2VvbG9jYXRpb24gZnJvbSBcIm5hdGl2ZXNjcmlwdC1nZW9sb2NhdGlvblwiO1xuaW1wb3J0IHsgQWNjdXJhY3kgfSBmcm9tIFwidWkvZW51bXNcIjtcblxuaW1wb3J0IHsgQ2FyZEV4Y2hhbmdlU2VydmljZSB9IGZyb20gXCIuLi9oZWxwZXIvY2FyZEV4Y2hhbmdlLnNlcnZpY2VcIjtcbmltcG9ydCB7IEF1dGhTZXJ2aWNlIH0gZnJvbSBcIi4uL2hlbHBlci9hdXRoLnNlcnZpY2VcIjtcblxuZW51bSBtb2RhbCB7XG4gIG9wZW5lZCA9IDAsXG4gIGZldGNoaW5nVXNlcnMgPSAxLFxuICBmZXRjaGVkVXNlcnMgPSAyLFxuICBub1VzZXJzRm91bmQgPSAzXG59XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJzaGFyZS1tb2RhbFwiLFxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxuICB0ZW1wbGF0ZVVybDogXCIuL3NoYXJlLm1vZGFsLmh0bWxcIixcbiAgc3R5bGVVcmxzOiBbXCIuL21vZGFsLnNjc3NcIl1cbn0pXG5leHBvcnQgY2xhc3MgU2hhcmVNb2RhbENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIHFyQ29kZTogYW55O1xuICBmZXRjaGVkVXNlcnM6IGFueTtcbiAgc2VsZWN0ZWRUYXJnZXRVc2Vyczogb2JqZWN0ID0ge307XG4gIG1vZGFsX3N0YXRlOiBudW1iZXI7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBwYXJhbXM6IE1vZGFsRGlhbG9nUGFyYW1zLFxuICAgIHByaXZhdGUgY2FyZEV4Y2hhbmdlU2VydmljZTogQ2FyZEV4Y2hhbmdlU2VydmljZSxcbiAgICBwcml2YXRlIGF1dGg6IEF1dGhTZXJ2aWNlXG4gICkge31cblxuICBuZ09uSW5pdCgpIHtcbiAgICB0aGlzLm1vZGFsX3N0YXRlID0gbW9kYWxbXCJvcGVuZWRcIl07XG4gIH1cblxuICBzZW5kTmVhckJ5KCkge1xuICAgIHRoaXMubW9kYWxfc3RhdGUgPSBtb2RhbFtcImZldGNoaW5nVXNlcnNcIl07XG4gICAgZ2VvbG9jYXRpb24uZW5hYmxlTG9jYXRpb25SZXF1ZXN0KCk7XG4gICAgZ2VvbG9jYXRpb24uaXNFbmFibGVkKCkudGhlbihpc0VuYWJsZWQgPT4ge1xuICAgICAgaWYgKGlzRW5hYmxlZCkge1xuICAgICAgICBnZW9sb2NhdGlvblxuICAgICAgICAgIC5nZXRDdXJyZW50TG9jYXRpb24oe1xuICAgICAgICAgICAgZGVzaXJlZEFjY3VyYWN5OiBBY2N1cmFjeS5oaWdoLFxuICAgICAgICAgICAgdGltZW91dDogMjAwMDBcbiAgICAgICAgICB9KVxuICAgICAgICAgIC50aGVuKGN1cnJlbnRMb2NhdGlvbiA9PiB7XG4gICAgICAgICAgICBjb25zdCBjb29yZHMgPSB7XG4gICAgICAgICAgICAgIGxvbmc6IGN1cnJlbnRMb2NhdGlvbi5sb25naXR1ZGUsXG4gICAgICAgICAgICAgIGxhdDogY3VycmVudExvY2F0aW9uLmxhdGl0dWRlXG4gICAgICAgICAgICB9O1xuICAgICAgICAgICAgdGhpcy5jYXJkRXhjaGFuZ2VTZXJ2aWNlLmZldGNoTmVhcmJ5VXNlcnMoY29vcmRzKS50aGVuKHJlcyA9PiB7XG4gICAgICAgICAgICAgIGlmICghIXJlcykge1xuICAgICAgICAgICAgICAgIGNvbnN0IHVzZXJzID0gcmVzLmNvbnRlbnQudG9KU09OKCk7XG4gICAgICAgICAgICAgICAgdGhpcy5mZXRjaGVkVXNlcnMgPSB1c2VycztcbiAgICAgICAgICAgICAgICB0aGlzLm1vZGFsX3N0YXRlID0gbW9kYWxbXCJmZXRjaGVkVXNlcnNcIl07XG4gICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy5tb2RhbF9zdGF0ZSA9IG1vZGFsW1wibm9Vc2Vyc0ZvdW5kXCJdO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9KTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGFsZXJ0KFwiUGxlYXNlIGVuYWJsZSB5b3VyIGxvY2F0aW9uIHNldHRpbmdzXCIpO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgdG9nZ2xlU2VsZWN0VXNlcihpbmRleDogbnVtYmVyKSB7XG4gICAgbGV0IHN0cmluZ19pbmRleCA9IGluZGV4LnRvU3RyaW5nKCk7XG4gICAgaWYgKCEhdGhpcy5zZWxlY3RlZFRhcmdldFVzZXJzW3N0cmluZ19pbmRleF0pIHtcbiAgICAgIGRlbGV0ZSB0aGlzLnNlbGVjdGVkVGFyZ2V0VXNlcnNbc3RyaW5nX2luZGV4XTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5zZWxlY3RlZFRhcmdldFVzZXJzW3N0cmluZ19pbmRleF0gPSB0aGlzLmZldGNoZWRVc2Vyc1tpbmRleF07XG4gICAgfVxuICB9XG5cbiAgc2VuZENhcmRUb1RhcmdldHMoKSB7XG4gICAgY29uc3QgYWN0aXZlVXNlciA9IHRoaXMuYXV0aC5nZXRhY3RpdmVVc2VyKCk7XG4gICAgY29uc3QgcHVzaE9iamVjdCA9IHtcbiAgICAgIGFjdGl2ZVVzZXJFbWFpbDogYWN0aXZlVXNlci5lbWFpbCxcbiAgICAgIHRhcmdldHM6IHRoaXMuc2VsZWN0ZWRUYXJnZXRVc2Vyc1xuICAgIH07XG4gICAgdGhpcy5jYXJkRXhjaGFuZ2VTZXJ2aWNlLnNlbmRUYXJnZXRVc2VycyhwdXNoT2JqZWN0KS50aGVuKHJlcyA9PiB7XG4gICAgICBjb25zdCByZXN1bHQgPSByZXMuY29udGVudC50b0pTT04oKTtcbiAgICAgIGlmIChyZXN1bHQuc3VjY2Vzcykge1xuICAgICAgICBkaWFsb2dzXG4gICAgICAgICAgLmFsZXJ0KHtcbiAgICAgICAgICAgIHRpdGxlOiBcIlN1Y2Nlc3MhXCIsXG4gICAgICAgICAgICBtZXNzYWdlOiBcIllvdXIgY2FyZCBoYXMgYmVlbiBzaGFyZWRcIixcbiAgICAgICAgICAgIG9rQnV0dG9uVGV4dDogXCJPa1wiXG4gICAgICAgICAgfSlcbiAgICAgICAgICAudGhlbigoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnBhcmFtcy5jbG9zZUNhbGxiYWNrKCk7XG4gICAgICAgICAgfSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBkaWFsb2dzXG4gICAgICAgICAgLmFsZXJ0KHtcbiAgICAgICAgICAgIHRpdGxlOiBcIkVycm9yXCIsXG4gICAgICAgICAgICBtZXNzYWdlOiBcIlNvbWV0aGluZyB3ZW50IHdyb25nXCIsXG4gICAgICAgICAgICBva0J1dHRvblRleHQ6IFwiT2tcIlxuICAgICAgICAgIH0pXG4gICAgICAgICAgLnRoZW4oKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5wYXJhbXMuY2xvc2VDYWxsYmFjaygpO1xuICAgICAgICAgIH0pO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgLy8gc2hvd1FyQ29kZSgpIHtcbiAgLy8gICBjb25zdCB6eCA9IG5ldyB6eGluZygpO1xuICAvLyAgIHRoaXMucXJDb2RlID0genguY3JlYXRlQmFyY29kZSh7XG4gIC8vICAgICBlbmNvZGU6IEpTT04uc3RyaW5naWZ5KHRoaXMucGFyYW1zKSxcbiAgLy8gICAgIGhlaWdodDogMjAwLFxuICAvLyAgICAgd2lkdGg6IDIwMCxcbiAgLy8gICAgIGZvcm1hdDogenhpbmcuUVJfQ09ERVxuICAvLyAgIH0pO1xuICAvLyB9XG4gIGNsb3NlKHJlc3BvbnNlOiBzdHJpbmcpIHtcbiAgICB0aGlzLnBhcmFtcy5jbG9zZUNhbGxiYWNrKHJlc3BvbnNlKTtcbiAgfVxufVxuIl19