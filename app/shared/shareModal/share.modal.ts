import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/directives/dialogs";
import * as dialogs from "tns-core-modules/ui/dialogs";

// const zxing = require("nativescript-zxing");

import * as geolocation from "nativescript-geolocation";
import { Accuracy } from "ui/enums";

import { CardExchangeService } from "../helper/cardExchange.service";
import { AuthService } from "../helper/auth.service";

enum modal {
  opened = 0,
  fetchingUsers = 1,
  fetchedUsers = 2,
  noUsersFound = 3
}

@Component({
  selector: "share-modal",
  moduleId: module.id,
  templateUrl: "./share.modal.html",
  styleUrls: ["./modal.scss"]
})
export class ShareModalComponent implements OnInit {
  qrCode: any;
  fetchedUsers: any;
  selectedTargetUsers: object = {};
  modal_state: number;

  constructor(
    private params: ModalDialogParams,
    private cardExchangeService: CardExchangeService,
    private auth: AuthService
  ) {}

  ngOnInit() {
    this.modal_state = modal["opened"];
  }

  sendNearBy() {
    this.modal_state = modal["fetchingUsers"];
    geolocation.enableLocationRequest();
    geolocation.isEnabled().then(isEnabled => {
      if (isEnabled) {
        geolocation
          .getCurrentLocation({
            desiredAccuracy: Accuracy.high,
            timeout: 20000
          })
          .then(currentLocation => {
            const coords = {
              long: currentLocation.longitude,
              lat: currentLocation.latitude
            };
            this.cardExchangeService.fetchNearbyUsers(coords).then(res => {
              if (!!res) {
                const users = res.content.toJSON();
                this.fetchedUsers = users;
                this.modal_state = modal["fetchedUsers"];
              } else {
                this.modal_state = modal["noUsersFound"];
              }
            });
          });
      } else {
        alert("Please enable your location settings");
      }
    });
  }

  toggleSelectUser(index: number) {
    let string_index = index.toString();
    if (!!this.selectedTargetUsers[string_index]) {
      delete this.selectedTargetUsers[string_index];
    } else {
      this.selectedTargetUsers[string_index] = this.fetchedUsers[index];
    }
  }

  sendCardToTargets() {
    const activeUser = this.auth.getactiveUser();
    const pushObject = {
      activeUserEmail: activeUser.email,
      targets: this.selectedTargetUsers
    };
    this.cardExchangeService.sendTargetUsers(pushObject).then(res => {
      const result = res.content.toJSON();
      if (result.success) {
        dialogs
          .alert({
            title: "Success!",
            message: "Your card has been shared",
            okButtonText: "Ok"
          })
          .then(() => {
            this.params.closeCallback();
          });
      } else {
        dialogs
          .alert({
            title: "Error",
            message: "Something went wrong",
            okButtonText: "Ok"
          })
          .then(() => {
            this.params.closeCallback();
          });
      }
    });
  }

  // showQrCode() {
  //   const zx = new zxing();
  //   this.qrCode = zx.createBarcode({
  //     encode: JSON.stringify(this.params),
  //     height: 200,
  //     width: 200,
  //     format: zxing.QR_CODE
  //   });
  // }
  close(response: string) {
    this.params.closeCallback(response);
  }
}
