// this import should be first in order to load some required settings (like globals and reflect-metadata)
import { platformNativeScriptDynamic } from "nativescript-angular/platform";

import { AppModule } from "./app.module";

import * as orientation from "nativescript-orientation";
orientation.setOrientation("portrait");
orientation.disableRotation();

import { configureOAuthProviders } from "./auth-helper";
configureOAuthProviders();

require("nativescript-plugin-firebase");
platformNativeScriptDynamic().bootstrapModule(AppModule);
