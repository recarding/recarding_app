import { Component, OnInit } from "@angular/core";

import * as application from "application";

import { ChecksService } from "./shared/helper/app-settings/checks.service";
import { StorageService } from "./shared/helper/app-settings/storage.service";
import { SyncService } from "./shared/helper/app-settings/sync.service";
import { AuthService } from "./shared/helper/auth.service";

import { RouterExtensions } from "nativescript-angular/router";

@Component({
  selector: "ns-app",
  template: `
  <page-router-outlet></page-router-outlet>
`,
})

export class AppComponent implements OnInit {

  constructor(
    private storageService: StorageService,
    private routerExtensions: RouterExtensions,
    private checksService: ChecksService,
    private authService: AuthService,
    private syncService: SyncService
  ) { }

  ngOnInit(): void {
    const token = this.authService.getAuthToken() || "";
    const fb_token = this.authService.getFacebookToken() || "";

    const isValid = this.authService.navigateIfTokenExpired(token);
    if (isValid) {
      this.routerExtensions.navigateByUrl("/home", { clearHistory: true });
    }
    if (fb_token !== "") {
      this.checksService.checkFacebookToken(fb_token)
        .then((res) => JSON.parse(res))
        .then((res) => {
          if (res.email) {
            this.routerExtensions.navigateByUrl("/home", { clearHistory: true });
          }
        });
    }
    application.on(application.suspendEvent, (args) => {
      if (args.ios) {
        this.connectivityStatus()
          .then((connected) => {
            if (connected) {
              const unSynced = this.storageService.getUnsyncedCards();
              const a = JSON.parse(unSynced)
              if (a.cards.length) {
                const task = this.syncService.syncNewCards(a)
                task.on("complete", (event) => {
                  this.storageService.clearUnSynced();
                  this.checksService.setFlagToShowSuccessToast();
                });
                task.on("error", event => {
                  console.error(event);
                });
              }
            }
          });
      }
    });
    application.on(application.resumeEvent, (args) => {
      if (args.ios) {
        const showSuccess = this.checksService.getFlagToShowSuccessToast();
        if (showSuccess) {
          this.checksService.unsetFlagToShowSuccessToast();
        }
      }
    });
  }

  connectivityStatus(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.checksService.checkConnectivityStatus()
        .then((res) => {
          resolve(res);
        })
        .catch((e) => {
          reject();
        });
    });
  }
}
