"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// this import should be first in order to load some required settings (like globals and reflect-metadata)
var platform_1 = require("nativescript-angular/platform");
var app_module_1 = require("./app.module");
var orientation = require("nativescript-orientation");
orientation.setOrientation("portrait");
orientation.disableRotation();
var auth_helper_1 = require("./auth-helper");
auth_helper_1.configureOAuthProviders();
require("nativescript-plugin-firebase");
platform_1.platformNativeScriptDynamic().bootstrapModule(app_module_1.AppModule);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpbi5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm1haW4udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSwwR0FBMEc7QUFDMUcsMERBQTRFO0FBRTVFLDJDQUF5QztBQUV6QyxzREFBd0Q7QUFDeEQsV0FBVyxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQztBQUN2QyxXQUFXLENBQUMsZUFBZSxFQUFFLENBQUM7QUFFOUIsNkNBQXdEO0FBQ3hELHFDQUF1QixFQUFFLENBQUM7QUFFMUIsT0FBTyxDQUFDLDhCQUE4QixDQUFDLENBQUM7QUFDeEMsc0NBQTJCLEVBQUUsQ0FBQyxlQUFlLENBQUMsc0JBQVMsQ0FBQyxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiLy8gdGhpcyBpbXBvcnQgc2hvdWxkIGJlIGZpcnN0IGluIG9yZGVyIHRvIGxvYWQgc29tZSByZXF1aXJlZCBzZXR0aW5ncyAobGlrZSBnbG9iYWxzIGFuZCByZWZsZWN0LW1ldGFkYXRhKVxyXG5pbXBvcnQgeyBwbGF0Zm9ybU5hdGl2ZVNjcmlwdER5bmFtaWMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcGxhdGZvcm1cIjtcclxuXHJcbmltcG9ydCB7IEFwcE1vZHVsZSB9IGZyb20gXCIuL2FwcC5tb2R1bGVcIjtcclxuXHJcbmltcG9ydCAqIGFzIG9yaWVudGF0aW9uIGZyb20gXCJuYXRpdmVzY3JpcHQtb3JpZW50YXRpb25cIjtcclxub3JpZW50YXRpb24uc2V0T3JpZW50YXRpb24oXCJwb3J0cmFpdFwiKTtcclxub3JpZW50YXRpb24uZGlzYWJsZVJvdGF0aW9uKCk7XHJcblxyXG5pbXBvcnQgeyBjb25maWd1cmVPQXV0aFByb3ZpZGVycyB9IGZyb20gXCIuL2F1dGgtaGVscGVyXCI7XHJcbmNvbmZpZ3VyZU9BdXRoUHJvdmlkZXJzKCk7XHJcblxyXG5yZXF1aXJlKFwibmF0aXZlc2NyaXB0LXBsdWdpbi1maXJlYmFzZVwiKTtcclxucGxhdGZvcm1OYXRpdmVTY3JpcHREeW5hbWljKCkuYm9vdHN0cmFwTW9kdWxlKEFwcE1vZHVsZSk7XHJcbiJdfQ==