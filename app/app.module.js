"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_module_1 = require("nativescript-angular/nativescript.module");
var http_1 = require("nativescript-angular/http");
var router_1 = require("nativescript-angular/router");
var app_routing_module_1 = require("./app-routing.module");
var app_component_1 = require("./app.component");
var storage_service_1 = require("./shared/helper/app-settings/storage.service");
var sync_service_1 = require("./shared/helper/app-settings/sync.service");
var auth_service_1 = require("./shared/helper/auth.service");
var ocr_service_1 = require("./shared/helper/ocr.service");
var checks_service_1 = require("./shared/helper/app-settings/checks.service");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            bootstrap: [
                app_component_1.AppComponent
            ],
            imports: [
                nativescript_module_1.NativeScriptModule,
                app_routing_module_1.AppRoutingModule,
                http_1.NativeScriptHttpModule,
                router_1.NativeScriptRouterModule
            ],
            declarations: [
                app_component_1.AppComponent
            ],
            providers: [
                checks_service_1.ChecksService,
                storage_service_1.StorageService,
                auth_service_1.AuthService,
                sync_service_1.SyncService,
                ocr_service_1.OcrService
            ],
            schemas: [
                core_1.NO_ERRORS_SCHEMA
            ]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwcC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkQ7QUFDM0QsZ0ZBQThFO0FBRTlFLGtEQUFtRTtBQUNuRSxzREFBdUU7QUFFdkUsMkRBQXdEO0FBQ3hELGlEQUErQztBQUUvQyxnRkFBOEU7QUFDOUUsMEVBQXdFO0FBQ3hFLDZEQUEyRDtBQUMzRCwyREFBeUQ7QUFDekQsOEVBQTRFO0FBMEI1RTtJQUFBO0lBQXlCLENBQUM7SUFBYixTQUFTO1FBeEJyQixlQUFRLENBQUM7WUFDTixTQUFTLEVBQUU7Z0JBQ1AsNEJBQVk7YUFDZjtZQUNELE9BQU8sRUFBRTtnQkFDTCx3Q0FBa0I7Z0JBQ2xCLHFDQUFnQjtnQkFDaEIsNkJBQXNCO2dCQUN0QixpQ0FBd0I7YUFDM0I7WUFDRCxZQUFZLEVBQUU7Z0JBQ1YsNEJBQVk7YUFDZjtZQUNELFNBQVMsRUFBRTtnQkFDUCw4QkFBYTtnQkFDYixnQ0FBYztnQkFDZCwwQkFBVztnQkFDWCwwQkFBVztnQkFDWCx3QkFBVTthQUNiO1lBQ0QsT0FBTyxFQUFFO2dCQUNMLHVCQUFnQjthQUNuQjtTQUNKLENBQUM7T0FDVyxTQUFTLENBQUk7SUFBRCxnQkFBQztDQUFBLEFBQTFCLElBQTBCO0FBQWIsOEJBQVMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSwgTk9fRVJST1JTX1NDSEVNQSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdE1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9uYXRpdmVzY3JpcHQubW9kdWxlXCI7XHJcblxyXG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRIdHRwTW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL2h0dHBcIjtcclxuaW1wb3J0IHsgTmF0aXZlU2NyaXB0Um91dGVyTW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xyXG5cclxuaW1wb3J0IHsgQXBwUm91dGluZ01vZHVsZSB9IGZyb20gXCIuL2FwcC1yb3V0aW5nLm1vZHVsZVwiO1xyXG5pbXBvcnQgeyBBcHBDb21wb25lbnQgfSBmcm9tIFwiLi9hcHAuY29tcG9uZW50XCI7XHJcblxyXG5pbXBvcnQgeyBTdG9yYWdlU2VydmljZSB9IGZyb20gXCIuL3NoYXJlZC9oZWxwZXIvYXBwLXNldHRpbmdzL3N0b3JhZ2Uuc2VydmljZVwiO1xyXG5pbXBvcnQgeyBTeW5jU2VydmljZSB9IGZyb20gXCIuL3NoYXJlZC9oZWxwZXIvYXBwLXNldHRpbmdzL3N5bmMuc2VydmljZVwiO1xyXG5pbXBvcnQgeyBBdXRoU2VydmljZSB9IGZyb20gXCIuL3NoYXJlZC9oZWxwZXIvYXV0aC5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IE9jclNlcnZpY2UgfSBmcm9tIFwiLi9zaGFyZWQvaGVscGVyL29jci5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IENoZWNrc1NlcnZpY2UgfSBmcm9tIFwiLi9zaGFyZWQvaGVscGVyL2FwcC1zZXR0aW5ncy9jaGVja3Muc2VydmljZVwiO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGJvb3RzdHJhcDogW1xyXG4gICAgICAgIEFwcENvbXBvbmVudFxyXG4gICAgXSxcclxuICAgIGltcG9ydHM6IFtcclxuICAgICAgICBOYXRpdmVTY3JpcHRNb2R1bGUsXHJcbiAgICAgICAgQXBwUm91dGluZ01vZHVsZSxcclxuICAgICAgICBOYXRpdmVTY3JpcHRIdHRwTW9kdWxlLFxyXG4gICAgICAgIE5hdGl2ZVNjcmlwdFJvdXRlck1vZHVsZVxyXG4gICAgXSxcclxuICAgIGRlY2xhcmF0aW9uczogW1xyXG4gICAgICAgIEFwcENvbXBvbmVudFxyXG4gICAgXSxcclxuICAgIHByb3ZpZGVyczogW1xyXG4gICAgICAgIENoZWNrc1NlcnZpY2UsXHJcbiAgICAgICAgU3RvcmFnZVNlcnZpY2UsXHJcbiAgICAgICAgQXV0aFNlcnZpY2UsXHJcbiAgICAgICAgU3luY1NlcnZpY2UsXHJcbiAgICAgICAgT2NyU2VydmljZVxyXG4gICAgXSxcclxuICAgIHNjaGVtYXM6IFtcclxuICAgICAgICBOT19FUlJPUlNfU0NIRU1BXHJcbiAgICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBBcHBNb2R1bGUgeyB9XHJcbiJdfQ==